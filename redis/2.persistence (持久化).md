# persistence (Redis持久化)

**Date: 2020/1/14 17:56**

## 持久化的作用

### 什么是持久化

持久化（Persistence），即把数据（如内存中的对象）保存到可永久保存的存储设备中（如磁盘）。
持久化Redis所有数据保持在内存中，对数据的更新将异步地保存到磁盘上。

```txt
Memory <------------------> Disk
       --Data Persistence->
       <-Data Discovery----
```

### 持久化的实现方式

#### 快照方式持久化

快照方式持久化就是在某时刻把所有数据进行完整备份。

例：Mysql的Dump方式、Redis的RDB方式。

#### 写日志方式持久化

写日志方式持久化就是把用户执行的所有写指令（增删改）备份到文件中，还原数据时只需要把备份的所有指令重新执行一遍即可。

例：Mysql的Binlog、Redis的AOF、Hbase的HLog。

## Redis持久化

Redis 提供了不同方式的持久化选项：

- RDB 持久化方式能够在指定的时间间隔能对你的数据进行快照存储。
- AOF 持久化方式记录每次对服务器写的操作，当服务器重启的时候会重新执行这些命令来恢复原始的数据，AOF 命令以 Redis 协议追加保存每次写的操作到文件末尾。Redis 还能对 AOF 文件进行后台重写，使得 AOF 文件的体积不至于过大。
- 如果你只希望你的数据在服务器运行的时候存在，你也可以不使用任何持久化方式。
- 你也可以同时开启两种持久化方式，在这种情况下，**当 Redis 重启的时候会优先载入 AOF 文件来恢复原始的数据，因为在通常情况下 AOF 文件保存的数据集要比 RDB 文件保存的数据集要完整。**

### RDB (Redis DataBase)

在 Redis 运行时， 程序将当前内存中的数据库快照保存到磁盘文件中， 在 Redis 重新启动时， 程序可以通过载入 RDB 文件来还原数据库的状态。

![RDB持久化](https://segmentfault.com/img/remote/1460000016021221)

####  快照

在默认情况下， Redis 将数据库快照保存在名字为 dump.rdb 的二进制文件中。你可以对 Redis 进行设置， 让它在“N 秒内数据集至少有 M 个改动“这一条件被满足时， 自动保存一次数据集。你也可以通过调用 ```SAVE```或者 ```BGSAVE``` ， 手动让 Redis 进行数据集保存操作。

比如说， 以下设置会让 Redis 在满足“ 60 秒内有至少有 1000 个键被改动”这一条件时， 自动保存一次数据集:

```txt
save 60 1000
```

这种持久化方式被称为快照 snapshotting 。

#### 工作方式

当 Redis 需要保存 dump.rdb 文件时，服务器执行以下操作：

1. Redis 调用[forks](https://linux.die.net/man/2/fork)。同时拥有父进程和子进程。
2. 子进程将数据集写入到一个临时 RDB 文件中。
3. 当子进程完成对新 RDB 文件的写入时，Redis 用新 RDB 文件替换原来的 RDB 文件，并删除旧的 RDB 文件。

这种工作方式使得 Redis 可以从写时复制（copy-on-write）机制中获益。

#### 触发RDB持久化

##### save 命令

```save``` 命令执行一个同步操作，以RDB文件的方式保存所有数据的快照。

```bash
127.0.0.1:6379> save
OK
(5.23s)
```

由于 ```save``` 命令是同步命令，会占用 Redis 的主进程。若 Redis 数据非常多时，```save``` 命令执行速度会非常慢，阻塞所有客户端的请求。因此很少在生产环境直接使用 ```save```  命令，可以使用 ```bgsave``` 命令代替。如果在 ```bgsave``` 命令的保存数据的子进程发生错误的时，用 ```save```  命令保存最新的数据是最后的手段。

![save命令阻塞所有客户端的请求](https://segmentfault.com/img/remote/1460000016021223?w=783&h=288)

##### bgsave 命令

```bgsave``` 命令执行一个异步操作，以RDB文件的方式保存所有数据的快照。

```txt
127.0.0.1:6379> bgsave
Background saving started
127.0.0.1:6379> lastsave
(integer) 1578989079
127.0.0.1:6379> lastsave
(integer) 1578989117
```

Redis使用Linux系统的 ```fork()``` 生成一个子进程，父进程继续提供服务以供客户端调用，子进程将DB数据保存到磁盘然后退出。
如果操作成功，可以通过客户端命令 ```lastsave``` 来检查操作结果。

![bgsave命令](https://segmentfault.com/img/remote/1460000016021224)

**save 与 bgsave 对比**

| 命令   | save               | bgsave                                    |
| ------ | ------------------ | ----------------------------------------- |
| IO类型 | 同步               | 异步                                      |
| 阻塞？ | 是                 | 是（阻塞发生在 ```fork()```，通常非常快） |
| 复杂度 | $O(n)$             | $O(n)$                                    |
| 优点   | 不会消耗额外的内存 | 不阻塞客户端命令                          |
| 缺点   | 阻塞客户端         | 需要 ```fork``` 子进程，消耗内存          |

##### 配置自动触发

除了手动执行 ```save``` 和 ```bgsave``` 命令实现 RDB 持久化以外，Redis 还提供了自动触发生成 RDB 的方式。

你可以通过配置文件对 Redis 进行设置， 让它在“ N 秒内数据集至少有 M 个改动”这一条件被满足时， 自动进行数据集保存操作。比如说， 以下设置会让 Redis 在满足” 60 秒内有至少有 1000 个键被改动“这一条件时， 自动进行数据集保存操作：

```txt
save 60 1000
```

save配置选项解释如下：

```bash
# Save the DB on disk:
#
#   save <seconds> <changes>
#
#   Will save the DB if both the given number of seconds and the given
#   number of write operations against the DB occurred.
#
#   In the example below the behaviour will be to save:
#   after 900 sec (15 min) if at least 1 key changed
#   after 300 sec (5 min) if at least 10 keys changed
#   after 60 sec if at least 10000 keys changed
#   Note: you can disable saving completely by commenting out all "save" lines.
#
#   It is also possible to remove all the previously configured save
#   points by adding a save directive with a single empty string argument
#   like in the following example:
#
#   save ""
save 900 1
save 300 10
save 60 10000

# By default Redis will stop accepting writes if RDB snapshots are enabled
# (at least one save point) and the latest background save failed.
# This will make the user aware (in a hard way) that data is not persisting
# on disk properly, otherwise chances are that no one will notice and some
# disaster will happen.
#
# If the background saving process will start working again Redis will
# automatically allow writes again.
#
# However if you have setup your proper monitoring of the Redis server
# and persistence, you may want to disable this feature so that Redis will
# continue to work as usual even if there are problems with disk,
# permissions, and so forth.
stop-writes-on-bgsave-error yes

# Compress string objects using LZF when dump .rdb databases?
# For default that's set to 'yes' as it's almost always a win.
# If you want to save some CPU in the saving child set it to 'no' but
# the dataset will likely be bigger if you have compressible values or keys.
rdbcompression yes

# Since version 5 of RDB a CRC64 checksum is placed at the end of the file.
# This makes the format more resistant to corruption but there is a performance
# hit to pay (around 10%) when saving and loading RDB files, so you can disable it
# for maximum performances.
#
# RDB files created with checksum disabled have a checksum of zero that will
# tell the loading code to skip the check.
rdbchecksum yes

# The filename where to dump the DB
dbfilename dump.rdb

# The working directory.
#
# The DB will be written inside this directory, with the filename specified
# above using the 'dbfilename' configuration directive.
#
# The Append Only File will also be created inside this directory.
#
# Note that you must specify a directory here, not a file name.
dir ./
```

#### 优点

1. RDB是一个非常紧凑的文件，它保存了某个时间点得数据集，非常适用于数据集的备份，比如你可以在每个小时报保存一下过去24小时内的数据，同时每天保存过去30天的数据，这样即使出了问题你也可以根据需求恢复到不同版本的数据集。
2. RDB是一个紧凑的单一文件，很方便传送到另一个远端数据中心或者亚马逊的S3（可能加密），非常适用于灾难恢复。
3. RDB在保存RDB文件时父进程唯一需要做的就是fork出一个子进程，接下来的工作全部由子进程来做，父进程不需要再做其他IO操作，所以RDB持久化方式可以最大化redis的性能。
4. 与AOF相比，在恢复大的数据集的时候，RDB方式会更快一些。

#### 缺点

1. 耗时、耗性能。RDB 需要经常fork子进程来保存数据集到硬盘上，当数据集比较大的时候，fork的过程是非常耗时的，可能会导致Redis在一些毫秒级内不能响应客户端的请求。如果数据集巨大并且CPU性能不是很好的情况下，这种情况会持续1秒，AOF也需要fork，但是你可以调节重写日志文件的频率来提高数据集的耐久度。
2. 不可控、丢失数据。如果你希望在redis意外停止工作（例如电源中断）的情况下丢失的数据最少的话，那么RDB不适合你。虽然你可以配置不同的save时间点(例如每隔5分钟并且对数据集有100个写的操作)，是Redis要完整的保存整个数据集是一个比较繁重的工作，你通常会每隔5分钟或者更久做一次完整的保存，万一在Redis意外宕机，你可能会丢失几分钟的数据。

### AOF (Append Only File)

快照功能（RDB）并不是非常耐久（durable）： 如果 Redis 因为某些原因而造成故障停机， 那么服务器将丢失最近写入、且仍未保存到快照中的那些数据。 从 1.1 版本开始， Redis 增加了一种完全耐久的持久化方式： AOF 持久化。

你可以在配置文件中打开AOF方式：

```txt
appendonly yes
```

打开 AOF 后， 每当 Redis 执行一个改变数据集的命令时（比如 SET）， 这个命令就会被追加到 AOF 文件的末尾。这样的话， 当 Redis 重新启时， 程序就可以通过重新执行 AOF 文件中的命令来达到重建数据集的目的。

1. AOF运行原理 - 创建

   ```mermaid
   graph LR
   ClientA[Client] -- SET hello world --> Redis[<Redis>]
   ClientB[Client] -- HMSET myhash a b --> Redis(Redis)
   ClientC[Client] -- SADD myset a b --> Redis(Redis)
   Redis --> AOF["AOF 文件"]
   ```

   

   ![创建AOF](https://segmentfault.com/img/remote/1460000016021226)

2. AOF运行原理 - 恢复

   ![AOF运行原理 - 恢复](https://segmentfault.com/img/remote/1460000016021227?w=724&h=336)

#### AOF 持久化的三种策略

1. appendfsync always

   每次有新命令追加到 AOF 文件时就执行一次 fsync ：非常慢，也非常安全。

2. appendfsync everysec

   每秒 fsync 一次：足够快（和使用 RDB 持久化差不多），并且在故障时只会丢失 1 秒钟的数据。推荐（并且也是默认）的措施为每秒 fsync 一次， 这种 fsync 策略可以兼顾速度和安全性。

3. appendfsync no

   从不 fsync ：将数据交给操作系统来处理，由操作系统来决定什么时候同步数据。更快，也更不安全的选择。

**always、everysec、no对比**

| 配置     | 优点                             | 缺点                              |
| -------- | -------------------------------- | --------------------------------- |
| always   | 不丢失数据                       | IO开销大，一般SATA磁盘只有几百TPS |
| everysec | 每秒进行与fsync，最多丢失1秒数据 | 可能丢失1秒数据                   |
| no       | 不用管                           | 不可控                            |

#### AOF Rewrite (AOF 重写)

因为 AOF 的运作方式是不断地将命令追加到文件的末尾， 所以随着写入命令的不断增加， AOF 文件的体积也会变得越来越大。举个例子， 如果你对一个计数器调用了 100 次 ```incr``` ， 那么仅仅是为了保存这个计数器的当前值， AOF 文件就需要使用 100 条记录（entry）。然而在实际上， 只使用一条 SET 命令已经足以保存计数器的当前值了， 其余 99 条记录实际上都是多余的。

为了处理这种情况， Redis 支持一种有趣的特性： 可以在不打断服务客户端的情况下， 对 AOF 文件进行重建（rebuild）。执行 BGREWRITEAOF 命令， Redis 将生成一个新的 AOF 文件， 这个文件包含重建当前数据集所需的最少命令。Redis 2.2 需要自己手动执行 ```bgrewriteaof``` 命令； Redis 2.4 则可以自动触发 AOF 重写。

![AOF重写](https://segmentfault.com/img/remote/1460000016021231?w=475&h=344)

##### AOF 重写的作用

- 减少磁盘占用量
- 加速数据恢复

##### AOF 重写的工作原理

AOF 重写和 RDB 创建快照一样，都巧妙地利用了写时复制机制（copy-on-write）：

- Redis 执行 ```fork()``` ，现在同时拥有父进程和子进程。
- 子进程开始将新 AOF 文件的内容写入到临时文件。
- 对于所有新执行的写入命令，父进程一边将它们累积到一个内存缓存中，一边将这些改动追加到现有 AOF 文件的末尾，这样样即使在重写的中途发生停机，现有的 AOF 文件也还是安全的。
- 当子进程完成重写工作时，它给父进程发送一个信号，父进程在接收到信号之后，将内存缓存中的所有数据追加到新 AOF 文件的末尾。
- 搞定！现在 Redis 原子地用新文件替换旧文件，之后所有命令都会直接追加到新 AOF 文件的末尾。

##### 触发AOF重写

###### bgrewriteaof 命令

Redis ```bgrewriteaof``` 命令用于异步执行一个 AOF（Append Only File）文件重写操作。重写会创建一个当前 AOF 文件的体积优化版本。
即使 ```bgrewriteaof``` 执行失败，也不会有任何数据丢失，因为旧的 AOF 文件在 ```bgrewriteaof``` 成功之前不会被修改。
AOF 重写由 Redis 自行触发，```bgrewriteaof``` 仅仅用于手动触发重写操作。
特殊情况：

- 如果一个 Redis 子进程正在创建一个磁盘快照（进行RDB持久化）， AOF 重写过程不会开始进行，而是直到保存 RDB 文件的子进程终止，它将会被重新调度（scheduled）。这种情况下 ```bgrewriteaof``` 仍然会返回一个积极的状态回复，而且带有适当的消息。从 Redis 2.6 起你可以通过 ```info``` 命令查看 AOF 重写的调度和执行情况。
- 如果一个 AOF 重写过程已经在进行中，再次执行 ```bgrewriteaof``` 命令将会返回一个错误， AOF 重写也不会在稍后再执行一次。
- 如果 AOF 重写启动了，但启动时失败了(例如，由于创建子进程时出错)，则向命令调用者返回一个错误。

###### 配置自动触发

| 配置名                      | 含义                    |
| --------------------------- | ----------------------- |
| auto-aof-rewrite-min-size   | 触发 AOF 重写的最小尺寸 |
| auto-aof-rewrite-percentage | 触发 AOF 重写的增长率   |

| 统计名           | 含义                                   |
| ---------------- | -------------------------------------- |
| aof_current_size | AOF 文件当前尺寸（字节）               |
| aof_base_size    | AOF 文件上次启动和重写时的尺寸（字节） |

AOF 重写自动触发机制，需要同时满足下面两个条件：

- aof_current_size > auto-aof-rewrite-min-size
- (aof_current_size - aof_base_size) * 100 / aof_base_size > auto-aof-rewrite-percentage

假设 Redis 的配置项为：

```
auto-aof-rewrite-min-size 64mb
auto-aof-rewrite-percentage 100
```

当 AOF 文件的体积大于 64Mb，并且 AOF 文件的体积比上一次重写的 AOF 文件体积大了至少一倍（100%）时，Redis 将执行 ```bgrewriteaof``` 命令进行重写。

#### 优点

1. 使用 AOF 会让你的 Redis 更加耐久（durable）：你可以使用不同的 fsync 策略：无 fsync ，每秒 fsync ，每次写的时候 fsync 。使用默认的每秒 fsync 策略，Redis 的性能依然很好（ fsync 是由后台线程进行处理的，主线程会尽力处理客户端请求），一旦出现故障，你最多丢失1秒的数据。
2. AOF 文件是一个只进行追加的日志文件，所以没有seek操作，即使由于某些原因（磁盘空间已满，写的过程中宕机等）未执行完整的写入命令，你也也可使用 redis-check-aof 工具修复这些问题。
3. Redis 可以在 AOF 文件体积变得过大时，自动地在后台对 AOF 进行重写。重写后的新 AOF 文件包含了恢复当前数据集所需的命令的最小集合。 整个重写操作是绝对安全的，因为 Redis 在创建新 AOF 文件的过程中，会继续将命令追加到现有的 AOF 文件里面，即使重写过程中发生停机，现有的 AOF 文件也不会丢失。 而一旦新 AOF 文件创建完毕，Redis 就会从旧 AOF 文件切换到新 AOF 文件，并开始对新 AOF 文件进行追加操作。
4. AOF 文件有序地保存了对数据库执行的所有写入操作， 这些写入操作以 Redis 协议的格式保存， 因此 AOF 文件的内容非常容易被人读懂， 对文件进行解析（parse）也很轻松。 导出（export） AOF 文件也非常简单。举个例子， 如果你不小心执行了 ```flushall``` 命令， 但只要 AOF 文件未被重写， 那么只要停止服务器， 删除 AOF 文件末尾的 ```flushall``` 命令， 并重新启动 Redis ， 就可以将数据集恢复到 ```flushall```执行之前的状态。

#### 缺点

1. 对于相同的数据集来说，AOF 文件的体积通常要大于 RDB 文件的体积。
2. 根据所使用的 fsync 策略，AOF 的速度可能会慢于 RDB 。 在一般情况下， 每秒 fsync 的性能依然非常高， 而关闭 fsync 可以让 AOF 的速度和 RDB 一样快， 即使在高负荷之下也是如此。 不过在处理巨大的写入负载时，RDB 可以提供更有保证的最大延迟时间（latency）。

### 如何选择

一般来说， 如果想达到足以媲美 PostgreSQL 的数据安全性， 你应该同时使用两种持久化功能。

如果你非常关心你的数据， 但仍然可以承受数分钟以内的数据丢失， 那么你可以只使用 RDB 持久化。

有很多用户都只使用 AOF 持久化， 但并不推荐这种方式： 因为定时生成 RDB 快照（snapshot）非常便于进行数据库备份， 并且 RDB 恢复数据集的速度也要比 AOF 恢复的速度要快。除此之外，使用 RDB 还可以避免之前提到的 AOF 程序的 bug。


