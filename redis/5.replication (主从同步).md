---
tags:
- redis replication
date: 2020-11-11 21:53:00 +8
---

[TOC]

# 5.replication (主从同步)

## 主从同步主要使用如下的3个机制

1. 主从节点连接正常时，主节点通过发送一系列的**流命令**给从节点，如客户端的写命令，键值对超时，缓存淘汰机制。
2. 当主从节点由于网络原因、或者因为主节点发送数据超时、或者从节点重新连接而发生连接中断，从节点将会重新连接并且尝试**部分数据重新同步**。这意味这从节点将会尽最大可能获取断开连接后的这部分的流命令数据。
3. 当部分重新同步不可能的时候，从节点将会要求**完全同步**。这包含一系列主节点的复杂操作，主节点需要创建所有数据集的RDB快照，把RDB发送给从节点，然后继续发送流命令。

**redis默认使用异步主从同步**，大规模数据时这是一个高性能低延迟的方案。redis从节点以固定间隔(periodically)异步确认从主节点接受到的数据，所以主节点不会每次都等待从节点处理同步的数据。如果有需要的话可以用同步复制。

**客户端可以使用`WAIT`命令来同步复制一些特定的数据。**`WAIT`仅仅能确保特定数量的复制到其他的redis实例，它不会转换redis实例到强一致的CP系统（分布式的CAP理论，一致性Consistency、可用性Availability、分区容错性Partition tolerance），当灾备恢复时(failover)确认的写命令仍然可能会丢失。

一些redis主从同步复制的重要情况：

1. redis使用异步复制，异步的确认主从复制的数据的数量。
2. 主节点可以有多个从节点。
3. 从节点还可以接受其他的从节点的连接。除了连接一些从节点到主节点，从节点也可以像级联结构一样接受其他的从节点的连接。
4. 主节点的主从复制是非阻塞的。这意味着主节点可以继续处理请求。
5. 从节点的主从复制也是非阻塞的。当主从复制在进行初始化同步时，从节点也可以处理请求，返回老版本的数据结果。初始化同步之后，老版本的数据必须被删除，新版本的数据将会被加载，这时从节点将会阻塞新来的连接。
6. 主从同步可以做一些扩展，例如：多个从节点可以执行只读的查询操作；一些 $O(N)$ 时间复杂度的操作也可以交给副本来做。
7. 你也可以使用主从同步机制做到避免主节点把完整的数据写到磁盘上，让从节点持久化数据到磁盘。这种情况要小心处理一些异常，重启主节点时，主节点的数据是空的，从节点同步时，从节点将会清空所有的数据。

> This system works using three main mechanisms:
>
> 1. When a master and a replica instances are well-connected, the master keeps the replica updated by sending a stream of commands to the replica, in order to replicate the effects on the dataset happening in the master side due to: client writes, keys expired or evicted, any other action changing the master dataset.
> 2. When the link between the master and the replica breaks, for network issues or because a timeout is sensed in the master or the replica, the replica reconnects and attempts to proceed with a partial resynchronization: it means that it will try to just obtain the part of the stream of commands it missed during the disconnection.
> 3. When a partial resynchronization is not possible, the replica will ask for a full resynchronization. This will involve a more complex process in which the master needs to create a snapshot of all its data, send it to the replica, and then continue sending the stream of commands as the dataset changes.
>
> Redis uses by default asynchronous replication, which being low latency and high performance, is the natural replication mode for the vast majority of Redis use cases. However, Redis replicas asynchronously acknowledge the amount of data they received periodically with the master. So the master does not wait every time for a command to be processed by the replicas, however it knows, if needed, what replica already processed what command. This allows having optional synchronous replication.
>
> Synchronous replication of certain data can be requested by the clients using the [WAIT](https://redis.io/commands/wait) command. However [WAIT](https://redis.io/commands/wait) is only able to ensure that there are the specified number of acknowledged copies in the other Redis instances, it does not turn a set of Redis instances into a CP system with strong consistency: acknowledged writes can still be lost during a failover, depending on the exact configuration of the Redis persistence. However with [WAIT](https://redis.io/commands/wait) the probability of losing a write after a failure event is greatly reduced to certain hard to trigger failure modes.
>
> The following are some very important facts about Redis replication:
>
> - Redis uses asynchronous replication, with asynchronous replica-to-master acknowledges of the amount of data processed.
> - A master can have multiple replicas.
> - Replicas are able to accept connections from other replicas. Aside from connecting a number of replicas to the same master, replicas can also be connected to other replicas in a cascading-like structure. Since Redis 4.0, all the sub-replicas will receive exactly the same replication stream from the master.
> - Redis replication is non-blocking on the master side. This means that the master will continue to handle queries when one or more replicas perform the initial synchronization or a partial resynchronization.
> - Replication is also largely non-blocking on the replica side. While the replica is performing the initial synchronization, it can handle queries using the old version of the dataset, assuming you configured Redis to do so in redis.conf. Otherwise, you can configure Redis replicas to return an error to clients if the replication stream is down. However, after the initial sync, the old dataset must be deleted and the new one must be loaded. The replica will block incoming connections during this brief window (that can be as long as many seconds for very large datasets). Since Redis 4.0 it is possible to configure Redis so that the deletion of the old data set happens in a different thread, however loading the new initial dataset will still happen in the main thread and block the replica.
> - Replication can be used both for scalability, in order to have multiple replicas for read-only queries (for example, slow O(N) operations can be offloaded to replicas), or simply for improving data safety and high availability.
> - It is possible to use replication to avoid the cost of having the master writing the full dataset to disk: a typical technique involves configuring your master `redis.conf` to avoid persisting to disk at all, then connect a replica configured to save from time to time, or with AOF enabled. However this setup must be handled with care, since a restarting master will start with an empty dataset: if the replica tries to synchronize with it, the replica will be emptied as well.

## Redis 复制的工作原理

每个 Redis 主节点都有一个复制 ID (Replication ID)：它是一个大型伪随机字符串，用于标记数据集。每个 master 还采用一个偏移量 (offset)，因为生成的复制流(replication stream)的每一个字节发送到副本，副本更新到了最新的状态，所以该偏移量就会增加。即使实际上没有连接副本，复制偏移也会增加，所以基本上每个给定的对：

```
Replication ID, offset
```

标识主数据集的确切版本。

当副本连接到主节点时，它们使用[PSYNC](https://redis.io/commands/psync)命令来发送它们的旧的master的 `Replication ID` 和它们到目前为止处理的 `offset`。通过这种方式，master 可以只发送副本所需的增量部分。但是，如果master缓冲区中没有足够的*积压* (backlog)，或者如果副本引用到了不再已知的历史记录的复制 ID，则会发生完全重新同步：在这种情况下，副本将获得数据集的完整副本， 从头开始。

这是完整同步的详细工作方式：

master启动后台保存过程以生成 RDB 文件。同时，它开始缓冲从客户端接收到的所有新写命令。当后台保存完成后，master 将RDB文件传输到副本，副本将其保存在磁盘上，然后将其加载到内存中。master服务器然后将所有缓冲的命令发送到副本。这是作为命令流完成的，并且与 Redis 协议本身的格式相同。

您可以通过 telnet 自己尝试。当服务器正在做一些工作时连接到 Redis 端口并发出[SYNC](https://redis.io/commands/sync)命令。您将看到批量传输，然后主机收到的每个命令都将在 telnet 会话中重新发出。实际上[SYNC](https://redis.io/commands/sync)是一个旧协议，不再被较新的 Redis 实例使用，但为了向后兼容，它仍然存在：它不允许部分重新同步，所以现在使用[PSYNC](https://redis.io/commands/psync)。

如前所述，当到master的链接由于某种原因出现故障时，副本能够自动重新连接。如果 master 收到多个并发副本同步请求，它会执行单个后台保存以服务所有这些请求。

### 全量同步

当一个redis服务器初次向主服务器发送salveof命令时，redis从服务器会进行一次全量同步。
同步的步骤如下图所示：

```mermaid
sequenceDiagram
    autonumber
    slave->>+master: 发送psync命令
    master->>+master: 执行bgsave命令生成RDB快照
    master->>+slave: 发送RDB快照
    slave->>+slave: 载入RDB快照
    master->>+slave: 发送写命令
    slave->>+slave: 执行写命令
```

```
SLAVEOF host port

PSYNC replicationid offset

BGSAVE [SCHEDULE]
```

步骤描述：

1. slave服务器向master发送psync命令（此时发送的是`psync <replicationid> -1`），告诉master我需要同步数据了。
2. master接收到psync命令后会进行BGSAVE命令生成RDB文件快照。生成完后，会将RDB文件发送给slave。
3. slave接收到文件会载入RDB快照，并且将数据库状态变更为master在执行BGSAVE时的状态一致。
4. master会发送保存在缓冲区里的所有写命令，告诉slave可以进行同步了。
5. slave执行这些写命令。

### 增量同步

slave已经同步过master了，那么如果后续master进行了写操作，比如说一个简单的 `set name redis`，那么master执行过当前命令后，会将当前命令发送给slave执行一遍，达成数据一致性。

### 重新同步

当slave与master断开后，重连后会进行重新同步，重新同步分为：完全同步和部分同步。步骤如下：

1. 当slave断开重连后，会发送 psync 命令给master。
2. master首先会对复制id（replicationid）进行判断，如果与自己相同就判断偏移量。如果不同则执行全量同步。
3. master会判断自己的偏移量与slave的偏移量是否一致。
4. 如果不一致，master会去缓冲区中判断slave的偏移量之后的数据是否存在。
5. 如果存在，就会返回+continue回复，表示slave可以执行部分同步了。如果不存在则执行全量同步。
6. master发送网络中断后的写命令（也就是复制积压缓冲区）给slave。
7. slave执行写命令。

## 复制 ID 说明

在上一节中我们说过，如果两个实例具有相同的复制 ID 和复制偏移量，则它们具有完全相同的数据。但是，了解复制 ID 到底是什么以及为什么实例实际上有两个 ` Replication ID`，即主 ID (main ID) 和辅助 ID (secondary ID) 是很有用的。

复制 ID 基本上标记了数据集。每次实例 (instance) 从头开始作为master重新启动，或将副本提升为master时，都会为该实例生成一个新的复制 ID。连接到master的副本将在握手后继承master的复制 ID。因此，具有相同复制 ID 的两个实例之间存在关联，因为它们拥有相同的数据，但可能处于不同的时间。这时offset就起到作用了，offset可以理解为逻辑时间 (logical time)，对于给定的复制 ID，offset就表明了持有的最新数据集。

例如，如果两个实例 A 和 B 具有相同的复制 ID，但是一个偏移量为 1000，一个偏移量为 1023，这意味着第一个缺少应用于数据集的某些命令。这也意味着 A，只需应用几条命令，就可以达到与 B 完全相同的状态。

Redis 实例之所以有两个复制ID，是因为 replicas 被提升为master。在故障转移后，提升的副本需要仍然记住它过去的复制 ID 是什么，因为这个复制 ID 是以前的master的复制 ID。这样，当其他副本将与新master同步时，它们将尝试使用旧的master的复制 ID 执行部分重新同步。这将按预期工作，因为当副本被提升为 master 时，它将其辅助 ID (secondary ID) 设置为其主 ID (main ID)，当ID发生切换时也记住了偏移量是多少。稍后它将选择一个新的随机复制 ID，因为新的历史开始了。在处理新副本连接时，master 会将它们的复制 ID 和偏移量与当前 ID 和辅助 ID 匹配（为了安全起见，最多可达给定的偏移量）。简而言之，这意味着在故障转移后，连接到新提升的master节点的副本不必执行完全同步。

如果您想知道为什么提升为 master 的副本需要在故障转移后更改其复制 ID：由于某些网络分区，旧的 master 可能仍在作为 master 工作：保留相同的复制 ID 会违反以下事实：任意两个随机实例的相同 ID 和相同偏移量意味着它们具有相同的数据集。

## 无磁盘复制

通常，完全重新同步需要在磁盘上创建 RDB 文件，然后从磁盘重新加载相同的 RDB，以便为副本提供数据。

慢速磁盘，这对于master来说可能是一个非常有压力的操作。Redis 2.8.18 版是第一个支持无盘复制的版本。在这种设置中，子进程直接通过线路将 RDB 发送到副本，而不使用磁盘作为中间存储。

## 配置

配置基本的 Redis 复制很简单：只需在副本配置文件中添加以下行：

```
replicaof 192.168.1.1 6379
```

当然，您需要将 192.168.1.1 6379 替换为您的主 IP 地址（或主机名）和端口。或者，您可以调用[REPLICAOF](https://redis.io/commands/replicaof)命令，master将开始与副本同步。

还有一些参数用于调优，通过让master执行部分重新同步，优化副本的backlog占用的内存。有关`redis.conf`更多信息，请参阅Redis 发行版附带的示例 。

可以使用`repl-diskless-sync`配置参数启用无盘复制。在第一个副本之后开始传输以等待更多副本到达的延迟由`repl-diskless-sync-delay` 参数控制。`redis.conf`有关更多详细信息，请参阅Redis 发行版中的示例文件。

## 参考

- https://blog.csdn.net/okiwilldoit/article/details/108527723
- https://redis.io/topics/replication