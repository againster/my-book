# floating point number(浮点数存储)

## 浮点数存储结构

存储如下图：

![floating-point-structure](./.img/floating-point-structure.png)

IEEE-754 number共有3个段构成：
- **sign**. The sign is stored in bit 32.
- **exponent**. The exponent can be computed from bits 24-31 by subtracting 127.
- **mantissa**. The mantissa (also known as significand or fraction) is stored in bits 1-23. An invisible leading bit (i.e. it is not actually stored) with value 1.0 is placed in front, then bit 23 has a value of 1/2, bit 22 has value 1/4 etc. As a result, the mantissa has a value between 1.0 and 2. If the exponent reaches -127 (binary 00000000), the leading 1 is no longer used to enable gradual underflow.

## 计算方式

$$
sign \times 2^{exponent} \times mantissa
$$

![floating-point-structure](.img/floating-point-structure.png)

对图表示的浮点数的解释：

**sign**: 第 32 bit 为1。表示负数。值为-1。

**exponent**: 第31, 26, 24 bit为1。unsigned int 8 表示的数为 $2^7+2^2+2^0 = 133$，exponent值为 $2^7+2^2+2^0 - 127 = 6$。

**mantissa**: 第23, 22, 21 bit为1。第23 bit的值为$1\times\frac{1}{2^1}$, 第22 bit的值为$1\times\frac{1}{2^2}$, 第21 bit的值为$1\times\frac{1}{2^3}$, 第20 bit的值为$0\times\frac{1}{2^4}$, 第19 bit的值为$0\times\frac{1}{2^5}$, ......, 第1 bit的值为$0\times\frac{1}{2^{23}}$, 累加所有bit的值为 $1\times\frac{1}{2^1}+1\times\frac{1}{2^2}+1\times\frac{1}{2^3}+0\times\frac{1}{2^4}+...+0\times\frac{1}{2^{23}}$。 结果值为0.875，但是mantissa有一个不可见，也不存储的前导1.0，所以mantissa的值为1.875。

整个浮点数的结果为：
$$
-1\times2^{6}\times1.875 = -120.0
$$

## Underflow

If the exponent has minimum value (all zero), special rules for denormalized values are followed. The exponent value is set to $2^{-126}$ and the "invisible" leading bit for the mantissa is no longer used.

The range of the mantissa is now $[0:1)$.

![floating-point-structure-underflow](.img/floating-point-structure-underflow.png)

## Rounding errors

Not every decimal number can be expressed exactly as a floating point number. This can be seen when entering "0.1" and examining its binary representation which is either slightly smaller or larger, depending on the last bit.

![floating-point-structure-0.1](.img/floating-point-structure-0.1.png)

## Special Values

"Infinity", "-Infinity" or "NaN" to get the corresponding special values for IEEE-754. Please note there are two kinds of zero: +0 and -0.

![floating-point-structure-inf](.img/floating-point-structure-inf.png)

## 参考

- https://www.h-schmidt.net/FloatConverter/IEEE754.html

