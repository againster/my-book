# socket SO_REUSEADDR SO_REUSEPORT

参考：

- https://www.jianshu.com/p/141aa1c41f15
- https://blog.csdn.net/haluoluo211/article/details/77119804
- http://stackoverflow.com/a/14388707/6037083
- https://zhuanlan.zhihu.com/p/37278278

## 背景

SO_REUSEADDR和SO_REUSEPORT主要是影响socket绑定ip和port的成功与否。先简单说几点绑定规则

- **规则1**：socket可以指定绑定到一个特定的ip和port，例如绑定到192.168.0.11:9000上。
-  **规则2**：同时也支持通配绑定方式，即绑定到本地"any address"（例如一个socket绑定为 0.0.0.0:21，那么它同时绑定了所有的本地地址）。
-  **规则3**：默认情况下，任意两个socket都无法绑定到相同的源IP地址和源端口。

在了解了上述背景后下面简单说明一下linux中SO_REUSEADDR和SO_REUSEPORT对绑定的影响。由于大多数平台对SO_REUSEADDR和SO_REUSEPORT的实现都是BSD上的衍生版本，因此的先介绍BSD中这两个参数的作用。

## SO_REUSEADDR

SO_REUSEADDR的作用主要包括两点：

- 改变了通配地址绑定时处理源地址冲突的处理方式
- 改变了系统对处于TIME_WAIT状态的socket的看待方式

**改变了通配绑定时处理源地址冲突的处理方式**

其具体的表现方式为：未设置SO_REUSEADDR时，socketA先绑定到0.0.0.0:21，后socketB绑定192.168.0.1:21将失败，不符合规则3。但在设置SO_REUSEADDR后socketB将绑定成功。并且这个设置对于socketA（通配绑定）和socketB（特定绑定）的绑定是顺序无关的。下表总结了BSD在各个情况下的绑定情况：

| SO_REUSEADDR | socketA        | socketB        | Result              |
| :----------- | :------------- | :------------- | :------------------ |
| ON / OFF     | 192.168.1.1:21 | 192.168.1.1:21 | ERROR（EADDRINUSE） |
| ON / OFF     | 192.168.1.1:21 | 10.0.1.1:21    | OK                  |
| ON / OFF     | 10.0.1.1:21    | 192.168.1.1:21 | OK                  |
| OFF          | 192.168.1.1:21 | 0.0.0.0:21     | ERROR（EADDRINUSE） |
| OFF          | 0.0.0.0:21     | 192.168.1.1:21 | ERROR（EADDRINUSE） |
| ON           | 192.168.1.1:21 | 0.0.0.0:21     | OK                  |
| ON           | 0.0.0.0:21     | 192.168.1.1:21 | OK                  |
| ON / OFF     | 0.0.0.0:21     | 0.0.0.0:21     | OK                  |

这个表格假定 socketA 已经成功地绑定了表格中对应的地址，然后 socketB 被初始化了，其SO_REUSEADDR 设置的情况如表格第一列所示，然后 socketB 试图绑定表格中对应地址。Result 列是 socketB 绑定的结果。如果第一列中的值是 ON/OFF，那么 SO_REUSEADDR 设置与否都与结果无关。

**改变了系统对处于TIME_WAIT状态的socket的看待方式**

要理解这个句话，首先先简单介绍以下什么是处于TIME_WAIT状态的socket？

每一个socket都有其相应的发送缓冲区（buffer）。当成功调用其`send()`方法的时候，实际上我们所要求发送的数据并不一定被立即发送出去，而是被添加到了发送缓冲区中。对于UDP socket来说，即使不是马上被发送，这些数据一般也会被很快发送出去。但对于TCP socket来说，在将数据添加到发送缓冲区之后，可能需要等待相对较长的时间之后数据才会被真正发送出去。因此，当我们关闭了一个TCP socket之后，其发送缓冲区中可能实际上还仍然有等待发送的数据。但此时因为`send()`返回了成功，我们的代码认为数据已经实际上被成功发送了。如果TCP socket在我们调用`close()`之后直接关闭，那么所有这些数据都将会丢失，而我们的代码根本不会知道。但是，TCP是一个可靠的传输层协议，直接丢弃这些待传输的数据显然是不可取的。实际上，如果在socket的发送缓冲区中还有待发送数据的情况下调用了其`close()`方法，其将会进入一个所谓的`TIME_WAIT`状态。在这个状态下，socket将会持续尝试发送缓冲区的数据直到所有数据都被成功发送或者直到超时，超时被触发的情况下socket将会被强制关闭。

操作系统的kernel在强制关闭一个socket之前的最长等待时间被称为延迟时间（**Linger Time**）。在大部分系统中延迟时间都已经被全局设置好了，并且相对较长（大部分系统将其设置为2分钟）。我们也可以在初始化一个socket的时候使用`SO_LINGER`选项来特定地设置每一个socket的延迟时间。我们甚至可以完全关闭延迟等待。但是需要注意的是，将延迟时间设置为0（完全关闭延迟等待）并不是一个好的编程实践。因为优雅地关闭TCP socket是一个比较复杂的过程，过程中包括与远程主机交换数个数据包（包括在丢包的情况下的丢失重传），而这个数据包交换的过程所需要的时间也包括在延迟时间中。如果我们停用延迟等待，socket不止会在关闭的时候直接丢弃所有待发送的数据，而且总是会被强制关闭（由于TCP是面向连接的协议，不与远端端口交换关闭数据包将会导致远端端口处于长时间的等待状态）。所以通常我们并不推荐在实际编程中这样做。TCP断开连接的过程超出了本文讨论的范围，如果对此有兴趣，可以参考[这个页面](http://www.freesoft.org/CIE/Course/Section4/11.htm)。并且实际上，如果我们禁用了延迟等待，而我们的程序没有显式地关闭socket就退出了，BSD（可能包括其他系统）会忽略我们的设置进行延迟等待。例如，如果我们的程序调用了`exit()`方法，或者其进程被使用某个信号终止了（包括进程因为非法内存访问之类的情况而崩溃）。所以我们无法百分之百保证一个socket在所有情况下忽略延迟等待时间而终止。

这里的问题在于操作系统如何对待处于`TIME_WAIT`阶段的socket：

- 如果`SO_REUSEADDR`选项没有被设置。处于`TIME_WAIT`阶段的socket仍然被认为是绑定在原来那个地址和端口上的。直到该socket被完全关闭之前（结束`TIME_WAIT`阶段），任何其他企图将一个新socket绑定该该地址端口对的操作都无法成功。这一等待的过程可能和延迟等待的时间一样长。所以我们并不能马上将一个新的socket绑定到一个刚刚被关闭的socket对应的地址端口对上。在大多数情况下这种操作都会失败。
- 如果我们在新的socket上设置了`SO_REUSEADDR`选项。如果此时有另一个socket绑定在当前的地址端口对且处于`TIME_WAIT`阶段，那么这个已存在的绑定关系将会被忽略。事实上处于`TIME_WAIT`阶段的socket已经是半关闭的状态，将一个新的socket绑定在这个地址端口对上不会有任何问题。这样的话原来绑定在这个端口上的socket一般不会对新的socket产生影响。但需要注意的是，在某些时候，将一个新的socket绑定在一个处于`TIME_WAIT`阶段但仍在工作的socket所对应的地址端口对会产生一些我们并不想要的，无法预料的负面影响。但这个问题超过了本文的讨论范围。而且幸运的是这些负面影响在实践中很少见到。

## SO_REUSEPORT

SO_REUSEPORT作用就比较明显直观，即打破了上面的规则3

- **允许将多个socket绑定到相同的地址和端口，前提每个socket绑定前都需设置SO_REUSEPORT**。如果第一个绑定的 socket未设置SO_REUSEPORT，那么其他的socket无论有没有设置SO_REUSEPORT都无法绑定到该地址和端口直到第一个socket释放了绑定。
- **SO_REUSEPORT并不表示SO_REUSEADDR**，即不具备上述SO_REUSEADDR的第二点作用（对TIME_WAIT状态的socket处理方式）。因此当有个socketA未设置SO_REUSEPORT绑定后处在TIME_WAIT状态时，如果socketB仅设置了SO_REUSEPORT在绑定和socketA相同的ip和端口时将会失败。

Linux 内核3.9 加入了SO_REUSEPORT。除上述功能外其额外实现了：

- 为了阻止port 劫持Port hijacking，限制所有使用相同ip和port的socket都必须拥有相同的有效用户id(effective user ID)。
- linux内核在处理SO_REUSEPORT socket的集合时，进行了简单的负载均衡操作，即对于UDP socket，内核尝试平均的转发数据报，对于TCP监听socket，内核尝试将新的客户连接请求(由accept返回)平均的交给共享同一地址和端口的socket(监听socket)。

## man7官网的解释

> ```txt
>        SO_REUSEADDR
>               Indicates that the rules used in validating addresses
>               supplied in a bind(2) call should allow reuse of local
>               addresses.  For AF_INET sockets this means that a socket
>               may bind, except when there is an active listening socket
>               bound to the address.  When the listening socket is bound
>               to INADDR_ANY with a specific port then it is not possible
>               to bind to this port for any local address.  Argument is
>               an integer boolean flag.
> 
>        SO_REUSEPORT (since Linux 3.9)
>               Permits multiple AF_INET or AF_INET6 sockets to be bound
>               to an identical socket address.  This option must be set
>               on each socket (including the first socket) prior to
>               calling bind(2) on the socket.  To prevent port hijacking,
>               all of the processes binding to the same address must have
>               the same effective UID.  This option can be employed with
>               both TCP and UDP sockets.
> 
>               For TCP sockets, this option allows accept(2) load
>               distribution in a multi-threaded server to be improved by
>               using a distinct listener socket for each thread.  This
>               provides improved load distribution as compared to
>               traditional techniques such using a single accept(2)ing
>               thread that distributes connections, or having multiple
>               threads that compete to accept(2) from the same socket.
> 
>               For UDP sockets, the use of this option can provide better
>               distribution of incoming datagrams to multiple processes
>               (or threads) as compared to the traditional technique of
>               having multiple processes compete to receive datagrams on
>               the same socket.
> ```

## 示例

```c++
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
void work() {
  int listenfd = socket(AF_INET, SOCK_STREAM, 0);
  if (listenfd < 0) {
    perror("listen socket");
    _exit(-1);
  }
  int ret = 0;
  int reuse = 1;
  ret = setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&reuse,
                   sizeof(int));
  if (ret < 0) {
    perror("setsockopt");
    _exit(-1);
  }
  ret = setsockopt(listenfd, SOL_SOCKET, SO_REUSEPORT, (const void *)&reuse,
                   sizeof(int));
  if (ret < 0) {
    perror("setsockopt");
    _exit(-1);
  }
  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  // addr.sin_addr.s_addr = inet_addr("10.95.118.221");
  addr.sin_addr.s_addr = inet_addr("0.0.0.0");
  addr.sin_port = htons(9980);
  ret = bind(listenfd, (struct sockaddr *)&addr, sizeof(addr));
  if (ret < 0) {
    perror("bind addr");
    _exit(-1);
  }
  printf("bind success\n");
  ret = listen(listenfd, 10);
  if (ret < 0) {
    perror("listen");
    _exit(-1);
  }
  printf("listen success\n");
  struct sockaddr clientaddr;
  socklen_t len = 0;
  while (1) {
    printf("process:%d accept...\n", getpid());
    int clientfd = accept(listenfd, (struct sockaddr *)&clientaddr, &len);
    if (clientfd < 0) {
      printf("accept:%d %s", getpid(), strerror(errno));
      _exit(-1);
    }
    close(clientfd);
    printf("process:%d close socket\n", getpid());
  }
}
int main() {
  printf("uid:%d euid:%d\n", getuid(), geteuid());
  int i = 0;
  for (i = 0; i < 6; i++) {
    pid_t pid = fork();
    if (pid == 0) {
      work();
    }
    if (pid < 0) {
      perror("fork");
      continue;
    }
  }
  int status, id;
  while ((id = waitpid(-1, &status, 0)) > 0) {
    printf("%d exit\n", id);
  }
  if (errno == ECHILD) {
    printf("all child exit\n");
  }
  return 0;
}
```

上述示例程序，启动了6个子进程，每个子进程创建自己的监听socket，bind相同的ip:port；独立的listen()，accept()。无论是否设置`SO_REUSEADDR`，程序启动都正常，使用root账号启动日志如下：

```bash
[root@admin-1 cc.d]# ./a.out 
uid:0 euid:0
bind success
listen success
process:28875 accept...
bind success
listen success
process:28876 accept...
bind success
listen success
process:28877 accept...
bind success
listen success
bind success
process:28878 accept...
listen success
process:28879 accept...
bind success
listen success
process:28880 accept...
```

安全性：是不是只要设置了SO_REUSEPORT选项的服务器程序，就能够监听相同的ip:port；并获取报文呢？当然不是，当前linux内核要求后来启动的服务器程序与前面启动的服务器程序的effective-user-id要相同。当保持root在运行./a.out，切换到其他账号xcentral启动：

```bash
[xcentral@admin-1 cc.d]$ ./a.out 
uid:1005 euid:1005
bind addr: Address already in use
bind addr: Address already in use
31826 exit
31830 exit
bind addr: Address already in use
bind addr: Address already in use
31827 exit
bind addr: Address already in use
31831 exit
bind addr: Address already in use
31828 exit
31829 exit
all child exit
```

