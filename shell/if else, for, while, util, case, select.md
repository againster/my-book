# if else, for, while, util, case, select

[Conditions in bash scripting参考](https://linuxacademy.com/blog/linux/conditions-in-bash-scripting-if-statements/#string-based-conditions)



## 简单介绍

## if else语法解析

```bash
if <condition>
then
    <statements>
[elif condition
    then statements ...]
[else
    statements]
fi
```

## for语法解析

```bash
for name [in list]
do
    statements that can use $name ...
done
```

## while语法解析

```bash
while condition
do
    statements ...
done
```

## util语法解析

```bash
until condition
do
    statements ...
done
```

## case语法解析

```bash
case expression in
pattern1 )
    statements ;;
pattern2 )
    statements ;;
...
esac
```

## select语法解析

```bash
select name [in list]
do
    statements that can use $name ...
done
```

## 断言条件

### 基于文件的条件(File-based condition)

```bash
-a file     True if file exists.
-b file     True if file exists and is a block special file.
-c file     True if file exists and is a character special file.
-d file     True if file exists and is a directory.
-e file     True if file exists.
-f file     True if file exists and is a regular file.
-g file     True if file exists and its set-group-id bit is set.
-h file     True if file exists and is a symbolic link.
-k file     True if file exists and its "sticky" bit is set.
-p file     True if file exists and is a named pipe (FIFO).
-r file     True if file exists and is readable.
-s file     True if file exists and has a size greater than zero.
-t fd       True if file descriptor fd is open and refers to a terminal.
-u file     True if file exists and its set-user-id bit is set.
-w file     True if file exists and is writable.
-x file     True if file exists and is executable.

-G file     True if file exists and is owned by the effective group id.
-L file     True if file exists and is a symbolic link.
-N file     True if file exists and has been modified since it was last read.
-O file     True if file exists and is owned by the effective user id.
-S file     True if file exists and is a socket.

file1 -ef file2
            True if file1 and file2 refer to the same device and inode numbers.
file1 -nt file2
            True if file1 is newer (according to modification date) than file2, or if file1 exists and file2 does not.
file1 -ot file2
            True if file1 is older than file2, or if file2 exists and file1 does not.
```

### 基于字符串的条件(String-based Condition)

```bash
string1 == string2      True if the strings are equal
string1 != string2      True if the strings are not equal
string1 > string2       True if string1 sorts after string2 lexicographically
string1 < string2       True if string1 sorts before string2 lexicographically
-n string               True if the length of string is non-zero
-z string               True if the length of string is zero
string =~ REGEXPATTERN  True if the string matches REGEXPATTERN. Only in double-bracket syntax.
```

### 基于数学的条件(Arithmetic-based Condition)

```bash
arg1 -eq arg2       True if arg1 is equal to arg2
arg1 -ge arg2       True if arg1 is or greater than or equal to arg2
arg1 -gt arg2 	    True if arg1 is greater than arg2
arg1 -le arg2       True if arg1 is less than or equal to arg2
arg1 -lt arg2       True if arg1 is less than arg2
arg1 -ne arg2       True if arg1 is not equal to arg2
```

Arithmetic operator for (()) double-parenthesis syntax:

```
== 	Equal
>= 	Greater Than or Equal
> 	Greater Than
<= 	Less Than or Equal
< 	Less Than
!= 	Not Equal
```

### 其他类型的条件(Other Condition)

```bash
-o optname
                True if the shell option optname is enabled. The list of options appears in the description of the -o option to the set builtin (see The Set Builtin).
-v varname      True if the shell variable varname is set (has been assigned a value).
-R varname      True if the shell variable varname is set and is a name reference.
```


### 组合条件比较(Compound comparison)

```bash
exp1 -a exp2    Logical and. True if both exp1 and exp2 are true.
exp1 -o exp2    Logical or. True if either exp1 or exp2 is true.
```

## 断言条件语法(Different condition syntaxes)

1. 单方括号语法(Single-bracket syntax)

    它是最古老的语法，支持3种类型的条件：(it’s the oldest supported syntax. It supports three types of conditions:)

    - 基于文件的条件(File-based condition)
        ```bash
        if [ -L symboliclink ]; then
        ```
    - 基于字符串的条件(String-based Condition)
        ```bash
        if [ -z "$emptystring" ]; then
        ```
    - 基于数学的条件(Arithmetic-based Condition)
        ```bash
        if [ $num -lt 1 ]; then
        ```
2. 双方括号语法(Double-bracket syntax)

    它是单方括号语法的加强版；和单方括号语法有相同的特性，却有重要的不同。不同点列表：The double-bracket syntax serves as an enhanced version of the single-bracket syntax; it mainly has the same features,but also some important differences with it. I will list them here:

    - 当比较字符串时，双方括号语法表现出shell通配符(shell globbing)特性，这意味着星号("*")将会扩展成任意字符，就像你在正常命令行下使用一样。不仅仅是星号("*")，其他形式的shell通配符(shell globbing)也可以在双括号语法中使用。when comparing strings, the double-bracket syntax features shell globbing.This means that an asterisk (“*”) will expand to literally anything, just as you probably know from normal command-line usage. Other forms of shell globbing are allowed, too. 

        ```bash
        if [[ "$stringvar" == *[sS]tring* ]]; then #
        ```
        需要注意的是仅仅常规的shell通配符被允许使用。bash特定的通配符像{1..4}或者{foo,bar}是不会起作用的。而且如果通配符被引号包含，也是不起作用的，让它离开引号范围内即可。Note that only general shell globbing is allowed. Bash-specific things like {1..4} or {foo,bar} will not work. Also note that the globbing will not work if you quote the right string. In this case you should leave it unquoted.

    - 第二点不同的是单词分割被保护。因此，你可以不用在字符串变量两边放置引号。The second difference is that word splitting is prevented.Therefore, you could omit placing quotes around string variables.

        ```bash
        stringvarwithspaces="halo world"
        if [[ $stringvarwithspaces != foo ]]; then
        ```
        不过，把字符变量用引号包含是一个好习惯，所以建议保持引号。Nevertheless, the quoting string variables remains a good habit, so I recommend just to keep doing it.
    
    - 第三点不同的是不会扩展文件名的构成。如下的两个例子一样，首先看一下古老的单括号情况：The third difference consists of not expanding filenames. I will illustrate this difference using two examples, starting with the old single-bracket situation:

        ```bash
        if [ -a *.sh ]; then
        ```
        如上的条件将会返回true，如果仅仅有一个以.sh为扩展名的文件存在于当前工作文件夹下；如果没有的话，将会返回false；如果有多个以.sh为扩展名的文件，bash将会抛出错误并停止执行脚本，这是因为*.sh展开成了当前目录下的所有文件名，使用双方括号可以避免这个：The above condition will return true if there is one single file in the working directory that has a .sh extension. If there are none, it will return false. If there are several .sh files, bash will throw an error and stop executing the script. This is because *.sh is expanded to the files in the working directory. Using double brackets prevents this:

        ```bash
        if [[ -a *.sh ]]; then
        ```
        如上的条件将会返回true，如果仅仅有一个文件名叫“*.sh”的文件在当前工作文件夹下，而不会涉及到其他.sh文件。这个星号被当做字面字符常量，因为双中括号不会展开文件名。The above condition will return true only if there is a file in the working directory called “*.sh”, no matter what other .sh files exist. The asterisk is taken literally, because the double-bracket syntax does not expand filenames.

    - 第四点不同的是可以使用更过额外的表达式，或者说操作符“&&”和"||"。当然来自于单方括号的-a和-o选项，即与和或，也是被支持使用的。The fourth difference is the addition of more generally known combining expressions, or, more specific, the operators “&&” and “||”. The -a and -o known from the single-bracket syntax is supported, too.

        ```bash
        if [[ $num -eq 3 && "$stringvar" == foo ]]; then
        ```

    - 第五点不同的是双中括号语法允许使用"=~"来做正则表达式匹配。The fifth difference is that the double-bracket syntax allows regex pattern matching using the “=~” operator.

        ```bash
        if [[ “$email” =~ “b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}b” ]]; then
            echo “$email contains a valid e-mail address.”
        fi
        ```
3. 双圆括号语法(Double-parenthesis syntax)

    这也是另外一个为数学条件的语法。它支持“&&”和“||”构成的表达式，而不支持由-a和-o构成的表达式。它相当于内置的let指令。There also is another syntax for arithmetic (number-based) conditions. It supports the “&&” and “||” combining expressions (but not the -a and -o ones!). It is equivalent to the built-in let command.

    ```bash
    if (( $num <= 5 )); then
    ```

date: 20180412
date: 20180721
___