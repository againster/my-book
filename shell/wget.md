# wget

## 参数解析

```bash
#下载：
-N, –timestamping #不要重新下载文件除非比本地文件新

#递归下载：
-r,  --recursive #定义递归下载

#递归接收/拒绝：
-np, --no-parent #不要上升到父目录
```

## 应用示例
1. 多线程下载的方法：[参考](http://blog.netflowdevelopments.com/2011/01/24/multi-threaded-downloading-with-wget/)

    1. 基础的方法

	    ```bash
	    wget -r -np -N [url] & wget -r -np -N [url] & wget -r -np -N [url] & wget -r -np -N [url]
	    ```
	    只需要重复```wget -r -np -N [url]```来达到你需要的线程数。

    2. 比方法1更好的方法
	
	    ```bash
	    THREADS=10
	    WGETFLAGS=’-r -np -N’
	    URL=”http://example.org/”
	    for i in {1..$THREADS}; do
	        wget $WGETFLAGS “$URL”
	    done
    ```

date:20180711
___