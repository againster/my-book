# ss命令


## 简单介绍

[ss指令和iproute2的关系](https://en.wikipedia.org/wiki/Iproute2)

> iproute2 collection contains the following command-line utilities: ip, ss, bridge, rtacct, rtmon, tc, ctstat, lnstat, nstat, routef, routel, rtstat, tipc and arpd. tc is used for traffic control. iproute2 utilities communicate with the Linux kernel using the netlink protocol. Some of the iproute2 utilities are often recommended over now-obsolete net-tools utilities that provide the same functionality.

iproute2集合包含如下的命令行工具：*ip, ss, bridge, rtacct, rtmon, tc, ctstat, lnstat, nstat, routef, routel, rtstat, tipc, arpd*。*tc*常用作流量控制，iproute2工具们使用链路协议与Linux内核通信。与现在过时的且提供相同功能的net-tools工具们相比，iproute2工具们常常被建议使用。

## 参数解析

[参考kernel](https://www.kernel.org/doc/man-pages/), 
[参考man7](http://man7.org/linux/man-pages/man8/ss.8.html#top_of_page), 
[参考freebsd](https://www.freebsd.org/cgi/man.cgi?query=ss&apropos=0&sektion=8&manpath=CentOS+7.1&arch=default&format=html)

1. 基本选项"options"
    ```bash
    NAME
        ss - another utility to investigate sockets
    SYNOPSIS
        ss [options] [ FILTER ]
    DESCRIPTION
        ss is used to dump socket statistics. It allows showing information
        similar to netstat.  It can display more TCP and state informations
        than other tools.
    OPTIONS
        When no option is used ss displays a list of open non-listening TCP
        sockets that have established connection.
        -h, --help
                Show summary of options.
        -V, --version
                Output version information.
        -n, --numeric
                Do not try to resolve service names.
        -r, --resolve
                Try to resolve numeric address/ports.
        -a, --all
                Display both listening and non-listening (for TCP this means
                established connections) sockets.
        -l, --listening
                Display only listening sockets (these are omitted by default).
        -o, --options
                Show timer information.
        -e, --extended
                Show detailed socket information
        -m, --memory
                Show socket memory usage.
        -p, --processes
                Show process using socket.
        -i, --info
                Show internal TCP information.
        -s, --summary
                Print summary statistics. This option does not parse socket
                lists obtaining summary from various sources. It is useful when
                amount of sockets is so huge that parsing /proc/net/tcp is
                painful.
        -b, --bpf
                Show socket BPF filters (only administrators are allowed to get
                these information).
        -4, --ipv4
                Display only IP version 4 sockets (alias for -f inet).
        -6, --ipv6
                Display only IP version 6 sockets (alias for -f inet6).
        -0, --packet
                Display PACKET sockets (alias for -f link).
        -t, --tcp
                Display TCP sockets.
        -u, --udp
                Display UDP sockets.
        -d, --dccp
                Display DCCP sockets.
        -w, --raw
                Display RAW sockets.
        -x, --unix
                Display Unix domain sockets (alias for -f unix).
        -f FAMILY, --family=FAMILY
                Display sockets of type FAMILY.  Currently the following
                families are supported: unix, inet, inet6, link, netlink.
        -A QUERY, --query=QUERY, --socket=QUERY
                List of socket tables to dump, separated by commas. The
                following identifiers are understood: all, inet, tcp, udp, raw,
                unix, packet, netlink, unix_dgram, unix_stream, packet_raw,
                packet_dgram.
        -D FILE, --diag=FILE
                Do not display anything, just dump raw information about TCP
                sockets to FILE after applying filters. If FILE is - stdout is
                used.
        -F FILE, --filter=FILE
                Read filter information from FILE.  Each line of FILE is
                interpreted like single command line option. If FILE is - stdin
                is used.
        FILTER := [ state TCP-STATE ] [ EXPRESSION ]
                Please take a look at the official documentation (Debian package
                iproute-doc) for details regarding filters.
    USAGE EXAMPLES
        ss -t -a
                Display all TCP sockets.
        ss -u -a
                Display all UDP sockets.
        ss -o state established '( dport = :ssh or sport = :ssh )'
                Display all established ssh connections.
        ss -x src /tmp/.X11-unix/*
                Find all local processes connected to X server.
        ss -o state fin-wait-1 '( sport = :http or sport = :https )' dst
        193.233.7/24
                List all the tcp sockets in state FIN-WAIT-1 for our apache to
                network 193.233.7/24 and look at their timers.   NAME        ss - another utility to investigate sockets SYNOPSIS        ss [options] [ FILTER ] DESCRIPTION
    ```

2. 过滤选项"FILTER"

    [参考cyberciti](http://www.cyberciti.biz/files/ss.html)

    1. 状态过滤"STATE-FILTER"
        > STATE-FILTER allows to construct arbitrary set of states to match. Its syntax is sequence of keywords state and exclude followed by identifier of state. Available identifiers are:
        
        > All standard TCP states: established, syn-sent, syn-recv, fin-wait-1, fin-wait-2, time-wait, closed, close-wait, last-ack, listen and closing.

        ```bash
        all - for all the states
        connected - all the states except for listen and closed
        synchronized - all the connected states except for syn-sent
        bucket - states, which are maintained as minisockets, i.e. time-wait and syn-recv.
        big - opposite to bucket  
        ```
    2. 地址过滤" ADDRESS_FILTER"
        
        ADDRESS_FILTER is boolean expression with operations and, or and not, which can be abbreviated in C style f.e. as &, &&.

        Predicates check socket addresses, both local and remote. There are the following kinds of predicates:
        
        ```bash
        dst ADDRESS_PATTERN - matches remote address and port 
        src ADDRESS_PATTERN - matches local address and port 
        dport RELOP PORT - compares remote port to a number 
        sport RELOP PORT - compares local port to a number 
        autobound - checks that socket is bound to an ephemeral port​  
        ```
        - 以下是"PORT"的说明：

            > PORT is syntactically ADDRESS_PATTERN with wildcard address part. Certainly, it is undefined for UNIX sockets.
        - 以下是地址匹配模式"ADDRESS_PATTERN"的说明：
            
            ```bash
            inet - ADDRESS_PATTERN consists of IP prefix, optionally followed by colon and port. If prefix or port part is absent or replaced with *, this means wildcard match.

            inet6 - The same as inet, only prefix refers to an IPv6 address. Unlike inet colon becomes ambiguous, so that ss allows to use scheme, like used in URLs, where address is suppounded with [ ... ].

            unix - ADDRESS_PATTERN is shell-style wildcard.

            packet - format looks like inet, only interface index stays instead of port and link layer protocol id instead of address.

            netlink - format looks like inet, only socket pid stays instead of port and netlink channel instead of address.  
            ```
        - 地址匹配模式"ADDRESS_PATTERN"的更多细节：

            Option list may contain address/port filter. It is boolean expression which consists of boolean operation or, and, not and predicates. Actually, all the flavors of names for boolean operations are eaten: &, &&, |, ||, !, but do not forget about special sense given to these symbols by unix shells and escape them correctly, when used from command line.

            Predicates may be of the folowing kinds:

            - A. Address/port match, where address is checked against mask and port is either wildcard or exact. It is one of:

                ```bash
                dst prefix:port
                src prefix:port
                src unix:STRING
                src link:protocol:ifindex
                src nl:channel:pid
                ```

                Both prefix and port may be absent or replaced with *, which means wildcard. UNIX socket use more powerful scheme matching to socket names by shell wildcards. Also, prefixes unix: and link: may be omitted, if address family is evident from context (with option -x or with -f unix or with unix keyword), F.e.

                ```bash
                dst 10.0.0.1
                dst 10.0.0.1:
                dst 10.0.0.1/32:
                dst 10.0.0.1:*
                ```
                are equivalent and mean socket connected to any port on host 10.0.0.1

                ```bash
                dst 10.0.0.0/24:22
                ```
                sockets connected to port 22 on network 10.0.0.0...255.

                Note that port separated of address with colon, which creates troubles with IPv6 addresses. Generally, we interpret the last colon as splitting port. To allow to give IPv6 addresses, trick like used in IPv6 HTTP URLs may be used:

                ```bash
                dst [::1]
                ```

                are sockets connected to ::1 on any port

                Another way is dst ::1128/. / helps to understand that colon is part of IPv6 address.

                Now we can add another alias for dst 10.0.0.1: dst [10.0.0.1]. :-)

                Address may be a DNS name. In this case all the addresses are looked up (in all the address families, if it is not limited by option -f or special address prefix inet:, inet6) and resulting expression is or over all of them.

            - B. Port expressions:

                ```bash
                dport >= :1024
                dport != :22
                sport < :32000
                ```
                etc. All the relations: <, >, =, >=, =, ==, !=, eq, ge, lt, ne... Use variant which you like more, but not forget to escape special characters when typing them in command line. :-) Note that port number syntactically coincides to the case A! You may even add an IP address, but it will not participate incomparison, except for == and !=, which are equivalent to corresponding predicates of type A. F.e.

                dst 10.0.0.1:22 is equivalent to dport eq 10.0.0.1:22 and not dst 10.0.0.1:22 is equivalent to dport neq 10.0.0.1:22

## 输出解析

[参考kernel](https://www.kernel.org/doc/man-pages/),
[参考man7](http://man7.org/linux/man-pages/man8/netstat.8.html),
[参考freebsd](https://www.freebsd.org/cgi/man.cgi?query=netstat&apropos=0&sektion=8&manpath=CentOS+7.1&arch=default&format=html)

```bash
Recv-Q
   Established: The count of bytes not copied by the user program con-
   nected to this socket. Listening: Since	Kernel 2.6.18 this column con-
   tains the current syn backlog.
Send-Q
   Established:  The  count	 of bytes not acknowledged by the remote host.
   Listening: Since Kernel 2.6.18 this column contains the maximum size of
   the syn backlog.
State
   The state of the	socket.	Since there are	no states in raw mode and usu-
   ally no states used in UDP and UDPLite, this column may be left	blank.
   Normally	this can be one	of several values:
   ESTABLISHED
      The socket has an	established connection.
   SYN_SENT
      The socket is actively attempting	to establish a connection.
   SYN_RECV
      A	connection request has been received from the network.
   FIN_WAIT1
      The socket is closed, and	the connection is shutting down.
   FIN_WAIT2
      Connection  is  closed, and the socket is	waiting	for a shutdown
      from the remote end.
   TIME_WAIT
      The socket is waiting after close	to handle packets still	in the
      network.
   CLOSE  The socket is not	being used.
   CLOSE_WAIT
      The remote end has shut down, waiting for	the socket to close.
   LAST_ACK
      The  remote end has shut down, and the socket is closed. Waiting
      for acknowledgement.
   LISTEN The socket is listening for incoming connections.	 Such  sockets
      are  not included	in the output unless you specify the --listen-
      ing (-l) or --all	(-a) option.
   CLOSING
      Both sockets are shut down but we	still don't have all our  data
      sent.
   UNKNOWN
      The state	of the socket is unknown. 
```

Six columns. The first is Netid, it denotes socket type and transport protocol, when it is ambiguous: tcp, udp, raw, u_str is abbreviation for unix_stream, u_dgr for UNIX datagram sockets, nl for netlink, p_raw and p_dgr for raw and datagram packet sockets. This column is optional, it will be hidden, if filter selects an unique netid.

The second column is State. Socket state is displayed here. The names are standard TCP names, except for UNCONN, which cannot happen for TCP, but normal for not connected sockets of another types. Again, this column can be hidden.

Then two columns (Recv-Q and Send-Q) showing amount of data queued for receive and transmit.

And the last two columns display local address and port of the socket and its peer address, if the socket is connected.

If options -o, -e or -p were given, options are displayed not in fixed positions but separated by spaces pairs: option:value. If value is not a single number, it is presented as list of values, enclosed to ( ... ) and separated with commas. F.e.

```bash
timer:(keepalive,111min,0)
```
is typical format for TCP timer (option -o).

```bash
users:((X,113,3))
```
is typical for list of users (option -p).