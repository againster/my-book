# bash基础

[markdown语法参考](https://guides.github.com/features/mastering-markdown/)

[GNU bash参考手册](http://www.gnu.org/software/bash/manual/html_node/index.html)
___

## 单引号，双引号，反引号

1. 单引号，保持所有字符的字面值，即使引号内的\，回车也不例外，但是字符串中不能出现单引号
2. 双引号，保持所有字符的字面值（包括回车），但以下情况除外：

    > - $变量名，可以取变量的值
    > - ``，表示命令替换
    > - \$，表示$的字面值
    > - \\\`，表示`的字面值
    > - \"，表示"的字面值
    > - \\\，表示\的字面值
    > - 其它字符前的\无特殊含义，只表示字面值

3. 反引号，起命令替换作用。命令替换是指shell能够将一个命令的标准输出插在一个命令行中任何位置。

date: 20180412
___

## 变量

[参考](https://www.cyberciti.biz/tips/bash-shell-parameter-substitution-2.html)

```bash
${变量#关键字}	若变量内容从头开始的数据符合“关键字”，则将符合的最短数据删除
${变量#关键字}	若变量内容从头开始的数据符合“关键字”，则将符合的最长数据删除
${变量%关键字}	若变量内容从尾开始的数据符合“关键字”，则将符合的最短数据删除
${变量%%关键字}	若变量内容从尾开始的数据符合“关键字”，则将符合的最长数据删除
${变量/旧字符串/新字符串}	若变量内容符合“旧字符串”，则首个旧字符会被新字符替换。
${变量/旧字符串//新字符串}	若变量内容符合“旧字符串”，则全部旧字符会被新字符替换。
```

## 环境变量

```bash
HOSTNAME
    #主机名
LD_LIBRARY_PATH
    #静态库和共享库的搜索路径
```

___

## linux文件系统结构

1. /etc目录

	[控制挂载和卸载文件系统(Control mounting and unmounting of filesystems)](https://www.ibm.com/developerworks/library/l-lpic1-104-3/index.html)
	
	```bash
	/etc/fstab
	    #开机后需要挂载的文件系统集合。
	/etc/hosts
	    #作用相当于DNS，提供IP地址到hostname主机名的对应。Linux系统在向DNS服务器发出域名解析请求之前会查询/etc/hosts文件，如果里面有相应的记录，就会使用hosts里面的记录
	
	/etc/ld.so.conf
	    #静态库和共享库的搜索路径配置文件
	/etc/ld.so.conf.d/
	    #静态库和共享库的搜索路径配置文件所在的目录
	
	/etc/sysconfig/network
	    #主机名，由/etc/rc.d/rc.sysinit脚本读取到环境变量HOSTNAME中
	```

2. /proc目录

	```bash
	/proc/sys/kernel/hostname
	    #主机名，由/etc/rc.d/rc.sysinit脚本设置。
	```

date:20180711
___