# git clean

## Name

git-clean - 从工作树中删除未跟踪的文件。Remove untracked files from the working tree

## SYNOPSIS

```bash
git clean [-d] [-f] [-i] [-n] [-q] [-e <pattern>] [-x | -X] [--] <path>…​
```

## DESCRIPTION

Cleans the working tree by recursively removing files that are not under version control, starting from the current directory.

Normally, only files unknown to Git are removed, but if the -x option is specified, ignored files are also removed. This can, for example, be useful to remove all build products.

If any optional ```<path>...``` arguments are given, only those paths are affected.

## OPTIONS

```bash
-d
Remove untracked directories in addition to untracked files. If an untracked directory is managed by a different Git repository, it is not removed by default. Use -f option twice if you really want to remove such a directory.

-f
--force
If the Git configuration variable clean.requireForce is not set to false, git clean will refuse to delete files or directories unless given -f, -n or -i. Git will refuse to delete directories with .git sub directory or file unless a second -f is given.

-i
--interactive
Show what would be done and clean files interactively. See “Interactive mode” for details.

-n
--dry-run
Don’t actually remove anything, just show what would be done.

-q
--quiet
Be quiet, only report errors, but not the files that are successfully removed.

-e <pattern>
--exclude=<pattern>
Use the given exclude pattern in addition to the standard ignore rules (see gitignore[5]).

-x
Don’t use the standard ignore rules (see gitignore[5]), but still use the ignore rules given with -e options from the command line. This allows removing all untracked files, including build products. This can be used (possibly in conjunction with git reset) to create a pristine working directory to test a clean build.

-X
Remove only files ignored by Git. This may be useful to rebuild everything from scratch, but keep manually created files.
```

## 例子

```bash
git clean -n #不真实的删除文件，仅仅展示

git clean -df #强制删除未跟踪的文件和目录
```