# git

- [git配置](#git配置)

***Author: ArrowLee***

***Date: 2019/4/4***

## git配置

1. 用户及密码配置

    ```bash
    #用户名为: 姓名+工号
    git config --global user.name 张三12345
    #邮箱为  : 工号@sangfor.com     
    git config --global user.email 12345@sangfor.com
    git config --list | grep user
    ```

2. 编辑器与换行符配置
    ```bash
    # 默认使用vim作为命令行提交日志的编辑器，对于习惯vim的同学推荐
    git config --global core.editor vim
    # 必须配置， 避免git checkout 代码时自动将unix换行符修改为windows的换行符，引发系列问题，设置git不要自动转换
    git config --global core.autocrlf false
    git config --list | grep core
    # windows支持长路径，要不然代码路径太长会报错
    git config --global core.longpaths true
    #windows默认下载git代码，没有显示链接link属性问题
    git config --global core.symlinks true
    ```
3. 快捷命令配置(可选)

    ```bash
    # 常用简写配置：如 git status → git st
    git config --global alias.st status
    git config --global alias.co checkout
    # 长命令简写配置： 如 格式化log →  git lg
    git config --global alias.lg 'log --oneline --graph --decorate --pretty=format:"%h%x20%Cgreen%d%x20%Cred%an%x20%C(yellow)%ad%x20%Creset%s" --date=short'
    ```

4. 配置SSH key

    若要通过.git协议访问，gitlab平台需要配置git公钥。ssh公钥一般放置在 ~/.ssh/id_rsa.pub中，查看是否存在，如不存在可以生成，如有也可以重新生成。之后把公钥内容拷贝到git。生成方法：
    ```bash
    # 请修改email
    ssh-keygen -t rsa -C "your.email@git.com" -b 4096
    cat ~/.ssh/id_rsa.pub
    ```

5. 配置文件

    ```ini
    [user]
        name = 张三12345
        email = 12345@sangfor.com
    [core]
        editor = vim
        autocrlf = false
    [alias]
        st = status
        cm = commit
        br = branch
        co = checkout
        pl = pull
        ps = push
        ft = fetch

        rb = rebase
        mr = merge
        cp = cherry-pick

        df = diff
        dft = difftool
        mt = mergetool

        last = log -1 HEAD
        lg = log --oneline --graph --decorate --pretty=format:"%h%x20%Cgreen%d%x20%Cred%an%x20%C(yellow)%ad%x20%Creset%s" --date=short

    [diff]
        tool = bc4
    [difftool]
        prompt = false
    [difftool "bc4"]
        cmd = "\"C:/program files/beyond compare 4/bcomp.exe\" \"$LOCAL\" \"$REMOTE\""
    [merge]
        tool = bc4
    [mergetool]
        prompt = false
    [mergetool "bc4"]
        cmd = "\"C:/program files/beyond compare 4/bcomp.exe\" \"$LOCAL\" \"$REMOTE\" \"$BASE\" \"$MERGED\""
    ```

## git log

- 语法说明：

    ```bash
    git log [<options>] [<revision range>] [[\--] <path>…​]
    ```

- 参数说明：
    ```bash
    -<number>
    -n <number>
    --max-count=<number>
        限制输出的提交数量
    ```

- 使用范例：

    ```bash
    #
    git log -n 1 --stat HEAD
    ```

	
## git rm

- 使用范例：

	```bash
	git rm --cached readme.txt #删除跟踪readme.txt，并保留在本地。
	git rm --f readme.txt #删除跟踪readme.txt，并且删除本地文件。
	```
	
	
## git diff

- 使用范例：

	```bash
	git diff --cached readme.txt #工作区与暂存区比较
	```