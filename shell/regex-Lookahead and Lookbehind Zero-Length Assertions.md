# regex - Lookahead and Lookbehind Zero-Length Assertions

## Lookahead and Lookbehind Zero-Length Assertions

前瞻（lookahead）和后顾（lookbehind），被统称为环顾四周（lookaround），都是零宽断言，就像在手册中的之前解释的[the start and end of line](https://www.regular-expressions.info/anchors.html)，[start and end of word](https://www.regular-expressions.info/wordboundaries.html)锚一样。不同之处在于，环顾四周（"lookaround"）实际上匹配字符，但随后放弃匹配，只返回结果:匹配或不匹配。这就是为什么它们叫做“断言”。它们不会消耗在字符串中的字符。而是仅仅断言是否是可能还是不可能。环顾四周（"lookaround"）允许你创建那些没有它们就不能创建的，或者没有它们正则表达式就变得非常冗余的正则表达式。Lookahead and lookbehind, collectively called "lookaround", are zero-length assertions just like the start and end of line, and start and end of word anchors explained earlier in this tutorial. The difference is that lookaround actually matches characters, but then gives up the match, returning only the result: match or no match. That is why they are called "assertions". They do not consume characters in the string, but only assert whether a match is possible or not. Lookaround allows you to create regular expressions that are impossible to create without them, or that would get very longwinded without them.

## Syntax

每个模式都被编写为一个圆括号组，而且第一个字符是问号。环顾四周（"lookaround"）的表示法是比较容易记住的，也有一些其他的实验中的模式也是相似的，所以让所有的字符按正确的顺序排列是很重要的。Every extended pattern is written as a parenthetical group with a question mark as the first character. The notation for the lookaround is fairly mnemonic, but there are some other, experimental patterns that are similar, so it is important to get all the characters in the right order.

```bash
(?=pattern)
    is a positive look-ahead assertion
(?!pattern)
    is a negative look-ahead assertion
(?<=pattern)
    is a positive look-behind assertion
(?<!pattern)
    is a negative look-behind assertion
```

注意：=或者!经常在最后面。方向指示符只出现在后顾（"lookbehind"）中，并出现在“正或负指示符”之前（=即是正指示符，!即是负指示符）。Notice that the = or ! is always last. The directional indicator is only present in the lookbehind, and comes before the positive-or-negative indicator.

## Positive and Negative Lookahead

负前瞻是很有必要的，如果你想要匹配的字符后面没有跟着一个字符。当解释字符集合（[character classes](https://www.regular-expressions.info/charclass.html)，），这个指导手册解释了为什么你不能使用一个排除型字符集合去匹配一个没有被```u```跟着的```q```。负前瞻提供给了一个解决方法：```q(?!u)```。负前瞻的结构是一对圆括号，圆括号后面跟着问号和感叹号。在这个前瞻中，我们有一个平常的正则表达式```u```。Negative lookahead is indispensable if you want to match something not followed by something else. When explaining character classes, this tutorial explained why you cannot use a negated character class to match a q not followed by a u. Negative lookahead provides the solution: q(?!u). The negative lookahead construct is the pair of parentheses, with the opening parenthesis followed by a question mark and an exclamation point. Inside the lookahead, we have the trivial regex u.

正前瞻工作的很相似。```q(?=u)```匹配一个被```u```跟着的```q```，而不是让```u```成为匹配的一部分。正前瞻结构是一对圆括号，圆括号后面跟着问号和等于号。Positive lookahead works just the same. q(?=u) matches a q that is followed by a u, without making the u part of the match. The positive lookahead construct is a pair of parentheses, with the opening parenthesis followed by a question mark and an equals sign.

你可以使用任何正则表达式在前瞻（lookahead）中（但不能使用后顾（lookbehind），解释如下）。任何合法的正则表达式可以使用在前瞻（lookahead）中。如果它包含了捕获分组（[capturing groups](https://www.regular-expressions.info/brackets.html)），那么这些组将正常捕获，而且对它们的反向引用将正常工作，甚至在前瞻（lookahead）之外也是如此（仅仅tcl除外，它把所有的在前瞻中的分组视作非捕获分组）。前瞻（lookahead）它自己不是一个捕获分组。它不会被包含在反向引用编号的计数中。如果你想存储在前瞻（lookahead）中的匹配字符串，在前瞻中你必须在正则表达式周围放置捕获括号，就像这样：```(?=(regex))```。其它方法将不起作用，因为在捕获组存储匹配项时，前瞻操作已经丢弃了正则表达式匹配项。

You can use any regular expression inside the lookahead (but not lookbehind, as explained below). Any valid regular expression can be used inside the lookahead. If it contains capturing groups then those groups will capture as normal and backreferences to them will work normally, even outside the lookahead. (The only exception is Tcl, which treats all groups inside lookahead as non-capturing.) The lookahead itself is not a capturing group. It is not included in the count towards numbering the backreferences. If you want to store the match of the regex inside a lookahead, you have to put capturing parentheses around the regex inside the lookahead, like this: (?=(regex)). The other way around will not work, because the lookahead will already have discarded the regex match by the time the capturing group is to store its match.

## Regex Engine Internals

首先，我们看一下正则表达式引擎怎样在字符串```Iraq```上应用```q(?!u)```。正则表达式的第一个字符是文字```q```。我们已经知道，这会让引擎去遍历字符串直到```q```被匹配到。字符串中的位置现在是字符串（Iraq）后面的空。下一个字符将会前瞻（lookahead）。引擎注意到了，现在在前瞻（lookahead）结构内部，而且开始匹配在前瞻中的正则表达式。所以下一个标记是```u```。不匹配字符后面的空字符。引擎注意到，前瞻（lookahead）内部的正则表达式匹配失败了。因为前瞻是负的，这意味着前瞻成功的匹配到了当前的位置。此时，整个正则表达式已经匹配，并返回```q```作为匹配项。First, let's see how the engine applies q(?!u) to the string Iraq. The first token in the regex is the literal q. As we already know, this causes the engine to traverse the string until the q in the string is matched. The position in the string is now the void after the string. The next token is the lookahead. The engine takes note that it is inside a lookahead construct now, and begins matching the regex inside the lookahead. So the next token is u. This does not match the void after the string. The engine notes that the regex inside the lookahead failed. Because the lookahead is negative, this means that the lookahead has successfully matched at the current position. At this point, the entire regex has matched, and q is returned as the match.

让我们用相同的正则表达式到```quit```。```q```匹配了```q```。在前瞻（lookahead）中的下一个标记是```u```。这个也匹配到了。引擎向前到下一个字符：```i```。然而在前瞻内部的正则表达式已经匹配。引擎注意到成功了，随后放弃了正则匹配。这将导致引擎在字符串中后退到```u```。Let's try applying the same regex to quit. q matches q. The next token is the u inside the lookahead. The next character is the u. These match. The engine advances to the next character: i. However, it is done with the regex inside the lookahead. The engine notes success, and discards the regex match. This causes the engine to step back in the string to u.

因为前瞻（lookahead）是负的，成功的匹配内部的正则导致了前瞻（lookahead）失败。因为没有其他的正则表达式排列（permutations），引擎不得不再次在开头开始。因为```q```不能匹配到任何其他地方（如果正则q(?!u)匹配```quitqk```，会报告成功，```qk```满足正则，说明正则会多次匹配字符串，即是贪婪的），引擎报告失败。
Because the lookahead is negative, the successful match inside it causes the lookahead to fail. Since there are no other permutations of this regex, the engine has to start again at the beginning. Since q cannot match anywhere else, the engine reports failure.

让我们再深入了解，去确保你理解了这个前瞻（lookahead）的含义。我们应用```q(?=u)i```到```quit```。前瞻（lookahead）现在是正的，而且跟了一个其他的标记（即```i```）。再次，```q```匹配了```q```而且```u```匹配了```u```。从前瞻（lookahead）中匹配的字符再一次被抛弃了，所以引擎从字符串```quit```的```i```回到了```u```。前瞻（lookahead）成功了，所以引擎在正则中的```i```继续。但是正则中的```i```不匹配字符串```quit```中的```u```。所以匹配尝试失败了。所有剩余的尝试也失败了，因为没有了更多的```q```在字符串中了。Let's take one more look inside, to make sure you understand the implications of the lookahead. Let's apply q(?=u)i to quit. The lookahead is now positive and is followed by another token. Again, q matches q and u matches u. Again, the match from the lookahead must be discarded, so the engine steps back from i in the string to u. The lookahead was successful, so the engine continues with i. But i cannot match u. So this match attempt fails. All remaining attempts fail as well, because there are no more q's in the string.

## Positive and Negative Lookbehind

后顾后相同的效果，但是是反着工作的。使用了负后顾，它告诉正则引擎去临时的反向前进，去检查是否在后顾（lookbehind）中的文本可以被匹配。```(?<!a)b```匹配一个前面没有"a"的"b"。它不会匹配```cab```，但是匹配在```bed```，```debt```的```b```（仅仅这个```b```）。```(?<=a)b```（正后顾（positive lookbehind））匹配在```cab```中的```b```（仅仅这个```b```），但是不匹配```bed```，```debt```。Lookbehind has the same effect, but works backwards. It tells the regex engine to temporarily step backwards in the string, to check if the text inside the lookbehind can be matched there. (?<!a)b matches a "b" that is not preceded by an "a", using negative lookbehind. It doesn't match cab, but matches the b (and only the b) in bed or debt. (?<=a)b (positive lookbehind) matches the b (and only the b) in cab, but does not match bed or debt.

正后顾（positive lookbehind）的结构是```(?<=text)```：一对圆括号，开头的圆括号后面跟着问号、“小于”符号和等号。负后顾（negative lookbehind）被写成```(?<!text)```，使用感叹号取代了等于号。The construct for positive lookbehind is (?<=text): a pair of parentheses, with the opening parenthesis followed by a question mark, "less than" symbol, and an equals sign. Negative lookbehind is written as (?<!text), using an exclamation point instead of an equals sign.


## More Regex Engine Internals

让我们应用```(?<=a)b```到```thingamabob```。引擎开始了后顾（lookbehind）和第一个字符（后顾不是说从字符串的最后一个字符到第一个字符）。在这个情况下，后顾（behind）告诉引擎反向前进一个字符，看看是否可以匹配。因为在```t```前面没有字符，引擎不能向后取一个字符。所以后顾（lookbehind）失败，而且引擎再次开始了下一个字符```h```。 再一次，引擎临时的反向前进一个字符，去检查是否"a"可以被发现。它发现了```t```，所以正后顾再次失败了。Let's apply (?<=a)b to thingamabob. The engine starts with the lookbehind and the first character in the string. In this case, the lookbehind tells the engine to step back one character, and see if a can be matched there. The engine cannot step back one character because there are no characters before the t. So the lookbehind fails, and the engine starts again at the next character, the h. (Note that a negative lookbehind would have succeeded here.) Again, the engine temporarily steps back one character to check if an "a" can be found there. It finds a t, so the positive lookbehind fails again.

后顾（lookbehind）继续失败，直到正则表达式到达字符串中的```m```。引擎再次反向前进一个字符，而且注意到```a```可以被匹配。正向后顾（positive lookbehind）匹配到了。因为它（后顾）是零宽的，在字符串当前的位置保持在了```m```。正则表达式的下一个标记是```b```，它在这里不匹配。下一个字符是字符串中的第二个```a```。引擎反向前进了，发现字符串中的```m```不匹配正则表达式中的```a```。
The lookbehind continues to fail until the regex reaches the m in the string. The engine again steps back one character, and notices that the a can be matched there. The positive lookbehind matches. Because it is zero-length, the current position in the string remains at the m. The next token is b, which cannot match here. The next character is the second a in the string. The engine steps back, and finds out that the m does not match a.

下一个字符是字符串中的第一个```b```。引擎反向前进了，而且发现```a```满足了后顾（lookbehind）。字符串中的```b```也匹配到了正则表达式中的```b```，整个正则表达式被成功匹配。它匹配到了一个字符：在字符串中的第一个```b```。The next character is the first b in the string. The engine steps back and finds out that a satisfies the lookbehind. b matches b, and the entire regex has been matched successfully. It matches one character: the first b in the string.

## Important Notes About Lookbehind

好的消息是你可以在正则表达式中到处使用后顾（lookbehind）了。如果你想发现一个单词不以```s```结尾，你可以使用```\b\w+(?<!s)\b```。这个显然和```\b\w+[^s]\b```不一样。当应用到```John's```时，前者匹配了```John```，后者匹配了```John'```(包括撇符号)。我把这个留下来让你去弄明白是为什么。（注意：```\b```匹配撇号和```s```之间的内容）。后者也不会匹配单个字母的单词像"a"或者"I"。正确的不使用后顾（lookbehind）的正则表达式是```\b\w*[^s\W]\b```（星号取代加好，而且\W在这个字符集中）。个人看来，我发现这个后顾（lookbehind）更容易让人理解。最后一个正则表达式，也工作正常，它有双重否定（\W在这个否定字符集中）。双重否定让人更趋向迷惑，而对正则表达式引擎而言却不是这样。（可能Tcl除外，它将否定字符类中的否定短字符视为错误）The good news is that you can use lookbehind anywhere in the regex, not only at the start. If you want to find a word not ending with an "s", you could use \b\w+(?<!s)\b. This is definitely not the same as \b\w+[^s]\b. When applied to John's, the former matches John and the latter matches John' (including the apostrophe). I will leave it up to you to figure out why. (Hint: \b matches between the apostrophe and the s). The latter also doesn't match single-letter words like "a" or "I". The correct regex without using lookbehind is \b\w*[^s\W]\b (star instead of plus, and \W in the character class). Personally, I find the lookbehind easier to understand. The last regex, which works correctly, has a double negation (the \W in the negated character class). Double negations tend to be confusing to humans. Not to regex engines, though. (Except perhaps for Tcl, which treats negated shorthands in negated character classes as an error.)

坏消息是，大多数正则表达式不允许在后顾（lookbehind）中使用任何正则表达式，因为它们不能向后应用正则表达式。正则表达式引擎需要能理解在检查后顾（lookbehind）之前有多少字符需要反向前进。当评估后顾（lookbehind）时，正则表达式引擎决定了在后顾（lookbehind）中正则的长度，然后在对象字符串中反向前进那么多字符，而且从左到右应用在后顾（lookbehind）中的正则表达式，就像正常的正则表达式一样。
The bad news is that most regex flavors do not allow you to use just any regex inside a lookbehind, because they cannot apply a regular expression backwards. The regular expression engine needs to be able to figure out how many characters to step back before checking the lookbehind. When evaluating the lookbehind, the regex engine determines the length of the regex inside the lookbehind, steps back that many characters in the subject string, and then applies the regex inside the lookbehind from left to right just as it would with a normal regex.

## Lookaround Is Atomic

事实是环顾四周（lookaround）是零宽的，这个让它是原子的。只要环顾四周（lookaround）条件被满足，正则引擎就忘记了在环顾四周（lookaround）中的一切。它将不会回溯进入环顾四周（lookaround）去尝试不同的排列。The fact that lookaround is zero-length automatically makes it atomic. As soon as the lookaround condition is satisfied, the regex engine forgets about everything inside the lookaround. It will not backtrack inside the lookaround to try different permutations.

唯一不同的情况是，当在环顾四周（lookaround）中使用捕获分组时。因为正则引擎不会回溯进入环顾四周（lookaround），它将不尝试不同的捕获分组的排列。The only situation in which this makes any difference is when you use capturing groups inside the lookaround. Since the regex engine does not backtrack into the lookaround, it will not try different permutations of the capturing groups.

正因为这个原因，正则表达式```(?=(\d+))\w+\1```不会匹配```123x12```。首先环顾四周（lookaround）捕获了```123```给了```\1```。然后```\w+```匹配了整个字符串，而且回溯直到仅仅匹配```1```。最终，```\w+```失败了，因为```\1```不能被在任何位置被匹配。现在正则引擎没有任何东西去回溯了，而且总的来说正则失败了。这个通过```\d+```创建的回溯步骤已经被丢弃了。正则绝不会到达前瞻（lookahead）捕获点```12```。
For this reason, the regex (?=(\d+))\w+\1 never matches 123x12. First the lookaround captures 123 into \1. \w+ then matches the whole string and backtracks until it matches only 1. Finally, \w+ fails since \1 cannot be matched at any position. Now, the regex engine has nothing to backtrack to, and the overall regex fails. The backtracking steps created by \d+ have been discarded. It never gets to the point where the lookahead captures only 12.

显然，正则引擎尝试了在字符串中的更多的位置（意思是说```(?=(\d+))```会尝试```123x12```中的```123```，```23```，```3```）。如果我们改变目标字符串，这个正则```(?=(\d+))\w+\1```匹配在```456x56```的```56x56```。Obviously, the regex engine does try further positions in the string. If we change the subject string, the regex (?=(\d+))\w+\1 does match 56x56 in 456x56.

如果你没有使用在环顾四周（lookaround）中的捕获分组，那么所有这些都没有关系。要么环顾四周（lookaround）条件可以被满足，要么不能被满足。可以被满足多少方式是不相干的。If you don't use capturing groups inside lookaround, then all this doesn't matter. Either the lookaround condition can be satisfied or it cannot be. In how many ways it can be satisfied is irrelevant.

## 参考

[regular-expressions](https://www.regular-expressions.info/lookaround.html)
[wikipedia](https://zh.wikipedia.org/wiki/%E6%AD%A3%E5%88%99%E8%A1%A8%E8%BE%BE%E5%BC%8F)
