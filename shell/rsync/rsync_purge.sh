#!/bin/bash

set -e
FILES_DIRS="/backup/rsync/mysql_backup"
# mesure unit G, 300G
LIMITS=$((300*1024*1024))

function sizeof {
    echo `du -s $1 | grep -oP "\d*[\s\t]+"`
}

function older {
    echo `stat --format="%y %n" $1/* | sort | head -n 1| awk '{print $4}'`
}

if [ ! -d $FILES_DIRS ]; then
    echo "$FILES_DIRS not exist"
    exit 1
fi

while true; do
    size=$(sizeof $FILES_DIRS)
    echo `date "+%x %X"` "$FILES_DIRS size $size, limit $LIMITS"
    if (( $size >= $LIMITS )); then
        old=$(older $FILES_DIRS)
        echo "ready to rm $old"
        rm -rf $old
    else
        break;
    fi
done

