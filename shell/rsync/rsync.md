# rsync

- [安装rsync](##安装rsync)
- [配置rsync](##配置rsync)
- [同步测试](##同步测试)
- [wiki](##wiki)
- [QA](##QA)

***Author: ArrowLee***

***Date: 2019/1/11***

## 安装rsync

```bash
yum install -y rsync
```

## 配置rsync

- 参考官方网站：[rsyncd.conf](http://man7.org/linux/man-pages/man5/rsyncd.conf.5.html)
- 参考官方网站：[man rsync(1)](http://man7.org/linux/man-pages/man1/rsync.1.html)

1. 创建MySQL用户和组

    ```bash
    groupadd -f rsync
    useradd -r -g rsync rsync

    #若配置文件rsync.conf中某模块有配置uid, gid，需要添加该用户和组
    groupadd -f mysql
    useradd -r -g mysql mysql
    ```

2. 配置rsync

    向/etc/rsyncd.conf添加配置
    ```ini
    uid=rsync
    gid=rsync
    port = 873
    timeout = 600
    use chroot = yes
    log file = /var/log/rsync.log
    pid file = /var/run/rsync.pid

    [MySQLbackup]
    #传输进来的文件以mysql的用户和组保存
    comment = MySQL backup
    uid=mysql
    gid=mysql
    max connections = 1000
    refuse options = delete
    write only = true
    read only = true
    strict modes = true
    path = /opt/data/rsync/mysql_backup
    lock file = /var/run/rsync_mysql_backup.lock
    ```

    创建配置文件中path的目录，更改其权限为同一模块配置的uid和gid
    ```bash
    mkdir -p /opt/data/rsync/mysql_backup
    chown -R mysql:mysql /opt/data/rsync/mysql_backup
    ```

3. secrets file配置

    本步配置可以省略，

    向/etc/rsyncd.conf的某个模块添加配置
    ```ini
    auth users = dc:w
    secrets file = /etc/rsyncd.passwd
    ```

    向/etc/rsyncd.passwd添加配置
    ```ini
    dc:xcentral123456
    ```

    更改secrets file权限
    ```bash
    chmod 600 /etc/rsyncd.passwd
    ```

    > - The passwords can contain any characters but be warned that many operating systems limit the length of passwords that can be typed at the client end, so you may find that passwords longer than 8 characters don't work.
    > - There is no default for the "secrets file" parameter, you must choose a name (such as /etc/rsyncd.secrets). The file must normally not be readable by "other"; see "strict modes". If the file is not found or is rejected, no logins for a "user auth" module will be possible.

4. 启动

    ```bash
    rsync --daemon
    ```

5. 其他配置

    - 配置防火墙
        ```bash
        #配置防火墙
        iptables -I INPUT -p tcp --dport 873 -j ACCEPT -m state --state NEW,ESTABLISHED
        ```

    - init.d配置

        ```bash
        ```

## 同步测试

1. 客户端推送到远程目录

    - 直接同步到远程
        ```bash
        rsync -rlzv /opt/data/backup rsync://10.118.195.109:873/MySQLbackup
        ```

    - 以rsyncd.conf-level级别用户同步到远程，需要"secrets file配置"，请参考[配置rsync](##配置rsync)第3步
        ```bash
        #以dc用户rsyncd.conf-level级别同步到远程
        rsync -rlzv /opt/data/backup rsync://dc@10.118.195.109:873/MySQLbackup

        #以dc用户rsyncd.conf-level级别同步到远程，通过密码文件免密码输入方式，--password-file配置文件第一行为密码，其他行将被忽略。
        cat << -END- > /etc/remote_host_rsync_pwd
        xcentral123456
        -END-
        rsync -rlzv --password-file /etc/remote_host_rsync_pwd /opt/data/backup rsync://dc@121.46.26.149:873/MySQLbackup
        ```

    - 以remote-shell-level级别用户ssh加密同步到远程，需要"secrets file配置"，请参考[配置rsync](##配置rsync)第3步
        ```bash
        #以remote-shell-level级别和rsyncd.conf-level级别同步到远程
        rsync -rlzv -e "ssh -l root" /opt/data/backup dc@121.46.26.149::MySQLbackup
        ```

2. 客户端下载远程目录

    - 以remote-shell-level级别用户ssh加密从远程同步到本地

        ```bash
        rsync -rlzv -e "ssh -l root" --password-file /etc/remote_host_rsync_pwd dc@121.46.26.149::MySQLbackup /opt/data/backup
        ```

## wiki

- USING RSYNC-DAEMON FEATURES VIA A REMOTE-SHELL CONNECTION

    It is sometimes useful to use various features of an rsync daemon (such as named modules) without actually allowing any new socket connections into a system (other than what is already required to allow remote-shell access).  Rsync supports connecting to a host using a remote shell and then spawning a single-use "daemon" server that expects to read its config file in the home dir of the remote user.  This can be useful if you want to encrypt a daemon-style transfer’s data, but since the daemon is started up fresh by the remote user, you may not be able to use features such as chroot or change the uid used by the daemon.  (For another way to encrypt a daemon transfer, consider using ssh to tunnel a local port to a remote machine and configure a normal rsync daemon on that remote host to only allow connections from "localhost".)

    From the user’s perspective, a daemon transfer via a remote-shell connection uses nearly the same command-line syntax as a normal rsync-daemon transfer, with the only exception being that you must explicitly set the remote shell program on the command-line with the --rsh=COMMAND option.  (Setting the RSYNC_RSH in the environment will not turn on this functionality.)  For example:

        rsync -av --rsh=ssh host::module /dest

    If you need to specify a different remote-shell user, keep in mind that the user@ prefix in front of the host is specifying the rsync-user value (for a module that requires user-based authentication).  This means that you must give the ’-l user’ option to ssh when specifying the remote-shell, as in this example that uses the short version of the --rsh option:

        rsync -av -e "ssh -l ssh-user" rsync-user@host::module /dest

    The "ssh-user" will be used at the ssh level; the "rsync-user" will be used to log-in to the "module".

## QA

1. 出现问题"invalid uid"

    可能的原因：
    - rsync的守护进程的配置文件uid, gid配置项没有对应的用户和组

2. 出现问题"ERROR: module is read only"

    可能的原因：
    - rsync的守护进程的配置文件read only=true应该设置为false

3. 出现问题"rsync: recv_generator: mkdir failed: Permission denied (13)"，"rsync: failed to set times: Operation not permitted"

    可能的原因：
    - rsync的守护进程的配置文件路径权限不属于配置的uid, gid

4. 出现问题"rsync: chgrp failed: Operation not permitted"

    可能的原因：
    - 远程目录权限比传送的文件权限低，请更改-o -g -a同步选项

5. 出现问题"auth failed on module MySQLbackup from UNKNOWN (113.87.162.246) for dc: ignoring secrets file"

    对应的secrets file文件权限没有满足"strict modes"的要求。参考[配置rsync](##配置rsync)的第3步。
