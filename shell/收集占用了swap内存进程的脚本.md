测试内存超配，swap被使用后也不知道是哪些进程占用的，咨询开发也没有很好的方法去统一查看，写了个小脚本可以统计所有占用了swap的进程。



#!/bin/bash



logfile="/sf/data/local/swap_stat.log"

[ -f $logfile ] && rm -f "$logfile"

for pid in `ls -l /proc | grep ^d | awk '{ print $9 }'| grep [0-9]`

do

	[ -f /proc/$pid/smaps ] || continue

	#swap=`grep Swap /proc/$pid/smaps | awk '{ sum+=$2;} END{ print sum }'`

	swap=`grep VmSwap /proc/$pid/status | awk '{print $2}'`

	if [[ $swap -eq 0 ]] || [[ -z $swap ]]; then

		continue

	fi

	swap=$(printf "%.2f" `echo "scale=2;$swap/1024"|bc`)

	pname=`ps aux | grep -w "$pid" | grep -v "grep" | awk '{print $11}' | head -1`

	printf "%-10s %-50s %-10s MB\n" ${pid} ${pname} ${swap} &>> $logfile

done

sort -n -k 3 $logfile -o $logfile

cat $logfile