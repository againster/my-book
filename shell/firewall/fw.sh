#!/bin/bash
# init the iptables : nat, filter
# yjh(2013)

. /lib/lsb/init-functions

BASH_CONF_PATH="/sf/cfg"
BASH_CONF_PATH_LENGTH=$(echo ${#BASH_CONF_PATH})

stripcmd ()
{
    echo "${1}" | tr ';&|<>$*$('  '________'
}

get_bbc_conf_path()
{
    local user_conf=$1
    if [ ! -f $user_conf ]
    then
        local dest_dir=$(dirname $user_conf)
        mkdir -p $dest_dir
        local dest_conf=$(echo ${user_conf:$BASH_CONF_PATH_LENGTH})
        cp /sf/etc/default_cfg${dest_conf} $user_conf
    fi
}

natset()
{
    ini="/sf/cfg/fw.d/snat.ini"
    for idx in $( cfgfile -e "$ini" ) ; do
        [ x"$idx" = x"config" ] && continue
        p=$( cfgfile -r "$ini" -s $idx -k p -v all )
        s=$( cfgfile -r "$ini" -s $idx -k s -v "0.0.0.0/0" )
        d=$( cfgfile -r "$ini" -s $idx -k d -v "0.0.0.0/0" )
        dport=$( cfgfile -r "$ini" -s $idx -k dport -v "1:65535" )
        tosource=$( cfgfile -r "$ini" -s $idx -k "to-source" -v "if" )
        if [ x"${tosource}" = x"if" ]; then
            tosource="-j MASQUERADE"
        else
            tosource="-j SNAT --to-source ${tosource}"
        fi
        o=$( cfgfile -r "$ini" -s $idx -k o -v "" )
        if [ x"${o}" != x"" ]; then
            o="-o ${o}"
        fi
        iptcmd=$(stripcmd "iptables -A POSTROUTING -t nat -p ${p} -s ${s} -d ${d} ${o} ${tosource}")
        echo "${idx}: ${iptcmd}" >> $logf
        ${iptcmd} 1>>$logf 2>&1
    done

    echo "snatset done at: $(date)" >> $logf
    echo "" >> $logf
}

inputset()
{
    ini="/sf/cfg/fw.d/input.ini"
    get_bbc_conf_path $ini


    #缺省策略
    police=$( cfgfile -r "$ini" -s config -k police -v ACCEPT )
    defaultruletarget="ACCEPT"
    [ x"$police" = x"ACCEPT" ] && defaultruletarget="DROP"
    [ x"$police" = x"DROP" ] && defaultruletarget="ACCEPT"

    echo "iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT" >> $logf
    iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
    echo "iptables -A INPUT -i lo -j ACCEPT" >> $logf
    iptables -A INPUT -i lo -j ACCEPT

    for idx in $( cfgfile -e "$ini" ) ; do
        [ x"$idx" = x"config" ] && continue

        #默认iptables参数
        IN=""
        PP=""
        SS=""
        DD=""
        DPORT=""
        SPORT=""

        #配置参数
		in=$( cfgfile -r "$ini" -s $idx -k in -v all )
        p=$( cfgfile -r "$ini" -s $idx -k p -v all )
        s=$( cfgfile -r "$ini" -s $idx -k s -v "0.0.0.0/0" )
        d=$( cfgfile -r "$ini" -s $idx -k d -v "0.0.0.0/0" )
        dport=$( cfgfile -r "$ini" -s $idx -k dport -v "" )
        sport=$( cfgfile -r "$ini" -s $idx -k sport -v "" )
        target=$( cfgfile -r "$ini" -s $idx -k target -v "$defaultruletarget" )
        #自动创建iptables chain
        echo ${target} | egrep -w "DROP|ACCEPT|REJECT" >/dev/null || iptables -N ${target}

        #参数检查处理
        [ x"$in" != x"all" ] && IN="-i $in"
        [ x"$p" != x"all" ] && PP="-p $p"
        [ x"$s" != x"0.0.0.0/0" ] && SS="-s $s"
        [ x"$d" != x"0.0.0.0/0" ] && DD="-d $d"
		[ x"$sport" != x"" ] && SPORT="--sport $sport"
		[ x"$dport" != x"" ] && DPORT="--dport $dport"
		if [ x"$p" = x"all" ]; then
			#分开设置tcp/udp
			PP="-p tcp"
			iptcmd=$(stripcmd "iptables -A INPUT $IN ${PP} ${SS} ${DD} ${DPORT} ${SPORT} -j ${target}")
			echo "${idx}: ${iptcmd}" >> $logf
			${iptcmd} 1>>$logf 2>&1
			PP="-p udp"
		fi

        iptcmd=$(stripcmd "iptables -A INPUT $IN ${PP} ${SS} ${DD} ${DPORT} ${SPORT} -j ${target}")
        echo "${idx}: ${iptcmd}" >> $logf
        ${iptcmd} 1>>$logf 2>&1
    done

    echo "iptables --policy INPUT $police" >> $logf
    iptables --policy INPUT $police 1>>$logf 2>&1

    echo "inputset done at: $(date)" >> $logf
    echo "" >> $logf
}

fwstop()
{
    [ -z "${1}" -o x"${1}" = x"input" ] && iptables --policy INPUT ACCEPT && iptables -F INPUT -t filter
#    [ -z "${1}" -o x"${1}" = x"forward" ] && iptables --policy FORWARD ACCEPT && iptables -F FORWARD -t filter
#    [ -z "${1}" -o x"${1}" = x"output" ] && iptables --policy OUTPUT ACCEPT && iptables -F OUTPUT -t filter
#    [ -z "${1}" -o x"${1}" = x"nat" ] && iptables -F -t nat
}

fwstart()
{
    [ -z "${1}" -o x"${1}" = x"input" ] && inputset 2>/dev/null && log_success_msg "start Input OK"
#    [ -z "${1}" -o x"${1}" = x"forward" ] && forwardset 2>/dev/null && log_success_msg "start Forward OK"
#    [ -z "${1}" -o x"${1}" = x"output" ] && outputset 2>/dev/null && log_success_msg "start Output OK"
#    [ -z "${1}" -o x"${1}" = x"nat" ] && natset 2>/dev/null && log_success_msg "start NAT OK"
}

FILE_LOCK="/var/lock/fw.lock"
# File lock for exclusively running this service.
{
    # Try to get lock before going next and no timeout for this action
    flock -x 200

    cd /tmp
    logf="/sf/log/today/fw.log"
    date >> $logf

    case "${1}" in
    stop)
        fwstop "${2}"
        ;;
    start)
        fwstart "${2}"
        ;;
    restart)
        fwstop "${2}"
        fwstart "${2}"
        ;;
    status)
        [ -z "${2}" ] || VV="-v"
        echo ">>>>>>>>>>>>>>>>>>>>>filter rules:"
        iptables -L -n -v ${VV} -t filter
        echo ""
        echo ">>>>>>>>>>>>>>>>>>>>>nat rules:"
        iptables -L -n -v ${VV} -t nat
        ;;
    *)
        echo "Usage: ${0} start|stop|restart|status"
        ;;
    esac
} 200<>$FILE_LOCK

exit 0
