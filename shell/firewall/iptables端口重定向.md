# iptables端口重定向

最近做ssl的证书自适应，需要测试大量的bypass规则，那我们真的要搭建那么多服务器么？no no no,iptables规则就好了啊，假定我们再443上有一个ssl服务，那么我们可以把其他端口的数据都重定向的443不就好了。

示例代码如下，你想要多少服务器都可以

```bash
#!/bin/bash -x

#功能：利用iptables规则，把多个端口重定向到一个端口，这样就可以测试多条bypass规则的情况#
dst_port=443
redicert_begin_port=6020
redicert_begin_end=7110

add_iptables_rules()
{
    port_tmp=$redicert_begin_port
    while [ $port_tmp -lt $redicert_begin_end ]; do
        iptables -t nat -A PREROUTING -p tcp --dport $port_tmp -j REDIRECT --to-ports $dst_port
        port_tmp=$((port_tmp+1))     
    done
}
add_iptables_rules
```