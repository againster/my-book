# bash - exec

## Description

**exec** is a critical function of any [Unix](https://www.computerhope.com/jargon/u/unix.htm)-like [operating system](https://www.computerhope.com/os.htm). Traditionally, the only way to create a new process in Unix is to [**fork**](https://www.computerhope.com/jargon/f/fork.htm) it. The **fork** [system call](https://www.computerhope.com/jargon/s/system-call.htm) makes a copy of the forking program. The copy then uses **exec** to *execute* the [child process](https://www.computerhope.com/jargon/p/parechil.htm) in its memory space.

## Syntax

```bash
exec [-c] [-l] [-a name] [command [arguments ...]] [redirection ...]
```

### Options and arguments:

| **-a** *name* | Pass the string *name* as the zeroth argument to *command*. This option is available in bash versions 4.2 and above. When used, it will [execute](https://www.computerhope.com/jargon/e/execute.htm) *command*, and set the special shell [variable](https://www.computerhope.com/jargon/v/variable.htm) **$0** to the value *name*, instead of *command*. For more information, see [bash special parameter 0](https://www.computerhope.com/unix/ubash.htm#special-parameters-0). |
| ------------- | ------------------------------------------------------------ |
| **-c**        | Execute *command* in an empty [environment](https://www.computerhope.com/jargon/e/environm.htm). |
| **-l**        | Insert a dash at the beginning of the zeroth argument. This can be used to start a login shell via **exec**. For more information about login shells, and bash's requirements about how they may be invoked, see [shell invocation in bash](https://www.computerhope.com/unix/ubash.htm#invocation). |
| *command*     | The command to be executed. If you do not specify a command, **exec** can still provide [redirection](https://www.computerhope.com/jargon/r/redirect.htm). |
| *arguments*   | The arguments for the command you'd like to execute.         |
| *redirection* | Any redirection for the command. If no *command* is specified, redirection applies to the current shell. |

### Description

**exec** is useful when you want to run a command, but you don't want a bash shell to be the [parent process](https://www.computerhope.com/jargon/p/parechil.htm). When you **exec** a command, it replaces bash entirely — no new process is forked, no new [PID](https://www.computerhope.com/jargon/p/pid.htm) is created, and all [memory](https://www.computerhope.com/jargon/m/memory.htm) controlled by bash is destroyed and [overwritten](https://www.computerhope.com/jargon/o/overwrit.htm). This can be useful if, for instance, you want to give a user restricted access to a certain command. If the command exits because of an [error](https://www.computerhope.com/jargon/e/error.htm), the user will not be returned to the [privileged](https://www.computerhope.com/jargon/p/permissi.htm) shell that executed it.

**exec** may also be used without any command, to redirect all output of the current shell to a file. For more information about redirection, see [redirection in the bash shell](https://www.computerhope.com/unix/ubash.htm#redirection).

## Example

1. ```bash
   exec > output.txt
   ```

   Redirect all output to the file **output.txt** for the current shell process. Redirections are a special case, and **exec** does not destroy the current shell process, but **bash** will no longer print output to the screen, writing it to the file instead. (This technique is much more useful in scripts — if the above command is executed in a script, all script output will be written to **output.txt**.) 

2. ```bash
   exec 3< out.txt 
   ```

   Open **out.txt** for reading ("**<**") on file descriptor **3**.  The above command is an example of explicitly opening a [file descriptor](https://www.computerhope.com/jargon/f/file-descriptor.htm).  After running the above command, you can read a line of **out.txt** by running the [**read**](https://www.computerhope.com/unix/bash/read.htm) command with the **-u** option: 

   ```bash
   read -u 3 outdata
   ```

   Here, "**-u 3**" tells **read** to get its data from file descriptor 3, which refers to **out.txt**. The contents are read, one line at a time, into the variable **outdata**. For example:

   ```bash
   echo "hello world" >> out.txt
   echo "hello china" >> out.txt
   exec 3< out.txt
   while read -u 3 outdata
   do
     echo "$outdata"
   done
   ```

3. ```bash
   exec 4> my.txt
   ```

   The above command opens **my.txt** for writing ("**>**") on file descriptor **4**. 

   ```bash
   exec 4> my.txt
   echo "hello world" >&4
   echo "hello china" >&4
   cat my.txt
   ```

4. ```bash
   exec 5>> append.txt
   ```

   Open **append.txt** for [appending](https://www.computerhope.com/jargon/a/append.htm) ("**>>**") as file descriptor **5**. 

5. ```bash
   exec 6<> myfile.txt
   ```

   Open **myfile.txt** for reading and writing ("**<>**") as file descriptor **6**. 

6. ```bash
   exec {myfd}> hello.txt
   ```

   Open **hello.txt** for writing. A new file descriptor number, chosen automatically, is assigned to the variable **myfd**. 

   ```bash
   echo "Text" >&$myfd
   ```

   Echo the text "**Text**" and redirect the output to the file (in this case, **hello.txt**) described by the write descriptor ("**>**") whose number is obtained by [dereferencing](https://www.computerhope.com/jargon/d/dereference-operator.htm) ("**&**") the variable named **myfd**. 

   ```bash
   exec {myfd}>> hello.txt
   echo "oops" >&$myfd
   echo "oops" >&$myfd
   cat hello.txt
   ```

7. ```bash
   exec 3<&-
   ```

   Close ("**&-**") the open read descriptor ("**<**") number **3**. 

8. ```bash
   exec 4>&-
   ```

   Close the open write descriptor ("**>**") number **4**. 

9. ```bash
   exec 5<>&-
   ```

   Close open read/write descriptor 5. 

10. ```bash
    exec {myfd}>&-
    ```

    Close open append descriptor **myfd**. 

