# tcptump命令

## 简单介绍

捕捉网络事件。tcpdump - dump traffic on a network。

Tcpdump打印出在对应网络接口上的匹配表达式的包内容的描述；描述内容前面有时间戳，默认是打印的，像时分秒和自从午夜的秒片段。其也可以带“-w”选项运行，这个选项会保存包数据到文件中，以便随后分析，或者带“-r”选项，此选项将会读取保存的包文件而不是读取包从网络接口。也可以带“-V”运行，此选项将会读取保存的包文件列表。在所有情况中，仅仅匹配表达式的包才会被tcpdump命令处理。Tcpdump prints out a description of the contents of packets on a network interface that match the boolean expression; the description is preceded by a time stamp, printed, by default, as hours, minutes, seconds, and fractions of a second since midnight. It can also be run with the -w flag, which causes it to save the packet data to a file for later analysis, and/or with the -r flag, which causes it to read from a saved packet file rather than to read packets from a network interface. It can also be run with the -V flag, which causes it to read a list of saved packet files. In all cases, only packets that match expression will be processed by tcpdump.

若没有带“-c”选项运行，tcpdump将会持续捕获包，直到它被SIGINT信号中断（产生方法，例如，输入中断字符，通常是CTL-C）或者是SIGTERM信号（通常通过kill(1)命令产生）；如果运行带有“-c”，将会捕获包直到被SIGINT、SIGTERM信号、定义的被处理的包的数量。Tcpdump will, if not run with the -c flag, continue capturing packets until it is interrupted by a SIGINT signal (generated, for example, by typing your interrupt character, typically control-C) or a SIGTERM signal (typically generated with the kill(1) command); if run with the -c flag, it will capture packets until it is interrupted by a SIGINT or SIGTERM signal or the specified number of packets have been processed.

当tcpdump完成捕获包，它将会报告计数：When tcpdump finishes capturing packets, it will report counts of:

- 被捕获的包（tcpdump接收和处理的包数量）。packets ``captured'' (this is the number of packets that tcpdump has received and processed);

- 通过过滤表达式接收的包（依赖系统的意思是你运行tcpdump在什么系统上。在操作系统配置的过程中，如果过滤器被定义在命令行，在一些系统中，它将计入这个包，不管包是否与过滤表示式匹配。即使包通过过滤表达式被匹配，不管tcpdump是否读取或者已经处理了他们，在其他系统中tcpdump仅仅计入通过过滤表达式匹配的包和被处理的包）；。packets ``received by filter'' (the meaning of this depends on the OS on which you're running tcpdump, and possibly on the way the OS was configured - if a filter was specified on the command line, on some OSes it counts packets regardless of whether they were matched by the filter expression and, even if they were matched by the filter expression, regardless of whether tcpdump has read and processed them yet, on other OSes it counts only packets that were matched by the filter expression regardless of whether tcpdump has read and processed them yet, and on other OSes it counts only packets that were matched by the filter expression and were processed by tcpdump);

- 通过内核丢弃的包（由于缺少缓冲区空间，那么在运行tcpdump的操作系统中，如果操作系统将这些信息报告给应用程序，tcpdump将计入包捕获机制丢弃的包数量；如果没有，则报告为0）。packets ``dropped by kernel'' (this is the number of packets that were dropped, due to a lack of buffer space, by the packet capture mechanism in the OS on which tcpdump is running, if the OS reports that information to applications; if not, it will be reported as 0).

在支持SIGINFO信号的平台上，像类BSDs（包括macOS）和Digital/Tru64 UNIX，当接受到SIGINFO信号（例如，通过键入状态字符，通常是CTL-T，即使在一些平台，像macOS，状态字符默认没有被设置，所以你必须设置它，通过stty(1)来使用它），tcpdump将报告这些计数，而且将会持续捕获包。在不支持SIGINFO信号的平台，通过SIGUSR1信号，同样可以达到相同的目的。On platforms that support the SIGINFO signal, such as most BSDs (including macOS) and Digital/Tru64 UNIX, it will report those counts when it receives a SIGINFO signal (generated, for example, by typing your ``status'' character, typically control-T, although on some platforms, such as macOS, the ``status'' character is not set by default, so you must set it with stty(1) in order to use it) and will continue capturing packets. On platforms that do not support the SIGINFO signal, the same can be achieved by using the SIGUSR1 signal.

使用SIGUSR2信号同“-w”选项，将会强制刷新包缓冲区到输出文件。Using the SIGUSR2 signal along with the -w flag will forcibly flush the packet buffer into the output file.

## 参数解析

[参考Manpage of TCPDUMP](https://www.tcpdump.org/manpages/tcpdump.1.html)

```shell
tcpdump [ -AbdDefhHIJKlLnNOpqStuUvxX# ] [ -B buffer_size ] 

         [ -c count ] [ -C file_size ]
         [ -E spi@ipaddr algo:secret,... ]
         [ -F file ] [ -G rotate_seconds ] [ -i interface ]
         [ --immediate-mode ] [ -j tstamp_type ] [ -m module ]
         [ -M secret ] [ --number ] [ --print ] [ -Q in|out|inout ]
         [ -r file ] [ -s snaplen ] [ -T type ] [ --version ]
         [ -V file ] [ -w file ] [ -W filecount ] [ -y datalinktype ]
         [ -z postrotate-command ] [ -Z user ]
         [ --time-stamp-precision=tstamp_precision ]
         [ expression ]

-A
    Print each packet (minus its link level header) in ASCII. Handy for capturing web pages.
-b
    Print the AS number in BGP packets in ASDOT notation rather than ASPLAIN notation.
-B buffer_size
--buffer-size=buffer_size
    Set the operating system capture buffer size to buffer_size, in units of KiB (1024 bytes).
-c count
    Exit after receiving count packets.
-C file_size
    Before writing a raw packet to a savefile, check whether the file is currently larger than file_size and, if so, close the current savefile and open a new one. Savefiles after the first savefile will have the name specified with the -w flag, with a number after it, starting at 1 and continuing upward. The units of file_size are millions of bytes (1,000,000 bytes, not 1,048,576 bytes).
-d
    Dump the compiled packet-matching code in a human readable form to standard output and stop.
-dd
    Dump packet-matching code as a C program fragment.
-ddd
    Dump packet-matching code as decimal numbers (preceded with a count).
-D
--list-interfaces
    Print the list of the network interfaces available on the system and on which tcpdump can capture packets. For each network interface, a number and an interface name, possibly followed by a text description of the interface, is printed. The interface name or the number can be supplied to the -i flag to specify an interface on which to capture.
    This can be useful on systems that don't have a command to list them (e.g., Windows systems, or UNIX systems lacking ifconfig -a); the number can be useful on Windows 2000 and later systems, where the interface name is a somewhat complex string.
    The -D flag will not be supported if tcpdump was built with an older version of libpcap that lacks the pcap_findalldevs(3PCAP) function.
-e
    Print the link-level header on each dump line. This can be used, for example, to print MAC layer addresses for protocols such as Ethernet and IEEE 802.11.
-E
    Use spi@ipaddr algo:secret for decrypting IPsec ESP packets that are addressed to addr and contain Security Parameter Index value spi. This combination may be repeated with comma or newline separation.
    Note that setting the secret for IPv4 ESP packets is supported at this time.
    Algorithms may be des-cbc, 3des-cbc, blowfish-cbc, rc3-cbc, cast128-cbc, or none. The default is des-cbc. The ability to decrypt packets is only present if tcpdump was compiled with cryptography enabled.
    secret is the ASCII text for ESP secret key. If preceded by 0x, then a hex value will be read.
    The option assumes RFC2406 ESP, not RFC1827 ESP. The option is only for debugging purposes, and the use of this option with a true `secret' key is discouraged. By presenting IPsec secret key onto command line you make it visible to others, via ps(1) and other occasions.
    In addition to the above syntax, the syntax file name may be used to have tcpdump read the provided file in. The file is opened upon receiving the first ESP packet, so any special permissions that tcpdump may have been given should already have been given up.
-f
    Print `foreign' IPv4 addresses numerically rather than symbolically (this option is intended to get around serious brain damage in Sun's NIS server --- usually it hangs forever translating non-local internet numbers).
    The test for `foreign' IPv4 addresses is done using the IPv4 address and netmask of the interface on which capture is being done. If that address or netmask are not available, available, either because the interface on which capture is being done has no address or netmask or because the capture is being done on the Linux "any" interface, which can capture on more than one interface, this option will not work correctly.
-F file
    Use file as input for the filter expression. An additional expression given on the command line is ignored.
-G rotate_seconds
    If specified, rotates the dump file specified with the -w option every rotate_seconds seconds. Savefiles will have the name specified by -w which should include a time format as defined by strftime(3). If no time format is specified, each new file will overwrite the previous.
    If used in conjunction with the -C option, filenames will take the form of `file<count>'.
-h
--help
    Print the tcpdump and libpcap version strings, print a usage message, and exit.
--version
    Print the tcpdump and libpcap version strings and exit.
-H
    Attempt to detect 802.11s draft mesh headers.
-i interface
--interface=interface
    Listen on interface. If unspecified, tcpdump searches the system interface list for the lowest numbered, configured up interface (excluding loopback), which may turn out to be, for example, ``eth0''.
    On Linux systems with 2.2 or later kernels, an interface argument of ``any'' can be used to capture packets from all interfaces. Note that captures on the ``any'' device will not be done in promiscuous mode.
    If the -D flag is supported, an interface number as printed by that flag can be used as the interface argument, if no interface on the system has that number as a name.
-I
--monitor-mode
    Put the interface in "monitor mode"; this is supported only on IEEE 802.11 Wi-Fi interfaces, and supported only on some operating systems.
    Note that in monitor mode the adapter might disassociate from the network with which it's associated, so that you will not be able to use any wireless networks with that adapter. This could prevent accessing files on a network server, or resolving host names or network addresses, if you are capturing in monitor mode and are not connected to another network with another adapter.
    This flag will affect the output of the -L flag. If -I isn't specified, only those link-layer types available when not in monitor mode will be shown; if -I is specified, only those link-layer types available when in monitor mode will be shown.
--immediate-mode
    Capture in "immediate mode". In this mode, packets are delivered to tcpdump as soon as they arrive, rather than being buffered for efficiency. This is the default when printing packets rather than saving packets to a ``savefile'' if the packets are being printed to a terminal rather than to a file or pipe.
-j tstamp_type
--time-stamp-type=tstamp_type
    Set the time stamp type for the capture to tstamp_type. The names to use for the time stamp types are given in pcap-tstamp(7); not all the types listed there will necessarily be valid for any given interface.
-J
--list-time-stamp-types
    List the supported time stamp types for the interface and exit. If the time stamp type cannot be set for the interface, no time stamp types are listed.
--time-stamp-precision=tstamp_precision
    When capturing, set the time stamp precision for the capture to tstamp_precision. Note that availability of high precision time stamps (nanoseconds) and their actual accuracy is platform and hardware dependent. Also note that when writing captures made with nanosecond accuracy to a savefile, the time stamps are written with nanosecond resolution, and the file is written with a different magic number, to indicate that the time stamps are in seconds and nanoseconds; not all programs that read pcap savefiles will be able to read those captures.
    When reading a savefile, convert time stamps to the precision specified by timestamp_precision, and display them with that resolution. If the precision specified is less than the precision of time stamps in the file, the conversion will lose precision.
    The supported values for timestamp_precision are micro for microsecond resolution and nano for nanosecond resolution. The default is microsecond resolution.
-K
--dont-verify-checksums
    Don't attempt to verify IP, TCP, or UDP checksums. This is useful for interfaces that perform some or all of those checksum calculation in hardware; otherwise, all outgoing TCP checksums will be flagged as bad.
-l
    Make stdout line buffered. Useful if you want to see the data while capturing it. E.g.,
    tcpdump -l | tee dat
    or
    tcpdump -l > dat & tail -f dat
    Note that on Windows,``line buffered'' means ``unbuffered'', so that WinDump will write each character individually if -l is specified.
-U
    is similar to -l in its behavior, but it will cause output to be ``packet-buffered'', so that the output is written to stdout at the end of each packet rather than at the end of each line; this is buffered on all platforms, including Windows.
-L
--list-data-link-types
    List the known data link types for the interface, in the specified mode, and exit. The list of known data link types may be dependent on the specified mode; for example, on some platforms, a Wi-Fi interface might support one set of data link types when not in monitor mode (for example, it might support only fake Ethernet headers, or might support 802.11 headers but not support 802.11 headers with radio information) and another set of data link types when in monitor mode (for example, it might support 802.11 headers, or 802.11 headers with radio information, only in monitor mode).
-m module
    Load SMI MIB module definitions from file module. This option can be used several times to load several MIB modules into tcpdump.
-M secret
    Use secret as a shared secret for validating the digests found in TCP segments with the TCP-MD5 option (RFC 2385), if present.
-n
    Don't convert addresses (i.e., host addresses, port numbers, etc.) to names.
-N
    Don't print domain name qualification of host names. E.g., if you give this flag then tcpdump will print ``nic'' instead of ``nic.ddn.mil''.
-#
--number
    Print an optional packet number at the beginning of the line.
-O
--no-optimize
    Do not run the packet-matching code optimizer. This is useful only if you suspect a bug in the optimizer.
-p
--no-promiscuous-mode
    Don't put the interface into promiscuous mode. Note that the interface might be in promiscuous mode for some other reason; hence, `-p' cannot be used as an abbreviation for `ether host {local-hw-addr} or ether broadcast'.
--print
    Print parsed packet output, even if the raw packets are being saved to a file with the -w flag.
-Q direction
--direction=direction
    Choose send/receive direction direction for which packets should be captured. Possible values are `in', `out' and `inout'. Not available on all platforms.
-q
    Quick (quiet?) output. Print less protocol information so output lines are shorter.
-r file
    Read packets from file (which was created with the -w option or by other tools that write pcap or pcapng files). Standard input is used if file is ``-''.
-S
--absolute-tcp-sequence-numbers
    Print absolute, rather than relative, TCP sequence numbers.
-s snaplen
--snapshot-length=snaplen
    Snarf snaplen bytes of data from each packet rather than the default of 262144 bytes. Packets truncated because of a limited snapshot are indicated in the output with ``[|proto]'', where proto is the name of the protocol level at which the truncation has occurred.
    Note that taking larger snapshots both increases the amount of time it takes to process packets and, effectively, decreases the amount of packet buffering. This may cause packets to be lost. Note also that taking smaller snapshots will discard data from protocols above the transport layer, which loses information that may be important. NFS and AFS requests and replies, for example, are very large, and much of the detail won't be available if a too-short snapshot length is selected.
    If you need to reduce the snapshot size below the default, you should limit snaplen to the smallest number that will capture the protocol information you're interested in. Setting snaplen to 0 sets it to the default of 262144, for backwards compatibility with recent older versions of tcpdump.
-T type
    Force packets selected by "expression" to be interpreted the specified type. Currently known types are aodv (Ad-hoc On-demand Distance Vector protocol), carp (Common Address Redundancy Protocol), cnfp (Cisco NetFlow protocol), lmp (Link Management Protocol), pgm (Pragmatic General Multicast), pgm_zmtp1 (ZMTP/1.0 inside PGM/EPGM), resp (REdis Serialization Protocol), radius (RADIUS), rpc (Remote Procedure Call), rtp (Real-Time Applications protocol), rtcp (Real-Time Applications control protocol), snmp (Simple Network Management Protocol), tftp (Trivial File Transfer Protocol), vat (Visual Audio Tool), wb (distributed White Board), zmtp1 (ZeroMQ Message Transport Protocol 1.0) and vxlan (Virtual eXtensible Local Area Network).
    Note that the pgm type above affects UDP interpretation only, the native PGM is always recognised as IP protocol 113 regardless. UDP-encapsulated PGM is often called "EPGM" or "PGM/UDP".
    Note that the pgm_zmtp1 type above affects interpretation of both native PGM and UDP at once. During the native PGM decoding the application data of an ODATA/RDATA packet would be decoded as a ZeroMQ datagram with ZMTP/1.0 frames. During the UDP decoding in addition to that any UDP packet would be treated as an encapsulated PGM packet.
-t
    Don't print a timestamp on each dump line.
-tt
    Print the timestamp, as seconds since January 1, 1970, 00:00:00, UTC, and fractions of a second since that time, on each dump line.
-ttt
    Print a delta (microsecond or nanosecond resolution depending on the --time-stamp-precision option) between current and previous line on each dump line. The default is microsecond resolution.
-tttt
    Print a timestamp, as hours, minutes, seconds, and fractions of a second since midnight, preceded by the date, on each dump line.
-ttttt
    Print a delta (microsecond or nanosecond resolution depending on the --time-stamp-precision option) between current and first line on each dump line. The default is microsecond resolution.
-u
    Print undecoded NFS handles.
-U
--packet-buffered
    If the -w option is not specified, or if it is specified but the --print flag is also specified, make the printed packet output ``packet-buffered''; i.e., as the description of the contents of each packet is printed, it will be written to the standard output, rather than, when not writing to a terminal, being written only when the output buffer fills.
    If the -w option is specified, make the saved raw packet output ``packet-buffered''; i.e., as each packet is saved, it will be written to the output file, rather than being written only when the output buffer fills.
    The -U flag will not be supported if tcpdump was built with an older version of libpcap that lacks the pcap_dump_flush(3PCAP) function.
-v
    When parsing and printing, produce (slightly more) verbose output. For example, the time to live, identification, total length and options in an IP packet are printed. Also enables additional packet integrity checks such as verifying the IP and ICMP header checksum.
    When writing to a file with the -w option, report, once per second, the number of packets captured.
-vv
    Even more verbose output. For example, additional fields are printed from NFS reply packets, and SMB packets are fully decoded.
-vvv
    Even more verbose output. For example, telnet SB ... SE options are printed in full. With -X Telnet options are printed in hex as well.
-V file
    Read a list of filenames from file. Standard input is used if file is ``-''.
-w file
    Write the raw packets to file rather than parsing and printing them out. They can later be printed with the -r option. Standard output is used if file is ``-''.
    This output will be buffered if written to a file or pipe, so a program reading from the file or pipe may not see packets for an arbitrary amount of time after they are received. Use the -U flag to cause packets to be written as soon as they are received.
    The MIME type application/vnd.tcpdump.pcap has been registered with IANA for pcap files. The filename extension .pcap appears to be the most commonly used along with .cap and .dmp. Tcpdump itself doesn't check the extension when reading capture files and doesn't add an extension when writing them (it uses magic numbers in the file header instead). However, many operating systems and applications will use the extension if it is present and adding one (e.g. .pcap) is recommended.
    See pcap-savefile(5) for a description of the file format.
-W
    Used in conjunction with the -C option, this will limit the number of files created to the specified number, and begin overwriting files from the beginning, thus creating a 'rotating' buffer. In addition, it will name the files with enough leading 0s to support the maximum number of files, allowing them to sort correctly.
    Used in conjunction with the -G option, this will limit the number of rotated dump files that get created, exiting with status 0 when reaching the limit. If used with -C as well, the behavior will result in cyclical files per timeslice.
-x
    When parsing and printing, in addition to printing the headers of each packet, print the data of each packet (minus its link level header) in hex. The smaller of the entire packet or snaplen bytes will be printed. Note that this is the entire link-layer packet, so for link layers that pad (e.g. Ethernet), the padding bytes will also be printed when the higher layer packet is shorter than the required padding.
-xx
    When parsing and printing, in addition to printing the headers of each packet, print the data of each packet, including its link level header, in hex.
-X
    When parsing and printing, in addition to printing the headers of each packet, print the data of each packet (minus its link level header) in hex and ASCII. This is very handy for analysing new protocols.
-XX
    When parsing and printing, in addition to printing the headers of each packet, print the data of each packet, including its link level header, in hex and ASCII.
-y datalinktype
--linktype=datalinktype
    Set the data link type to use while capturing packets to datalinktype.
-z postrotate-command
    Used in conjunction with the -C or -G options, this will make tcpdump run " postrotate-command file " where file is the savefile being closed after each rotation. For example, specifying -z gzip or -z bzip2 will compress each savefile using gzip or bzip2.
    Note that tcpdump will run the command in parallel to the capture, using the lowest priority so that this doesn't disturb the capture process.
    And in case you would like to use a command that itself takes flags or different arguments, you can always write a shell script that will take the savefile name as the only argument, make the flags & arguments arrangements and execute the command that you want.
-Z user
--relinquish-privileges=user
    If tcpdump is running as root, after opening the capture device or input savefile, but before opening any savefiles for output, change the user ID to user and the group ID to the primary group of user.
    This behavior can also be enabled by default at compile time.
expression
    selects which packets will be dumped. If no expression is given, all packets on the net will be dumped. Otherwise, only packets for which expression is `true' will be dumped.
    For the expression syntax, see pcap-filter(7).

    The expression argument can be passed to tcpdump as either a single Shell argument, or as multiple Shell arguments, whichever is more convenient. Generally, if the expression contains Shell metacharacters, such as backslashes used to escape protocol names, it is easier to pass it as a single, quoted argument rather than to escape the Shell metacharacters. Multiple arguments are concatenated with spaces before being parsed.
```

## pcap-filter过滤语法

pcap_compile() is used to compile a string into a filter program. The resulting filter program can then be applied to some stream of packets to determine which packets will be supplied to pcap_loop(3PCAP), pcap_dispatch(3PCAP), pcap_next(3PCAP), or pcap_next_ex(3PCAP).

The filter expression consists of one or more primitives. Primitives usually consist of an id (name or number) preceded by one or more qualifiers. There are three different kinds of qualifier:

- **type**

    type qualifiers say what kind of thing the id name or number refers to. Possible types are host, net , port and portrange. E.g., `host foo', `net 128.3', `port 20', `portrange 6000-6008'. If there is no type qualifier, host is assumed.

- **dir**

    dir qualifiers specify a particular transfer direction to and/or from id. Possible directions are src, dst, src or dst, src and dst, ra, ta, addr1, addr2, addr3, and addr4. E.g., `src foo', `dst net 128.3', `src or dst port ftp-data'. If there is no dir qualifier, src or dst is assumed. The ra, ta, addr1, addr2, addr3, and addr4 qualifiers are only valid for IEEE 802.11 Wireless LAN link layers. For some link layers, such as SLIP and the ``cooked'' Linux capture mode used for the ``any'' device and for some other device types, the inbound and outbound qualifiers can be used to specify a desired direction.

- **proto**

    proto qualifiers restrict the match to a particular protocol. Possible protos are: ether, fddi, tr, wlan, ip, ip6, arp, rarp, decnet, tcp and udp. E.g., `ether src foo', `arp net 128.3', `tcp port 21', `udp portrange 7000-7009', `wlan addr2 0:2:3:4:5:6'. If there is no proto qualifier, all protocols consistent with the type are assumed. E.g., `src foo' means `(ip or arp or rarp) src foo' (except the latter is not legal syntax), `net bar' means `(ip or arp or rarp) net bar' and `port 53' means `(tcp or udp) port 53'.

[`fddi' is actually an alias for `ether'; the parser treats them identically as meaning ``the data link level used on the specified network interface.'' FDDI headers contain Ethernet-like source and destination addresses, and often contain Ethernet-like packet types, so you can filter on these FDDI fields just as with the analogous Ethernet fields. FDDI headers also contain other fields, but you cannot name them explicitly in a filter expression.

Similarly, `tr' and `wlan' are aliases for `ether'; the previous paragraph's statements about FDDI headers also apply to Token Ring and 802.11 wireless LAN headers. For 802.11 headers, the destination address is the DA field and the source address is the SA field; the BSSID, RA, and TA fields aren't tested.]

In addition to the above, there are some special `primitive' keywords that don't follow the pattern: gateway, broadcast, less, greater and arithmetic expressions. All of these are described below.

More complex filter expressions are built up by using the words and, or and not to combine primitives. E.g., `host foo and not port ftp and not port ftp-data'. To save typing, identical qualifier lists can be omitted. E.g., `tcp dst port ftp or ftp-data or domain' is exactly the same as `tcp dst port ftp or tcp dst port ftp-data or tcp dst port domain'.

Allowable primitives are:

- **dst host** *host*

    True if the IPv4/v6 destination field of the packet is host, which may be either an address or a name.

- **src host** *host*

    True if the IPv4/v6 source field of the packet is host.

- **host** *host*

    True if either the IPv4/v6 source or destination of the packet is host.

    Any of the above host expressions can be prepended with the keywords, ip, arp, rarp, or ip6 as in:
    ip host host

    which is equivalent to:
    ether proto \ip and host host

    If host is a name with multiple IP addresses, each address will be checked for a match.

- **ether dst** ehost

    True if the Ethernet destination address is ehost. Ehost may be either a name from /etc/ethers or a numerical MAC address of the form "xx:xx:xx:xx:xx:xx", "xx.xx.xx.xx.xx.xx", "xx-xx-xx-xx-xx-xx", "xxxx.xxxx.xxxx", "xxxxxxxxxxxx", or various mixes of ':', '.', and '-', where each "x" is a hex digit (0-9, a-f, or A-F).

- **ether src** *ehost*

    True if the Ethernet source address is ehost.

- **ether host** *ehost*

    True if either the Ethernet source or destination address is ehost.

- **gateway** *host*

    True if the packet used host as a gateway. I.e., the Ethernet source or destination address was host but neither the IP source nor the IP destination was host. Host must be a name and must be found both by the machine's host-name-to-IP-address resolution mechanisms (host name file, DNS, NIS, etc.) and by the machine's host-name-to-Ethernet-address resolution mechanism (/etc/ethers, etc.). (An equivalent expression is

    ether host ehost and not host host

    which can be used with either names or numbers for host / ehost.) This syntax does not work in IPv6-enabled configuration at this moment.

- **dst net** *net*

    True if the IPv4/v6 destination address of the packet has a network number of net. Net may be either a name from the networks database (/etc/networks, etc.) or a network number. An IPv4 network number can be written as a dotted quad (e.g., 192.168.1.0), dotted triple (e.g., 192.168.1), dotted pair (e.g, 172.16), or single number (e.g., 10); the netmask is 255.255.255.255 for a dotted quad (which means that it's really a host match), 255.255.255.0 for a dotted triple, 255.255.0.0 for a dotted pair, or 255.0.0.0 for a single number. An IPv6 network number must be written out fully; the netmask is ff:ff:ff:ff:ff:ff:ff:ff, so IPv6 "network" matches are really always host matches, and a network match requires a netmask length.

- **src net** *net*

    True if the IPv4/v6 source address of the packet has a network number of net.

- **net** *net*

    True if either the IPv4/v6 source or destination address of the packet has a network number of net.

- **net** *net* **mask** *netmask*

    True if the IPv4 address matches net with the specific netmask. May be qualified with src or dst. Note that this syntax is not valid for IPv6 net.

- **net** *net/len*

    True if the IPv4/v6 address matches net with a netmask len bits wide. May be qualified with src or dst.

- **dst port** *port*

    True if the packet is ip/tcp, ip/udp, ip6/tcp or ip6/udp and has a destination port value of port. The port can be a number or a name used in /etc/services (see tcp(4P) and udp(4P)). If a name is used, both the port number and protocol are checked. If a number or ambiguous name is used, only the port number is checked (e.g., dst port 513 will print both tcp/login traffic and udp/who traffic, and port domain will print both tcp/domain and udp/domain traffic).

- **src port** *port*

    True if the packet has a source port value of port.
- **port** *port*

    True if either the source or destination port of the packet is port.

- **dst portrange** *port1-port2*

    True if the packet is ip/tcp, ip/udp, ip6/tcp or ip6/udp and has a destination port value between port1 and port2. port1 and port2 are interpreted in the same fashion as the port parameter for port.

- **src portrange** *port1-port2*

    True if the packet has a source port value between port1 and port2.

- **portrange** *port1-port2*

    True if either the source or destination port of the packet is between port1 and port2.

    Any of the above port or port range expressions can be prepended with the keywords, tcp or udp, as in:

    tcp src port port

    which matches only tcp packets whose source port is port.

- **less** *length*

    True if the packet has a length less than or equal to length. This is equivalent to:
    len <= length.

- **greater** *length*

    True if the packet has a length greater than or equal to length. This is equivalent to:

    len >= length.

- **ip proto** *protocol*

    True if the packet is an IPv4 packet (see ip(4P)) of protocol type protocol. Protocol can be a number or one of the names icmp, icmp6, igmp, igrp, pim, ah, esp, vrrp, udp, or tcp. Note that the identifiers tcp, udp, and icmp are also keywords and must be escaped via backslash (\). Note that this primitive does not chase the protocol header chain.
- **ip6 proto** *protocol*

    True if the packet is an IPv6 packet of protocol type protocol. Note that this primitive does not chase the protocol header chain.

- **proto** *protocol*

    True if the packet is an IPv4 or IPv6 packet of protocol type protocol. Note that this primitive does not chase the protocol header chain.

- **tcp, udp, icmp**

    Abbreviations for:

    proto p

    where p is one of the above protocols.

- **ip6 protochain** *protocol*

    True if the packet is IPv6 packet, and contains protocol header with type protocol in its protocol header chain. For example,

    ip6 protochain 6

    matches any IPv6 packet with TCP protocol header in the protocol header chain. The packet may contain, for example, authentication header, routing header, or hop-by-hop option header, between IPv6 header and TCP header. The BPF code emitted by this primitive is complex and cannot be optimized by the BPF optimizer code, and is not supported by filter engines in the kernel, so this can be somewhat slow, and may cause more packets to be dropped.

- **ip protochain** *protocol*

    Equivalent to ip6 protochain protocol, but this is for IPv4.

- **protochain** *protocol*

    True if the packet is an IPv4 or IPv6 packet of protocol type protocol. Note that this primitive chases the protocol header chain.

- **ether broadcast**

    True if the packet is an Ethernet broadcast packet. The ether keyword is optional.

- **ip broadcast**

    True if the packet is an IPv4 broadcast packet. It checks for both the all-zeroes and all-ones broadcast conventions, and looks up the subnet mask on the interface on which the capture is being done.

    If the subnet mask of the interface on which the capture is being done is not available, either because the interface on which capture is being done has no netmask or because the capture is being done on the Linux "any" interface, which can capture on more than one interface, this check will not work correctly.

- **ether multicast**

    True if the packet is an Ethernet multicast packet. The ether keyword is optional. This is shorthand for `ether[0] & 1 != 0'.

- **ip multicast**

    True if the packet is an IPv4 multicast packet.

- **ip6 multicast**

    True if the packet is an IPv6 multicast packet.

- **ether proto** *protocol*

    True if the packet is of ether type protocol. Protocol can be a number or one of the names ip, ip6, arp, rarp, atalk, aarp, decnet, sca, lat, mopdl, moprc, iso, stp, ipx, or netbeui. Note these identifiers are also keywords and must be escaped via backslash (\).

    [In the case of FDDI (e.g., `fddi proto arp'), Token Ring (e.g., `tr proto arp'), and IEEE 802.11 wireless LANS (e.g., `wlan proto arp'), for most of those protocols, the protocol identification comes from the 802.2 Logical Link Control (LLC) header, which is usually layered on top of the FDDI, Token Ring, or 802.11 header.

    When filtering for most protocol identifiers on FDDI, Token Ring, or 802.11, the filter checks only the protocol ID field of an LLC header in so-called SNAP format with an Organizational Unit Identifier (OUI) of 0x000000, for encapsulated Ethernet; it doesn't check whether the packet is in SNAP format with an OUI of 0x000000. The exceptions are:

    - iso

        the filter checks the DSAP (Destination Service Access Point) and SSAP (Source Service Access Point) fields of the LLC header;

    - stp and netbeui

        the filter checks the DSAP of the LLC header;

    - atalk

        the filter checks for a SNAP-format packet with an OUI of 0x080007 and the AppleTalk etype.

    In the case of Ethernet, the filter checks the Ethernet type field for most of those protocols. The exceptions are:

    - iso, stp, and netbeui

        the filter checks for an 802.3 frame and then checks the LLC header as it does for FDDI, Token Ring, and 802.11;

    - atalk

        the filter checks both for the AppleTalk etype in an Ethernet frame and for a SNAP-format packet as it does for FDDI, Token Ring, and 802.11;

    - aarp

        the filter checks for the AppleTalk ARP etype in either an Ethernet frame or an 802.2 SNAP frame with an OUI of 0x000000;

    - ipx

        the filter checks for the IPX etype in an Ethernet frame, the IPX DSAP in the LLC header, the 802.3-with-no-LLC-header encapsulation of IPX, and the IPX etype in a SNAP frame.

- **ip, ip6, arp, rarp, atalk, aarp, decnet, iso, stp, ipx, netbeui**

    Abbreviations for:

    ether proto p

    where p is one of the above protocols.

- **lat, moprc, mopdl**

    Abbreviations for:

    ether proto p

    where p is one of the above protocols. Note that not all applications using pcap(3PCAP) currently know how to parse these protocols.

- **decnet src** *host*

    True if the DECNET source address is host, which may be an address of the form ``10.123'', or a DECNET host name. [DECNET host name support is only available on ULTRIX systems that are configured to run DECNET.]

- **decnet dst** *host*

    True if the DECNET destination address is host.

- **decnet host** *host*

    True if either the DECNET source or destination address is host.

- **llc**

    True if the packet has an 802.2 LLC header. This includes:

    Ethernet packets with a length field rather than a type field 
    that aren't raw NetWare-over-802.3 packets;

    IEEE 802.11 data packets;

    Token Ring packets (no check is done for LLC frames);

    FDDI packets (no check is done for LLC frames);

    LLC-encapsulated ATM packets, for SunATM on Solaris.

- **llc** Fitype

    True if the packet has an 802.2 LLC header and has the specified type. type can be one of:

    - **i**

        Information (I) PDUs

    - **s**

        Supervisory (S) PDUs

    - **u**

        Unnumbered (U) PDUs

    - **rr**

        Receiver Ready (RR) S PDUs

    - **rnr**

        Receiver Not Ready (RNR) S PDUs

    - **rej**

    Reject (REJ) S PDUs

    - **ui**
Unnumbered Information (UI) U PDUs
ua
Unnumbered Acknowledgment (UA) U PDUs
disc
Disconnect (DISC) U PDUs
sabme
Set Asynchronous Balanced Mode Extended (SABME) U PDUs
test
Test (TEST) U PDUs
xid
Exchange Identification (XID) U PDUs
frmr
Frame Reject (FRMR) U PDUs
ifname interface
True if the packet was logged as coming from the specified interface (applies only to packets logged by OpenBSD's or FreeBSD's pf(4)).
on interface
Synonymous with the ifname modifier.
rnr num
True if the packet was logged as matching the specified PF rule number (applies only to packets logged by OpenBSD's or FreeBSD's pf(4)).
rulenum num
Synonymous with the rnr modifier.
reason code
True if the packet was logged with the specified PF reason code. The known codes are: match, bad-offset, fragment, short, normalize, and memory (applies only to packets logged by OpenBSD's or FreeBSD's pf(4)).
rset name
True if the packet was logged as matching the specified PF ruleset name of an anchored ruleset (applies only to packets logged by OpenBSD's or FreeBSD's pf(4)).
ruleset name
Synonymous with the rset modifier.
srnr num
True if the packet was logged as matching the specified PF rule number of an anchored ruleset (applies only to packets logged by OpenBSD's or FreeBSD's pf(4)).
subrulenum num
Synonymous with the srnr modifier.
action act
True if PF took the specified action when the packet was logged. Known actions are: pass and block and, with later versions of pf(4), nat, rdr, binat and scrub (applies only to packets logged by OpenBSD's or FreeBSD's pf(4)).
wlan ra ehost
True if the IEEE 802.11 RA is ehost. The RA field is used in all frames except for management frames.
wlan ta ehost
True if the IEEE 802.11 TA is ehost. The TA field is used in all frames except for management frames and CTS (Clear To Send) and ACK (Acknowledgment) control frames.
wlan addr1 ehost
True if the first IEEE 802.11 address is ehost.
wlan addr2 ehost
True if the second IEEE 802.11 address, if present, is ehost. The second address field is used in all frames except for CTS (Clear To Send) and ACK (Acknowledgment) control frames.
wlan addr3 ehost
True if the third IEEE 802.11 address, if present, is ehost. The third address field is used in management and data frames, but not in control frames.
wlan addr4 ehost
True if the fourth IEEE 802.11 address, if present, is ehost. The fourth address field is only used for WDS (Wireless Distribution System) frames.
type wlan_type
True if the IEEE 802.11 frame type matches the specified wlan_type. Valid wlan_types are: mgt, ctl and data.
type wlan_type subtype wlan_subtype
True if the IEEE 802.11 frame type matches the specified wlan_type and frame subtype matches the specified wlan_subtype.
If the specified wlan_type is mgt, then valid wlan_subtypes are: assoc-req, assoc-resp, reassoc-req, reassoc-resp, probe-req, probe-resp, beacon, atim, disassoc, auth and deauth.
If the specified wlan_type is ctl, then valid wlan_subtypes are: ps-poll, rts, cts, ack, cf-end and cf-end-ack.
If the specified wlan_type is data, then valid wlan_subtypes are: data, data-cf-ack, data-cf-poll, data-cf-ack-poll, null, cf-ack, cf-poll, cf-ack-poll, qos-data, qos-data-cf-ack, qos-data-cf-poll, qos-data-cf-ack-poll, qos, qos-cf-poll and qos-cf-ack-poll.
subtype wlan_subtype
True if the IEEE 802.11 frame subtype matches the specified wlan_subtype and frame has the type to which the specified wlan_subtype belongs.
dir dir
True if the IEEE 802.11 frame direction matches the specified dir. Valid directions are: nods, tods, fromds, dstods, or a numeric value.
vlan [vlan_id]
True if the packet is an IEEE 802.1Q VLAN packet. If [vlan_id] is specified, only true if the packet has the specified vlan_id. Note that the first vlan keyword encountered in expression changes the decoding offsets for the remainder of expression on the assumption that the packet is a VLAN packet. The vlan [vlan_id] expression may be used more than once, to filter on VLAN hierarchies. Each use of that expression increments the filter offsets by 4.
For example:
vlan 100 && vlan 200
filters on VLAN 200 encapsulated within VLAN 100, and
vlan && vlan 300 && ip
filters IPv4 protocols encapsulated in VLAN 300 encapsulated within any higher order VLAN.
mpls [label_num]
True if the packet is an MPLS packet. If [label_num] is specified, only true is the packet has the specified label_num. Note that the first mpls keyword encountered in expression changes the decoding offsets for the remainder of expression on the assumption that the packet is a MPLS-encapsulated IP packet. The mpls [label_num] expression may be used more than once, to filter on MPLS hierarchies. Each use of that expression increments the filter offsets by 4.
For example:
mpls 100000 && mpls 1024
filters packets with an outer label of 100000 and an inner label of 1024, and
mpls && mpls 1024 && host 192.9.200.1
filters packets to or from 192.9.200.1 with an inner label of 1024 and any outer label.
pppoed
True if the packet is a PPP-over-Ethernet Discovery packet (Ethernet type 0x8863).
pppoes [session_id]
True if the packet is a PPP-over-Ethernet Session packet (Ethernet type 0x8864). If [session_id] is specified, only true if the packet has the specified session_id. Note that the first pppoes keyword encountered in expression changes the decoding offsets for the remainder of expression on the assumption that the packet is a PPPoE session packet.
For example:
pppoes 0x27 && ip
filters IPv4 protocols encapsulated in PPPoE session id 0x27.
geneve [vni]
True if the packet is a Geneve packet (UDP port 6081). If [vni] is specified, only true if the packet has the specified vni. Note that when the geneve keyword is encountered in expression, it changes the decoding offsets for the remainder of expression on the assumption that the packet is a Geneve packet.
For example:
geneve 0xb && ip
filters IPv4 protocols encapsulated in Geneve with VNI 0xb. This will match both IP directly encapsulated in Geneve as well as IP contained inside an Ethernet frame.
iso proto protocol
True if the packet is an OSI packet of protocol type protocol. Protocol can be a number or one of the names clnp, esis, or isis.
clnp, esis, isis
Abbreviations for:
iso proto p
where p is one of the above protocols.
l1, l2, iih, lsp, snp, csnp, psnp
Abbreviations for IS-IS PDU types.
vpi n
True if the packet is an ATM packet, for SunATM on Solaris, with a virtual path identifier of n.
vci n
True if the packet is an ATM packet, for SunATM on Solaris, with a virtual channel identifier of n.
lane
True if the packet is an ATM packet, for SunATM on Solaris, and is an ATM LANE packet. Note that the first lane keyword encountered in expression changes the tests done in the remainder of expression on the assumption that the packet is either a LANE emulated Ethernet packet or a LANE LE Control packet. If lane isn't specified, the tests are done under the assumption that the packet is an LLC-encapsulated packet.
oamf4s
True if the packet is an ATM packet, for SunATM on Solaris, and is a segment OAM F4 flow cell (VPI=0 & VCI=3).
oamf4e
True if the packet is an ATM packet, for SunATM on Solaris, and is an end-to-end OAM F4 flow cell (VPI=0 & VCI=4).
oamf4
True if the packet is an ATM packet, for SunATM on Solaris, and is a segment or end-to-end OAM F4 flow cell (VPI=0 & (VCI=3 | VCI=4)).
oam
True if the packet is an ATM packet, for SunATM on Solaris, and is a segment or end-to-end OAM F4 flow cell (VPI=0 & (VCI=3 | VCI=4)).
metac
True if the packet is an ATM packet, for SunATM on Solaris, and is on a meta signaling circuit (VPI=0 & VCI=1).
bcc
True if the packet is an ATM packet, for SunATM on Solaris, and is on a broadcast signaling circuit (VPI=0 & VCI=2).
sc
True if the packet is an ATM packet, for SunATM on Solaris, and is on a signaling circuit (VPI=0 & VCI=5).
ilmic
True if the packet is an ATM packet, for SunATM on Solaris, and is on an ILMI circuit (VPI=0 & VCI=16).
connectmsg
True if the packet is an ATM packet, for SunATM on Solaris, and is on a signaling circuit and is a Q.2931 Setup, Call Proceeding, Connect, Connect Ack, Release, or Release Done message.
metaconnect
True if the packet is an ATM packet, for SunATM on Solaris, and is on a meta signaling circuit and is a Q.2931 Setup, Call Proceeding, Connect, Release, or Release Done message.
expr relop expr
True if the relation holds, where relop is one of >, <, >=, <=, =, !=, and expr is an arithmetic expression composed of integer constants (expressed in standard C syntax), the normal binary operators [+, -, *, /, %, &, |, ^, <<, >>], a length operator, and special packet data accessors. Note that all comparisons are unsigned, so that, for example, 0x80000000 and 0xffffffff are > 0.
The % and ^ operators are currently only supported for filtering in the kernel on Linux with 3.7 and later kernels; on all other systems, if those operators are used, filtering will be done in user mode, which will increase the overhead of capturing packets and may cause more packets to be dropped.
To access data inside the packet, use the following syntax:
proto [ expr : size ]
Proto is one of ether, fddi, tr, wlan, ppp, slip, link, ip, arp, rarp, tcp, udp, icmp, ip6 or radio, and indicates the protocol layer for the index operation. (ether, fddi, wlan, tr, ppp, slip and link all refer to the link layer. radio refers to the "radio header" added to some 802.11 captures.) Note that tcp, udp and other upper-layer protocol types only apply to IPv4, not IPv6 (this will be fixed in the future). The byte offset, relative to the indicated protocol layer, is given by expr. Size is optional and indicates the number of bytes in the field of interest; it can be either one, two, or four, and defaults to one. The length operator, indicated by the keyword len, gives the length of the packet.
For example, `ether[0] & 1 != 0' catches all multicast traffic. The expression `ip[0] & 0xf != 5' catches all IPv4 packets with options. The expression `ip[6:2] & 0x1fff = 0' catches only unfragmented IPv4 datagrams and frag zero of fragmented IPv4 datagrams. This check is implicitly applied to the tcp and udp index operations. For instance, tcp[0] always means the first byte of the TCP header, and never means the first byte of an intervening fragment.

Some offsets and field values may be expressed as names rather than as numeric values. The following protocol header field offsets are available: icmptype (ICMP type field), icmp6type (ICMP v6 type field) icmpcode (ICMP code field), icmp6code (ICMP v6 code field), and tcpflags (TCP flags field).

The following ICMP type field values are available: icmp-echoreply, icmp-unreach, icmp-sourcequench, icmp-redirect, icmp-echo, icmp-routeradvert, icmp-routersolicit, icmp-timxceed, icmp-paramprob, icmp-tstamp, icmp-tstampreply, icmp-ireq, icmp-ireqreply, icmp-maskreq, icmp-maskreply.

The following ICMPv6 type fields are available: icmp6-echo, icmp6-echoreply, icmp6-multicastlistenerquery, icmp6-multicastlistenerreportv1, icmp6-multicastlistenerdone, icmp6-routersolicit, icmp6-routeradvert, icmp6-neighborsolicit, icmp6-neighboradvert, icmp6-redirect, icmp6-routerrenum, icmp6-nodeinformationquery, icmp6-nodeinformationresponse, icmp6-ineighbordiscoverysolicit, icmp6-ineighbordiscoveryadvert, icmp6-multicastlistenerreportv2, icmp6-homeagentdiscoveryrequest, icmp6-homeagentdiscoveryreply, icmp6-mobileprefixsolicit, icmp6-mobileprefixadvert, icmp6-certpathsolicit, icmp6-certpathadvert, icmp6-multicastrouteradvert, icmp6-multicastroutersolicit, icmp6-multicastrouterterm.

The following TCP flags field values are available: tcp-fin, tcp-syn, tcp-rst, tcp-push, tcp-ack, tcp-urg, tcp-ece, tcp-cwr.

Primitives may be combined using:

A parenthesized group of primitives and operators.
Negation (`!' or `not').
Concatenation (`&&' or `and').
Alternation (`||' or `or').
Negation has highest precedence. Alternation and concatenation have equal precedence and associate left to right. Note that explicit and tokens, not juxtaposition, are now required for concatenation.

If an identifier is given without a keyword, the most recent keyword is assumed. For example,

not host vs and ace
is short for
not host vs and host ace
which should not be confused with
not ( host vs or ace )
