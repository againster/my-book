#!/bin/bash
#global daemon start/stop/restart/reload script,
#yjh(2012)->

#usage:
#create link:
#  ln -sfT  /.../init.d/daemon.sh  /.../init.d/cmdname
#  ln -sfT  /.../init.d/daemon.sh  /.../rc.d/S50cmdname
#daemon manager:
#  /.../init.d/cmdname start/stop/status
#  /.../rc.d/S50cmdname start/stop/status

. /.PATH
. /lib/lsb/init-functions

TCMDNAME=${0}
#get SNNcmdname from "/path/to/SNNcmdname"
#TCMDNAME=`basename "${CMDNAME}"`
TCMDNAME=${TCMDNAME##*/}
#get cmdname from "SNNcmdname"
CMDNAME=${TCMDNAME#[S,K][0-9][0-9]}

#get cmdname from "rc.d/NNcmdname", (delete first two numbers)
if [ X"${CMDNAME}" = X"${TCMDNAME}" ]; then
    #cmdname == SNNcmdname, maybe run from init.d
    RCdotD=0
else
    #cmdname != SNNcmdname, maybe run from rc.d/SNN...
    RCdotD=1
fi

LOCKFILE="/var/run/${CMDNAME}.pid"

#截获SIGINT,解决脚本在运行过程中，执行Ctrl+C，导致锁目录未删除
trap 'vtp_unlock "/var/lock/${CMDNAME}_lock";exit' SIGINT SIGTERM


is_pid_in_docker(){
    local pid="$1"
    local pids_in_docker="$2"
    for pid_in_docker in $pids_in_docker; do
        if [ x$pid == x$pid_in_docker ]; then
            return 0
        fi
    done
    return 1
}


filter_pids_in_docker(){
    local pids=$@
    local pids_in_docker=$(cat /mnt/cgroup/cpu/docker/*/tasks 2>/dev/null)

    # if no proc in docker, then return original pids
    if [ x"$pids_in_docker" == x ]; then
        echo "$pids"
        return 0
    fi

    local pids_after_filter=''
    for pid in $pids; do
        if ! is_pid_in_docker "$pid" "$pids_in_docker"; then
            pids_after_filter="$pids_after_filter $pid"
        fi
    done
    echo $pids_after_filter
    return 0
}


getcmdpid() {
    PIDS=$(pgrep -f "^${CMDNAME}\b|bin/${CMDNAME}\b")
    #^这个正则为了排除掉init.d/cmdname和rc.d/cmdname
    PIDS=$(filter_pids_in_docker $PIDS)
    echo $PIDS
    #^echo为了消除pgrep的换行符
    if [ x"$PIDS" == x ]; then
        return 1
    else
        return 0
    fi

#    if [ x"${CMDNAME##*.sh}" = x"" ]; then
#        pidof -o $$ -x "${CMDNAME}"
#    else
#        pidof "${CMDNAME}"
#    fi
}


#只获取cmd的进程号，不包含supervisor的进程号
getcmdpid2()
{
    PID=$(pidof "${CMDNAME}")
    PID=$(filter_pids_in_docker $PID)
    echo $PID
    if [ x"$PID" == x ]; then
        return 1
    else
        return 0
    fi
}


cmdstart() {
    #if runing, just return
    if getcmdpid >/dev/null ; then
        [ ! -e "${LOCKFILE}" ] && getcmdpid2 >"${LOCKFILE}"
        log_warning_msg "    Already runing: ${CMDNAME} $(cat ${LOCKFILE})"
        return 0
    fi

    #读取服务配置，配置不存在则使用默认设置“”。下面不使用source方式读取是担心配置文件的安全隐患。
    RCCFG="/sf/etc/rcargs.d/${CMDNAME}"

    CFGCMD=cfgfile

    #是否自动启动
    if [ $RCdotD = 0 ]; then
        #从init.d执行？忽略配置
        RCENABLE="yes"
    else
        #从rcx.d执行，读取配置
        RCENABLE="$($CFGCMD -r "$RCCFG" -s "" -k "RCENABLE" -v "yes")"
        #if daemon disabled, just return
        case "${RCENABLE}" in
            [Nn]*)
            echo "    Start (${CMDNAME}) skip: RCENABLE=${RCENABLE}"
            exit 0
            ;;
        esac
    fi

    #启动参数
    RCARGS="$($CFGCMD -r "$RCCFG" -s "" -k "RCARGS" -v "")"
    #运行用户身份
    RCUSER="$($CFGCMD -r "$RCCFG" -s "" -k "RCUSER" -v "")"
    #可执行文件路径
    RCPATHNAME="$($CFGCMD -r "$RCCFG" -s "" -k "RCPATHNAME" -v "")"
    #是否在后台启动
    RCBACKGROUND="$($CFGCMD -r "$RCCFG" -s "" -k "RCBACKGROUND" -v "no")"
    #前置执行程序
    RCPRERUN="$($CFGCMD -r "$RCCFG" -s "" -k "RCPRERUN" -v "")"

    #是否监控该服务：异常退出时立即拉起来
    RCWATCHDOG="$($CFGCMD -r "$RCCFG" -s "" -k "RCWATCHDOG" -v "no")"
    #该服务的最大内存限制，默认限制1GB
    RCMAXMEM="$($CFGCMD -r "$RCCFG" -s "" -k "RCMAXMEM" -v "1048576")"
    #该服务的最大句柄数限制
    RCMAXFD="$($CFGCMD -r "$RCCFG" -s "" -k "RCMAXFD" -v "")"
    #该服务的最大coredump大小，默认限制1GB
    RCCORESIZE="$($CFGCMD -r "$RCCFG" -s "" -k "RCCORESIZE" -v "${RCMAXMEM}")"
    #该服务的进程调度优先级
    RCPRIORITY="$($CFGCMD -r "$RCCFG" -s "" -k "RCPRIORITY" -v "")"
    #该服务的cpu绑定设置
    RCCPUBIND="$($CFGCMD -r "$RCCFG" -s "" -k "RCCPUBIND" -v "")"
    #该服务的资源分组设置(cgroup)
    RCCGROUP="$($CFGCMD -r "$RCCFG" -s "" -k "RCCGROUP" -v "")"
    #监控进程命令行
    RCDOGCMD="$($CFGCMD -r "$RCCFG" -s "" -k "RCDOGCMD" -v "")"

    [ x"$CFGCMD" = x"cfgfilesock" ] && cfgfilesock --close="$RCCFG" >/dev/null 2>&1

    #set EXEPATH="/path/to/cmdname"
    if [ X"${RCPATHNAME}" != X"" ]; then
        #指定了可执行路径
        EXEPATH="${RCPATHNAME}"
    else
        EXEPATH="$(which "${CMDNAME}")"
        #找不到则设置默认路径
        [ -z "$EXEPATH" ] && EXEPATH="${CMDNAME}"
    fi

    #append $RCARGS to EXEPATH("/path/to/cmdname")
    if [ X"${RCARGS}" != X"" ]; then
        EXEPATH="${EXEPATH} ${RCARGS}"
    fi
    #now run the prerun command
    if [ X"${RCPRERUN}" != X"" ]; then
        #echo "    Prerun: ${RCPRERUN}"
        bash -c "${RCPRERUN}"
    fi

    #设置优先级（设置当前shell，继承到子进程）
    [ -n "$RCPRIORITY" ] && renice -n "$RCPRIORITY" -p "$$"

    #设置内存限制,文件句柄,coredump大小
    [ -n "$RCMAXMEM" ] && ulimit -m "$RCMAXMEM"
    [ -n "$RCMAXFD" ] && ulimit -n "$RCMAXFD"
    [ -n "$RCCORESIZE" ] && ulimit -c "$RCCORESIZE"
    [ -n "$RCCPUBIND" ] && taskset -p "${RCCPUBIND}" $$ >/dev/null

    #设置资源分组(cgroup)
    [ -n "$RCCGROUP" ] && cgclassify -g "$RCCGROUP" $$ >/dev/null

    if [ -n "${RCWATCHDOG}" ]; then
        if expr match "${RCWATCHDOG}" "[Y,y]" >/dev/null ; then
            #启用服务监控
            #modify supervisor by wtd@20150323, add DOGCMD
            if [ "${RCDOGCMD}" = "" ]; then
                RCDOGCMD="supervisor --flag-file '${LOCKFILE}' --flag 010 -w"
            else
                RCDOGCMD=`eval echo \"${RCDOGCMD}\"`
            fi
            EXEPATH="${RCDOGCMD} '${EXEPATH}'"
        else
            #禁用服务监控
            if expr match "${RCBACKGROUND}" "[Y,y]" >/dev/null ; then
                #启用后台运行
                EXEPATH="${EXEPATH} &"
            fi
        fi
    fi

    #now run the command
    if [ -n "$RCUSER" ]; then
        #切换用户身份
        su "$RCUSER" -c "${EXEPATH}" 2>&1
    else
        #echo "${EXEPATH}"
        eval ${EXEPATH} 2>&1
    fi

    #check the runing status
    startok=$?
    getcmdpid2 >"${LOCKFILE}"
    if [ ${startok} = 0 ]; then
        #echo "    Start (${EXEPATH}) OK! $(cat ${LOCKFILE})"
        log_success_msg "Start (${EXEPATH}) OK! $(cat ${LOCKFILE})"
        return 0
    else
        #echo "    Start (${EXEPATH}) failed!"
        log_failure_msg "Start (${EXEPATH}) failed!"
        return 1
    fi
}

cmdstop() {
    rm -f "${LOCKFILE}"
    if ! getcmdpid >/dev/null ; then
        echo "    Already stoped: ${CMDNAME}"
        return 0
    fi
    log_daemon_msg "Stoping ${CMDNAME}: "
    kill $(getcmdpid) 2>/dev/null
    sleep 0.1
    for i in 1 2 3 4 5 6 7 8 9 10 ; do
        if ! getcmdpid >/dev/null ; then
            log_progress_msg " OK!"
            log_end_msg 0
            rm -f "${LOCKFILE}"
            return 0
        fi
       log_progress_msg "."
        sleep 0.5
    done

    log_end_msg 1
    log_daemon_msg " -TERM time out, trying -KILL: "
    for i in `seq 1 24` ; do
        kill -KILL $(getcmdpid) 2>/dev/null
        if ! getcmdpid >/dev/null ; then
           log_progress_msg " OK!"
           log_end_msg 0
            rm -f "${LOCKFILE}"
            return 0
        fi
        log_progress_msg "."
        sleep 0.5
    done
    ps auxf|grep "$(getcmdpid)" >> /sf/log/blackbox/LOG_ps.txt
    log_progress_msg " failed! $(getcmdpid)"
    log_end_msg 1
    return 1
}

vtp_lock "/var/lock/${CMDNAME}_lock" 120
if [ "$?" -eq "1" ];then
    log_progress_msg "got no lock, fail!"
    exit 1
fi

case "${1}" in
    stop)
        cmdstop
        ;;
    start)
        cmdstart
        ;;
    restart)
        cmdstop
        cmdstart
        ;;
    reload)
        if getcmdpid >/dev/null ; then
            kill -HUP $(getcmdpid)
        else
            cmdstart
        fi
        ;;
    status)
        if test -e "${LOCKFILE}" ; then
            echo -n "${CMDNAME} is started "

        else
            echo -n "${CMDNAME} is stopped "
        fi
        PIDS="$(getcmdpid)"
        if [ -n "${PIDS}" ]; then
            echo "and running: ${PIDS}"
        else
            echo "and not running."
        fi
        ;;
    *)
        echo "Usage: ${0} start|stop|restart|status"
        ;;
esac
vtp_unlock "/var/lock/${CMDNAME}_lock"
