# curl命令

## 简单介绍

> curl is a tool to transfer data from or to a server, using one of the supported protocols (DICT, FILE,	FTP, FTPS, GOPHER, HTTP, HTTPS,	 IMAP, IMAPS,  LDAP,  LDAPS,  POP3,  POP3S,  RTMP, RTSP, SCP, SFTP, SMB, SMBS, SMTP, SMTPS, TELNET and TFTP). The command is designed to work  without user interaction.

## 参数解析

```bash
curl [options] [URL...]

-I, --head
    #(HTTP FTP FILE) Fetch the headers only!
--insecure
    #This option allows curl to proceed and operate even for server connections otherwise considered insecure.
```

date: 20180416
___
