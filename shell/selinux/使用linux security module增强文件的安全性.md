linux系统中，众所周知root是最高的权限，主宰系统的所有对象。安全领域中，如果攻击者拿到了系统的root权限，也就意味着系统被完全攻击了。

随着selinux的出现，可以通过策略限制root的这种能力，比如某个策略可以只允许用户A去查看和修改他创建的文件，连root都无法查看用户A的文件。明显这种方式提高了安全性，即使系统攻击后，被泄露的数据也是有限的。如果程序会产生敏感数据，可以保证攻击者拿到root后也无法读取到。

实际应用中，可能存在这样的场景:

程序产生的数据只有程序自己使用，不希望任何其他用户可以查看，以提高安全性。例如一个交易过程中产生的安全证书或者程序自己使用的安全配置。因为这类数据只有程序本身使用和修改。

当某个程序存在漏洞，黑客攻击该程序后很可能会拿到root，但是如果其他程序的敏感数据做了保护，那么系统可泄露的数据也会降到最低。例如使用了selinux技术的linux系统或者上述自己实现的保护模块。

为什么不直接使用selinux呢？这是因为selinux的使用开始就需要设计应用程序的访问控制策略，很多情况下，程序都是很久就写好的，一步步迭代过来的，导致程序本身并没有控制好自己的访问策略，这也导致selinux无法直接使用。

公司的产品里，就没有使用selinux模块。但是我们也可以实现自己的安全模块来阻止不安全的文件访问。

在linux security module的indoe_premission钩子/file_permission钩子做一些处理即可实现这种功能。原理并不复杂，就是钩子处判断访问是否合法，不合法则返回没有权限。至于如何判断访问是否合法，那是根据自身需要来判断的。

我这里实现了一个保护目录OKERNEL，我不允许这个文件被应用层程序访问，当然root权限也是不行的，但是它可以被内核读写。简要贴出几个相关函数:

```c
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/security.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <asm/uaccess.h> 
#include <linux/mm.h> 
#include <linux/mount.h>
#include <linux/dcache.h>
#include <linux/utsname.h>
#include "dsi_kernel_rw.h"
#include "sha1_cache.h"
#include "dsi_debug.h"
#include "test_cache.h"
#include "inode_cache.h"
#include "digest.h"
#include "dsi_cdev.h"

#define PROTECT_DIR "OKERNEL"

//获取一个inode的i_security域
#define get_inode_security(ino) ((unsigned long)(ino->i_security))

//设置一个indoe的i_security域
#define set_inode_security(ino,val) (ino->i_security = (void *)val)

//获取一个文件file的f_security域
#define get_file_security(file) ((unsigned long)(file->f_security))

//设置一个文件file的f_security域
#define set_file_security(file,val) (file->f_security = (void *)val)

/*
* 使用jiffies统计时间依赖于HZ精度，测试机器上为1000，那么对应的单位为1ms
* 结果，如果函数运行的时间低于10ms，统计的都是0，明显不能实现想要的结果
* 如果精度不够，可以使用do_gettimeofday()来处理，精度达到微妙级别
*/
#define start_digsig_bench \
		exec_time = jiffies;//统计执行时间的，用于测试新能的时候需要

#define end_digsig_bench \
		exec_time = jiffies - exec_time; \
		s_total_jiffies += exec_time; \
		DSM_PRINT(DEBUG_INFO, \
			"Time to execute %s on %s is %lu\n", \
			__FUNCTION__, p_file->f_dentry->d_name.name, exec_time);
	
extern int register_security(struct security_operations *ops);		//lsm模块注册函数
extern int unregister_security(struct security_operations *ops);	//lsm模块注销函数 [注意:这两个函数在linux2.6.22以后的版本上不在导出，如果使用，需要导出这两个函数]

// 控制调试输出的级别
long g_debug_level = DEBUG_INFO;

//代表整个模块运行级别的静态变量,该值在运行时可修改
enum RUN_LEVEL g_run_level = PERMISSIVE;

//控制访问保护目录的全局变量，0代表不允许，1代表允许
int g_sangfor_security_flag = 0; 

//统计总的运行时间的统计量
static unsigned long s_total_jiffies = 0;

//统计总请求次数的变量
static unsigned long s_total_requests = 0;

//统计的请求命中inode缓存的次数
static unsigned long s_cached_requests = 0; 
//-------------------实现特定目录保护所需的相关钩子-----------------------//

/*
* 该函数通过检查目录项以及其父目录项是否是保护的目录
* @param p_dentry 待检查的目录项结构
* @return 0表示通过检查，<0表示未通过检查
*/
static int sangfor_dentry_check(struct dentry* p_dentry)
{
	struct dentry* p_current_dentry = p_dentry;
	
	BUG_ON(p_dentry == NULL);

	while(1)
	{
		if (!strcmp(p_current_dentry->d_iname, PROTECT_DIR))
		{
			if(g_sangfor_security_flag)
			{
				return 0;
			}
			return -EACCES;
		}

		if(p_current_dentry == p_current_dentry->d_parent)
		{
			return 0;
		}else
		{
			p_current_dentry = p_current_dentry->d_parent;
		}
	}

	return 0;
}

/*
* 在文件读写之前会调用此钩子 [钩子详细说明参见security.h中的注释]
* 这个钩子并不阻止在保护目录下创建删除目录或者文件
* @param file 准备操作的文件指针
* @param mask 操作的权限 为 MAY_READ,MAY_WRITE,MAY_APPEND,MAY_ACCESS,MAY_EXEC,MAY_OPEN的组合
* @return 0表示通过，<0表示验证失败
*/
static int pte_file_permission(struct file* p_file, int mask)
{
	/*
	* 由于inode_permission已经做检查，这里只是辅助功能
	* 由于使用g_sangfor_security_flag来使得内核通过检查，
	* 如果刚内核刚好设置该变量，而应用层程序也恰好在此刻
	* 进行访问该文件时，存在极小的概率通过了inode_permission
	* 但是，及时通过了在这个钩子出依然会被劫持下来，以确保
	* 文件内容绝对安全
	*/
	
	int retval = 0;
	int access_mask = MAY_WRITE | MAY_APPEND;
	
	if(g_run_level == SHUTDOWN)
		return 0;
		
	if ((mask & access_mask))
	{
		retval = sangfor_dentry_check(p_file->f_path.dentry);
	}
	
	if (retval)
	{
		DSM_PRINT(DEBUG_ALERT, "%s:failed,and dentry is %s\n",
		__FUNCTION__, p_file->f_path.dentry->d_iname);
		retval = -EACCES;
	}
	
	if(g_run_level == PERMISSIVE )
		retval = 0;

	return retval;
}

/*
* 禁止在保护安全目录下生成文件,但这同时也导致内核无法在保护目录下创建文件
* 如果内核有需求在保护目录下创建文件，该钩子可以直接返回0.虽然此时用户层程序同样可以在
* 保护目录下创建文件，但是写入有效内容到文件
*/
static int pte_inode_create(struct inode* p_dir,struct dentry* p_dentry, int mode)
{
	int retval = 0;
	if(g_run_level == SHUTDOWN)
		return 0;
		
	retval = sangfor_dentry_check(p_dentry);
	
	if (retval)
	{
		DSM_PRINT(DEBUG_ALERT, "%s:failed, and dentry is %s\n",
		__FUNCTION__, p_dentry->d_iname);
	}
	
	if(g_run_level == PERMISSIVE )
		retval = 0;
	
	return retval;
}

/*
* 创建一个硬链接到保护目录中的文件会触发该钩子
*/
static int pte_inode_link(struct dentry* p_old_dentry,struct inode* p_dir,
struct dentry* p_new_dentry)
{
	int retval = 0;
	if(g_run_level == SHUTDOWN)
		return 0;
		
	retval = sangfor_dentry_check(p_old_dentry);
	
	if (retval)
	{
		DSM_PRINT(DEBUG_ALERT, "%s:failed, and dentry is %s\n",
		__FUNCTION__, p_old_dentry->d_iname);
	}
	
	if(g_run_level == PERMISSIVE )
		retval = 0;
		
	return retval;
}

/*
* 该钩子用于处理删除硬链接(例如删除保护目录下的文件情况)
* 阻止保护目录下文件的删除
*/
static int pte_inode_unlink(struct inode* p_dir, struct dentry* p_dentry)
{
	int retval = 0;
	if(g_run_level == SHUTDOWN)
		return 0;
		
	retval = sangfor_dentry_check(p_dentry);
	if (retval)
	{
		DSM_PRINT(DEBUG_ALERT, "%s:failed, and dentry is %s\n",
		__FUNCTION__, p_dentry->d_iname);
	}
	
	if(g_run_level == PERMISSIVE)
		retval = 0;
	
	if(is_cached_inode(p_dentry->d_inode) == 0)//缓存有这个inode
	{
		remove_cached_inode(p_dentry->d_inode);
	}
	
	return retval;
}

/*
* 禁止在保护目录里创建新目录
*/
static int pte_inode_mkdir(struct inode* p_dir,struct dentry* p_dentry, int mode)
{
	int retval = 0;
	
	if(g_run_level == SHUTDOWN)
		return 0;
	retval = sangfor_dentry_check(p_dentry);
	
	if (retval)
	{
		DSM_PRINT(DEBUG_ALERT, "%s:failed, and dentry is %s\n",
		__FUNCTION__, p_dentry->d_iname);
	}
	
	if(g_run_level == PERMISSIVE )
		retval = 0;
		
	return retval;
}

/*
* 禁止删除保护目录中的子目录
*/
static int pte_inode_rmdir(struct inode* p_dir,struct dentry* p_dentry)
{
	int retval = 0;
	if(g_run_level == SHUTDOWN)
		return 0;
		
	retval = sangfor_dentry_check(p_dentry);
	
	if (retval)
	{
		DSM_PRINT(DEBUG_ALERT, "%s:failed, and dentry is %s\n",
		__FUNCTION__, p_dentry->d_iname);
	}
	
	if(g_run_level == PERMISSIVE )
		retval = 0;
		
	return retval;
}

/*
* 禁止保护目录下的文件的重命名 (可以处理mv导致的重命名以及rename操作进行的重命名)
* mv 文件进保护目录的时候不会经过这个钩子，但是从保护目录mv文件出去 会调用此钩子
*/
static int pte_inode_rename(struct inode* p_old_dir,struct dentry* p_old_dentry,
struct inode* p_new_dir, struct dentry* p_new_dentry)
{
	int retval = 0;
	
	if(g_run_level == SHUTDOWN)
		return 0;
		
	retval = sangfor_dentry_check(p_old_dentry);
	
	if (retval)
	{
		DSM_PRINT(DEBUG_ALERT, "%s:failed, and dentry is %s\n",
		__FUNCTION__, p_old_dentry->d_iname);
	}
	
	if(g_run_level == PERMISSIVE )
		retval = 0;
		
	return retval;
}

/*
* inode访问的钩子，设计到indoe访问的都会经过次钩子
*/
static int pte_inode_permission(struct inode* p_inode, int mask)
{
	int retval = 0;
	unsigned long isec = 0;
	struct dentry* p_current_dentry = NULL;

	if(g_run_level == SHUTDOWN)
		return 0;
	
	/*
	* 如果inode指向的是一个字符设备，直接允许
	* 按照理论，应该无需处理这里，但是测试发现
	* 存在字符设备的inode的security被污染的情况
	*/
	if(p_inode->i_cdev)	
		return 0;
	               
	p_current_dentry = d_find_alias(p_inode);
	if(p_current_dentry)
	{
		retval = sangfor_dentry_check(p_current_dentry);
	}
	
	if(retval)
	{
		DSM_PRINT(DEBUG_ALERT, "%s:security dir check failed, and dentry is %s\n",
		__FUNCTION__, p_current_dentry->d_iname);
	}
	
	/*
	*保护映射的文件不被写,注意这里并没有阻止文件被删除，应该允许文件被删除
	*只有常规文件才可能在缓存中，可首先进行文件类型判断，因为缓存在结点中的
	*只可能是常规文件，因此无需关注非常规文件。预先处理可提高效率
	*/
	if(p_inode && S_ISREG(p_inode->i_mode) && (mask & MAY_WRITE))
	{	
		/*
		* 读取i_security需要加锁保护
		* 一个inode可以对应着多个file，当多个file在被mmap的时候，会并发的修改
		* i_secueity,因此需要锁来保证读取的数值是正确的
		*/
		spin_lock(&p_inode->i_lock);
		isec = get_inode_security(p_inode);
		
		if(isec > 0)
		{
			DSM_PRINT(DEBUG_ALERT, "%s:inode security check failed, and dentry is %s, i_security is %lu\n",
			__FUNCTION__, p_current_dentry->d_iname, isec);
			retval = -EPERM;
		}
		
		//缓存命中的话，需要将文件的inode从缓存中删除
		if(is_cached_inode(p_inode) == 0)
		{
			remove_cached_inode(p_inode);
		}
		spin_unlock(&p_inode->i_lock);
	}
	
	if(g_run_level == PERMISSIVE)//许可模式运行
		retval = 0;
	
	return retval;
}


//------------------分区挂载的相关钩子------------------------//
/*
* 分区在挂载的时候mount命令调用时会被执行,flags中有挂载参数，
* ro挂载时为0，rw挂载时为1.重新挂载时对应的钩子也为这个函数
* 可以在这里防止攻击者重新挂载你的分区，例如重要文件被你放到
* 了只读的分区中，因此需要保护此分区不能被重新挂载为可写的
*/
static int pte_sb_mount(char* p_dev_name, struct path* p_path, char* p_type,
unsigned long flags, void* p_data)
{
	int retval = 0;
	struct dentry* p_current_dentry = NULL;
	if(g_run_level == SHUTDOWN)
		return 0;
		
	p_current_dentry = p_path->dentry;
	DSM_PRINT(DEBUG_INFO, "%s:dev_name %s\n", __FUNCTION__, p_dev_name);
	
	while(1)	//此while循环只是一个测试输出
	{
		if(p_current_dentry  == p_current_dentry->d_parent)
		{
			break;
		}else
		{
			p_current_dentry  = p_current_dentry->d_parent;
		}
	}
	
	DSM_PRINT(DEBUG_INFO, "%s:type %s\n", __FUNCTION__, p_type);
	DSM_PRINT(DEBUG_INFO, "%s:flags %ld\n", __FUNCTION__, flags);
	DSM_PRINT(DEBUG_INFO, "%s:data %s\n", __FUNCTION__, (char*)p_data);
	
	{
		//可以在这里添加安全验证的逻辑
	}
	
	if(g_run_level == PERMISSIVE)//许可模式运行
		retval = 0;
		
	return retval; 
}

/*
* 分区再被umount的时候会调用此钩子
* 同样在这里可以进行安全检查
*/
static int pte_sb_umount(struct vfsmount* p_mnt, int flags)
{
	int retval = 0;
	if(g_run_level == SHUTDOWN)
		return 0;
		
	DSM_PRINT(DEBUG_INFO, "%s:mnt->mnt_devname %s\n", __FUNCTION__, p_mnt->mnt_devname);
	DSM_PRINT(DEBUG_INFO, "%s:flags %d\n", __FUNCTION__, flags);
	
	//vfsmount里有详细的挂载信息，这里也不输出测试了
	{
		//可以在这里添加安全验证逻辑
	}
	
	if(g_run_level == PERMISSIVE)//许可模式运行
		retval = 0;
		
	return retval;
}


//------------------安全验证相关钩子-------------------------//


 /*
 * 注意这里对远程文件的处理
 * 如果文件是nfs系统的，则不予处理
 * @param p_file 待检测的文件指针
 * @return 0说明是保护的文件 <0说明是不需要保护的文件
 */
static inline int is_unprotected_file(struct file *p_file)
{
	BUG_ON(p_file == NULL);

	if (strcmp(p_file->f_dentry->d_inode->i_sb->s_type->name, "nfs") == 0)
		return -1;	//表示是nfs文件，那么返回1表示 不需要保护
	return 0;		//否则本地的文件都需要保护
}

/*
* 如果文件被打开而写，那么就要拒绝为执行而mmap
* @file  我们预想禁止写操作的elf 文件
* @return 0代表成功 <0表示操作失败
*/
static int digsig_deny_write_access(struct file* p_file)
{
	struct inode* p_inode = NULL;
	unsigned long isec = 0;

	p_inode = p_file->f_dentry->d_inode;

	spin_lock(&p_inode->i_lock);
	isec = get_inode_security(p_inode);			//获取文件inode的i_security

	if (atomic_read(&p_inode->i_writecount) > 0) {//有进程在写文件，直接返回异常
		spin_unlock(&p_inode->i_lock);
		return -ETXTBSY;
	}

	set_inode_security(p_inode, (isec+1));			//设置我们自己的i_security标识，用户保护文件不能被写
	set_file_security(p_file, 1);					//同样也要设置f_security标识，肯定为1
	spin_unlock(&p_inode->i_lock);

	return 0;
}

/*
* digsig_deny_write_access的无锁版本,调用处会使用同步保护
* @file  我们预想禁止写操作的elf 文件
* @return 0代表成功 <0表示操作失败
*/
/*static int digsig_deny_write_access_no_lock(struct file* p_file)
{
	struct inode* p_inode = NULL;
	unsigned long isec = 0;

	p_inode = p_file->f_dentry->d_inode;
	isec = get_inode_security(p_inode);

	if (atomic_read(&p_inode->i_writecount) > 0) 
	{
		return -ETXTBSY;
	}

	set_inode_security(p_inode, (isec+1));
	set_file_security(p_file, 1);

	return 0;
}*/


/*
 * 和digsig_deny_write_access相反
 * @param p_file目标文件指针 
 */
static void digsig_allow_write_access(struct file* p_file)
{
	struct inode* p_inode = p_file->f_dentry->d_inode;
	unsigned long isec = 0;

	spin_lock(&p_inode->i_lock);

	isec = get_inode_security(p_inode);
	set_inode_security(p_inode, (isec-1));	//放开文件的写权限
	set_file_security(p_file, 0);			//设置为0表示资源是true
	
	spin_unlock(&p_inode->i_lock);
	return;
}

/*
* file_mmap钩子(这个函数原型依赖于内核版本，现在的是2.6.34的内核)
* @param p_file准备映射的文件指针
* @param reqprot 映射的参数，权限的
* @param other 不关注的参数
* @return 0表示通过钩子函数验证， <0表示未通过验证
*/
static int pte_file_mmap(struct file* p_file, unsigned long reqprot,
			unsigned long prot, unsigned long flags,
			unsigned long addr, unsigned long addr_only)
{
	int retval = 0;
	int allow_write_on_exit = 0;
	unsigned long exec_time = 0;
	
	if(g_run_level == SHUTDOWN)
		return 0; //关闭模式下直接返回0
		
	if(!(reqprot & VM_EXEC))
		return 0; //非运行模式
	
	if(!p_file)
		return 0;
	
	if(!p_file->f_dentry)
		return 0;
	
	if(!p_file->f_dentry->d_name.name)
		return 0;
		
	if (is_unprotected_file(p_file) < 0)//如果是nfs文件系统上的文件，也直接返回，不进行验证处理
	{
		if(g_run_level == STRICT)
			return -EPERM;			//严格模式下不允许运行nfs中的文件
		else
			return 0;
	}

	s_total_requests++;
	DSM_PRINT(DEBUG_INFO, "%s:binary is %s\n", __FUNCTION__, p_file->f_dentry->d_name.name);
	if(g_run_level == PERMISSIVE)
		start_digsig_bench; //统计时间
		
	/*
	* 虽然get_file_security中读取的是f_security,但是注意同一个file指针不会
	* 同时进行file_mmap ,因此读取file的f_security是不需要同步保护的。
	*/
	if(get_file_security(p_file) == 0)
	{
		allow_write_on_exit = 1;
		retval = digsig_deny_write_access(p_file);
		if(retval)	//二进制文件正在被写，不允许被映射
		{
			allow_write_on_exit = 0;
			goto out_file_no_buf;
		}
	}	
	
	
	retval = 0;
	//inode缓存命中
	if(is_cached_inode(p_file->f_dentry->d_inode) == 0)
	{
		DSM_PRINT(DEBUG_INFO, "%s:binary %s had a cached signature validation\n",
		__FUNCTION__, p_file->f_dentry->d_name.name);
		allow_write_on_exit = 0;//缓存命中后，就将p_file->f_security设置为1，表明此文件不可被写
		s_cached_requests++;
		goto out_file_no_buf;
	}
	
	
	//inode缓存未命中，去sha1缓存查询
	retval = digsig_verify_sign(p_file, HASH_SHA1);
	if(retval < 0)
	{
		if(retval == -SHA1_NOT_FOUND)	//sha1缓存中也未命中
		{
			DSM_PRINT(DEBUG_ALERT, "%s:binary %s not passed sha1 verification\n",
			__FUNCTION__, p_file->f_dentry->d_name.name);
		}else
		{	//非验证错误
			DSM_PRINT(DEBUG_ERROR, "%s:Binary %s had inner verification error\n",
			__FUNCTION__, p_file->f_dentry->d_name.name);
		}
		
		goto out_file_no_buf;
	}
	
	//sha1缓存命中了，将新的inode加入到inode缓存里。无需检查该函数返回值
	inode_cache_insert(p_file->f_dentry->d_inode);
	allow_write_on_exit = 0; //即使上面过程是失败的，也要设置不可写标识，因为文件将被映射后，不允许修改
	
out_file_no_buf:
	if(allow_write_on_exit == 1)
	{
		digsig_allow_write_access(p_file);
	}
	
	if(g_run_level == PERMISSIVE)
	{
		end_digsig_bench;
		retval = 0;
	}
		
	return retval;
}

/*
* file_free_security钩子，文件被关闭时会调用
* @param p_file目标文件指针
*/
static void pte_file_free_security(struct file* p_file)
{
	if(p_file->f_security)
	{
		digsig_allow_write_access(p_file);
	}
}


/*
* inode_free_security钩子
* @param p_inode 目标inode指针
*/
static void pte_inode_free_security(struct inode* p_inode)
{
	if(is_cached_inode(p_inode) == 0)
		remove_cached_inode(p_inode);
}


//本lsm安全模块的security_ops定义
static struct security_operations task_ops = {
    .file_permission = pte_file_permission,
	.inode_create = pte_inode_create,
	.inode_link = pte_inode_link,
	.inode_unlink = pte_inode_unlink,
	.inode_mkdir = pte_inode_mkdir,
	.inode_rmdir = pte_inode_rmdir,
	.inode_rename = pte_inode_rename,
	.sb_mount = pte_sb_mount,
	.sb_umount = pte_sb_umount,
	.inode_permission = pte_inode_permission,//这个钩子可以完成indoe相关的所有劫持工作，实际代码需要详细测试
	.file_mmap = pte_file_mmap,
	.file_free_security = pte_file_free_security,
	.inode_free_security = pte_inode_free_security,
};

/*
* 模块注册的初始化函数
*/
static int __init task_init(void)
{
	DSM_PRINT(DEBUG_NOTICE, "%s:DIGSIG_MODULE init...kernel release: %s\n",
	__FUNCTION__, utsname()->release);

	//初始化sha1缓存
	if(digsig_init_sha1_cache() < 0)
	{
		DSM_PRINT(DEBUG_ERROR, "%s:Init sha1 cache failed\n", __FUNCTION__);
		goto out;
	}
	
	if(inode_cache_init() < 0)
	{
		DSM_PRINT(DEBUG_ERROR, "%s:Init inode cache failed\n", __FUNCTION__);
		goto sha1_err;
	}
	
	if(digsig_sign_first_init(HASH_SHA1))
	{
		DSM_PRINT(DEBUG_ERROR, "%s:Init digest struct failed\n", __FUNCTION__);
		goto inode_err;
	}
	
	if(register_security(&task_ops))
	{
		DSM_PRINT(DEBUG_ERROR, "%s:Lsm security_operations register failed\n", __FUNCTION__);
		goto sign_err;
	}
	
	if(digsig_cdev_init() < 0)
	{
		DSM_PRINT(DEBUG_ERROR, "%s:Digsig cdev init failed\n", __FUNCTION__);
		goto sign_err;
	}
	
	return 0;

sign_err:
	digsig_sign_last_cleanup();
inode_err:
	inode_cache_cleanup();
sha1_err:
	digsig_sha1_cache_cleanup();
out:
	return -1;
}


/*
* 模块注册的退出函数
*/
static void __exit task_exit(void)
{
	//模块需要首先卸载钩子
    if(unregister_security(&task_ops)){
        DSM_PRINT(DEBUG_ERROR, "%s:Lsm security_operations unregister failed\n", __FUNCTION__);
    }
	
	//然后调用模块相关的清理函数
	digsig_sign_last_cleanup();
	inode_cache_cleanup();
	digsig_sha1_cache_cleanup();
	digsig_cdev_exit();
	 
	DSM_PRINT(DEBUG_NOTICE, "%s:DIGSIG_MODULE exit,file_mmap requests:%lu , and cached requests:%lu\n",
	__FUNCTION__, s_total_requests, s_cached_requests);
    return;
}

MODULE_LICENSE("GPL");
module_init(task_init);
module_exit(task_exit);	//如果不想改安全模块被卸载，可注视掉module_exit函数，那么在rmmod 本模块时会返回失败

```
