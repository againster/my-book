1.问题背景

eps 2.0 防篡改使用的技术方案对系统进程有侵入，涉及到execmem, execmod,

mprected等比较敏感的操作。centos系统作为我们支持的服务器对象，它默认开启

的selinux防护机制导致了较多的问题。不过回头来说，selinux的安全机制做的好。



2.问题现象

    eps 2.0 防篡改使用了注入的技术，这个技术使用的一些敏感操作被selinux视为有

威胁的，因此默认会拒绝。而centos/redhat默认又是开启selinux的，总不好去实施

的时候对客户说，你要把selinux关掉啊。由此导致eps 2.0在centos/redhat系列的

操作系统上从安装、注入到生效性的过程上都遇到了各种各样的权限问题。


3.解决方法

虽然搜selinux相关内容，得到的多数是告诉你关闭掉呗:setenforce 0，要是能这样

说服客户，那就真省事了。

经过一阵子的摸索，得出了一个较完整的解决方法，能做到不关闭selinux，不用安装

额外的软件，使用普通的selinux内置工具即可解决的通用方法。

第一步：从/var/log/audit/audit.log中获取到deny的具体内容:

cat /var/log/audit/audit.log | grep "denied" > /tmp/enable.txt

一般得出的结果如下：

type=AVC msg=audit(1475918211.732:820): avc:  denied  { execmem } for  pid=24103 comm="sshd" scontext=system_u:system_r:sshd_t:s0-s0:c0.c1023 tcontext=system_u:system_r:sshd_t:s0-s0:c0.c1023 tclass=process

type=AVC msg=audit(1476234861.982:2747): avc:  denied  { execmod } for  pid=16300 comm="lloader" path="/usr/local/eps_agent/lib/libpolarssl.so" dev=dm-0 ino=3311037 scontext=root:system_r:unconfined_t:s0-s0:c0.c1023 tcontext=root:object_r:usr_t:s0 tclass=file

第二步：将这个结果在有semanage工具组件的centos服务器上使用audit2allow生成放通策略：

cat /tmp/enable.txt | audit2allow enable

此时当前目录生成了enable.pp enable.te两个文件。

其中enable.pp就是selinux使用的策略数据。

第三步：把enable.pp 放到设备上，使用semodule -M enable 即可立即生效，此时

selinux的指定安全策略就被关闭了。

4.说明

a.selinux系统上一般都会有/var/log/audit/audit.log，实在没有就试图开启吧。

selinux有自己的版本，它有自己的版本，一般配置格式都是通用。即按照上述方法

得出的enable.pp对版本通用。

b.方法涉及的命令中audit2allow是非默认的，但是这个由a的特性可知执行一次即可，

依赖解除，即实际上只是要semodule命令即可，而这个命令是selinux默认就有的。

c.以上方法对系统位数不介意。

d.承认：关闭是降低了安全性，但是比粗暴的直接将selinux搞掉好。


ps.以上不涉及selinux的任何相关知识，有需要的直接看centos的man就好了，作为mac的

它设计上和linux的dac有对比价值的。

