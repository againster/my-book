
service zabbix-agent start无法成功启动，zabbix log中是空的，也没有日志显示原因.只有启动服务显示：
Redirecting to /bin/systemctl start  zabbix-agent.service
Job for zabbix-agent.service failed because a configured resource limit was exceeded. See "systemctl status zabbix-agent.service" and "journalctl -xe" for details.


audit log显示：
type=AVC msg=audit(1561790786.393:32165): avc:  denied  { open } for  pid=22154 comm="zabbix_agentd" path="/var/log/zabbix/zabbix_ag        entd.log" dev="dm-0" ino=101314435 scontext=system_u:system_r:zabbix_agent_t:s0 tcontext=system_u:object_r:var_log_t:s0 tclass=file

可以看到目标安全上下文不一致导致访问不了。把target改成zabbix_log_t即可：chcon -t zabbix_log_t /var/log/zabbix/zabbix_agentd.log
-rw-rw-r--. root root system_u:object_r:var_log_t:s0   zabbix_agentd.log
-rw-rw-r--. root root system_u:object_r:zabbix_log_t:s0 zabbix_agentd.log

再次启动zabbix-agent，发现还是报告
Redirecting to /bin/systemctl start  zabbix-agent.service
Job for zabbix-agent.service failed because a configured resource limit was exceeded. See "systemctl status zabbix-agent.service" and "journalctl -xe" for details.

audit log显示：
type=SERVICE_START msg=audit(1561794833.637:33769): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=zabbix-agent comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
type=SERVICE_STOP msg=audit(1561794833.637:33770): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=zabbix-agent comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
type=AVC msg=audit(1561794833.712:33771): avc:  denied  { dac_override } for  pid=28598 comm="zabbix_agentd" capability=1  scontext=system_u:system_r:zabbix_agent_t:s0 tcontext=system_u:system_r:zabbix_agent_t:s0 tclass=capability
type=SYSCALL msg=audit(1561794833.712:33771): arch=c000003e syscall=2 success=no exit=-13 a0=7f4e9472f1c0 a1=241 a2=1b6 a3=24 items=0 ppid=28597 pid=28598 auid=4294967295 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=(none) ses=4294967295 comm="zabbix_agentd" exe="/usr/sbin/zabbix_agentd" subj=system_u:system_r:zabbix_agent_t:s0 key=(null)
type=SERVICE_START msg=audit(1561794833.713:33772): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=zabbix-agent comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=failed'
可以看到已经不是之前的错误了。报告了dac_override这个就好办了，而且zabbix的log也说明了这个错误

zabbix的log显示报告如下错误：
zabbix_agentd [1164]: cannot create PID file [/run/zabbix/zabbix_agentd.pid]: [13] Permission denied
这种情况应该就是linux的DAC访问控制导致的，修改/run/zabbix文件所有者为root即可：chown -R root:root /run/zabbix


启动zabbix后zabbix的log又显示报告如下错误：
cannot set resource limit: [13] Permission denied
这玩意应该是setrlimit没有设置成功导致的。


audit log又显示：
type=SERVICE_START msg=audit(1561796037.387:34257): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=zabbix-agent comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
type=SERVICE_STOP msg=audit(1561796037.387:34258): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=zabbix-agent comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
type=AVC msg=audit(1561796037.403:34259): avc:  denied  { setrlimit } for  pid=30007 comm="zabbix_agentd" scontext=system_u:system_r:zabbix_agent_t:s0 tcontext=system_u:system_r:zabbix_agent_t:s0 tclass=process
type=SYSCALL msg=audit(1561796037.403:34259): arch=c000003e syscall=160 success=no exit=-13 a0=4 a1=7ffd83e1f890 a2=0 a3=8 items=0 ppid=30006 pid=30007 auid=4294967295 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=(none) ses=4294967295 comm="zabbix_agentd" exe="/usr/sbin/zabbix_agentd" subj=system_u:system_r:zabbix_agent_t:s0 key=(null)
type=SERVICE_START msg=audit(1561796037.404:34260): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=zabbix-agent comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=failed'


再次google，解决方法有两种：https://arrfab.net/posts/2016/Nov/25/zabbix-selinux-and-centos-731611/
So in the mean time, what you have to do is :
1. either put zabbix_agent_t into permissive mode with semanage permissive -a zabbix_agent_t
2. either build and distribute a custom selinux policy in your infra (preferred method for me)
既然第二种方法看起来优雅一点，那我们也用第二种方法吧。需要把如下的代码编译成selinux的策略模块。然后安装。

module local-zabbix 1.0;

require {
    type zabbix_agent_t;
    class process setrlimit;
}

#============= zabbix_agent_t ==============
allow zabbix_agent_t self:process setrlimit;


执行如下3条指令：https://wiki.gentoo.org/wiki/SELinux/Tutorials/Creating_your_own_policy_module_file，https://wiki.centos.org/HowTos/SELinux#customizing-local-policy，https://wiki.centos.org/HowTos/SELinux
checkmodule -M -m -o local-zabbix.mod local-zabbix.te
semodule_package -o local-zabbix.pp -m local-zabbix.mod
semodule -i local-zabbix.pp

当然zabbix-agent是安装了很多台，只需要编译一次，然后在每台执行安装.pp文件就可以了。

先把selinux 开启为enforce。
setenforce 1
然后再次启动zabbix-agent：wow. make it!!! 竟然神奇的启动了。再也不用担心重新设置selinux为enforcing后zabbix-agent会失败了。

到此坑已踩完。
