# /etc/shadow

## 简介Name
shadow - 影子密码文件。shadowed password file

## 描述Description

shadow是一个文件，它包含了系统账号的密码信息和可选的时间信息。shadow is a file which contains the password information for the system's accounts and optional aging information.

如果要维护系统密码安全性，这个文件必须是对普通用户（除root外的用户）不可读的。This file must not be readable by regular users if password security is to be maintained.

文件的每一行包含9个域，它们用冒号（":"）分割，而且按照如下的顺序：。Each line of this file contains 9 fields, separated by colons (":"), in the following order:

```
登录名:加密的密码:最近一次密码更改的时间:最小密码年龄:最大密码年龄:密码过期警告期限:密码不活动期限:账号过期日期:保留域

login name:encrypted password:date of last password change:minimum password age:maximum password age:password warning period:password inactivity period:account expiration date:reserved field
```

### login name
必须是存在于系统中的合法账号名称。It must be a valid account name, which exist on the system.

### encrypted password
参考```crypt(3)```对这个字符串的详细解释。Refer to crypt(3) for details on how this string is interpreted.

如果这个密码域包含一些不是```crypt(3)```结果的合法字符串，例如!或者*，用户将不能使用unix密码登录（但是用户可以通过其他方式登录进系统）。If the password field contains some string that is not a valid result of crypt(3), for instance ! or *, the user will not be able to use a unix password to log in (but the user may log in the system by other means).

这个域可能是空的，在这种情况下，不需要密码来验证指定的登录名。然而，如果密码域是空的，一些应用读取了/etc/shadow文件后可能决定不允许访问了。This field may be empty, in which case no passwords are required to authenticate as the specified login name. However, some applications which read the /etc/shadow file may decide not to permit any access at all if the password field is empty.

密码域以感叹号!开始意味着，密码被锁定了。剩余的字符呈现了密码被锁定之前的密码域。A password field which starts with a exclamation mark means that the password is locked. The remaining characters on the line represent the password field before the password was locked.

### date of last password change
最后一次修改密码的日期，用自从1970年1月1日的天数表示。0有特殊的含义，那就是用户在下一次登录进系统时应该修改他的密码。The date of the last password change, expressed as the number of days since Jan 1, 1970.
The value 0 has a special meaning, which is that the user should change her pasword the next time she will log in the system.

An empty field means that password aging features are disabled.

### minimum password age
最小密码年龄是一个天数，用户必须等待直到被允许再次改变的天数，空和0意味着没有最小密码年龄。The minimum password age is the number of days the user will have to wait before she will be allowed to change her password again.
An empty field and value 0 mean that there are no minimum password age.

### maximum password age
最大密码年龄也是一个天数，在这个天数之后，用户必须修改密码。在经过这些天数之后，密码可能仍然有效，用户在下次登录时，将被询问修改密码。The maximum password age is the number of days after which the user will have to change her password.
After this number of days is elapsed, the password may still be valid. The user should be asked to change her password the next time she will log in.

An empty field means that there are no maximum password age, no password warning period, and no password inactivity period (see below).

If the maximum password age is lower than the minimum password age, the user cannot change her password.

### password warning period
密码过期前的天数，在这个天数内用户将被警告。空和0意味着没有密码警告期限。The number of days before a password is going to expire (see the maximum password age above) during which the user should be warned.
An empty field and value 0 mean that there are no password warning period.

### password inactivity period
密码过期后的天数（参考上面的最大密码年龄），在这个天数内，密码应该仍然被接受（而且用户在下次登录时应该修改密码）。在密码过期且此过期期限已过之后，无法使用当前用户的密码登录。用户应该联系管理员。The number of days after a password has expired (see the maximum password age above) during which the password should still be accepted (and the user should update her password during the next login).
After expiration of the password and this expiration period is elapsed, no login is possible using the current user's password. The user should contact her administrator.

空意味着不强制无活动期限。An empty field means that there are no enforcement of an inactivity period.

### account expiration date
账号过期日期。用自从1970年1月1日以来的天数表示。需要注意账号过期不同于密码过期。账号过期情况下，用户可能不被允许登录。密码过期情况下，用户不被允许使用这个密码登录。The date of expiration of the account, expressed as the number of days since Jan 1, 1970.
Note that an account expiration differs from a password expiration. In case of an acount expiration, the user shall not be allowed to login. In case of a password expiration, the user is not allowed to login using her password.

空意味着账号永不过期。An empty field means that the account will never expire.

0值不应该被使用，因为0被解释成要么一个账号不过期，要么在1970年1月1日过期。The value 0 should not be used as it is interpreted as either an account with no expiration, or as an expiration on Jan 1, 1970.

### reserved field
保留域，为未来使用。This field is reserved for future use.

## Files
- /etc/passwd

    User account information.
- /etc/shadow

    Secure user account information.
- /etc/shadow-

    /etc/shadow的备份文件。Backup file for /etc/shadow.

    这个文件被shadow工具套件使用，而不是所有的用户和密码管理工具。Note that this file is used by the tools of the shadow toolsuite, but not by all user and password management tools.

## 参考
[shadow](https://linux.die.net/man/5/shadow)