# git命令

[参考git-book](https://git-scm.com/book/zh/v1/)


## git基础

### 撤销操作

1. 修改最后一次提交

    ```bash
    git commit --amend
    ```
    此命令将使用当前的暂存区域快照提交。如果刚才提交完没有作任何改动，直接运行此命令的话，相当于有机会重新编辑提交说明，但将要提交的文件快照和之前的一样。

    启动文本编辑器后，会看到上次提交时的说明，编辑它确认没问题后保存退出，就会使用新的提交说明覆盖刚才失误的提交。

    如果刚才提交时忘了暂存某些修改，可以先补上暂存操作，然后再运行 ```--amend``` 提交：

    ```bash
    git commit -m 'initial commit'
    git add forgotten_file
    git commit --amend
    ```
    上面的三条命令最终只是产生一个提交，第二个提交命令修正了第一个的提交内容。

