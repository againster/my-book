# GNU链接器ld

## 参数解析

```bash
-rpath PATH
    #运行时共享库搜索路径, "Set runtime shared library search path"

-rpath-link PATH
    #链接时共享库搜索路径, "Set link time shared library search path"
```

date: 20180416
___
