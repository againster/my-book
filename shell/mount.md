# mount

[mount参考](https://linux.die.net/man/8/mount)

[mount.cifs参考](https://linux.die.net/man/8/mount.cifs)

## 参数解析

```bash
#选项：
-o, --options <list> #用逗号分隔的挂载选项

-t, --types <list> #表明文件系统类型，当前支持的有：adfs, affs, autofs, cifs, coda, coherent, cramfs, debugfs, devpts, efs, ext, ext2, ext3, ext4, hfs, hfsplus, hpfs, iso9660, jfs, minix, msdos, ncpfs, nfs, nfs4, ntfs, proc, qnx4, ramfs, reiserfs, romfs, squashfs, smbfs, sysv, tmpfs, ubifs, udf, ufs, umsdos, usbfs, vfat, xenix, xfs, xiafs
```

## 应用示例

1. 挂载网络共享文件夹： [参考1](https://www.cyberciti.biz/tips/how-to-mount-remote-windows-partition-windows-share-under-linux.html), [参考2](https://www.centos.org/docs/5/html/5.2/Deployment_Guide/s1-samba-mounting.html)
    
    ```bash
    mount -t cifs -o username=test,password=test "//200.200.0.3/临时文件夹/lizhi/" "/mnt/0.3/"
    ```
2. 挂载iso映像文件：[参考](https://www.tecmint.com/how-to-mount-and-unmount-an-iso-image-in-linux/)
    
    ```bash
    mount -t iso9660 -o loop /media/CentOS-7-x86_64-Everything-1804.iso /mnt/CentOS
    ```

date:20180711
___