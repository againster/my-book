#!/bin/bash
# to rotate
monthday=$(date "+%m%d")
logpath=/opt/data/rsyslog
logrotateconf=/etc/logrotate.d/logrotate_cut.conf
syslogpidfile=/run/syslogd.pid
test -d $logpath/$monthday || mkdir -p $logpath/$day
ln -sfT $logpath/$monthday $logpath/today
logrotate -v $logrotateconf || { echo "log rotate failed"; exit 1; }
test -s $syslogpidfile && kill -HUP $(cat $syslogpidfile)

# to clean
limitcount=90 # 90 days
dirs=$logpath

function older(){
    echo `stat --format="%y %n" $1/* | sort | head -n 1| awk '{print $4}'`
}

function countof(){
    echo `ls -1 $1 | grep -oP "\d+" | wc -l`
}

if [ ! -d $dirs ]; then
    echo "$dirs not exist, log rotate exit"
    exit 1
fi

while true; do
    count=$(countof $dirs)
    echo "$dirs count: $count, limit count: $limitcount"
    if (( $count > $limitcount )); then
        old=$(older $dirs)
        echo "ready to rm $old"
        rm -rf $old || { echo "rm $old failed"; exit 1; }
    else
        break;
    fi  
done

echo "log rotate OK"
