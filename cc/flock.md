# flock

## 简介

flock - 在打开的文件上应用或者移除劝告锁。flock - apply or remove an advisory lock on an open file

## 语法Synopsis

```c
#include <sys/file.h>
int flock(int fd, int operation);
```

## 描述Description

在一个打开的文件上应用或者移除一个劝告锁，operation参数是下列之一：。Apply or remove an advisory lock on the open file specified by fd. The argument operation is one of the following:

- LOCK_SH 放置一个共享锁。在给定的时间在给定的文件上，超过一个进程可以拥有一个共享锁。Place a shared lock. More than one process may hold a shared lock for a given file at a given time.
- LOCK_EX 放置一个互斥锁。在给定的时间在给定的文件上，仅仅一个进程可以拥有一个互斥锁。Place an exclusive lock. Only one process may hold an exclusive lock for a given file at a given time.
- LOCK_UN 拥有锁的进程移除一个已存在的锁。Remove an existing lock held by this process.

如果一个不兼容的锁被其他进程拥有，调用```flock()```可能会阻塞。为了不阻塞请求，要包含```LOCK_NB```（by ORing）与以上的operations，即```LOCK_NB | LOCK_EX```传递给第二个参数operations。A call to flock() may block if an incompatible lock is held by another process. To make a nonblocking request, include LOCK_NB (by ORing) with any of the above operations.

某一个单独的文件不会并发地同时既拥有共享锁，又拥有互斥锁。A single file may not simultaneously have both shared and exclusive locks.

被```flock()```创建的锁，与打开的文件表项(open file table entry)是关联的。这意味着重复的文件描述符（通过fork(2)，或dup(2)创建），引用了相同的锁，而且这个锁可以通过这些描述符被修改或者释放。而且，这些锁可以被释放，要么通过明确的```LOCK_UN```操作，即调用```flock(fd,LOCK_UN)```，要么当所有的这些文件描述符都被关闭。Locks created by flock() are associated with an open file table entry. This means that duplicate file descriptors (created by, for example, fork(2) or dup(2)) refer to the same lock, and this lock may be modified or released using any of these descriptors. Furthermore, the lock is released either by an explicit LOCK_UN operation on any of these duplicate descriptors, or when all such descriptors have been closed.

如果一个进程使用```open(2)```（或者相似的函数）去在一个相同的文件上获取超过一个文件描述符，这些文件描述符将被```flock()```独立的对待。使用这些文件描述符，尝试去锁定这个文件时，可能被拒绝，当调用的进程通过另外一个描述符已经放置了一个锁。If a process uses open(2) (or similar) to obtain more than one descriptor for the same file, these descriptors are treated independently by flock(). An attempt to lock the file using one of these file descriptors may be denied by a lock that the calling process has already placed via another descriptor.

一个进程在一个文件上仅仅拥有一种类型的锁（共享的或者互斥的）。随后的在已经锁定的文件上的```flock()```调用将转换一个已经存在的锁模式到一个新的锁模式，即共享锁或者互斥锁类型会转换。A process may only hold one type of lock (shared or exclusive) on a file. Subsequent flock() calls on an already locked file will convert an existing lock to the new lock mode.

```flock()```创建的锁通过```execve(2)```保存。Locks created by flock() are preserved across an execve.

无论打开文件的模式如何，共享的、互斥的锁其一都可以被放置在一个文件上。A shared or exclusive lock can be placed on a file regardless of the mode in which the file was opened.

## 返回值Return Value

成功返回0，错误返回-1，errno是更精确的。On success, zero is returned. On error, -1 is returned, and errno is set appropriately.

## 错误Errors

- EBADF 文件描述符未被打开。fd is not an open file descriptor.
- EINTR 当等待请求锁时，调用方被中断通过信号传递捕获。While waiting to acquire a lock, the call was interrupted by delivery of a signal caught by a handler; see signal(7).
- EINVAL 操作部合法。operation is invalid.
- ENOLCK 当分配lock records时，内核内存溢出。The kernel ran out of memory for allocating lock records.
- EWOULDBLOCK 文件被锁，而且LOCK_NB标记被选定。The file is locked and the LOCK_NB flag was selected.

## 注意Notes

```flock()```在NFS文件系统上不会锁定文件，使用```fcntl(2)```取代：给定一个足够现代的Linux版本而且服务器支持锁定，这个函数将会在NFS系统上工作。flock() does not lock files over NFS. Use fcntl(2) instead: that does work over NFS, given a sufficiently recent version of Linux and a server which supports locking.

自从2.0内核以来，```flock()```被实现成了一个系统调用，它以它自己的方式运作而不是在模拟GNU C库中当做```fcntl(2)```的调用。这将产生真正的BSD语义：在通过```flock```和```fcntl(2)```放置锁时，两者没有相互作用相互影响，而且```flock()```不会探测死锁。Since kernel 2.0, flock() is implemented as a system call in its own right rather than being emulated in the GNU C library as a call to fcntl(2). This yields true BSD semantics: there is no interaction between the types of lock placed by flock() and fcntl(2), and flock() does not detect deadlock.

```flock```仅仅放置劝告锁，在文件上给定合适的权限，而一个进程可以自由的去忽略```flock()```的使用，自由的在文件上执行I/O操作。flock() places advisory locks only; given suitable permissions on a file, a process is free to ignore the use of flock() and perform I/O on the file.

```flock```和```fcntl(2)```锁定，在```fork```子进程和```dup(2)```时有不同的语义。在通过```fcntl(2)```实现```flock()```的系统上，```flock()```的语义不同于在本页的描述。flock() and fcntl(2) locks have different semantics with respect to forked processes and dup(2). On systems that implement flock() using fcntl(2), the semantics of flock() will be different from those described in this manual page.

转换锁（共享锁到互斥锁，或者反过来）不被确保是原子的：这个存在的锁首先被移除，随后新的锁被建立。在这两步中间，另一个进程的挂起的锁定请求可能被准许，导致转换结果要么被阻塞，要么当```LOCK_NB```给定时失败（这个是原始BSD系统的行为，而且发生在了许多其他功能实现上）。Converting a lock (shared to exclusive, or vice versa) is not guaranteed to be atomic: the existing lock is first removed, and then a new lock is established. Between these two steps, a pending lock request by another process may be granted, with the result that the conversion either blocks, or fails if LOCK_NB was specified. (This is the original BSD behavior, and occurs on many other implementations.)

## 参考

[flock(2) - Linux man page](https://linux.die.net/man/2/flock)