# Linux namespace - IPC

## 什么是IPC(Inter-Process Communication) namespace？

IPC namespace用来隔离System V IPC objects和POSIX message queues。其中System V IPC objects包含Message queues、Semaphore sets和Shared memory segments.

对于其他几种IPC而言：

1. signal没必要隔离，因为它和pid密切相关，当pid隔离后，signal自然就隔离了，能不能跨pid namespace发送signal则由pid namespace决定。
2. pipe好像也没必要隔离，对匿名pipe来说，只能在父子进程之间通讯，所以隔离的意义不大，而命名管道和文件系统有关，所以只要做好文件系统的隔离，命名管道也就隔离了。
3. socket和协议栈有关，而不同的network namespace有不同的协议栈，所以socket就被network namespace隔离了。


## namespace的相关工具

从本文开始，不再像介绍UTS namespace那样自己写代码，而是用ubuntu 16.04中现成的两个工具，他们的实现和上一篇文章中介绍UTS namespace时的代码类似，只是多了一些参数处理。

1. nsenter：加入指定进程的指定类型的namespace，然后执行参数中指定的命令。详情请参考[帮助文档](http://man7.org/linux/man-pages/man1/nsenter.1.html)和[代码](https://github.com/karelzak/util-linux/blob/master/sys-utils/nsenter.c)。
2. unshare：离开当前指定类型的namespace，创建且加入新的namespace，然后执行参数中指定的命令。详情请参考[帮助文档](http://man7.org/linux/man-pages/man1/unshare.1.html)和[代码](https://github.com/karelzak/util-linux/blob/master/sys-utils/unshare.c)。

## IPC namespace实战

接下来，我们将以消息队列为例，演示一下IPC隔离效果，在本例中将用到两个ipc相关的命令：

1. ipcmk：创建shared memory segments, message queues, 和semaphore arrays
2. ipcs：查看shared memory segments, message queues, 和semaphore arrays的相关信息

为了使演示更直观，我们在创建新的ipc namespace的时候，同时也创建新的uts namespace，然后为新的utsnamespace设置新hostname，这样就能通过shell提示符一眼看出这是属于新的namespace的bash。


首先，我们先查询当前的uts和ipc namespace number：

```
[root@devops-1 testc]# readlink /proc/$$/ns/uts
uts:[4026532185]
[root@devops-1 testc]# readlink /proc/$$/ns/ipc 
ipc:[4026531839]
```

接下来，我们记录现有的ipc Message Queues。
Ps：默认情况下没有message queue。

```bash
ipcs -q
```

结果如下：

```bash
[root@devops-1 testc]# ipcs -q

------ Message Queues --------
key        msqid      owner      perms      used-bytes   messages    
```

下面，我们创建一个message queue：

```
ipcmk -Q
```

然后再次查询当前的ipc Message Queues：

```
ipcs -q
```

可以看到，已经能查询到一个queue：

```bash
[root@devops-1 testc]# ipcmk -Q
Message queue id: 0
[root@devops-1 testc]# ipcs -q

------ Message Queues --------
key        msqid      owner      perms      used-bytes   messages    
0xa03c3dc9 0          root       644        0            0           

```

下面，我们使用`unshare`命令创建新的ipc和uts namespace，并且在新的namespace中启动bash：

```
unshare -iu /bin/bash
```

其中，-i表示启动新的ipc namespace，-u表示启动新的uts namespace。
现在，我们可以验证下ipc和uts的namespace是否发生了变化：

```
[root@devops-1 testc]# readlink /proc/$$/ns/uts
uts:[4026532186]
[root@devops-1 testc]# readlink /proc/$$/ns/ipc 
ipc:[4026532187]
```

可以看到uts和ipc的namespace已经发生了变化。

下面，我们可以修改hostname已验证uts是否有效更新：

```
hostname container001
exec bash # 当hostname改变后，bash不会自动修改它的命令行提示符，所以运行exec bash重新加载bash
```

此时，可以看到命令行提示符已经发生了变化：

```bash
[root@devops-1 testc]# hostname container001
[root@devops-1 testc]# exec bash
[root@container001 testc]# 
```

接下来，我们可以查看message queues：

```bash
ipcs -q
```

结果如下：

```bsh
[root@container001 testc]# ipcs -q

------ Message Queues --------
key        msqid      owner      perms      used-bytes   messages    

```

可以看到，我们已经查询不到原来namespace里面的消息队列了，说明IPC已经被隔离了。

下面，我们创建一条新的message queue并查询：

```
ipcmk -Q
ipcs -q
```

```bash
[root@container001 testc]# ipcmk -Q
Message queue id: 0
[root@container001 testc]# ipcs -q

------ Message Queues --------
key        msqid      owner      perms      used-bytes   messages    
0x8ed400cf 0          root       644        0            0           

```

下面，我们打开一个新的窗口，并查询消息队列信息，看是否受到新的namespace的影响：

```
ipcs -q
```

```bash
[root@devops-1 testc]# ipcs -q

------ Message Queues --------
key        msqid      owner      perms      used-bytes   messages    
0xa03c3dc9 0          root       644        0            0           

```

可以看到，无论是hostname还是消息队列的key，都与之前一致，没有受到新的namespace的修改影响。

现在，我们试着在当前窗口加入之前创建的新的namespace：

```
nsenter -t 12291 -u -i /bin/bash
```

其中：-t后面跟pid用来指定加入哪个进程所在的namespace。

```bash
[root@devops-1 testc]# nsenter -t 12291 -u -i /bin/bash
[root@container001 testc]# ipcs -q

------ Message Queues --------
key        msqid      owner      perms      used-bytes   messages    
0x8ed400cf 0          root       644        0            0           

```

可以看到，加入成功后，bash的提示符自动变过来了；同时，查询消息队列也可以查询到新的namespace下的消息队列了。


## 总结

本文中介绍了IPC namespace和两个常用的跟namespace相关的工具，从实战过程可以看出，IPC namespace差不多和UTS namespace一样简单，没有太复杂的逻辑，也没有父子namespace关系。
不过后续将要介绍的其他类型的namespace就要相对复杂多了。