# linux namespace - network

## 什么是network namespace？

network namespace用来隔离网络设备, IP地址, 端口等。 每个namespace将会有自己独立的网络栈，路由表，防火墙规则，socket等。

每个新的network namespace默认有一个本地环回接口，除了lo接口外，所有的其他网络设备（物理/虚拟网络接口，网桥等）只能属于一个network namespace。每个socket也只能属于一个network namespace。

当新的network namespace被创建时，lo接口默认是关闭的，需要自己手动启动。

标记为"local devices"的设备不能从一个namespace移动到另一个namespace，比如loopback, bridge, ppp等。

我们可以通过ethtool -k命令来查看设备的是否为local devices：

```bash
ethtool -k lo
```

结果如下：

```txt
root@raspberrypi:/home/pi# ethtool -k lo
Features for lo:
rx-checksumming: on [fixed]
tx-checksumming: on
        tx-checksum-ipv4: off [fixed]
        tx-checksum-ip-generic: on [fixed]
        tx-checksum-ipv6: off [fixed]
        tx-checksum-fcoe-crc: off [fixed]
        tx-checksum-sctp: on [fixed]
scatter-gather: on
        tx-scatter-gather: on [fixed]
        tx-scatter-gather-fraglist: on [fixed]
tcp-segmentation-offload: on
        tx-tcp-segmentation: on
        tx-tcp-ecn-segmentation: on
        tx-tcp-mangleid-segmentation: on
        tx-tcp6-segmentation: on
udp-fragmentation-offload: off
generic-segmentation-offload: on
generic-receive-offload: on
large-receive-offload: off [fixed]
rx-vlan-offload: off [fixed]
tx-vlan-offload: off [fixed]
ntuple-filters: off [fixed]
receive-hashing: off [fixed]
highdma: on [fixed]
rx-vlan-filter: off [fixed]
vlan-challenged: on [fixed]
tx-lockless: on [fixed]
netns-local: on [fixed]
tx-gso-robust: off [fixed]
tx-fcoe-segmentation: off [fixed]
tx-gre-segmentation: off [fixed]
tx-gre-csum-segmentation: off [fixed]
tx-ipxip4-segmentation: off [fixed]
tx-ipxip6-segmentation: off [fixed]
tx-udp_tnl-segmentation: off [fixed]
tx-udp_tnl-csum-segmentation: off [fixed]
tx-gso-partial: off [fixed]
tx-sctp-segmentation: on
tx-esp-segmentation: off [fixed]
tx-udp-segmentation: off [fixed]
fcoe-mtu: off [fixed]
tx-nocache-copy: off [fixed]
loopback: on [fixed]
rx-fcs: off [fixed]
rx-all: off [fixed]
tx-vlan-stag-hw-insert: off [fixed]
rx-vlan-stag-hw-parse: off [fixed]
rx-vlan-stag-filter: off [fixed]
l2-fwd-offload: off [fixed]
hw-tc-offload: off [fixed]
esp-hw-offload: off [fixed]
esp-tx-csum-hw-offload: off [fixed]
rx-udp_tunnel-port-offload: off [fixed]
tls-hw-tx-offload: off [fixed]
tls-hw-rx-offload: off [fixed]
rx-gro-hw: off [fixed]
tls-hw-record: off [fixed]
```

其中，on表示该设备不能被移动到其他network namespace。


## 实战

接下来，我们将会通过一个实例演示如何创建新的network namespace并同外面的namespace进行通信。

首先记录当前的network namespace信息：

```bash
readlink /proc/$$/ns/net
```

接下来，我们可以创建新的network namespace：

```bash
unshare --uts --net /bin/bash
hostname container001
exec bash
readlink /proc/$$/ns/net
```

结果如下：

```txt
root@raspberrypi:/home/pi# readlink /proc/$$/ns/net
net:[4026531905]
root@raspberrypi:/home/pi# unshare --uts --net /bin/bash
root@raspberrypi:/home/pi# hostname container001
root@raspberrypi:/home/pi# exec bash
root@container001:/home/pi# readlink /proc/$$/ns/net
net:[4026532271]
```

可以看到，当前进程的network namespace已经发送了变化。

接下来，我们可以通过`ifconfig`命令查询net信息，但实际上命令返回为空。

接下来，我们可以启动lo，并再次查询net信息：

```bash
ip link set lo up
ifconfig
```

结果如下：

```txt
root@container001:/home/pi# ifconfig
root@container001:/home/pi# ip link set lo up
root@container001:/home/pi# ifconfig
lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

同时，我们记录下当前namespace bash的进程号：

```bash
echo $$
```

结果如下：

```txt
root@container001:/home/pi# echo $$
1470
```

接下来，我们再次打开一个新的终端，创建新的虚拟以太网设备，让两个namespace能通讯：

```bash
ip link add veth0 type veth peer name veth1
# 将veth1移动到上面第一个窗口中的namespace
ip link set veth1 netns 1470 # 1470为container001 namespace中bash的进程ID
```

接下来，为veth0分配IP并启动veth0：

```bash
ip address add dev veth0 192.168.8.1/24
ip link set veth0 up
```

查询veth0相关信息：

```bash
ifconfig veth0
```

结果如下：

```txt
root@raspberrypi:~# ip link add veth0 type veth peer name veth1
root@raspberrypi:~# ip link set veth1 netns 1470
root@raspberrypi:~# ip address add dev veth0 192.168.8.1/24
root@raspberrypi:~# ip link set veth0 up
root@raspberrypi:~# ifconfig veth0
veth0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.8.1  netmask 255.255.255.0  broadcast 0.0.0.0
        ether 92:f8:96:a2:65:ed  txqueuelen 1000  (Ethernet)
        RX packets 29  bytes 4936 (4.8 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 29  bytes 4936 (4.8 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```

此时，我们再次回到第一个窗口，即新的container001 namespace中，为veth1分配IP地址并启动它：

```bash
ip address add dev veth1 192.168.8.2/24
ip link set veth1 up
ifconfig veth1
```

结果如下：

```txt
root@container001:/home/pi# ip address add dev veth1 192.168.8.2/24
root@container001:/home/pi# ip link set veth1 up
root@container001:/home/pi# ifconfig veth1
veth1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.8.2  netmask 255.255.255.0  broadcast 0.0.0.0
        inet6 fe80::74b5:60ff:fe10:18d0  prefixlen 64  scopeid 0x20<link>
        ether 76:b5:60:10:18:d0  txqueuelen 1000  (Ethernet)
        RX packets 38  bytes 6391 (6.2 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 32  bytes 5202 (5.0 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```

此时，可以连接根namespace下的network了：

```bash
ping 192.168.8.1
```

结果如下：

```txt
root@container001:/home/pi# ping 192.168.8.1
PING 192.168.8.1 (192.168.8.1) 56(84) bytes of data.
64 bytes from 192.168.8.1: icmp_seq=1 ttl=64 time=0.231 ms
64 bytes from 192.168.8.1: icmp_seq=2 ttl=64 time=0.076 ms
64 bytes from 192.168.8.1: icmp_seq=3 ttl=64 time=0.081 ms
^C
--- 192.168.8.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 44ms
rtt min/avg/max/mdev = 0.076/0.129/0.231/0.072 ms
```

服务连接成功~

到目前为止，两个namespace之间可以网络通信了，但在container001里还是不能访问外网。下面将通过NAT的方式让container001能够上外网。这部分内容完全是网络相关的知识，跟namespace已经没什么关系了。

回到根namespace的窗口中，确认IP forward是否已经开通，这里1表示开通了：

```bash
cat /proc/sys/net/ipv4/ip_forward
# 1
```

Ps：如果当前查询到为0，则需要执行如下命令开通：

```bash
sysctl -w net.ipv4.ip_forward=1
```

接下来，我们需要添加NAT规则，这里eth0是机器上连接外网的网卡。

```bash
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
```

再次回到新的namespace的创建中，添加默认网关：

```bash
ip route add default via 192.168.8.1
route -n
```

结果如下：

```txt
root@raspberrypi:~# cat /proc/sys/net/ipv4/ip_forward
0
root@raspberrypi:~# sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
root@raspberrypi:~# cat /proc/sys/net/ipv4/ip_forward
1
root@raspberrypi:~# iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
```

到此为止，我们就可以访问外网了。

```txt
root@container001:/home/pi# ping www.baidu.com
PING www.a.shifen.com (14.215.177.38) 56(84) bytes of data.
64 bytes from 14.215.177.38 (14.215.177.38): icmp_seq=1 ttl=53 time=48.8 ms
64 bytes from 14.215.177.38 (14.215.177.38): icmp_seq=2 ttl=53 time=157 ms
64 bytes from 14.215.177.38 (14.215.177.38): icmp_seq=3 ttl=53 time=77.9 ms
^C
--- www.a.shifen.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 5ms
rtt min/avg/max/mdev = 48.804/94.636/157.241/45.831 ms
```

事实上，network namespace的概念比较简单，但如何做好网络的隔离和连通却比较难，包括性能和安全相关的考虑，需要很好的Linux网络知识。


## ip netns介绍

在单独操作network namespace时，ip netns是一个很方便的工具，并且它可以给namespace取一个名字，然后根据名字来操作namespace。那么给namespace取名字并且根据名字来管理namespace里面的进程是怎么实现的呢？可以了解其[源代码](https://github.com/shemminger/iproute2/blob/master/ip/ipnetns.c)。

事实上，给namespace取名字其实就是创建一个文件，然后通过mount --bind将新创建的namespace文件和该文件绑定，就算该namespace里的所有进程都退出了，内核还是会保留该namespace，以后我们还可以通过这个绑定的文件来加入该namespace。
