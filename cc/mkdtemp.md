# mkdtemp(3)

## 简介

mkdtemp - 创建一个唯一的临时目录create a unique temporary directory

## 语法Synopsis

```c
#include <stdlib.h>

char *mkdtemp(char *template);
Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
mkdtemp():
_BSD_SOURCE
|| /* Since glibc 2.10: */
(_POSIX_C_SOURCE >= 200809L || _XOPEN_SOURCE >= 700)

```

## 描述Description

```mkdtemp()```函数从模板生成一个唯一名字的临时目录。模板最后的6个字符必须是XXXXXX，而且它们被替换成一个字符串，这个使文件名字唯一。目录被以0700权限创建。因为它将被修改，所以模板必须不是一个字符串常量，而应该被声明为一个字符数组。The mkdtemp() function generates a uniquely named temporary directory from template. The last six characters of template must be XXXXXX and these are replaced with a string that makes the directory name unique. The directory is then created with permissions 0700. Since it will be modified, template must not be a string constant, but should be declared as a character array.

## 返回值Return Value

```mkdtemp()```成功时，函数返回一个指向修改模板字符串的指针；失败时返回NULL，errno有精确的错误。The mkdtemp() function returns a pointer to the modified template string on success, and NULL on failure, in which case errno is set appropriately.

## 错误Errors

- EINVAL The last six characters of template were not XXXXXX. Now template is unchanged.

## 版本Versions

Available since glibc 2.1.91.

## 参考

[mkdtemp(3)- create unique temporary directory - Linux man page](https://linux.die.net/man/3/mkdtemp)