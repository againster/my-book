# gdb misc (gdb小问题)

## gdb 如何开启和关闭layout

参考：http://sourceware.org/gdb/onlinedocs/gdb/TUI-Keys.html#TUI-Keys

通过快捷键`Ctl + x  + a`可以打开或者关闭layout窗口。

> C-x C-a
>
> C-x a
>
> C-x A
>
> Enter or leave the TUI mode. When leaving the TUI mode, the curses window management stops and GDB operates using its standard mode, writing on the terminal directly. When reentering the TUI mode, control is given back to the curses windows. The screen is then refreshed.
>
> This key binding uses the bindable Readline function `tui-switch-mode`.
>
> - `PgUp`
>
>   Scroll the active window one page up.
>
> - `PgDn`
>
>   Scroll the active window one page down.
>
> - `Up`
>
>   Scroll the active window one line up.
>
> - `Down`
>
>   Scroll the active window one line down.
>
> - `Left`
>
>   Scroll the active window one column left.
>
> - `Right`
>
>   Scroll the active window one column right.
>
> - C-L
>
>   Refresh the screen.

## gdb 如何设置源码搜索路径

参考：https://alex.dzyoba.com/blog/gdb-source-path/

通过`directory <dir>`指令来添加源码搜索路径。

## gdb 如何切换栈帧调试

参考：https://blog.csdn.net/csdn_dzh/article/details/84592740

可以使用指令`up n`或这`down n`来向上或向下选择函数堆栈帧，n是层数。

```c++
#include <stdio.h>

int func1(int a) {
	return 2 * a;
}

int func2(int a) {
	int c = 0;
	c = 2 * func1(a);
	return c;
}

int func3(int a) {
	int c = 0;
	c = 2 * func2(a);
	return c;
}

int main (int argc, char *argv[]) {
    printf("%d\n", func3(10));
    
    return 0;
}
```

调试过程如下：

```txt
(gdb) b 4
Breakpoint 1 at 0x400567: file bt.cc, line 4.
(gdb) r
Starting program: /home/al/cc.d/gdbthread/./a.out

Breakpoint 1, func1 (a=10) at bt.cc:4
4               return 2 * a;
Missing separate debuginfos, use: debuginfo-install glibc-2.17-317.el7.x86_64 libgcc-4.8.5-44.el7.x86_64 libstdc++-4.8.5-44.el7.x86_64
(gdb) bt
#0  func1 (a=10) at bt.cc:4
#1  0x000000000040059a in func2 (a=10) at bt.cc:9
#2  0x00000000004005ca in func3 (a=10) at bt.cc:15
#3  0x0000000000400602 in main (argc=1, argv=0x7fffffffe4f8) at bt.cc:20
(gdb) up 2
#2  0x00000000004005ca in func3 (a=10) at bt.cc:15
15              c = 2 * func2(a);
(gdb) bt
#0  func1 (a=10) at bt.cc:4
#1  0x000000000040059a in func2 (a=10) at bt.cc:9
#2  0x00000000004005ca in func3 (a=10) at bt.cc:15
#3  0x0000000000400602 in main (argc=1, argv=0x7fffffffe4f8) at bt.cc:20
(gdb) p c
$1 = 0
(gdb) n
func2 (a=10) at bt.cc:10
10              return c;
(gdb) p c
$2 = 40
(gdb) bt
#0  func2 (a=10) at bt.cc:10
#1  0x00000000004005ca in func3 (a=10) at bt.cc:15
#2  0x0000000000400602 in main (argc=1, argv=0x7fffffffe4f8) at bt.cc:20
```

在第9，17行时我们可以看到栈帧时没有变化的。第24行执行`n`时，栈帧变成了29行所示的结果。

## gdb 如何打印当前函数参数，函数局部变量

参考：https://www.cnblogs.com/chengliangsheng/p/3597010.html

- `info args`，打印当前栈帧的参数。
- `info locals`，打印当前栈帧的局部变量。

> info args
>     Argument variables of current stack frame.
>
> info locals
>     Local variables of current stack frame.

















