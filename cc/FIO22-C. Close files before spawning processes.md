# FIO22-C. Close files before spawning processes

标准的FILE对象和它们的底层呈现（在POSIX平台是file descriptors，其它平台是hendlers）都是有限的资源，它们必须被小心地管理。实现确保可以被并发打开的文件的数量，与定义在<stdio.h>中的FOPEN_MAX宏绑定。宏的值被确保至少是8。因此，可移植的程序必须要么避免同一时间保持超过FOPEN_MAX的文件数，要么准备处理像fopen()这样的函数，由于资源耗尽而失败。

再也不需要的文件，当关闭失败时，这可能允许攻击者耗尽，或者操作系统资源。这个现象有时被叫做文件描述符泄漏(file descriptor leakage)，因为文件指针(file pointers)可能被用来作为一个攻击向量。此外，保持文件长时间打开，增加了风险，即写入内存buffers的数据将不会被flush，当在abnormal program termination事件中。为了避免文件描述符泄漏，确保任何被buffer的数据将flush到持久化存储(permanent storage)，当文件不再需要时，它们必须被关闭。

一个程序的行为是未定义的(undefined)，当关联的文件被关闭之后，这个程序仍使用指向这个文件的FILE对象时。关闭了标准的streams(特别是stdout, stderr, stdin)的程序，在随后的函数调用中必须小心地不要去使用流对象(stream objects)，特别是在这些对象上有不明确的操作的函数（例如printf(), perror()，和getc()）

在这个不合法的代码示例中，继承自一个脆弱的OpenBSD的chpass程序，一个包含了敏感的数据的文件，被打开后去读时。这个get_validated_editor()函数从EDITOR环境变量获取了注册的编辑器，而且认为它是一个合法的编辑器，与FIO02-C. Canonicalize path names originating from tainted sources一致。这个函数返回了一个去调用编辑器的命令，这个命令随后被传递给了system()函数。如果system()函数以衍生子进程方式被实现，子进程将继承父进程的文件描述符。如果这个发生了，就像在POSIX系统中做的一样，子进程将能够访问潜在的敏感文件file_name内容。

```c
#include <stdio.h>
#include <stdlib.h>
extern const char *get_validated_editor(void);
void func(const char *file_name) {
FILE *f;
const char *editor;
f = fopen(file_name, "r");
if (f == NULL) {
/* Handle error */
}
editor = get_validated_editor();
if (editor == NULL) {
/* Handle error */
}
if (system(editor) == -1) {
/* Handle error */
}
}
```

如果get_validated_editor()返回的命令将经常是一个简单的路径（像/usr/bin/vim），而且命令运行在POSIX系统，这个程序可以通过调用execve()而不是system()，来更加强健。和ENV33-C. Do not call system()是一致的。

在基于UNIX的系统中，子进程典型地被衍生，是通过使用fork()和exec()函数。而且子进程通常继承了它父进程的任何没有close-on-exec标记的文件描述符。在Microsoft Windows上，file-handle的继承被决定在一个per-file和per-spawned的进程基础。详情查看WIN03-C. Understand HANDLE inheritance。

## Compliant Solution

在这个兼容的方案中，file_name是被关闭的，在运行编辑器之前：

```c
#include <stdio.h>
#include <stdlib.h>
extern const char *get_validated_editor(void);
void func(const char *file_name) {
FILE *f;
const char *editor;
f = fopen(file_name, "r");
if (f == NULL) {
/* Handle error */
}
fclose(f);
f = NULL;
editor = get_validated_editor();
if (editor == NULL) {
/* Handle error */
}
/* Sanitize environment before calling system() */
if (system(editor) == -1) {
/* Handle error */
}
}
```

## Compliant Solution (POSIX)

有时对程序来说，在发出一个系统调用像system()或exec()之前，关闭所有的活动的文件描述符是不切实际的。当有条件时，一个在POSIX上可选项是使用FD_CLOEXEC标记，或者O_CLOEXEC标记。

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
extern const char *get_validated_editor(void);
void func(const char *file_name) {
int flags;
char *editor;
int fd = open(file_name, O_RDONLY);
if (fd == -1) {
/* Handle error */
}
flags = fcntl(fd, F_GETFD);
if (flags == -1) {
/* Handle error */
}
if (fcntl(fd, F_SETFD, flags | FD_CLOEXEC) == -1) {
/* Handle error */
}
editor = get_validated_editor();
if (editor == NULL) {
/* Handle getenv() error */
}
if (system(editor) == -1) {
/* Handle error */
}
}
```

## Compliant Solution (Linux)

一些系统（像那些带有Linux内核版本2.6.23或者之后的）有一个O_CLOEXEC标记，直接在open()提供了这个close-on-exec功能。这些标记被要求通过IEEE Std 1003.1 [IEEE Std 1003.1:2013]。在多线程程序中，如果可能的话，这个标记应该被使用，因为它预防了在open()和fcntl()之间的一个时间空洞，即在哪一个线程可以创建子进程而文件描述符没有close-on-exec标记期间。

```c
#include <stdio.h>
#include <stdlib.h>
extern const char *get_validated_editor(void);
void func(const char *file_name) {
char *editor;
int fd = open(file_name, O_RDONLY | O_CLOEXEC);
if (fd == -1) {
/* Handle error */
}
editor = get_validated_editor();
if (editor == NULL) {
/* Handle error */
}
if (system(editor) == -1) {
/* Handle error */
}
}
```

## 参考

[FIO22-C. Close files before spawning processes](https://wiki.sei.cmu.edu/confluence/display/c/FIO22-C.+Close+files+before+spawning+processes)