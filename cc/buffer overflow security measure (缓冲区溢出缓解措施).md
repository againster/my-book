# 内存漏洞缓解措施

CVE-2020-8597漏洞概况：

蓝军上周协助VPN团队复现了VPN远程任意代码执行漏洞，并完成了相关漏洞的完整利用。本次漏洞(CVE-2020-8597, CVSS 9.8 CRITICAL)是由于协议状态控制不严格导致以及缓冲区长度检验不合格导致的缓冲区溢出，攻击者只需要掌握基本的Linux攻击技巧(ROP, shellcode编写)即可复现该漏洞，利用难度低，在没有开启全部缓解措施的情况下，可以达到认证前RCE的效果，属于最严重的安全漏洞。在进行测试的过程中发现服务器上的程序并没有完全打开缓解措施，只开启了NX。

在不打开缓解措施的情况下，攻击者不需要任何服务器的凭证就可以直接拿到系统的执行权限，由于pppd通常是在root权限下执行，无任何权限的攻击者可以得到系统的最高权限，完全控制开启脆弱服务的服务器。本次完整漏洞利用揭示了目前我们产品线上缓解措施部署不足的严重安全风险，和友商差距很大。本文以下内容为漏洞缓解措施简介。

## 各种内存漏洞缓解措施介绍

当前常见的linux缓解措施有Canary, NX, PIE, Fortify, RelRO，和其他缓解措施他们的功能原理和开启方法如下：

### Canary

通常栈溢出的利用方式是通过溢出存在于栈上的局部变量，从而让多出来的数据覆盖 ebp、eip 等，从而达到劫持控制流的目的。栈溢出保护是一种缓冲区溢出攻击缓解手段，当函数存在缓冲区溢出攻击漏洞时，攻击者可以覆盖栈上的返回地址来让 shellcode 能够得到执行。当启用栈保护后，函数开始执行的时候会先往栈底插入 cookie 信息，当函数真正返回的时候会验证 cookie 信息是否合法 (栈帧销毁前测试该值是否被改变)，如果不合法就停止程序运行 (栈溢出发生)。攻击者在覆盖返回地址的时候往往也会将 cookie 信息给覆盖掉，导致栈保护检查失败而阻止 shellcode 的执行，避免漏洞利用成功。在 Linux 中我们将 cookie 信息称为 Canary。

开启方法：

```bash
gcc -o test test.c ## 默认情况下，不开启Canary保护
gcc -fno-stack-protector -o test test.c  ## 禁用栈保护
gcc -fstack-protector -o test test.c   ## 启用堆栈保护，不过只为局部变量中含有 char 数组的函数插入保护代码
gcc -fstack-protector-all -o test test.c ## 启用堆栈保护，为所有函数插入保护代码
```

### FORTIFY

fority其实非常轻微的检查，用于检查是否存在缓冲区溢出的错误。适用情形是程序采用大量的字符串或者内存操作函数，如memcpy，memset，stpcpy，strcpy，strncpy，strcat，strncat，sprintf，snprintf，vsprintf，vsnprintf，gets以及宽字符的变体。

- _FORTIFY_SOURCE=1，并且将编译器设置为优化1(gcc -O1)，以及出现上述情形，那么程序编译时就会进行检查但又不会改变程序功能。gcc -D_FORTIFY_SOURCE=1 仅仅只会在编译时进行检查 (特别像某些头文件 #include <string.h>)
- FORTIFY_SOURCE=2，有些检查功能会加入，但是这可能导致程序崩溃。gcc -D_FORTIFY_SOURCE=2 程序执行时也会有检查 (如果检查到缓冲区溢出，就终止程序)

开启方法：

```bash
gcc -o test test.c ## 默认情况下，不会开这个检查
gcc -D_FORTIFY_SOURCE=1 -o test test.c ## 较弱的检查
gcc -D_FORTIFY_SOURCE=2 -o test test.c ## 较强的检查
```

### NX

NX即No-eXecute（不可执行）的意思，NX（DEP）的基本原理是将数据所在内存页标识为不可执行，当程序溢出成功转入shellcode时，程序会尝试在数据页面上执行指令，此时CPU就会抛出异常，而不是去执行恶意指令。

开启方法：

```bash
gcc -o test test.c ## 默认情况下，开启NX保护
gcc -z execstack -o test test.c ## 禁用NX保护
gcc -z noexecstack -o test test.c ## 开启NX保护
```

### PIE

 一般情况下NX（Windows平台上称其为DEP）和地址空间分布随机化（ASLR）会同时工作。
内存地址随机化机制（address space layout randomization)，有以下三种情况：

- 0 - 表示关闭进程地址空间随机化。
- 1 - 表示将mmap的基址，stack和vdso页面随机化。
- 2 - 表示在1的基础上增加栈（heap）的随机化。

可以防范基于Ret2libc方式的针对DEP的攻击。ASLR和DEP配合使用，能有效阻止攻击者在堆栈上运行恶意代码。

Built as PIE：位置独立的可执行区域（position-independent executables）。这样使得在利用缓冲溢出和移动操作系统中存在的其他内存崩溃缺陷时采用面向返回的编程（return-oriented programming）方法变得难得多。

开启方法：

```bash
gcc -o test test.c ## 默认情况下，不开启PIE
gcc -fpie -pie -o test test.c ## 开启PIE，此时强度为1
gcc -fPIE -pie -o test test.c ## 开启PIE，此时为最高强度2
gcc -fpic -o test test.c ## 开启PIC，此时强度为1，不会开启PIE
gcc -fPIC -o test test.c ## 开启PIC，此时为最高强度2，不会开启PIE
```

### RELRO

在Linux系统安全领域数据可以写的存储区就会是攻击的目标，尤其是存储函数指针的区域。 所以在安全防护的角度来说尽量减少可写的存储区域对安全会有极大的好处。

GCC, GNU linker以及Glibc-dynamic linker一起配合实现了一种叫做relro的技术: read only relocation。大概实现就是由linker指定binary的一块经过dynamic linker处理过 relocation之后的区域为只读。

设置符号重定向表格为只读或在程序启动时就解析并绑定所有动态符号，从而减少对GOT（Global Offset Table）攻击。RELRO为” Partial RELRO”，说明我们对GOT表具有写权限。

开启方法：

```bash
gcc -o test test.c ## 默认情况下，是Partial RELRO
gcc -z norelro -o test test.c ## 关闭，即No RELRO
gcc -z lazy -o test test.c ## 部分开启，即Partial RELRO
gcc -z now -o test test.c ## 全部开启，即
```


在加上了这些保护之后，会使攻击者利用的难度的提高，在本次漏洞中就是无法使用栈溢出来控制程序的执行流。

## 其他缓解措施

### CFI

Clang实现了一系列控制流完整性保护，在检测到可能使攻击者控制程序执行流的未定义行为时中止程序的运行。

### 混淆

混淆是指对程序做的一种等价转换，转换后的程序有与转换前的程序一样的行为，但是增加了程序逆向工程的难度。如果有需求，蓝军后期可以做编译器插件。
### 其他
待前面缓解措施落实后，还可自定义其他缓解措施。

## 业界情况

### 微软 

**软件缓解措施 **

采用了许多缓解措施如栈溢出检测、数据段执行保护、地址随机化、结构异常处理覆写保护、堆栈元信息保护等技术并逐年完善，使系统中存在的漏洞的可用极大地降低了。

**硬件缓解措施**

微软通过添加管理影子堆栈和影子堆栈页面的硬件保护指令，从硬件上防御缓冲区溢出

### 苹果

**沙盒**

所有第三方 App 均已经过“沙盒化”，因此它们在访问其他 App 储存的文件或对设备进行更改时会受到限制。沙盒化可以防止 App 收集或修改其他 App 储存的信息。每个 App 还拥有唯一的主目录来存放其文件，主目录是在安装 App 时随机分配的。如果第三方 App 需要访问除自身信息以外的其他信息，只能通过 iOS 和 iPadOS 明确提供的服务来实现。

系统文件和资源也会与用户的 App 保持隔离。与所有第三方 App 一样，iOS 和 iPadOS 的绝大部分 App 以非权限用户“mobile”的身份运行。整个操作系统分区以只读方式装载。不必要的工具（如远程登录服务）未包含在系统软件中，并且 API 不允许 App 提升自己的权限来修改其他 App 或者 iOS 和 iPadOS。

**使用授权**

iOS 使用声明的授权来控制第三方 App 对用户信息及功能（如 iCloud 和扩展功能）的访问。授权是签名到 App 的键值对，允许对运行时因素之外的内容（如 UNIX 用户 ID）进行认证。授权已经过数字签名，因此无法更改。系统 App 和监控程序广泛应用授权来进行特定权限操作，如果不使用授权，则进程需要以 root 用户身份运行才能进行这些操作。这极大降低了遭入侵的系统 App 或监控程序提升权限的可能性。

### 华为

华为连基带都上了缓解措施

## 结论与建议

**结论**

本次漏洞属于认证前RCE，利用简单且能够造成极大危害。漏洞被轻易利用的主要原因之一是因为程序没有开启缓解措施，攻击者能够直接利用漏洞泄露信息并得到服务器的控制权限。

目前VPN产品线漏洞缓解措施部署不足。漏洞是修不完的，但是缓解措施一定要及时跟上，极大提高漏洞利用成本。

**建议**

因此，建议将开启内存漏洞缓解措施推广到所有产品，在新编译的产品中开启这些保护，并检查之前存在漏洞的产品中是否有未开启保护的组件，重新编译并开启保护

以下为详细建议： 

所有新版本编译时开启 NX/PIE/Canary/Fortify/ReLRO等基础缓解措施。所有发生过漏洞的服务，重新编译，开启NX/PIE/Canary/Fortify/ReLRO等基础缓解措施，以补丁包形式推送。

时间允许的情况下，所有的组件开启缓解措施，以补丁包形式推送。后期可由蓝军或SDL协助产品部署针对性更强的自定义缓解措施。

成本：NX/PIE/Canary/Fortify/ReLRO等基础缓解措施是非常成熟的方案，引入后会对组件性能稍微有所影响，但可忽略不计。对程序功能性基本上也不会有影响，但需要测试团队切入确保功能正常。