# linux namespace - mount

## 什么是mount namespace？

Mount namespace用来隔离文件系统的挂载点, 使得不同的mount namespace拥有自己独立的挂载点信息，不同的namespace之间不会相互影响，这对于构建用户或者容器自己的文件系统目录非常有用。

当前进程所在mount namespace里的所有挂载信息可以在`/proc/[pid]/mounts`、`/proc/[pid]/mountinfo`和`/proc/[pid]/mountstats`里面找到。

Mount namespaces是第一个被加入Linux的namespace，由于当时没想到还会引入其它的namespace，所以取名为`CLONE_NEWNS`，而没有叫`CLONE_NEWMOUNT`。

每个mount namespace都拥有一份自己的挂载点列表，当用`clone`或者`unshare`函数创建新的mount namespace时，新创建的namespace将拷贝一份老namespace里的挂载点列表，但从这之后，他们就没有关系了，通过mount和umount增加和删除各自namespace里面的挂载点都不会相互影响。


## Demo实战

首先，我们需要先制作两个iso文件，用于后续的mount测试。

```bash
mkdir iso
cd iso/
mkdir -p iso01/subdir01
mkdir -p iso02/subdir02
mkisofs -o ./001.iso ./iso01
mkisofs -o ./002.iso ./iso02
ls
# 001.iso  002.iso  iso01  iso02
```

接下来，我们需要用于准备mount的节点：

```bash
mkdir /mnt/iso1 /mnt/iso2
```

记录当前所在的mount namespace：

```bash
readlink /proc/$$/ns/mnt
# mnt:[4026531840]
```

挂载iso文件到指定节点并验证：

```bash
mkdir /mnt/iso1/
mount -t iso9660 -o loop ./001.iso /mnt/iso1/
mount |grep /001.iso
```

接下来，我们可以创建并进入新的mount和uts namespace：

```bash
unshare --mount --uts /bin/bash
hostname container001
exec bash
```

验证是否进入新的mount namespace：

```bash
readlink /proc/$$/ns/mnt
# mnt:[4026533824]
```

验证老namespace里的挂载点的信息已经拷贝到新的namespace里：

```bash
mount |grep /001.iso
# /tmp/namespace/iso/001.iso on /mnt/iso1 type iso9660 (ro,loop=/dev/loop0)
```

现在，我们在新的namespace中挂载iso：

```bash
mkdir /mnt/iso2/
mount -t iso9660 -o loop ./002.iso /mnt/iso2/
mount |grep iso
# /tmp/namespace/iso/001.iso on /mnt/iso1 type iso9660 (ro,loop=/dev/loop0)
# /tmp/namespace/iso/002.iso on /mnt/iso2 type iso9660 (ro,loop=/dev/loop2)
```

接下来，我们在新的namespace中卸载iso1：

```bash
umount /mnt/iso1
```

此时，可以验证/mnt/iso1目录变为空：

```bash
tree /mnt/
```

结果如下：

```txt
/mnt/
├── iso1
└── iso2
    └── subdir02
```

现在，我们打开一个新的窗口，进入老的namespace中查询信息：

```bash
tree /mnt/
```

结果如下：

```txt
/mnt
├── iso1
│?? └── subdir01
└── iso2
```

这验证了两个namespace中的mount信息是隔离的。


## Shared subtrees

在某些情况下，比如系统添加了一个新的硬盘，这个时候如果mount namespace是完全隔离的，想要在各个namespace里面用这个硬盘，就需要在每个namespace里面手动mount这个硬盘，这个是很麻烦的，这时[Shared subtrees](https://www.kernel.org/doc/Documentation/filesystems/sharedsubtree.txt)就可以帮助我们解决这个问题。

关于Shared subtrees的详细介绍请参考[Linux mount](https://segmentfault.com/a/1190000006899213)，里面有他的详细介绍以及bind nount的例子。


## Shared subtrees实战

对Shared subtrees而言，mount namespace和bind mount的情况差不多，这里就简单演示一下shared和private两种类型。

准备4个虚拟的disk，并在上面创建ext2文件系统，用于后续的mount测试：

```bash
mkdir disks && cd disks
dd if=/dev/zero bs=1M count=32 of=./disk1.img
dd if=/dev/zero bs=1M count=32 of=./disk2.img
dd if=/dev/zero bs=1M count=32 of=./disk3.img
dd if=/dev/zero bs=1M count=32 of=./disk4.img

mkfs.ext2 ./disk1.img
mkfs.ext2 ./disk2.img
mkfs.ext2 ./disk3.img
mkfs.ext2 ./disk4.img
```

准备两个目录用于挂载上面创建的disk：

```bash
mkdir disk1 disk2
ls
# disk1  disk2  disk1.img  disk2.img  disk3.img  disk4.img
```

显式的分别以shared和private方式挂载disk1和disk2:

```bash
mount -t ext2 ./disk1.img ./disk1
mount --make-shared ./disk1
mount -t ext2 ./disk2.img ./disk2
mount --make-private ./disk2
```

验证挂载方式：

```bash
cat /proc/self/mountinfo |grep disk| sed 's/ - .*//'
# 164 24 7:1 / /home/dev/disks/disk1 rw,relatime shared:105
# 173 24 7:2 / /home/dev/disks/disk2 rw,relatime
```

记录当前mount namespace编号：

```bash
readlink /proc/$$/ns/mnt
# mnt:[4026531840]
```

下面，我们重新打开一个新的窗口，创建新的mount namespace。默认情况下，unshare会将新namespace里面的所有挂载点的类型设置成private，所以这里用到了参数--propagation unchanged，让新namespace里的挂载点的类型和老namespace里保持一致。

--propagation参数还支持private|shared|slave类型，和mount命令的那些--make-private参数一样，他们的背后都是通过调用mount（...）函数传入不同的参数实现的。

```bash
unshare --mount --uts --propagation unchanged /bin/bash
hostname container001
exec bash
readlink /proc/$$/ns/mnt
# mnt:[4026532463]
```

由于前面指定了--propagation unchanged，所以新namespace里面的/home/dev/disks/disk1也是shared，且和老namespace里面的/home/dev/disks/disk1属于同一个peer group 105。

因为在不同的namespace里面，所以这里挂载点的ID和原来namespace里的不一样了。

```bash
cat /proc/self/mountinfo |grep disk| sed 's/ - .*//'
# 221 177 7:1 / /home/dev/disks/disk1 rw,relatime shared:105
# 222 177 7:2 / /home/dev/disks/disk2 rw,relatime
```

分别在disk1和disk2目录下创建disk3和disk4，然后挂载disk3，disk4到这两个目录。

```bash
mkdir ./disk1/disk3 ./disk2/disk4
mount ./disk3.img ./disk1/disk3/
mount ./disk4.img ./disk2/disk4/
cat /proc/self/mountinfo |grep disk| sed 's/ - .*//'
# 221 177 7:1 / /home/dev/disks/disk1 rw,relatime shared:105
# 222 177 7:2 / /home/dev/disks/disk2 rw,relatime
# 223 221 7:3 / /home/dev/disks/disk1/disk3 rw,relatime shared:107
# 227 222 7:4 / /home/dev/disks/disk2/disk4 rw,relatime
```

下面，我们回到之前的窗口，可以看出由于/home/dev/disks/disk1是shared，且两个namespace里的这个挂载点都属于peer group 105，所以在新namespace里面挂载的disk3，在老的namespace里面也看的到，但是看不到disk4的挂载信息，那是因为/home/dev/disks/disk2是private的：

```bash
cat /proc/self/mountinfo |grep disk| sed 's/ - .*//'
# 164 24 7:1 / /home/dev/disks/disk1 rw,relatime shared:105
# 173 24 7:2 / /home/dev/disks/disk2 rw,relatime
# 224 164 7:3 / /home/dev/disks/disk1/disk3 rw,relatime shared:107
```

我们可以随时修改挂载点的propagation type，这里我们通过mount命令将disk3改成了private类型。

```bash
mount --make-private /home/dev/disks/disk1/disk3
cat /proc/self/mountinfo |grep disk3| sed 's/ - .*//'
# 224 164 7:3 / /home/dev/disks/disk1/disk3 rw,relatime
```

再次回到第二个窗口：

```bash
cat /proc/self/mountinfo |grep disk3| sed 's/ - .*//'
# 223 221 7:3 / /home/dev/disks/disk1/disk3 rw,relatime shared:107
```

disk3的propagation type还是shared，表明在老的namespace里面对propagation type的修改不会影响新namespace里面的挂载点。

关于mount命令和mount namespace的配合，里面有很多技巧，后面如果需要用到更复杂的用法，会再做详细的介绍。
