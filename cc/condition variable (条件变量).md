# [Using Condition Variables](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html)

This section explains using condition variables. [Table 4-6](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-71474) lists the functions that are available.

Table 4-6 Condition Variables Functions

| Operation                        | Destination Discussion                                       |
| -------------------------------- | ------------------------------------------------------------ |
| Initialize a condition variable  | ["pthread_cond_init(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-59145) |
| Block on a condition variable    | ["pthread_cond_wait(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-44265) |
| Unblock a specific thread        | ["pthread_cond_signal(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-53686) |
| Block until a specified event    | ["pthread_cond_timedwait(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-46756) |
| Unblock all threads              | ["pthread_cond_broadcast(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-40617) |
| Destroy condition variable state | ["pthread_cond_destroy(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-68029) |

## Initialize a Condition Variable



### pthread_cond_init(3THR)



Use [pthread_cond_init(3THR)](https://docs.oracle.com/docs/cd/E19455-01/806-0630/6j9vkb8fr/index.html) to initialize the condition variable pointed at by **cv** to its default value (cattr is `NULL`), or to specify condition variable attributes that are already set with **pthread_condattr_init()**. The effect of cattr being `NULL` is the same as passing the address of a default condition variable attribute object, but without the memory overhead. (For Solaris threads, see ["cond_init(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h033u/index.html#sthreads-33119).)



```
Prototype:
int	pthread_cond_init(pthread_cond_t *cv,
    const pthread_condattr_t *cattr);
```



```
#include <pthread.h>

pthread_cond_t cv;
pthread_condattr_t cattr;
int ret;

/* initialize a condition variable to its default value */
ret = pthread_cond_init(&cv, NULL);

/* initialize a condition variable */
ret = pthread_cond_init(&cv, &cattr); 
```

Statically defined condition variables can be initialized directly to have default attributes with the macro `PTHREAD_COND_INITIALIZER`. This has the same effect as dynamically allocating **pthread_cond_init()** with null attributes. No error checking is done.

Multiple threads must not simultaneously initialize or reinitialize the same condition variable. If a condition variable is reinitialized or destroyed, the application must be sure the condition variable is not in use.



#### Return Values

**pthread_cond_init()** returns zero after completing successfully. Any other returned value indicates that an error occurred. When any of the following conditions occurs, the function fails and returns the corresponding value.

-  `EINVAL`

  The value specified by cattr is invalid.

-  `EBUSY`

  The condition variable is being used.

-  `EAGAIN`

  The necessary resources are not available.

-  `ENOMEM`

  There is not enough memory to initialize the condition variable.



## Block on a Condition Variable



### pthread_cond_wait(3THR)



Use [pthread_cond_wait(3THR)](https://docs.oracle.com/docs/cd/E19455-01/806-0630/6j9vkb8fu/index.html) to atomically release the mutex pointed to by mp and to cause the calling thread to block on the condition variable pointed to by cv. (For Solaris threads, see ["cond_wait(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h033u/index.html#sthreads-71527).)



```
Prototype:
int	pthread_cond_wait(pthread_cond_t *cv,pthread_mutex_t *mutex);
```



```
#include <pthread.h>

pthread_cond_t cv;
pthread_mutex_t mp;
int ret;

/* wait on condition variable */
ret = pthread_cond_wait(&cv, &mp); 
```

The blocked thread can be awakened by a **pthread_cond_signal()**, a **pthread_cond_broadcast()**, or when interrupted by delivery of a signal.

Any change in the value of a condition associated with the condition variable cannot be inferred by the return of **pthread_cond_wait()**, and any such condition must be reevaluated.

The **pthread_cond_wait()** routine always returns with the mutex locked and owned by the calling thread, even when returning an error.

This function blocks until the condition is signaled. It atomically releases the associated mutex lock before blocking, and atomically acquires it again before returning.

In typical use, a condition expression is evaluated under the protection of a mutex lock. When the condition expression is false, the thread blocks on the condition variable. The condition variable is then signaled by another thread when it changes the condition value. This causes one or all of the threads waiting on the condition to unblock and to try to acquire the mutex lock again.

Because the condition can change before an awakened thread returns from **pthread_cond_wait()**, the condition that caused the wait must be retested before the mutex lock is acquired. The recommended test method is to write the condition check as a **while()** loop that calls **pthread_cond_wait()**.



```
    pthread_mutex_lock();
        while(condition_is_false)
            pthread_cond_wait();
    pthread_mutex_unlock();
```

No specific order of acquisition is guaranteed when more than one thread blocks on the condition variable.

------

**Note -**

**pthread_cond_wait()** is a cancellation point. If a cancel is pending and the calling thread has cancellation enabled, the thread terminates and begins executing its cleanup handlers while continuing to hold the lock.

------



#### Return Values

**pthread_cond_wait()** returns zero after completing successfully. Any other returned value indicates that an error occurred. When the following condition occurs, the function fails and returns the corresponding value.

-  `EINVAL`

  The value specified by cv or mp is invalid.



## Unblock a Specific Thread



### pthread_cond_signal(3THR)



Use [pthread_cond_signal(3THR)](https://docs.oracle.com/docs/cd/E19455-01/806-0630/6j9vkb8fs/index.html) to unblock one thread that is blocked on the condition variable pointed to by cv. (For Solaris threads, see ["cond_signal(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h033u/index.html#sthreads-22611).)



```
Prototype:
int	pthread_cond_signal(pthread_cond_t *cv);
```



```
#include <pthread.h>

pthread_cond_t cv;
int ret;

/* one condition variable is signaled */
ret = pthread_cond_signal(&cv); 
```

Call **pthread_cond_signal()** under the protection of the same mutex used with the condition variable being signaled. Otherwise, the condition variable could be signaled between the test of the associated condition and blocking in **pthread_cond_wait()**, which can cause an infinite wait.

The scheduling policy determines the order in which blocked threads are awakened. For `SCHED_OTHER`, threads are awakened in priority order.

When no threads are blocked on the condition variable, calling **pthread_cond_signal()** has no effect.



#### Return Values

**pthread_cond_signal()** returns zero after completing successfully. Any other returned value indicates that an error occurred. When the following condition occurs, the function fails and returns the corresponding value.

-  `EINVAL`

  cv points to an illegal address.

[Example 4-8](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-ex-23) shows how to use **pthread_cond_wait()** and **pthread_cond_signal()**.



------

##### Example 4-8 Using **pthread_cond_wait()** and **pthread_cond_signal()**



```
pthread_mutex_t count_lock;
pthread_cond_t count_nonzero;
unsigned count;

decrement_count()
{
    pthread_mutex_lock(&count_lock);
    while (count == 0)
        pthread_cond_wait(&count_nonzero, &count_lock);
    count = count - 1;
    pthread_mutex_unlock(&count_lock);
}

increment_count()
{
    pthread_mutex_lock(&count_lock);
    if (count == 0)
        pthread_cond_signal(&count_nonzero);
    count = count + 1;
    pthread_mutex_unlock(&count_lock);
}
```

------



## Block Until a Specified Event



### pthread_cond_timedwait(3THR)



```
Prototype:
int	pthread_cond_timedwait(pthread_cond_t *cv,
    pthread_mutex_t *mp, const struct timespec *abstime);
```



```
#include <pthread.h>
#include <time.h>

pthread_cond_t cv;
pthread_mutex_t mp;
timestruct_t abstime;
int ret;

/* wait on condition variable */
ret = pthread_cond_timedwait(&cv, &mp, &abstime); 
```

Use [pthread_cond_timedwait(3THR)](https://docs.oracle.com/docs/cd/E19455-01/806-0630/6j9vkb8ft/index.html) as you would use **pthread_cond_wait()**, except that **pthread_cond_timedwait()** does not block past the time of day specified by abstime. **pthread_cond_timedwait()** always returns with the mutex locked and owned by the calling thread, even when it is returning an error. (For Solaris threads, see ["cond_timedwait(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h033u/index.html#sthreads-43512).)

The **pthread_cond_timedwait()** function blocks until the condition is signaled or until the time of day, specified by the last argument, has passed.

------

**Note -**

**pthread_cond_timedwait()** is also a cancellation point.

------



#### Return Values

**pthread_cond_timedwait()** returns zero after completing successfully. Any other returned value indicates that an error occurred. When either of the following conditions occurs, the function fails and returns the corresponding value.

-  `EINVAL`

  cv or abstime points to an illegal address.

-  `ETIMEDOUT`

  The time specified by abstime has passed.

The timeout is specified as a time of day so that the condition can be retested efficiently without recomputing the value, as shown in [Example 4-9](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-ex-25).



------



##### Example 4-9 Timed Condition Wait



```
pthread_timestruc_t to;
pthread_mutex_t m;
pthread_cond_t c;
...
pthread_mutex_lock(&m);
to.tv_sec = time(NULL) + TIMEOUT;
to.tv_nsec = 0;
while (cond == FALSE) {
    err = pthread_cond_timedwait(&c, &m, &to);
    if (err == ETIMEDOUT) {
        /* timeout, do something */
        break;
    }
}
pthread_mutex_unlock(&m);
```

------



## Unblock All Threads



### pthread_cond_broadcast(3THR)



```
Prototype:
int	pthread_cond_broadcast(pthread_cond_t *cv);
```



```
#include <pthread.h>

pthread_cond_t cv;
int ret;

/* all condition variables are signaled */
ret = pthread_cond_broadcast(&cv); 
```

Use [pthread_cond_broadcast(3THR)](https://docs.oracle.com/docs/cd/E19455-01/806-0630/6j9vkb8fp/index.html) to unblock all threads that are blocked on the condition variable pointed to by cv, specified by **pthread_cond_wait()**. When no threads are blocked on the condition variable, **pthread_cond_broadcast()** has no effect. (For Solaris threads, see ["cond_broadcast(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h033u/index.html#sthreads-94886).)



#### Return Values

**pthread_cond_broadcast()** returns zero after completing successfully. Any other returned value indicates that an error occurred. When the following condition occurs, the function fails and returns the corresponding value.

-  `EINVAL`

  cv points to an illegal address.



#### Condition Variable Broadcast Example

Since **pthread_cond_broadcast()** causes all threads blocked on the condition to contend again for the mutex lock, use it with care. For example, use **pthread_cond_broadcast()** to allow threads to contend for varying resource amounts when resources are freed, as shown in [Example 4-10](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-ex-28).



------



##### Example 4-10 Condition Variable Broadcast



```
pthread_mutex_t rsrc_lock;
pthread_cond_t rsrc_add;
unsigned int resources;

get_resources(int amount)
{
    pthread_mutex_lock(&rsrc_lock);
    while (resources < amount) {
        pthread_cond_wait(&rsrc_add, &rsrc_lock);
    }
    resources -= amount;
    pthread_mutex_unlock(&rsrc_lock);
}

add_resources(int amount)
{
    pthread_mutex_lock(&rsrc_lock);
    resources += amount;
    pthread_cond_broadcast(&rsrc_add);
    pthread_mutex_unlock(&rsrc_lock);
}
```

------

Note that in **add_resources()** it does not matter whether **resources** is updated first or if **pthread_cond_broadcast()** is called first inside the mutex lock.

Call **pthread_cond_broadcast()** under the protection of the same mutex that is used with the condition variable being signaled. Otherwise, the condition variable could be signaled between the test of the associated condition and blocking in **pthread_cond_wait()**, which can cause an infinite wait.



## Destroy Condition Variable State



### pthread_cond_destroy(3THR)



Use [pthread_cond_destroy(3THR)](https://docs.oracle.com/docs/cd/E19455-01/806-0630/6j9vkb8fq/index.html) to destroy any state associated with the condition variable pointed to by cv. (For Solaris threads, see ["cond_destroy(3THR)"](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h033u/index.html#sthreads-61568).)



```
Prototype:
int	pthread_cond_destroy(pthread_cond_t *cv);
```



```
#include <pthread.h>

pthread_cond_t cv;
int ret;

/* Condition variable is destroyed */
ret = pthread_cond_destroy(&cv); 
```

Note that the space for storing the condition variable is not freed.



#### Return Values

**pthread_cond_destroy()** returns zero after completing successfully. Any other returned value indicates that an error occurred. When any of the following conditions occur, the function fails and returns the corresponding value.

-  `EINVAL`

  The value specified by cv is invalid.



## The Lost Wake-Up Problem

Calling **pthread_cond_signal()** or **pthread_cond_broadcast()** when the thread does not hold the mutex lock associated with the condition can lead to **lost wake-up** bugs.

A lost wake-up occurs when:



- A thread calls **pthread_cond_signal()** or **pthread_cond_broadcast()**.



- **And** another thread is between the test of the condition and the call to **pthread_cond_wait()**.



- **And** no threads are waiting.

  The signal has no effect, and therefore is lost.



## The Producer/Consumer Problem

This problem is one of the small collection of standard, well-known problems in concurrent programming: a finite-size buffer and two classes of threads, producers and consumers, put items into the buffer (producers) and take items out of the buffer (consumers).

A producer must wait until the buffer has space before it can put something in, and a consumer must wait until something is in the buffer before it can take something out.

A condition variable represents a queue of threads waiting for some condition to be signaled.

[Example 4-11](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-30944) has two such queues, one (less) for producers waiting for a slot in the buffer, and the other (more) for consumers waiting for a buffer slot containing information. The example also has a mutex, as the data structure describing the buffer must be accessed by only one thread at a time.



------

##### Example 4-11 The Producer/Consumer Problem and Condition Variables



```
typedef struct {
    char buf[BSIZE];
    int occupied;
    int nextin;
    int nextout;
    pthread_mutex_t mutex;
    pthread_cond_t more;
    pthread_cond_t less;
} buffer_t;

buffer_t buffer;
```

------

As [Example 4-12](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-ex-32) shows, the producer thread acquires the mutex protecting the buffer data structure and then makes certain that space is available for the item being produced. If not, it calls **pthread_cond_wait()**, which causes it to join the queue of threads waiting for the condition less, representing **there is room in the buffer**, to be signaled.

At the same time, as part of the call to **pthread_cond_wait()**, the thread releases its lock on the mutex. The waiting producer threads depend on consumer threads to signal when the condition is true (as shown in [Example 4-12](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-ex-32)). When the condition is signaled, the first thread waiting on less is awakened. However, before the thread can return from **pthread_cond_wait()**, it must acquire the lock on the mutex again.

This ensures that it again has mutually exclusive access to the buffer data structure. The thread then must check that there really is room available in the buffer; if so, it puts its item into the next available slot.

At the same time, consumer threads might be waiting for items to appear in the buffer. These threads are waiting on the condition variable more. A producer thread, having just deposited something in the buffer, calls **pthread_cond_signal()** to wake up the next waiting consumer. (If there are no waiting consumers, this call has no effect.)

Finally, the producer thread unlocks the mutex, allowing other threads to operate on the buffer data structure.



------

##### Example 4-12 The Producer/Consumer Problem--the Producer



```
void producer(buffer_t *b, char item)
{
    pthread_mutex_lock(&b->mutex);
   
    while (b->occupied >= BSIZE)
        pthread_cond_wait(&b->less, &b->mutex);

    assert(b->occupied < BSIZE);

    b->buf[b->nextin++] = item;

    b->nextin %= BSIZE;
    b->occupied++;

    /* now: either b->occupied < BSIZE and b->nextin is the index
       of the next empty slot in the buffer, or
       b->occupied == BSIZE and b->nextin is the index of the
       next (occupied) slot that will be emptied by a consumer
       (such as b->nextin == b->nextout) */

    pthread_cond_signal(&b->more);

    pthread_mutex_unlock(&b->mutex);
}
```

------

Note the use of the **assert()** statement; unless the code is compiled with `NDEBUG` defined, **assert()** does nothing when its argument evaluates to true (that is, nonzero), but causes the program to abort if the argument evaluates to false (zero). Such assertions are especially useful in multithreaded programs--they immediately point out runtime problems if they fail, and they have the additional effect of being useful comments.

The comment that begins `/* now: either b->occupied ...` could better be expressed as an assertion, but it is too complicated as a Boolean-valued expression and so is given in English.

Both the assertion and the comments are examples of invariants. These are logical statements that should not be falsified by the execution of the program, except during brief moments when a thread is modifying some of the program variables mentioned in the invariant. (An assertion, of course, should be true whenever any thread executes it.)

Using invariants is an extremely useful technique. Even if they are not stated in the program text, think in terms of invariants when you analyze a program.

The invariant in the producer code that is expressed as a comment is always true whenever a thread is in the part of the code where the comment appears. If you move this comment to just after the **mutex_unlock()**, this does not necessarily remain true. If you move this comment to just after the **assert()**, this is still true.

The point is that this invariant expresses a property that is true at all times, except when either a producer or a consumer is changing the state of the buffer. While a thread is operating on the buffer (under the protection of a mutex), it might temporarily falsify the invariant. However, once the thread is finished, the invariant should be true again.

[Example 4-13](https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-ex-33) shows the code for the consumer. Its flow is symmetric with that of the producer.



------

##### Example 4-13 The Producer/Consumer Problem--the Consumer



```
char consumer(buffer_t *b)
{
    char item;
    pthread_mutex_lock(&b->mutex);
    while(b->occupied <= 0)
        pthread_cond_wait(&b->more, &b->mutex);

    assert(b->occupied > 0);

    item = b->buf[b->nextout++];
    b->nextout %= BSIZE;
    b->occupied--;

    /* now: either b->occupied > 0 and b->nextout is the index
       of the next occupied slot in the buffer, or
       b->occupied == 0 and b->nextout is the index of the next
       (empty) slot that will be filled by a producer (such as
       b->nextout == b->nextin) */

    pthread_cond_signal(&b->less);
    pthread_mutex_unlock(&b->mutex);

    return(item);
}
```

