## c 语言的宏(macro)中 `##` 和`#`有什么意义

参考：

- https://www.cprogramming.com/reference/preprocessor/token-pasting-operator.html

### ##

如redis的代码`atomicvar.h`中有一段：

```c
#define atomicIncr(var,count) do { \
    pthread_mutex_lock(&var ## _mutex); \
    var += (count); \
    pthread_mutex_unlock(&var ## _mutex); \
} while(0)
```

其中`pthread_mutex_lock(&var ## _mutex);`的`##`就是典型的用法。

作用：**在预编译阶段，把`##`两边的符号拼接成一个符号**。

**示例代码：**

redis中`layzfree.c`有如下的调用：

```c
...
static size_t lazyfree_objects = 0;
pthread_mutex_t lazyfree_objects_mutex = PTHREAD_MUTEX_INITIALIZER;
...
int dbAsyncDelete(redisDb *db, robj *key) {
    ...
    atomicIncr(lazyfree_objects,1);
    ...
}
...
```

那么上面的宏定义中的`pthread_mutex_lock(&var ## _mutex);`锁定和`pthread_mutex_unlock(&var ## _mutex);`解锁就是：`pthread_mutex_lock(&lazyfree_objects_mutex)`，`pthread_mutex_unlock(&lazyfree_objects_mutex)`

> # [## macro operator](https://www.cprogramming.com/reference/preprocessor/token-pasting-operator.html)
>
> ```c
> <token> ## <token>
> ```
>
> The ## operator takes two separate tokens and pastes them together to form a single token. The resulting token could be a variable name, class name or any other identifier. A trivial example would be
>
> ```c
> #define type i##nt
> type a; // same as int a; since i##nt pastes together to "int"
> ```
>
> Real world uses of the token pasting operator often involve class, variable, or function names. For example, you might decide to create a macro that declares two variables, one with a name based on parameter to the macro and another with a different name, that stores the original value of the variable (perhaps for debugging).
>
> ```c
> #define DECLARE_AND_SET(type, varname, value) type varname = value; type orig_##varname = varname;c
> ```
>
> Now you can write code like
>
> ```c
> DECLARE_AND_SET( int, area, 2 * 6 );
> ```
>
> and orig_area always has the original value of area no matter how the variable is changed.

### #

参考：https://stackoverflow.com/questions/7952903/what-is-operator-in-c

作用：**在预编译阶段，执行字符串化(stringfy)操作。**

> Is there a '#' operator in C ?
>
> If yes then in the code
>
> ```c
> enum {ALPS, ANDES, HIMALYAS};
> ```
>
> what would the following return ?
>
> ```c
>  #ALPS 
> ```

>The C language does not have an `#` operator, but the pre-processor (the program that handles `#include` and `#define`) does. The pre-processor simple makes `#ALPS` into the string `"ALPS"`.
>
>However, this "stringify" operator can only be used in the `#define` pre-processor directive. For example:
>
>```c
>#define MAKE_STRING_OF_IDENTIFIER(x)  #x
>char alps[] = MAKE_STRING_OF_IDENTIFIER(ALPS);
>```
>
>The pre-processor will convert the above example into the following:
>
>```c
>char alps[] = "ALPS";
>```



## c/c++中.c文件`#include *.c`会发生什么

参考：

- https://stackoverflow.com/questions/11334371/cant-we-include-c-file/11338919

c/c++的编译单元是c/cpp/hpp文件。在一个`.c`文件中有`#include *.c`，则在预编译阶段会把`*.c`的内容拷贝到这个`.c`文件中。

> Today I had an interview there they asked me can we include `.c file` to a source file? I said `yes`. Because few years back I saw the same in some project where they have include `.c file`. But just now I was trying the same.
>
> **abc.c**
>
> ```c
> #include<stdio.h>
> void abc()
> { printf("From ABC() \n"); }
> ```
>
> **main.c**
>
> ```c
> #include<stdio.h>
> #include "abc.c"
> int main()
> {   void abc();
>     return 0;
> }
> ```
>
> Getting an error:
>
> > ```
> > D:\Embedded\...\abc.c :- multiple definition of 'abc'
> > ```
>
> Where is it going wrong?
>
> I wrote an abc.h file (the body of abc.h is `{ extern void abc(void); }`), and included the file in `abc.c` (commenting out `#include abc.c`). Worked fine.

解答如下：

> Do it as follows:
>
> `abc.c`:
>
> ```c
> #include <stdio.h>
> void abc()
> {printf("From ABC() \n");}
> ```
>
> `main.c`:
>
> ```c
> #include<stdio.h>
> #include "abc.c"
> int main()
> {   
>     abc();
>     return 0;
> }
> ```
>
> (no need for the header file)
>
> Then, to compile, you'd **only** compile `main.c`. **Do not** attempt to **compile both** `abc.c` and `main.c`, because then you'd have the `abc()` function defined twice.
>
> You need to understand that `#include` is basically "copy-paste", nothing more. If you tell it `#include "abc.c"`, it will simply take the contents of `abc.c`, and "paste" them in your `main.c` file. Therefore, using the above for `main.c`, after the preprocessor processes it, your `main.c` will look like this (I'm ignoring the `#include <stdio.h>`s):
>
> ```c
> #include<stdio.h>
> #include <stdio.h>
> void abc()
> {printf("From ABC() \n");}
> int main()
> {   
>     abc();
>     return 0;
> }
> ```
>
> which is a valid program.
>
> That said you should generally not do this; you should compile all your `.c` files separately and only then link them together.

## 可重入函数(reentrant function)的意思

参考：

- https://stackoverflow.com/questions/34758863/what-is-reentrant-function-in-c

函数执行到中间时，如果被打断，然后再次被安全的调用完成（第一次的未执行完成，第二次调用就开始了），那么就是可重入的。

Wikipedia has quite nice article on [re-entrancy](https://en.wikipedia.org/wiki/Reentrancy_(computing)).

> Function is called `reentrant` if it can be interrupted in the middle of its execution and then safely called again ("re-entered") before its previous invocations complete execution

What makes one function not being re-entrant? Check the article further, but roughly:

- Do not use static or global variables in your function since those may be changed by time your function resumes
- Function must not modify its own code (e.g. some low level graphic routines may have "habit" to generate itself)
- Do not call any function that does not comply with the two rules above
- When to use re-entrant function? Here are some examples:
  - Functions executed in interrupt context must be re-entrant.
  - Functions that will be called from multiple threads/tasks must be re-entrant.



