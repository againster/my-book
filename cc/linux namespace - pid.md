# Linux namespace - PID

## 什么是PID namespace？

PID namespaces用来隔离进程的ID空间，使得不同pid namespace里的进程ID可以重复且相互之间不影响。

PID namespace可以嵌套，也就是说有父子关系，在当前namespace里面创建的所有新的namespace都是当前namespace的子namespace。父namespace里面可以看到所有子孙后代namespace里的进程信息，而子namespace里看不到祖先或者兄弟namespace里的进程信息。

目前PID namespace最多可以嵌套32层，由内核中的宏MAX_PID_NS_LEVEL来定义。

Linux下的每个进程都有一个对应的/proc/PID目录，该目录包含了大量的有关当前进程的信息。 对一个PID namespace而言，/proc目录只包含当前namespace和它所有子孙后代namespace里的进程的信息。

在Linux系统中，进程ID从1开始往后不断增加，并且不能重复（当然进程退出后，ID会被回收再利用），进程ID为1的进程是内核启动的第一个应用层进程，一般是init进程（现在采用systemd的系统第一个进程是systemd），具有特殊意义，当系统中一个进程的父进程退出时，内核会指定init进程成为这个进程的新父进程，而当init进程退出时，系统也将退出。

除了在init进程里指定了handler的信号外，内核会帮init进程屏蔽掉其他任何信号，这样可以防止其他进程不小心kill掉init进程导致系统挂掉。不过有了PID namespace后，可以通过在父namespace中发送SIGKILL或者SIGSTOP信号来终止子namespace中的ID为1的进程。

由于ID为1的进程的特殊性，所以每个PID namespace的第一个进程的ID都是1。当这个进程运行停止后，内核将会给这个namespace里的所有其他进程发送SIGKILL信号，致使其他所有进程都停止，于是namespace被销毁掉。


## 基本实战

首先，我们先记录当前的pid namespace信息：

```bash
readlink /proc/$$/ns/pid
# pid:[4026531836]
```

接下来，我们创建一个新的namespace：

```bash
unshare --uts --pid --mount --fork /bin/bash
```

其中：

1. --pid表示创建一个新的pid namespace
2. --uts表示创建一个新的uts namespace，用于设置一个新的hostname，便于和老的namespace区分。
3. --mount表示创建新的mount namespace，是为了方便我们修改新namespace里面的mount信息，避免对老namespace造成影响。
4. --fork是为了让unshare进程fork一个新的进程出来，然后再用bash替换掉新的进程（效果相当于之前提到的clone）。因为对于pid namespace而言，进程所属的pid namespace在它创建的时候就确定了，不能更改。所以调用unshare和nsenter后，原来的进程还是属于老的namespace，只有新fork出来的进程才属于新的namespace。


接下来，我们修改hostname用于方便标识：

```bash
hostname container001
exec bash
```

结果如下：

```bash
root@raspberrypi:~# readlink /proc/$$/ns/pid
pid:[4026531836]
root@raspberrypi:~# unshare --uts --pid --mount --fork /bin/bash
root@raspberrypi:~# hostname container001
root@raspberrypi:~# exec bash
root@container001:~#
```

接下来，我们可以查询下进程间的关系：

```bash
pstree -pl
```

结果如下：

```txt
root@container001:~# pstree -pl
systemd(1)─┬
           ├─sshd(461) ─┬─
                        └─sshd(1152) ───sshd(1162) ───bash(1165) ───su(1229) ───bash(1234) ───unshare(1252) ───bash(1253) ───pstree(1258)
```

其中，unshare(1252)和bash(1253)应该分别属于不同的namespace。我们可以进行相关验证：

```bash
root@container001:~# readlink /proc/1252/ns/pid
pid:[4026531836]
root@container001:~# readlink /proc/1253/ns/pid
pid:[4026532268]
```

但是，如果通过如下方式查询当前pid namespace：

```bash
root@container001:~# readlink /proc/$$/ns/pid
pid:[4026531836]
```
得到的pid namespace仍然是旧的namespace信息，这是为什么呢？

我们可以查询当前的进程ID，可以看到它的进程ID是1，说明当前bash是当前namespace的第1个进程，也就是说它已经进入了新的namespace。

```bash
root@container001:~# echo $$
1
```

但是实际上，由于我们新的namespace的挂载信息是从老的namespace拷贝过来的，所以这里看到的还是老namespace里面的进程号为1的namespace信息。

同样，如果此时我们使用`ps`命令查询进程相关信息时，得到的也是原始namespace中的进程信息。其核心原因是无论是`ps`命令实际依赖/proc目录，是从/proc目录中获取的信息，由于目录没有隔离，因此查询到的信息仍然是原有namespace中的信息。

所以我们需要重新挂载我们的`/proc`目录：

```
mount -t proc proc /proc
```

此时，再次查询根据进程ID查询namespace或者通过ps查询进程，则均已经正常了。

```
readlink /proc/$$/ns/pid
ps -ef
```

结果如下：

```bash
root@container001:~# mount -t proc proc /proc
root@container001:~# readlink /proc/$$/ns/pid
pid:[4026532268]
root@container001:~# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 16:50 pts/0    00:00:00 bash
root        10     1  0 16:57 pts/0    00:00:00 ps -ef
```


## namespace嵌套

关于pid namespace，其具有如下特点：

1. 调用unshare或者setns函数后，当前进程的namespace不会发生变化，不会加入到新的namespace，而它的子进程会加入到新的namespace。也就是说进程属于哪个namespace是在进程创建的时候决定的，并且以后再也无法更改。
2. 在一个PID namespace里的进程，它的父进程可能不在当前namespace中，而是在外面的namespace里面（这里外面的namespace指当前namespace的祖先namespace），这类进程的pid都是0。比如新namespace里面的第一个进程，他的父进程就在外面的namespace里。通过setns的方式加入到新namespace中的进程的父进程也在外面的namespace中。
3. 可以在祖先namespace中看到子namespace的所有进程信息，且可以发信号给子namespace的进程，但同一个进程在不同namespace中的PID是不一样的。


接下来，我们也同样通过一个实例来了解什么是pid namespace嵌套。

首先，我们先记录当前的namespace信息，然后再创建一个新的namespace:

```bash
readlink /proc/$$/ns/pid
unshare --uts --pid --mount --fork --mount-proc /bin/bash #注意这个参数和上面的不一样
hostname container001
exec bash
readlink /proc/$$/ns/pid
```

结果如下：

```bash
root@raspberrypi:~# readlink /proc/$$/ns/pid
pid:[4026531836]
root@raspberrypi:~# unshare --uts --pid --mount --fork --mount-proc /bin/bash #注意这个参数和上面的不一样
root@raspberrypi:~# hostname container001
root@raspberrypi:~# exec bash
root@container001:~# readlink /proc/$$/ns/pid
pid:[4026532268]
```

接下来，我们继续在当前namespace下创建一个新的namespace：

```bash
unshare --uts --pid --mount --fork --mount-proc /bin/bash
hostname container002
exec bash
readlink /proc/$$/ns/pid
```

结果如下：

```bash
root@container001:~# unshare --uts --pid --mount --fork --mount-proc /bin/bash
root@container001:~# hostname container002
root@container001:~# exec bash
root@container002:~# readlink /proc/$$/ns/pid
pid:[4026532271]
```

我们再次在当前namespace下创建一个新的namespace：

```bash
unshare --uts --pid --mount --fork --mount-proc /bin/bash
hostname container003
exec bash
readlink /proc/$$/ns/pid
```

结果如下：

```bash
root@container002:~# unshare --uts --pid --mount --fork --mount-proc /bin/bash
root@container002:~# hostname container003
root@container002:~# exec bash
root@container003:~# readlink /proc/$$/ns/pid
pid:[4026532274]
```

此时，可以看到在namespace container003里面就一个bash进程：

```bash
root@container003:~# pstree -p
bash(1)───pstree(4)
```

这样我们就有了三层pid namespace，他们的父子关系为container001->container002->container003。

此时，我们打开一个新的窗口，在最外层的namespace中查看上面新创建的三个namespace中的bash进程：

```bash
root@raspberrypi:/home/pi# pstree -pl|grep bash|grep unshare
           |           |-sshd(1152)---sshd(1162)---bash(1165)---su(1229)---bash(1234)---unshare(1271)---bash(1272)---unshare(1275)---bash(1276)---unshare(1279)---bash(1280)
```

从这里可以看出，这里显示的bash进程的PID和上面container003里看到的bash(1)不一样。

各个unshare进程的子bash进程分别属于上面的三个pid namespace：

```bash
readlink /proc/1272/ns/pid
readlink /proc/1276/ns/pid
readlink /proc/1280/ns/pid
```

结果如下：

```bash
root@raspberrypi:/home/pi# readlink /proc/1272/ns/pid
pid:[4026532268]
root@raspberrypi:/home/pi# readlink /proc/1276/ns/pid
pid:[4026532271]
root@raspberrypi:/home/pi# readlink /proc/1280/ns/pid
pid:[4026532274]
```

实际上，PID在各个namespace里的映射关系可以通过/proc/[pid]/status查看到，我们可以查询namespace container003中bash进程的信息：

```bash
grep pid /proc/1280/status
# NSpid:	29801	169	85	1
```

结果如下：

```txt
root@raspberrypi:/home/pi# grep pid /proc/1280/status
NSpid:  1280    9       5       1
```

其中，1280是在最外面namespace中看到的pid，9、5、1分别是在container001，container002和container003中的pid。

接下来，我们创建一个新的bash并加入container002：

```bash
nsenter --uts --mount --pid -t 1276 /bin/bash
```

我们可以验证下对应的container003中的pid是否为5：

```bash
pstree -p
```

结果如下：

```txt
root@raspberrypi:/home/pi# nsenter --uts --mount --pid -t 1276 /bin/bash
root@container002:/# pstree -p
bash(1)───unshare(4)───bash(5)
```

可以看到，查询到的pid信息的确与在最外层namespace中查询到的一致。

为什么上面pstree的结果里面没看到nsenter加进来的bash呢？

```txt
root@container002:/# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 16:58 pts/0    00:00:00 bash
root         4     1  0 16:59 pts/0    00:00:00 unshare --uts --pid --mount --fork --mount-proc /bin/bash
root         5     4  0 16:59 pts/0    00:00:00 bash
root         9     0  0 17:11 pts/1    00:00:00 /bin/bash
root        11     9  0 17:12 pts/1    00:00:00 ps -ef
```

通过ps命令我们发现，我们新加进来的那个/bin/bash的ppid是0，所以pstree里面显示不出来。此外，从这里可以看出，跟最外层namespace不一样的地方就是，这里可以有多个进程的ppid为0。

从这里的TTY也可以看出哪些命令是在哪些窗口执行的，pts/0对应第一个shell窗口，pts/1对应第二个shell窗口。


现在，我们可以退出当前namespace，并加入container001 namespace中：

```bash
exit
nsenter --uts --mount --pid -t 1272 /bin/bash
ps -ef
```

结果如下：

```txt
root@container003:/# exit
exit
root@raspberrypi:/home/pi# nsenter --uts --mount --pid -t 1272 /bin/bash
root@container001:/# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 16:58 pts/0    00:00:00 bash
root         4     1  0 16:58 pts/0    00:00:00 unshare --uts --pid --mount --fork --mount-proc /bin/bash
root         5     4  0 16:58 pts/0    00:00:00 bash
root         8     5  0 16:59 pts/0    00:00:00 unshare --uts --pid --mount --fork --mount-proc /bin/bash
root         9     8  0 16:59 pts/0    00:00:00 bash
root        19     0  0 17:15 pts/1    00:00:00 /bin/bash
root        20    19  0 17:18 pts/1    00:00:00 ps -ef
```

通过pstree和ps -ef我们可看到所有三个namespace中的进程及他们的关系:

1. bash(1)───unshare(4)属于container001
2. bash(5)───unshare(8)属于container002
3. bash(9)属于container003


同上面ps的结果比较我们可以看出，同样的进程在不同的namespace里面拥有不同的PID。

现在，我们发送信号来终止contain002中的bash：

```bash
kill -9 5
ps -ef
```

再次查询进程信息，可以看到container002和container003的相关信息都已经不存在了：

```txt
root@container001:/# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 16:58 pts/0    00:00:00 bash
root        19     0  0 17:15 pts/1    00:00:00 /bin/bash
root        21    19  0 17:27 pts/1    00:00:00 ps -ef
```

这说明父namespace是可以发信号给子namespace中的进程的。


## 关于"init"进程

PID为1的进程我们称之为init进程，init进程有着相对特殊的作用：

1. 当一个进程的父进程被kill掉后，该进程将会被当前namespace中pid为1的进程接管，而不是被最外层的系统级别的init进程接管。
2. 当pid为1的进程停止运行后，内核将会给这个namespace及其子孙namespace里的所有其他进程发送SIGKILL信号，致使其他所有进程都停止，于是当前namespace及其子孙后代的namespace都被销毁掉。


## 补充说明

1. 通常情况下，如果PID namespace中的进程都退出了，这个namespace将会被销毁，但就如在前面“Namespace概述”里介绍的，有两种情况会导致就算进程都退出了，这个namespace还会存在。但对于PID namespace来说，就算namespace还在，由于里面没有“init”进程，Kernel不允许其它进程加入到这个namespace，所以这个存在的namespace没有意义。
2. 当一个PID通过UNIX domain socket在不同的PID namespace中传输时，PID将会自动转换成目的namespace中的PID。