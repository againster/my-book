# [linux oom score adj (oom评分调整)](https://www.jianshu.com/p/bbaeff371019)

在 linux 系统下，内存不足会触发 OOM killer 去杀进程。如同下方的模拟代码`oom.c`，几秒之后显示被`Killed`了：

```c
#include <stdlib.h>
#include <stdio.h>

#define BYTES (8 * 1024 * 1024)

int main(void) 
{
    printf("hello OOM \n");
    while(1)
    {
        char *p = malloc(BYTES);
        if (p == NULL)
        {
            return -1;
        }
    }
    return 0;
}
```

编译`gcc oom.c `，然后执行`./a.out`，结果如下：

```text
hello OOM 
Killed
```

用 `dmesg` 命令可以看到相关 log：

```csharp
[350942.274802] a.out invoked oom-killer: gfp_mask=0x280da, order=0, oom_score_adj=0
[350942.274808] a.out cpuset=/ mems_allowed=0
[350942.274812] CPU: 0 PID: 7095 Comm: a.out Kdump: loaded Tainted: G               ------------ T 3.10.0-1160.6.1.el7.x86_64 #1
[350942.274814] Hardware name: Bochs Bochs, BIOS rel-1.7.5.1-20180921_224418 04/01/2014
[350942.274816] Call Trace:
[350942.274851]  [<ffffffff9cd81400>] dump_stack+0x19/0x1b
[350942.274855]  [<ffffffff9cd7bd20>] dump_header+0x90/0x229
[350942.274866]  [<ffffffff9c90e84b>] ? cred_has_capability+0x6b/0x120
[350942.274895]  [<ffffffffc04547ca>] ? virtballoon_oom_notify+0x2a/0x70 [virtio_balloon]
[350942.274908]  [<ffffffff9c7c20cd>] oom_kill_process+0x2cd/0x490
[350942.274911]  [<ffffffff9c90e92e>] ? selinux_capable+0x2e/0x40
[350942.274914]  [<ffffffff9c7c27ba>] out_of_memory+0x31a/0x500
[350942.274917]  [<ffffffff9cd7c83d>] __alloc_pages_slowpath+0x5db/0x729
[350942.274920]  [<ffffffff9c7c8db6>] __alloc_pages_nodemask+0x436/0x450
[350942.274928]  [<ffffffff9c81c2a9>] alloc_pages_vma+0xa9/0x200
[350942.274932]  [<ffffffff9c7f5f07>] handle_mm_fault+0xcb7/0xfb0
[350942.274938]  [<ffffffff9cd8f653>] __do_page_fault+0x213/0x500
[350942.274940]  [<ffffffff9cd8fa26>] trace_do_page_fault+0x56/0x150
[350942.274943]  [<ffffffff9cd8efa2>] do_async_page_fault+0x22/0xf0
[350942.274947]  [<ffffffff9cd8b7a8>] async_page_fault+0x28/0x30
[350942.274949] Mem-Info:
[350942.274954] active_anon:599 inactive_anon:848 isolated_anon:32
 active_file:138 inactive_file:4371 isolated_file:0
 unevictable:0 dirty:0 writeback:810 unstable:0
 slab_reclaimable:18606 slab_unreclaimable:14980
 mapped:125 shmem:0 pagetables:390965 bounce:0
 free:13048 free_pcp:0 free_cma:0
[350942.274958] Node 0 DMA free:7528kB min:380kB low:472kB high:568kB active_anon:0kB inactive_anon:0kB active_file:0kB inactive_file:0kB unevictable:0kB isolated(anon):0kB isolated(file):0kB present:15992kB managed:15908kB mlocked:0kB dirty:0kB writeback:0kB mapped:0kB shmem:0kB slab_reclaimable:4352kB slab_unreclaimable:520kB kernel_stack:144kB pagetables:2376kB unstable:0kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB writeback_tmp:0kB pages_scanned:0 all_unreclaimable? yes
[350942.274965] lowmem_reserve[]: 0 1819 1819 1819
[350942.274968] Node 0 DMA32 free:44664kB min:44672kB low:55840kB high:67008kB active_anon:2396kB inactive_anon:3392kB active_file:552kB inactive_file:17484kB unevictable:0kB isolated(anon):128kB isolated(file):0kB present:2080640kB managed:1865956kB mlocked:0kB dirty:0kB writeback:3240kB mapped:500kB shmem:0kB slab_reclaimable:70072kB slab_unreclaimable:59400kB kernel_stack:5520kB pagetables:1561484kB unstable:0kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB writeback_tmp:0kB pages_scanned:43596 all_unreclaimable? yes
[350942.274976] lowmem_reserve[]: 0 0 0 0
[350942.274978] Node 0 DMA: 6*4kB (UE) 4*8kB (UE) 5*16kB (E) 0*32kB 1*64kB (U) 1*128kB (U) 2*256kB (U) 1*512kB (U) 0*1024kB 1*2048kB (M) 1*4096kB (M) = 7496kB
[350942.274990] Node 0 DMA32: 11184*4kB (UM) 0*8kB 0*16kB 0*32kB 0*64kB 0*128kB 0*256kB 0*512kB 0*1024kB 0*2048kB 0*4096kB = 44736kB
[350942.275029] Node 0 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=2048kB
[350942.275031] 5431 total pagecache pages
[350942.275034] 890 pages in swap cache
[350942.275036] Swap cache stats: add 889600, delete 888687, find 10195866/10224892
[350942.275038] Free swap  = 1468156kB
[350942.275039] Total swap = 4063228kB
[350942.275041] 524158 pages RAM
[350942.275043] 0 pages HighMem/MovableOnly
[350942.275044] 53692 pages reserved
[350942.275046] [ pid ]   uid  tgid total_vm      rss nr_ptes swapents oom_score_adj name
[350942.275054] [  480]     0   480    11772        0      29       92             0 systemd-journal
[350942.275058] [  507]     0   507    50282        0      31      399             0 lvmetad
[350942.275062] [  512]     0   512    12165        1      25      567         -1000 systemd-udevd
[350942.275066] [  610]     0   610    13883        0      27      114         -1000 
[350942.275066] [  610]     0   610    13883        0      27      114         -1000 auditd
[350942.275070] [  631]     0   631     5386        0      15       79             0 irqbalance
[350942.275074] [  633]    81   633    16613        0      35      188          -900 dbus-daemon
[350942.275077] [  638]     0   638     6695        1      20      165             0 systemd-logind
[350942.275081] [  639]   998   639   153250        0      65     2112             0 polkitd
[350942.275084] [  647]     0   647    27552        1       9       32             0 agetty
[350942.275088] [  665]     0   665    90775        0      98     6732             0 firewalld
[350942.275092] [ 1039]     0  1039   146609        0     101     3374             0 tuned
[350942.275096] [ 1046]     0  1046    78977        0      71      469             0 rsyslogd
[350942.275099] [ 1074]     0  1074   127928        0      59     7148          -999 containerd
[350942.275103] [ 1076]     0  1076    28953        0      13       39             0 rhsmcertd
[350942.275107] [ 1082]  1004  1082   166077        0      56     2777             0 es-email-sender
[350942.275110] [ 1711]     0  1711    22438        0      45      260             0 master
[350942.275114] [ 1718]    89  1718    22481        0      44      254             0 qmgr
[350942.275118] [ 2491]     0  2491    28386        0      12      132             0 abs_monitor
[350942.275121] [ 2500]     0  2500    11955        0      28      181             0 abs_deployer
[350942.275125] [ 2600]     0  2600    10757        0      23      186             0 edr_monitor
[350942.275129] [ 2664]     0  2664    48757        1      30      289             0 sfupdatemgr
[350942.275132] [ 2665]     0  2665    10829        1      25      183             0 ipc_proxy
[350942.275136] [ 2666]     0  2666   177366        0     121     4946             0 edr_agent
[350942.275139] [ 2667]     0  2667     4265        0      13       75             0 cpulimit
[350942.275142] [ 2668]     0  2668    28386        0      12      140             0 asset_collectio
[350942.275146] [ 2669]     0  2669    32156        0      32      249             0 edr_sec_plan
[350942.275149] [ 2670]     0  2670    29508        0      56      890             0 lloader
[350942.275153] [ 3122]     0  3122   138104        0     103    12404          -500 dockerd
[350942.275157] [ 9766]     0  9766    28323        0      12       52             0 eps_services_ct
[350942.275164] [20258]     0 20258     6848        0      20      130             0 sshd
[350942.275168] [20273]     0 20273     5957        1      18      105             0 sftp-server
[350942.275172] [ 8635]     0  8635     7431        0      21      720             0 sshd
[350942.275175] [ 8722]     0  8722    28356        1      12       83             0 bash
[350942.275179] [ 8768]     0  8768    28322        0      12       48             0 sh
[350942.275182] [ 8776]     0  8776   240456        0     176     5739             0 node
[350942.275186] [ 8880]     0  8880   254369        0     624    39822             0 node
[350942.275190] [ 8887]     0  8887   268400        0     367    25820             0 node

[350942.275193] [ 9528]     0  9528   302557        0     187    54404             0 cpptools
[350942.275197] [26362]     0 26362     6119        0      18      114         -1000 sshd
[350942.275201] [26432]    38 26432     6435        1      17      147             0 ntpd
[350942.275204] [26509]     0 26509    31597        1      19      189             0 crond
[350942.275208] [28099]     0 28099    13740        0      28      291             0 nginx
[350942.275211] [28100]   996 28100    14824        0      30     1371             0 nginx
[350942.275215] [28101]   996 28101    14824        0      30     1371             0 nginx
[350942.275219] [28102]   996 28102    14824        0      30     1371             0 nginx
[350942.275222] [28103]   996 28103    14824        0      30     1371             0 nginx
[350942.275226] [28131]     0 28131   366123        0     145    24229             0 admin
[350942.275229] [28190]     0 28190   290543        0      96    14025             0 opsweb
[350942.275233] [ 3565]     0  3565     6152        0      16      126             0 sshd
[350942.275236] [ 3574]     0  3574    28920        1      12      127             0 bash
[350942.275240] [ 5555]    89  5555    22464        0      45      253             0 pickup
[350942.275244] [17799]     0 17799  1205322        0      68    18858             0 cpptools-srv
[350942.275248] [30201]     0 30201  1205097        0      42     1960             0 cpptools-srv
[350942.275252] [ 6169]     0  6169     6895        1      19      181             0 sshd
[350942.275256] [ 6180]     0  6180     6847        0      18      130             0 sshd
[350942.275259] [ 6182]     0  6182     5957        1      17      106             0 sftp-server
[350942.275263] [ 6204]     0  6204    28920        1      13      133             0 bash
[350942.275266] [ 6592]     0  6592  1205097        0      36     1590             0 cpptools-srv
[350942.275269] [ 6690]     0  6690    27014        0       9       23             0 sleep
[350942.275272] [ 6728]     0  6728    28921        0      14      132             0 bash
[350942.275275] [ 7095]     0  7095 793698600      608  387523   386937             0 a.out
[350942.275279] [ 7096]     0  7096    28315        1      11       37             0 sh
[350942.275282] [ 7097]     0  7097    27018        0      10       34             0 md5sum
[350942.275285] Out of memory: Kill process 7095 (a.out) score 505 or sacrifice child
[350942.275426] Killed process 7095 (a.out), UID 0, total-vm:3174794400kB, anon-rss:2404kB, file-rss:28kB, shmem-rss:0kB
```

## oom_score_adj

上面打印了`oom_score_adj=0`以及`score 505`，OOM killer 给进程打分，把 `oom_score` 最大的进程先杀死。打分主要有两部分组成：

1. 系统根据该进程的内存占用情况打分，进程的内存开销是变化的，所以该值也会动态变化。

2. 用户可以设置的 `oom_score_adj`，范围是 `-1000`到 `1000`，定义在：[https://elixir.bootlin.com/linux/v5.0/source/include/uapi/linux/oom.h#L9](https://links.jianshu.com/go?to=https%3A%2F%2Felixir.bootlin.com%2Flinux%2Fv5.0%2Fsource%2Finclude%2Fuapi%2Flinux%2Foom.h%23L9)

   ```c
   /*
    * /proc/<pid>/oom_score_adj set to OOM_SCORE_ADJ_MIN disables oom killing for
    * pid.
    */
   #define OOM_SCORE_ADJ_MIN   (-1000)
   #define OOM_SCORE_ADJ_MAX   1000
   ```

如果用户将该进程的 `oom_score_adj` 设定成 `-1000`，表示`禁止`OOM killer 杀死该进程（代码在[https://elixir.bootlin.com/linux/v5.0/source/mm/oom_kill.c#L222](https://links.jianshu.com/go?to=https%3A%2F%2Felixir.bootlin.com%2Flinux%2Fv5.0%2Fsource%2Fmm%2Foom_kill.c%23L222) ）。比如 `sshd` 等非常重要的服务可以配置为 `-1000`。

- 如果设置为负数，表示分数会打一定的折扣。

- 如果设置为正数，分数会增加，可以优先杀死该进程。

- 如果设置为`0` ，表示用户不调整分数，`0` 是默认值。

测试设置 `oom_score_adj` 对 `oom_score` 的影响

```c
#include <stdlib.h>
#include <stdio.h>

#define BYTES (8 * 1024 * 1024)
#define N (10240)

int main(void) 
{
    printf("hello OOM \n");
    int i;
    for (i = 0; i < N; i++)
    {
        char *p = malloc(BYTES);
        if (p == NULL)
        {
            return -1;
        }
    }
    printf("while... \n");
    while(1);
    return 0;
}
```

下面是初始的分数：

```ruby
$ cat /proc/$(pidof a.out)/oom_score_adj
0
$ cat /proc/$(pidof a.out)/oom_score
62
```

下面修改 `oom_score_adj`，`oom_score` 也随之发生了变化：

```ruby
$ sudo sh -c "echo -50 > /proc/$(pidof a.out)/oom_score_adj"
$ cat /proc/$(pidof a.out)/oom_score_adj
-50
$ cat /proc/$(pidof a.out)/oom_score
12
$ sudo sh -c "echo -60 > /proc/$(pidof a.out)/oom_score_adj"
$ cat /proc/$(pidof a.out)/oom_score_adj
-60
$ cat /proc/$(pidof a.out)/oom_score
2
$ sudo sh -c "echo -500 > /proc/$(pidof a.out)/oom_score_adj"
$ cat /proc/$(pidof a.out)/oom_score_adj
-500
$ cat /proc/$(pidof a.out)/oom_score
0
```

测试设置 `oom_score_adj` 设置为`-1000` 对系统的影响。如果把一个无限申请内存的进程设置为`-1000`，会发生什么呢：

```bash
$ sudo sh -c "echo -1000 > /proc/$(pidof a.out)/oom_score_adj"
```

结果如下：

```bash
$ dmesg | grep "Out of memory"
Out of memory: Kill process 1000 (mysqld) score 67 or sacrifice child
Out of memory: Kill process 891 (vmhgfs-fuse) score 1 or sacrifice child
Out of memory: Kill process 321 (systemd-journal) score 1 or sacrifice child
Out of memory: Kill process 1052 ((sd-pam)) score 1 or sacrifice child
Out of memory: Kill process 1072 (bash) score 0 or sacrifice child
```

因为 `bash` 挂了，所以 `a.out` 也挂了。如果 `./a.out &` 在后台运行，就可以看到用更多进程的 score 是 0 仍然挂掉了，比如 sshd、dhclient、systemd-logind、systemd-timesyn、dbus-daemon 等，所以设置错误的 `oom_score_adj` 后果比较严重。