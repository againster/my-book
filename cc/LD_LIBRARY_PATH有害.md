在asv系统中strace时，发现一个奇怪的现象，如下，（只截取了一个so的寻找过程）

（所有进程都是这样，不只是这里作为栗子的ls ！！）

Sangfor:aSV/host-0010184aa004 /sf # strace -e open ls

open("/etc/ld.so.preload", O_RDONLY)    = 3

open("/sf/vs/lib/libhook_kill.so", O_RDONLY) = 3

open("/sf/vs/lib/libvs_hook_gai.so", O_RDONLY) = 3

open("/sf/lib/tls/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/lib/tls/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/lib/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/lib/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/lib/links/tls/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/lib/links/tls/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/lib/links/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/lib/links/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/cluster/lib/tls/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/cluster/lib/tls/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/cluster/lib/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/cluster/lib/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/sdn/lib/tls/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/sdn/lib/tls/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/sdn/lib/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/sdn/lib/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/authorize/lib/tls/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/authorize/lib/tls/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/authorize/lib/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/authorize/lib/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/vs/lib/tls/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/vs/lib/tls/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/vs/lib/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/vs/lib/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/vs/glusterfs/lib/tls/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/vs/glusterfs/lib/tls/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/vs/glusterfs/lib/x86_64/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/sf/vs/glusterfs/lib/libselinux.so.1", O_RDONLY) = -1 ENOENT (No such file or directory)

open("/etc/ld.so.cache", O_RDONLY)      = 3

open("/lib/x86_64-linux-gnu/libselinux.so.1", O_RDONLY) = 3



一个so需要经过28个（这个数量和so有关，有的多些有的少些）失败的open系统调用后才最终找到 ！

看最后面2行，最终是通过ld.so.cache找到的，为啥不是直接从ld.so.cache中找呢？

man ld.so发现，LD_LIBRARY_PATH的优先级比ld.so.cache要高，如果LD_LIBRARY_PATH这个环境变量设置了，

ld.so会优先遍历LD_LIBRARY_PATH指定的路径寻找so，即使是libc等系统库；

在asv上看一下，echo $LD_LIBRARY_PATH，果然



Sangfor:aSV/host-0010184aa004 /sf # echo $LD_LIBRARY_PATH

/sf/lib:/sf/lib/links:/sf/cluster/lib:/sf/sdn/lib:/sf/authorize/lib:/sf/vs/lib:/sf/vs/glusterfs/lib



优化的做法是把库路径加入/etc/ld.so.conf.d/*，然后ldconfig更新ld.so.cache，ldconfig -p可以看到自己的库有没有被正确缓存；

综上，个人认为LD_LIBRARY_PATH有害，对整个系统的性能肯定是有负面影响的；

