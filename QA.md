# QA

[markdown语法参考](https://guides.github.com/features/mastering-markdown/)
___

## 好用工具

```
Jenkins 持续集成工具
review board 基于web的团队协作代码审查工具
gometalinter 静态go语言源代码规范扫描工具
sonarQube 代码质量自动检查，静态分析bug，代码安全弱点
tcpdump 命令行网络包分析工具
googletest google开发的c++测试框架
valgrind 内存debug，内存泄露探测
greenshot github上开源的屏幕截图工具
snipaste 免费截图工具http://www.snapfiles.com/get/snipaste.html
proxifier 很专业的桌面代理工具
apache jmeter 性能测试工具
tor 网络匿名通信代理工具
postman 完全的API开发环境测试工具
zabbix 基于web的分布式系统资源监视器
ansible 基于python开发的自动化运维工具
7zip 开源解压缩软件
everything windows平台的文件索引后搜索工具
listary windows平台的快速查找文件和app的工具
glide go语言的包依赖管理工具
gradle java语言的包依赖管理工具
maven java语言的包依赖管理工具
eolinker是一个api接口平台
mathpix snipping tool是一个ocr识别数学公式并转换为latex代码的工具
vagrant用于创建和部署虚拟机环境的管理工具
typora是一款markdown文档编辑器
doteditor是graphviz形式的自动排版的作图软件
microsoft threat modeling是微软的STRIDE威胁建模工具
burpsuite是http攻击工具
staruml是uml画图工具
winmerge是文档对比工具
texmaker是latex文档编辑工具
pandoc是一款通用的文档转换工具
hmailserver是开源的windows桌面邮件服务器
clover是windows资源管理器多tab工具
fiddler是web调试和跟踪工具
```

### web渗透测试工具

```txt
AppScan 
AWVS
Nessus 
BurpSuite
SqlMap 
Metasploit 是一款开源的安全漏洞检测工具，可以帮助安全和IT专业人士识别安全性问题，验证漏洞的缓解措施，并管理专家驱动的安全性进行评估，提供真正的安全风险情报。这些功能包括智能开发，代码审计，Web应用程序扫描，社会工程。
Kali Linux an Advanced Penetration Testing Linux distribution used for Penetration Testing, Ethical Hacking and network security assessments.
```

### 源码安全审计工具

```txt
RIPS RIPS is the most popular static code analysis tool to automatically detect vulnerabilities in PHP applications. 
Findsecuritybugs The SpotBugs plugin for security audits of Java web applications.
gosec Inspects source code for security problems by scanning the Go AST. 
bandit Bandit is a tool designed to find common security issues in Python code. 
Checkmarx
FORTIFY
```

## 按下F12后chrome的控制台没有出现

[Chrome console(f12) doesn't show up?](https://stackoverflow.com/questions/24200292/chrome-consolef12-doesnt-show-up)

> Q: my chrome console can't show up anymore, I even tried to uninstall and install chrome but nothing helps.

> A:
>- Go to %LOCALAPPDATA%\Google\Chrome\User Data\
>- Rename the folder "Default" to "Backup" or something else
>- Launch Chrome (it will recreate the Default folder with a blank user profile)

date: 20180412
___

## 执行```service network restart```出现问题：

```
Jul 11 04:41:33 localhost.localdomain network[10767]: Bringing up loopback interface:  [  OK  ]
Jul 11 04:41:33 localhost.localdomain NetworkManager[3347]: <info>  [1531298493.7881] audit: op="connection-activate" uuid="93e00bcb-9b70-3646-9830-3e4b5d002553" name="Wired connection 1" result="fail" reason="No suitable device found for this connection."
Jul 11 04:41:33 localhost.localdomain network[10767]: Bringing up interface Wired_connection_1:  Error: Connection activation failed: No suitable device found for this connection.
Jul 11 04:41:33 localhost.localdomain network[10767]: [FAILED]
Jul 11 04:41:33 localhost.localdomain network[10767]: Bringing up interface Wired_connection_2:  [  OK  ]
Jul 11 04:41:34 localhost.localdomain network[10767]: Bringing up interface ens18:  [  OK  ]
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain network[10767]: RTNETLINK answers: File exists
Jul 11 04:41:34 localhost.localdomain systemd[1]: network.service: control process exited, code=exited status=1
Jul 11 04:41:34 localhost.localdomain systemd[1]: Failed to start LSB: Bring up/down networking.
-- Subject: Unit network.service has failed
-- Defined-By: systemd
-- Support: http://lists.freedesktop.org/mailman/listinfo/systemd-devel
```

未确认的解决方案：https://www.centos.org/forums/viewtopic.php?t=57187

    date:20180711
___

## linux系统怎样生成uuid

[how-to-create-a-uuid-in-bash](https://serverfault.com/questions/103359/how-to-create-a-uuid-in-bash)

```bash
UUID=$(cat /proc/sys/kernel/random/uuid)
```

date:20180711
___

## 在搭建rsyslog的过程中，遇到“rsyslogd: omfile: creating parent directories for file  'Permission denied' failed: /sf/var/log/messages [v8.24.0]”。
环境：
 - rsyslog 8.24.0
 - CentOS7
 - rsyslog的运行权限用户为root,组为root。
 - /etc/rsyslog.d/xentral.conf的内容为:
    ```
    *.* action(type="omfile" createDirs="on" File="/sf/var/log/xcentral/messages")
    ```
解决过程：
 1. 按照[www.mail-archive.com](https://www.mail-archive.com/rsyslog@lists.adiscon.com/msg03393.html)的配置
	```
    $FileOwner syslog
	$FileGroup adm
	$FileCreateMode 0640
	$DirCreateMode 0755
	$Umask 0022
	$PrivDropToUser syslog
	$PrivDropToGroup syslog
    ```
	依然没有效果。
 2. 参考官方的详细权限配置项[www.rsyslog.com](https://www.rsyslog.com/doc/v8-stable/configuration/modules/omfile.html#id2)，也没有解决问题。
 3. 在命令行执行```setenforce 0```马上不报告错误了。这个是selinux的类似于ACL访问控制，导致的。
 
