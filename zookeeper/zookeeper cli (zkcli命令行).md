## ZooKeeper的命令行操作

运行 `zkCli.sh –server <ip:port>` 进入命令行工具，连接后使用help可以查看所有命令：

```txt
ZooKeeper -server host:port cmd args
        stat path [watch]
        set path data [version]
        ls path [watch]
        delquota [-n|-b] path
        ls2 path [watch]
        setAcl path acl
        setquota -n|-b val path
        history
        redo cmdno
        printwatches on|off
        delete path [version]
        sync path
        listquota path
        rmr path
        get path [watch]
        create [-s] [-e] path data acl
        addauth scheme auth
        quit
        getAcl path
        close
        connect host:port
```

1.使用 ls 命令来查看当前 ZooKeeper 中所包含的内容：

```
[zk: 202.115.36.251:2181(CONNECTED) 1] ls /
```

2.创建一个新的 znode ，使用 create /zk myData 。这个命令创建了一个新的 znode 节点“ zk ”以及与它关联的字符串：

```
[zk: 202.115.36.251:2181(CONNECTED) 2] create /zk "myData“
```

3.我们运行 get 命令来确认 znode 是否包含我们所创建的字符串：

```
[zk: 202.115.36.251:2181(CONNECTED) 3] get /zk
```

监听这个节点的变化,当另外一个客户端改变/zk时,它会打印下面的

```
[zk: localhost:2181(CONNECTED) 4] get /zk watch
WATCHER::
WatchedEvent state:SyncConnected type:NodeDataChanged path:/zk
```

4.下面我们通过 set 命令来对 zk 所关联的字符串进行设置：

```
[zk: 202.115.36.251:2181(CONNECTED) 4] set /zk "zsl“
```

5.下面我们将刚才创建的 znode 删除：

```
[zk: 202.115.36.251:2181(CONNECTED) 5] delete /zk
```

6.如果znode下面有子节点，用delete是删除不了的，要用递归删除：rmr

```
[zk: 202.115.36.251:2181(CONNECTED) 5] rmr /zk
```

实际测试：

```
[root@BC-VM-edce4ac67d304079868c0bb265337bd4 zookeeper-3.4.6]# bin/zkCli.sh -127.0.0.1:2181
Connecting to localhost:2181
2015-06-11 10:55:14,387 [myid:] - INFO  [main:Environment@100] - Client environment:zookeeper.version=3.4.6-1569965, built on 02/20/2014 09:09 GMT
    ...

[zk: localhost:2181(CONNECTED) 5] help
ZooKeeper -server host:port cmd args
        connect host:port
        get path [watch]
        ls path [watch]
        set path data [version]
        rmr path
        delquota [-n|-b] path
        quit
        printwatches on|off
        create [-s] [-e] path data acl
        stat path [watch]
        close
        ls2 path [watch]
        history
        listquota path
        setAcl path acl
        getAcl path
        sync path
        redo cmdno
        addauth scheme auth
        delete path [version]
        setquota -n|-b val path
```

下面测试一下常用的命令：

- **create**：创建路径结点。
- **ls**：查看路径下的所有结点。
- **get**：获得结点上的值。
- **set**：修改结点上的值。
- **delete**：删除结点。

```
[zk: localhost:2181(CONNECTED) 6] create /zktest mydata
Created /zktest
[zk: localhost:2181(CONNECTED) 12] ls /
[zktest, zookeeper]
[zk: localhost:2181(CONNECTED) 7] ls /zktest
[]
[zk: localhost:2181(CONNECTED) 13] get /zktest
mydata
cZxid = 0x1c
ctime = Thu Jun 11 10:58:06 CST 2015
mZxid = 0x1c
mtime = Thu Jun 11 10:58:06 CST 2015
pZxid = 0x1c
cversion = 0
dataVersion = 0
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 6
numChildren = 0
[zk: localhost:2181(CONNECTED) 14] set /zktest junk
cZxid = 0x1c
ctime = Thu Jun 11 10:58:06 CST 2015
mZxid = 0x1f
mtime = Thu Jun 11 10:59:08 CST 2015
pZxid = 0x1c
cversion = 0
dataVersion = 1
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 4
numChildren = 0
[zk: localhost:2181(CONNECTED) 15] delete /zktest
[zk: localhost:2181(CONNECTED) 16] ls /
[zookeeper]
```

## ZooKeeper API的使用

org.apache.zookeeper.Zookeeper是客户端入口主类，负责建立与server的会话。
它提供了如下几类主要方法：

| 功能         | 描述                                |
| ------------ | ----------------------------------- |
| create       | 在本地目录树中创建一个节点          |
| delete       | 删除一个节点                        |
| exists       | 测试本地是否存在目标节点            |
| get/set data | 从目标节点上读取 / 写数据           |
| get/set ACL  | 获取 / 设置目标节点访问控制列表信息 |
| get children | 检索一个子节点上的列表              |
| sync         | 等待要被传送的数据                  |

### 增删改查的demo

```
package com.lzumetal.zookeeper.helloworld;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * Created by liaosi on 2017/11/18.
 */
public class SimpleZkClient {

    private static final Logger log = LoggerFactory.getLogger(SimpleZkClient.class);

    private static String connectString = "server01:2181,server02:2181,server03:2181";
    private static int timeoutMils = 2000;
    private ZooKeeper zooKeeper;


    @Before
    public void init() throws Exception {
        zooKeeper = new ZooKeeper(connectString, timeoutMils, new Watcher() {

            //收到事件通知后的回调函数（即我们自己的实际处理逻辑）
            @Override
            public void process(WatchedEvent event) {
                log.info(event.getPath() + "---->" + event.getType());
            }
        });

    }


    /**
     * 数据的增删改查
     */

    //创建节点
    @Test
    public void testCreate() throws KeeperException, InterruptedException {
        //参数1：节点的路径；参数2：节点的数据；参数3：节点的权限；参数4：节点的类型
        zooKeeper.create("/idea", "hello zookeeper".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        //上传的数据可以是任何类型，但都要转成byte数组
    }

    //获取子节点
    @Test
    public void getGetChild() throws KeeperException, InterruptedException {

        //true表示注册了创建zookeeper时的监听器，也可以重新注册一个新的监听器
        List<String> childrens = zooKeeper.getChildren("/", true);

        for (String children : childrens) {
            log.info(children);
        }

    }

    //判断节点是否存在
    @Test
    public void testExist() throws KeeperException, InterruptedException {
        Stat exists = zooKeeper.exists("/idea", true);
        System.out.println(exists == null ? "Not exist" : "Exist");
    }

    //获取节点数据
    @Test
    public void testGetData() throws KeeperException, InterruptedException {
        byte[] data = zooKeeper.getData("/idea", false, null);
        System.out.println(new String(data));
    }

    //删除节点
    @Test
    public void testDelete() throws KeeperException, InterruptedException {
        //-1表示将所有节点都删除
        zooKeeper.delete("/idea", -1);
    }
}
```