# zookeeper distributed lock (基于zookeeper的分布式锁)

zookeeper的实现比较复杂，但是它提供的模型抽象却是非常简单的。Zookeeper提供一个多层级的节点命名空间（节点称为znode），每个节点都用一个以斜杠（/）分隔的路径表示，而且每个节点都有父节点（根节点除外），非常类似于文件系统。例如，/foo/doo这个表示一个znode，它的父节点为/foo，父父节点为/，而/为根节点没有父节点。与文件系统不同的是，这些节点都可以设置关联的数据，而文件系统中只有文件节点可以存放数据而目录节点不行。Zookeeper为了保证高吞吐和低延迟，在内存中维护了这个树状的目录结构，这种特性使得Zookeeper不能用于存放大量的数据，每个节点的存放数据上限为1M。

而为了保证高可用，zookeeper需要以集群形态来部署，这样只要集群中大部分机器是可用的（能够容忍一定的机器故障），那么zookeeper本身仍然是可用的。客户端在使用zookeeper时，需要知道集群机器列表，通过与集群中的某一台机器建立TCP连接来使用服务，客户端使用这个TCP链接来发送请求、获取结果、获取监听事件以及发送心跳包。如果这个连接异常断开了，客户端可以连接到另外的机器上。

客户端的读请求可以被集群中的任意一台机器处理，如果读请求在节点上注册了监听器，这个监听器也是由所连接的zookeeper机器来处理。对于写请求，这些请求会同时发给其他zookeeper机器并且达成一致后，请求才会返回成功。因此，随着zookeeper的集群机器增多，读请求的吞吐会提高但是写请求的吞吐会下降。

有序性是zookeeper中非常重要的一个特性，所有的更新都是全局有序的，每个更新都有一个唯一的时间戳，这个时间戳称为zxid（Zookeeper Transaction Id）。而读请求只会相对于更新有序，也就是读请求的返回结果中会带有这个zookeeper最新的zxid。

## 基于EMPHEMERAL_SEQUENTIAL类型节点实现分布式锁

参考：

- https://blog.csdn.net/qiangcuo6087/article/details/79067136
- https://www.imooc.com/article/284956?block_id=tuijian_wz

![https://img2.sycdn.imooc.com/5caee45d0001717508780485.jpg](https://img2.sycdn.imooc.com/5caee45d0001717508780485.jpg)

在描述算法流程之前，先看下zookeeper中几个关于节点的有趣的性质：

- 有序节点：假如当前有一个父节点为/lock，我们可以在这个父节点下面创建子节点；zookeeper提供了一个可选的有序特性，例如我们可以创建子节点“/lock/node-”并且指明有序，那么zookeeper在生成子节点时会根据当前的子节点数量自动添加整数序号，也就是说如果是第一个创建的子节点，那么生成的子节点为/lock/node-0000000000，下一个节点则为/lock/node-0000000001，依次类推。
- 临时节点：客户端可以建立一个临时节点，在会话结束或者会话超时后，zookeeper会自动删除该节点。
- 事件监听：在读取数据时，我们可以同时对节点设置事件监听，当节点数据或结构变化时，zookeeper会通知客户端。当前zookeeper有如下四种事件：1）节点创建；2）节点删除；3）节点数据修改；4）子节点变更。

下面描述使用zookeeper实现分布式锁的算法流程，假设锁空间的根节点为/lock：

1. 客户端连接zookeeper，并在/lock下创建临时的且有序的子节点，第一个客户端对应的子节点为/lock/lock-0000000000，第二个为/lock/lock-0000000001，以此类推。
2. 客户端获取/lock下的子节点列表，判断自己创建的子节点是否为当前子节点列表中序号最小的子节点，如果是则认为获得锁，否则监听/lock的子节点变更消息，获得子节点变更通知后重复此步骤直至获得锁；
3. 执行业务代码；
4. 完成业务流程后，删除对应的子节点释放锁。

步骤1中创建的临时节点能够保证在故障的情况下锁也能被释放，考虑这么个场景：假如客户端a当前创建的子节点为序号最小的节点，获得锁之后客户端所在机器宕机了，客户端没有主动删除子节点；如果创建的是永久的节点，那么这个锁永远不会释放，导致死锁；由于创建的是临时节点，客户端宕机后，过了一定时间zookeeper没有收到客户端的心跳包判断会话失效，将临时节点删除从而释放锁。

另外细心的朋友可能会想到，在步骤2中获取子节点列表与设置监听这两步操作的原子性问题，考虑这么个场景：客户端a对应子节点为/lock/lock-0000000000，客户端b对应子节点为/lock/lock-0000000001，客户端b获取子节点列表时发现自己不是序号最小的，但是在设置监听器前客户端a完成业务流程删除了子节点/lock/lock-0000000000，客户端b设置的监听器岂不是丢失了这个事件从而导致永远等待了？这个问题不存在的。因为zookeeper提供的API中设置监听器的操作与读操作是原子执行的，也就是说在读子节点列表时同时设置监听器，保证不会丢失事件。

另外细心的朋友可能会想到，在步骤2中获取子节点列表与设置监听这两步操作的原子性问题，考虑这么个场景：客户端a对应子节点为/lock/lock-0000000000，客户端b对应子节点为/lock/lock-0000000001，客户端b获取子节点列表时发现自己不是序号最小的，但是在设置监听器前客户端a完成业务流程删除了子节点/lock/lock-0000000000，客户端b设置的监听器岂不是丢失了这个事件从而导致永远等待了？这个问题不存在的。因为zookeeper提供的API中设置监听器的操作与读操作是原子执行的，也就是说在读子节点列表时同时设置监听器，保证不会丢失事件。

所以调整后的分布式锁算法流程如下：

- 客户端连接zookeeper，并在/lock下创建临时的且有序的子节点，第一个客户端对应的子节点为/lock/lock-0000000000，第二个为/lock/lock-0000000001，以此类推；
- 客户端获取/lock下的子节点列表，判断自己创建的子节点是否为当前子节点列表中序号最小的子节点，如果是则认为获得锁，否则监听刚好在自己之前一位的子节点删除消息，获得子节点变更通知后重复此步骤直至获得锁；
- 执行业务代码；
- 完成业务流程后，删除对应的子节点释放锁。

## 代码实现

参考：

- https://github.com/twitter-archive/commons/src/java/com/twitter/common/zookeeper/DistributedLockImpl.java

```java
// =================================================================================================
// Copyright 2011 Twitter, Inc.
// -------------------------------------------------------------------------------------------------
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this work except in compliance with the License.
// You may obtain a copy of the License in the LICENSE file, or at:
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =================================================================================================

package com.twitter.common.zookeeper;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.concurrent.ThreadSafe;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;

import com.twitter.common.base.MorePreconditions;

/**
 * Distributed locking via ZooKeeper. Assuming there are N clients that all try to acquire a lock,
 * the algorithm works as follows. Each host creates an ephemeral|sequential node, and requests a
 * list of children for the lock node. Due to the nature of sequential, all the ids are increasing
 * in order, therefore the client with the least ID according to natural ordering will hold the
 * lock. Every other client watches the id immediately preceding its own id and checks for the lock
 * in case of notification. The client holding the lock does the work and finally deletes the node,
 * thereby triggering the next client in line to acquire the lock. Deadlocks are possible but
 * avoided in most cases because if a client drops dead while holding the lock, the ZK session
 * should timeout and since the node is ephemeral, it will be removed in such a case. Deadlocks
 * could occur if the the worker thread on a client hangs but the zk-client thread is still alive.
 * There could be an external monitor client that ensures that alerts are triggered if the least-id
 * ephemeral node is present past a time-out.
 * <p/>
 * Note: Locking attempts will fail in case session expires!
 *
 * @author Florian Leibert
 */
@ThreadSafe
public class DistributedLockImpl implements DistributedLock {

  private static final Logger LOG = Logger.getLogger(DistributedLockImpl.class.getName());

  private final ZooKeeperClient zkClient;
  private final String lockPath;
  private final ImmutableList<ACL> acl;

  private final AtomicBoolean aborted = new AtomicBoolean(false);
  private CountDownLatch syncPoint;
  private boolean holdsLock = false;
  private String currentId;
  private String currentNode;
  private String watchedNode;
  private LockWatcher watcher;

  /**
   * Equivalent to {@link #DistributedLockImpl(ZooKeeperClient, String, Iterable)} with a default
   * wide open {@code acl} ({@link ZooDefs.Ids#OPEN_ACL_UNSAFE}).
   */
  public DistributedLockImpl(ZooKeeperClient zkClient, String lockPath) {
    this(zkClient, lockPath, ZooDefs.Ids.OPEN_ACL_UNSAFE);
  }

  /**
   * Creates a distributed lock using the given {@code zkClient} to coordinate locking.
   *
   * @param zkClient The ZooKeeper client to use.
   * @param lockPath The path used to manage the lock under.
   * @param acl The acl to apply to newly created lock nodes.
   */
  public DistributedLockImpl(ZooKeeperClient zkClient, String lockPath, Iterable<ACL> acl) {
    this.zkClient = Preconditions.checkNotNull(zkClient);
    this.lockPath = MorePreconditions.checkNotBlank(lockPath);
    this.acl = ImmutableList.copyOf(acl);
    this.syncPoint = new CountDownLatch(1);
  }

  private synchronized void prepare()
    throws ZooKeeperClient.ZooKeeperConnectionException, InterruptedException, KeeperException {

    ZooKeeperUtils.ensurePath(zkClient, acl, lockPath);
    LOG.log(Level.FINE, "Working with locking path:" + lockPath);

    // Create an EPHEMERAL_SEQUENTIAL node.
    currentNode =
        zkClient.get().create(lockPath + "/member_", null, acl, CreateMode.EPHEMERAL_SEQUENTIAL);

    // We only care about our actual id since we want to compare ourselves to siblings.
    if (currentNode.contains("/")) {
      currentId = currentNode.substring(currentNode.lastIndexOf("/") + 1);
    }
    LOG.log(Level.FINE, "Received ID from zk:" + currentId);
    this.watcher = new LockWatcher();
  }

  @Override
  public synchronized void lock() throws LockingException {
    if (holdsLock) {
      throw new LockingException("Error, already holding a lock. Call unlock first!");
    }
    try {
      prepare();
      watcher.checkForLock();
      syncPoint.await();
      if (!holdsLock) {
        throw new LockingException("Error, couldn't acquire the lock!");
      }
    } catch (InterruptedException e) {
      cancelAttempt();
      throw new LockingException("InterruptedException while trying to acquire lock!", e);
    } catch (KeeperException e) {
      // No need to clean up since the node wasn't created yet.
      throw new LockingException("KeeperException while trying to acquire lock!", e);
    } catch (ZooKeeperClient.ZooKeeperConnectionException e) {
      // No need to clean up since the node wasn't created yet.
      throw new LockingException("ZooKeeperConnectionException while trying to acquire lock", e);
    }
  }

  @Override
  public synchronized boolean tryLock(long timeout, TimeUnit unit) {
    if (holdsLock) {
      throw new LockingException("Error, already holding a lock. Call unlock first!");
    }
    try {
      prepare();
      watcher.checkForLock();
      boolean success = syncPoint.await(timeout, unit);
      if (!success) {
        return false;
      }
      if (!holdsLock) {
        throw new LockingException("Error, couldn't acquire the lock!");
      }
    } catch (InterruptedException e) {
      cancelAttempt();
      return false;
    } catch (KeeperException e) {
      // No need to clean up since the node wasn't created yet.
      throw new LockingException("KeeperException while trying to acquire lock!", e);
    } catch (ZooKeeperClient.ZooKeeperConnectionException e) {
      // No need to clean up since the node wasn't created yet.
      throw new LockingException("ZooKeeperConnectionException while trying to acquire lock", e);
    }
    return true;
  }

  @Override
  public synchronized void unlock() throws LockingException {
    if (currentId == null) {
      throw new LockingException("Error, neither attempting to lock nor holding a lock!");
    }
    Preconditions.checkNotNull(currentId);
    // Try aborting!
    if (!holdsLock) {
      aborted.set(true);
      LOG.log(Level.INFO, "Not holding lock, aborting acquisition attempt!");
    } else {
      LOG.log(Level.INFO, "Cleaning up this locks ephemeral node.");
      cleanup();
    }
  }

  //TODO(Florian Leibert): Make sure this isn't a runtime exception. Put exceptions into the token?

  private synchronized void cancelAttempt() {
    LOG.log(Level.INFO, "Cancelling lock attempt!");
    cleanup();
    // Bubble up failure...
    holdsLock = false;
    syncPoint.countDown();
  }

  private void cleanup() {
    LOG.info("Cleaning up!");
    Preconditions.checkNotNull(currentId);
    try {
      Stat stat = zkClient.get().exists(currentNode, false);
      if (stat != null) {
        zkClient.get().delete(currentNode, ZooKeeperUtils.ANY_VERSION);
      } else {
        LOG.log(Level.WARNING, "Called cleanup but nothing to cleanup!");
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    holdsLock = false;
    aborted.set(false);
    currentId = null;
    currentNode = null;
    watcher = null;
    syncPoint = new CountDownLatch(1);
  }

  class LockWatcher implements Watcher {

    public synchronized void checkForLock() {
      MorePreconditions.checkNotBlank(currentId);

      try {
        List<String> candidates = zkClient.get().getChildren(lockPath, null);
        ImmutableList<String> sortedMembers = Ordering.natural().immutableSortedCopy(candidates);

        // Unexpected behavior if there are no children!
        if (sortedMembers.isEmpty()) {
          throw new LockingException("Error, member list is empty!");
        }

        int memberIndex = sortedMembers.indexOf(currentId);

        // If we hold the lock
        if (memberIndex == 0) {
          holdsLock = true;
          syncPoint.countDown();
        } else {
          final String nextLowestNode = sortedMembers.get(memberIndex - 1);
          LOG.log(Level.INFO, String.format("Current LockWatcher with ephemeral node [%s], is " +
              "waiting for [%s] to release lock.", currentId, nextLowestNode));

          watchedNode = String.format("%s/%s", lockPath, nextLowestNode);
          Stat stat = zkClient.get().exists(watchedNode, this);
          if (stat == null) {
            checkForLock();
          }
        }
      } catch (InterruptedException e) {
        LOG.log(Level.WARNING, String.format("Current LockWatcher with ephemeral node [%s] " +
            "got interrupted. Trying to cancel lock acquisition.", currentId), e);
        cancelAttempt();
      } catch (KeeperException e) {
        LOG.log(Level.WARNING, String.format("Current LockWatcher with ephemeral node [%s] " +
            "got a KeeperException. Trying to cancel lock acquisition.", currentId), e);
        cancelAttempt();
      } catch (ZooKeeperClient.ZooKeeperConnectionException e) {
        LOG.log(Level.WARNING, String.format("Current LockWatcher with ephemeral node [%s] " +
            "got a ConnectionException. Trying to cancel lock acquisition.", currentId), e);
        cancelAttempt();
      }
    }

    @Override
    public synchronized void process(WatchedEvent event) {
      // this handles the case where we have aborted a lock and deleted ourselves but still have a
      // watch on the nextLowestNode. This is a workaround since ZK doesn't support unsub.
      if (!event.getPath().equals(watchedNode)) {
        LOG.log(Level.INFO, "Ignoring call for node:" + watchedNode);
        return;
      }
      //TODO(Florian Leibert): Pull this into the outer class.
      if (event.getType() == Watcher.Event.EventType.None) {
        switch (event.getState()) {
          case SyncConnected:
            // TODO(Florian Leibert): maybe we should just try to "fail-fast" in this case and abort.
            LOG.info("Reconnected...");
            break;
          case Expired:
            LOG.log(Level.WARNING, String.format("Current ZK session expired![%s]", currentId));
            cancelAttempt();
            break;
        }
      } else if (event.getType() == Event.EventType.NodeDeleted) {
        checkForLock();
      } else {
        LOG.log(Level.WARNING, String.format("Unexpected ZK event: %s", event.getType().name()));
      }
    }
  }
}
```

