# Fisher–Yates shuffle

参考：

- https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle

The Fisher–Yates shuffle, in its original form, was described in 1938 by [Ronald Fisher](https://en.wikipedia.org/wiki/Ronald_Fisher) and [Frank Yates](https://en.wikipedia.org/wiki/Frank_Yates) in their book *Statistical tables for biological, agricultural and medical research*.[[1\]](https://en.wikipedia.org/wiki/Fisher–Yates_shuffle#cite_note-fisheryates-1) Their description of the algorithm used pencil and paper; a table of random numbers provided the randomness. The basic method given for generating a random permutation of the numbers 1 through *N* goes as follows:

1. Write down the numbers from 1 through *N*.
2. Pick a random number *k* between one and the number of unstruck numbers remaining (inclusive).
3. Counting from the low end, strike out the *k*th number not yet struck out, and write it down at the end of a separate list.
4. Repeat from step 2 until all the numbers have been struck out.
5. The sequence of numbers written down in step 3 is now a random permutation of the original numbers.

## Pencil-and-paper method

As an example, we'll permute the numbers from 1 to 8 using [Fisher and Yates' original method](https://en.wikipedia.org/wiki/Fisher–Yates_shuffle#Fisher_and_Yates'_original_method). We'll start by writing the numbers out on a piece of scratch paper:

| Range | Roll |     Scratch     | Result |
| :---: | :--: | :-------------: | :----: |
|       |      | 1 2 3 4 5 6 7 8 |        |

Now we roll a random number *k* from 1 to 8—let's make it 3—and strike out the *k*th (i.e. third) number on the scratch pad and write it down as the result:

| Range | Roll |     Scratch     | Result |
| :---: | :--: | :-------------: | :----: |
|  1–8  |  3   | 1 2 3 4 5 6 7 8 |   3    |

Now we pick a second random number, this time from 1 to 7: it turns out to be 4. Now we strike out the fourth number *not yet struck* off the scratch pad—that's number 5—and add it to the result:

| Range | Roll |     Scratch     | Result |
| :---: | :--: | :-------------: | :----: |
|  1–7  |  4   | 1 2 3 4 5 6 7 8 |  3 5   |

Now we pick the next random number from 1 to 6, and then from 1 to 5, and so on, always repeating the strike-out process as above:

| Range | Roll |     Scratch     |     Result      |
| :---: | :--: | :-------------: | :-------------: |
|  1–6  |  5   | 1 2 3 4 5 6 7 8 |      3 5 7      |
|  1–5  |  3   | 1 2 3 4 5 6 7 8 |     3 5 7 4     |
|  1–4  |  4   | 1 2 3 4 5 6 7 8 |    3 5 7 4 8    |
|  1–3  |  1   | 1 2 3 4 5 6 7 8 |   3 5 7 4 8 1   |
|  1–2  |  2   | 1 2 3 4 5 6 7 8 |  3 5 7 4 8 1 6  |
|       |      | 1 2 3 4 5 6 7 8 | 3 5 7 4 8 1 6 2 |

```c++
vector<int> Fisher_Yates_Shuffle(vector<int>& arr)
{
    std::random_device rd;
    srand(rd());
    vector<int> ret;
    for (int i = 0, j, n = arr.size(); i < n; ++i) {
        j = rand() % arr.size();
        ret.push_back(arr[j]);
        arr.erase(arr.begin() + j);
    }
    return ret;
}
```

## Modern method - [Durstenfeld's version](https://en.wikipedia.org/wiki/Fisher–Yates_shuffle#The_modern_algorithm)

We'll now do the same thing using [Durstenfeld's version](https://en.wikipedia.org/wiki/Fisher–Yates_shuffle#The_modern_algorithm) of the algorithm: this time, instead of striking out the chosen numbers and copying them elsewhere, we'll swap them with the last number not yet chosen. We'll start by writing out the numbers from 1 to 8 as before:

| Range | Roll |     Scratch     | Result |
| :---: | :--: | :-------------: | :----: |
|       |      | 1 2 3 4 5 6 7 8 |        |

For our first roll, we roll a random number from 1 to 8: this time it is 6, so we swap the 6th and 8th numbers in the list:

| Range | Roll |      Scratch      | Result |
| :---: | :--: | :---------------: | :----: |
|  1–8  |  6   | 1 2 3 4 5 **8** 7 | **6**  |

The next random number we roll from 1 to 7, and turns out to be 2. Thus, we swap the 2nd and 7th numbers and move on:

| Range | Roll |     Scratch     | Result  |
| :---: | :--: | :-------------: | :-----: |
|  1–7  |  2   | 1 **7** 3 4 5 8 | **2** 6 |

The next random number we roll is from 1 to 6, and just happens to be 6, which means we leave the 6th number in the list (which, after the swap above, is now number 8) in place and just move to the next step. Again, we proceed the same way until the permutation is complete:

| Range | Roll |   Scratch   |      Result       |
| :---: | :--: | :---------: | :---------------: |
|  1–6  |  6   |  1 7 3 4 5  |     **8** 2 6     |
|  1–5  |  1   | **5** 7 3 4 |    **1** 8 2 6    |
|  1–4  |  3   |  5 7 **4**  |   **3** 1 8 2 6   |
|  1–3  |  3   |     5 7     |  **4** 3 1 8 2 6  |
|  1–2  |  1   |    **7**    | **5** 4 3 1 8 2 6 |

At this point there's nothing more that can be done, so the resulting permutation is 7 5 4 3 1 8 2 6.

```c++
void Knuth_Durstenfeld_Shuffle(vector<int>& arr)
{
    int j;
    std::random_device rd;
    srand(rd());
    for (int i = arr.size()-1;i>=0;--i)
    {
        j = rand() % (i + 1);
        swap(arr[j],arr[i]);
    }
}
```

## The "inside-out" algorithm

基本思思是从前向后扫描数组，把数组位置i的数据随机插入到前i个（包括第i个）位置中（假设为k），这个操作是在新数组中进行，然后把原始数组中位置k的数字替换到新数组位置i的数字。其实效果相当于新数组中位置k和位置i的数字进行交换。

**伪代码：**

```c++
To initialize an array a of n elements to a randomly shuffled copy of source, both 0-based:
  for i from 0 to n − 1 do
      j ← random integer such that 0 ≤ j ≤ i
      if j ≠ i
          a[i] ← a[j]
      a[j] ← source[i]
```

**具体实现：**

```c++
vector<int> Inside_Out_Shuffle(const vector<int> &arr) {
  vector<int> ret(arr.size(), 0);
  copy(arr.begin(), arr.end(), ret.begin());
  std::random_device rd;
  srand(rd());
  for (int i = 0, j; i < arr.size(); ++i) {
    j = rand() % (i + 1);
    ret[i] = ret[j];
    ret[j] = arr[i];
  }
  return ret;
}
```





