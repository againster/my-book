# DJBX33A hash (DJBX33A哈希算法)

参考：

- https://www.python.org/dev/peps/pep-0456/
- https://www.laruence.com/2009/07/23/994.html
- https://blog.csdn.net/wusuopubupt/article/details/11479869
- https://stackoverflow.com/questions/1579721/why-are-5381-and-33-so-important-in-the-djb2-algorithm

time33 哈希函数，又叫 DJBX33A 哈希函数。

最简单版本，这个版本最可以体现time33的算法思路，够简单。

```c
uint32_t time33(char const *str, int len) {
  unsigned long hash = 0;
  for (int i = 0; i < len; i++) {
    hash = hash * 33 + (unsigned long)str[i];
  }
  return hash;
}
```

把乘法换成位操作。

```c
unsigned long time33(char const *str, int len) {
  unsigned long hash = 0;
  for (int i = 0; i < len; i++) {
    hash = ((hash << 5) + hash) + (unsigned long)str[i];
  }
  return hash;
}
```

和变种：

```c
unsigned long hash(unsigned char *str) {
  unsigned long hash = 5381;
  int c;

  while (c = *str++)
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

  return hash;
}
```

至于说, 为什么是Times 33而不是Times 其他数字：

```txt
DJBX33A (Daniel J. Bernstein, Times 33 with Addition)
  This is Daniel J. Bernstein's popular `times 33' hash function as
  posted by him years ago on comp.lang.c. It basically uses a function
  like ``hash(i) = hash(i-1) * 33 + str[i]''. This is one of the best
  known hash functions for strings. Because it is both computed very
  fast and distributes very well.
  The magic of number 33, i.e. why it works better than many other
  constants, prime or not, has never been adequately explained by
  anyone. So I try an explanation: if one experimentally tests all
  multipliers between 1 and 256 (as RSE did now) one detects that even
  numbers are not useable at all. The remaining 128 odd numbers
  (except for the number 1) work more or less all equally well. They
  all distribute in an acceptable way and this way fill a hash table
  with an average percent of approx. 86%.
  If one compares the Chi^2 values of the variants, the number 33 not
  even has the best value. But the number 33 and a few other equally
  good numbers like 17, 31, 63, 127 and 129 have nevertheless a great
  advantage to the remaining numbers in the large set of possible
  multipliers: their multiply operation can be replaced by a faster
  operation based on just one shift plus either a single addition
  or subtraction operation. And because a hash function has to both
  distribute good _and_ has to be very fast to compute, those few
  numbers should be preferred and seems to be the reason why Daniel J.
  Bernstein also preferred it.
                   -- Ralf S. Engelschall <rse@engelschall.com>
```

> This hash function is similar to a [Linear Congruential Generator](http://en.wikipedia.org/wiki/Linear_congruential_generator) (LCG - a simple class of functions that generate a series of psuedo-random numbers), which generally has the form:
>
> ```
> X = (a * X) + c;  // "mod M", where M = 2^32 or 2^64 typically
> ```
>
> Note the similarity to the djb2 hash function... a=33, M=2^32. In order for an LCG to have a "full period" (i.e. as random as it can be), *a* must have certain properties:
>
> - a-1 is divisible by all prime factors of M (a-1 is 32, which is divisible by 2, the only prime factor of 2^32)
> - a-1 is a multiple of 4 if M is a multiple of 4 (yes and yes)
>
> In addition, *c* and *M* are supposed to be relatively prime (which will be true for odd values of *c*).
>
> So as you can see, this hash function somewhat resembles a good LCG. And when it comes to hash functions, you want one that produces a "random" distribution of hash values given a realistic set of input strings.
>
> As for why this hash function is good for strings, I think it has a good balance of being extremely fast, while providing a reasonable distribution of hash values. But I've seen many other hash functions which claim to have much better output characteristics, but involved many more lines of code. For instance see [this page about hash functions](http://www.azillionmonkeys.com/qed/hash.html)

还有其他的一些hash算法，murmurhash，cityhash，siphash。