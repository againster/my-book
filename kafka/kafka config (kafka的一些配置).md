

#### [heartbeat.interval.ms](https://kafka.apache.org/documentation/#heartbeat.interval.ms)

The expected time between heartbeats to the group coordinator when using Kafka's group management facilities. Heartbeats are used to ensure that the worker's session stays active and to facilitate rebalancing when new members join or leave the group. The value must be set lower than `session.timeout.ms`, but typically should be set no higher than 1/3 of that value. It can be adjusted even lower to control the expected time for normal rebalances.

|         Type: | int              |
| ------------: | ---------------- |
|      Default: | 3000 (3 seconds) |
| Valid Values: |                  |
|   Importance: | high             |

#### [session.timeout.ms](https://kafka.apache.org/documentation/#session.timeout.ms)

The timeout used to detect worker failures. The worker sends periodic heartbeats to indicate its liveness to the broker. If no heartbeats are received by the broker before the expiration of this session timeout, then the broker will remove the worker from the group and initiate a rebalance. Note that the value must be in the allowable range as configured in the broker configuration by `group.min.session.timeout.ms` and `group.max.session.timeout.ms`.

|         Type: | int                |
| ------------: | ------------------ |
|      Default: | 10000 (10 seconds) |
| Valid Values: |                    |
|   Importance: | high               |

#### [rebalance.timeout.ms](https://kafka.apache.org/documentation/#rebalance.timeout.ms)

The maximum allowed time for each worker to join the group once a rebalance has begun. This is basically a limit on the amount of time needed for all tasks to flush any pending data and commit offsets. If the timeout is exceeded, then the worker will be removed from the group, which will cause offset commit failures.

|         Type: | int              |
| ------------: | ---------------- |
|      Default: | 60000 (1 minute) |
| Valid Values: |                  |
|   Importance: | high             |

#### [max.poll.interval.ms](https://kafka.apache.org/documentation/#max.poll.interval.ms)

The maximum delay between invocations of poll() when using consumer group management. This places an upper bound on the amount of time that the consumer can be idle before fetching more records. If poll() is not called before expiration of this timeout, then the consumer is considered failed and the group will rebalance in order to reassign the partitions to another member. For consumers using a non-null `group.instance.id` which reach this timeout, partitions will not be immediately reassigned. Instead, the consumer will stop sending heartbeats and partitions will be reassigned after expiration of `session.timeout.ms`. This mirrors the behavior of a static consumer which has shutdown.

|         Type: | int                |
| ------------: | ------------------ |
|      Default: | 300000 (5 minutes) |
| Valid Values: | [1,...]            |
|   Importance: | medium             |

#### [max.poll.records](https://kafka.apache.org/documentation/#max.poll.records)

The maximum number of records returned in a single call to poll().

|         Type: | int     |
| ------------: | ------- |
|      Default: | 500     |
| Valid Values: | [1,...] |
|   Importance: | medium  |

[partition.assignment.strategy](https://kafka.apache.org/documentation/#partition.assignment.strategy)

A list of class names or class types, ordered by preference, of supported partition assignment strategies that the client will use to distribute partition ownership amongst consumer instances when group management is used.

In addition to the default class specified below, you can use the `org.apache.kafka.clients.consumer.RoundRobinAssignor`class for round robin assignments of partitions to consumers.

Implementing the `org.apache.kafka.clients.consumer.ConsumerPartitionAssignor` interface allows you to plug in a custom assignmentstrategy.

|         Type: | list                                                  |
| ------------: | ----------------------------------------------------- |
|      Default: | class org.apache.kafka.clients.consumer.RangeAssignor |
| Valid Values: | non-null string                                       |
|   Importance: | medium                                                |

#### [auto.commit.interval.ms](https://kafka.apache.org/documentation/#auto.commit.interval.ms)

The frequency in milliseconds that the consumer offsets are auto-committed to Kafka if `enable.auto.commit` is set to `true`.

|         Type: | int              |
| ------------: | ---------------- |
|      Default: | 5000 (5 seconds) |
| Valid Values: | [0,...]          |
|   Importance: | low              |

#### [flush.messages](https://kafka.apache.org/documentation/#flush.messages)

This setting allows specifying an interval at which we will force an fsync of data written to the log. For example if this was set to 1 we would fsync after every message; if it were 5 we would fsync after every five messages. In general we recommend you not set this and use replication for durability and allow the operating system's background flush capabilities as it is more efficient. This setting can be overridden on a per-topic basis (see [the per-topic configuration section](https://kafka.apache.org/documentation/#topicconfigs)).

|                    Type: | long                        |
| -----------------------: | --------------------------- |
|                 Default: | 9223372036854775807         |
|            Valid Values: | [0,...]                     |
| Server Default Property: | log.flush.interval.messages |
|              Importance: | medium                      |

#### [flush.ms](https://kafka.apache.org/documentation/#flush.ms)

This setting allows specifying a time interval at which we will force an fsync of data written to the log. For example if this was set to 1000 we would fsync after 1000 ms had passed. In general we recommend you not set this and use replication for durability and allow the operating system's background flush capabilities as it is more efficient.

|                    Type: | long                  |
| -----------------------: | --------------------- |
|                 Default: | 9223372036854775807   |
|            Valid Values: | [0,...]               |
| Server Default Property: | log.flush.interval.ms |
|              Importance: | medium                |

