# API接口自动化测试 - newman/postman

- [简介](##简介)
- [安装](##安装)
- [使用方法](##使用方法)
- [命令行选项](##命令行选项)
- [newman VS selinium](##newman\ VS\ selinium)
- [参考](##参考)

***Author: lizhi***

***Date: 2019/11/26***

***Change a bit, change the world.***

## 简介

Newman是Postman的一个命令行形式的套件测试工具。它允许你以命令行形式轻松地运行和测试Postman的套件。它构建时考虑到了扩展性，所以你可以把它轻松的整合进你的CI服务器和构建系统中。

> Newman is a command-line collection runner for Postman. It allows you to effortlessly run and test a Postman collection directly from the command-line. It is built with extensibility in mind so that you can easily integrate it with your continuous integration servers and build systems.

## 安装

1. 安装nodejs

    ```bash
    ## 安装nodejs
    yum install -y nodejs

    ## 配置npm的registry，请修改下载速度最快的仓库
    cat << __END__ ~/.npmrc
    registry = "http://npm.sangfor.org/"
    __END__
    ```
    nodejs是跨平台的javascript库，请移步至[Installing Node.js via package manager](##参考)，查看其他平台的安装方法。

2. 安装newman

    ```bash
    ## 安装newman
    npm install -g newman
    ```

## 使用方法

首先看一下API自动化测试的效果：

```bash
$ newman run account.postman_collection.json -e dev-account.postman_environment.json -d account.testcase.csv ## 执行命令
newman

account

Iteration 1/3

→ /es/v1/account/create
  POST http://10.118.16.25:8080/es/v1/account/create [200 OK, 155B, 146ms]
  √  Successful POST request
  √  Check Response Code

Iteration 2/3

→ /es/v1/account/create
  POST http://10.118.16.25:8080/es/v1/account/create [200 OK, 155B, 20ms]
  √  Successful POST request
  √  Check Response Code

Iteration 3/3

→ /es/v1/account/create
  POST http://10.118.16.25:8080/es/v1/account/create [200 OK, 155B, 20ms]
  √  Successful POST request
  √  Check Response Code

┌─────────────────────────┬───────────────────┬───────────────────┐
│                         │          executed │            failed │
├─────────────────────────┼───────────────────┼───────────────────┤
│              iterations │                 3 │                 0 │
├─────────────────────────┼───────────────────┼───────────────────┤
│                requests │                 3 │                 0 │
├─────────────────────────┼───────────────────┼───────────────────┤
│            test-scripts │                 3 │                 0 │
├─────────────────────────┼───────────────────┼───────────────────┤
│      prerequest-scripts │                 0 │                 0 │
├─────────────────────────┼───────────────────┼───────────────────┤
│              assertions │                 6 │                 0 │
├─────────────────────────┴───────────────────┴───────────────────┤
│ total run duration: 317ms                                       │
├─────────────────────────────────────────────────────────────────┤
│ total data received: 96B (approx)                               │
├─────────────────────────────────────────────────────────────────┤
│ average response time: 62ms [min: 20ms, max: 146ms, s.d.: 59ms] │
└─────────────────────────────────────────────────────────────────┘
```

其中account.postman_collection.json文件定义了RESTful请求参数，javascript响应断言；-e dev-account.postman_environment.json文件定义了环境变量；-d account.testcase.csv定义了测试案例。这两个json文件都可以在postmant中编辑完成后，导出给newman使用。具体如何使用请移步至[Working with data files](##参考)。

## 集成到Jekins

既然有了命令行，那集成到CI/CD就很简单了，请移步至[Integration with Jenkins](##参考)。

## 命令行选项

newman命令行语法：

```bash
newman run <collection-file-source> [options]

-e <source>, --environment <source>
Specify an environment file path or URL. Environments provide a set of variables that one can use within collections. Read More

-g <source>, --globals <source>
Specify the file path or URL for global variables. Global variables are similar to environment variables but have a lower precedence and can be overridden by environment variables having the same name.

-d <source>, --iteration-data <source>
Specify a data source file (CSV) to be used for iteration as a path to a file or as a URL. Read More

-n <number>, --iteration-count <number>
Specifies the number of times the collection has to be run when used in conjunction with iteration data file.

--folder <name>
Run requests within a particular folder/folders in a collection. Multiple folders can be specified by using --folder multiple times, like so: --folder f1 --folder f2.

--working-dir <path>
Set the path of the working directory to use while reading files with relative paths. Default to current directory.

--no-insecure-file-read
Prevents reading of files situated outside of the working directory.

--export-environment <path>
The path to the file where Newman will output the final environment variables file before completing a run.

--export-globals <path>
The path to the file where Newman will output the final global variables file before completing a run.

--export-collection <path>
The path to the file where Newman will output the final collection file before completing a run.

--timeout <ms>
Specify the time (in milliseconds) to wait for the entire collection run to complete execution.

--timeout-request <ms>
Specify the time (in milliseconds) to wait for requests to return a response.

--timeout-script <ms>
Specify the time (in milliseconds) to wait for scripts to complete execution.

-k, --insecure
Disables SSL verification checks and allows self-signed SSL certificates.

--ignore-redirects
Prevents newman from automatically following 3XX redirect responses.

--delay-request
Specify the extent of delay between requests (milliseconds).

--bail [optional modifiers]
Specify whether or not to stop a collection run on encountering the first test script error.
Can optionally accept modifiers, currently include folder and failure.
folder allows you to skip the entire collection run in case an invalid folder was specified using the --folder option or an error was encountered in general.
On the failure of a test, failure would gracefully stop a collection run after completing the current test script.

-x, --suppress-exit-code
Specify whether or not to override the default exit code for the current run.

--color <value>
Enable or Disable colored CLI output. The color value can be any of the three: on, off or auto(default).
With auto, Newman attempts to automatically turn color on or off based on the color support in the terminal. This behaviour can be modified by using the on or off value accordingly.

--disable-unicode
Specify whether or not to force the unicode disable option. When supplied, all symbols in the output will be replaced by their plain text equivalents.

--global-var "<global-variable-name>=<global-variable-value>"
Allows the specification of global variables via the command line, in a key=value format. Multiple CLI global variables can be added by using --global-var multiple times, like so: --global-var "foo=bar" --global-var "alpha=beta".

--env-var "<environment-variable-name>=<environment-variable-value>"
Allows the specification of environment variables via the command line, in a key=value format. Multiple CLI environment variables can be added by using --env-var multiple times, like so: --env-var "foo=bar" --env-var "alpha=beta".

--verbose
Show detailed information of collection run and each request sent.
```

## newman VS selinium

selinium和newman的github下载量和star请移步至[chakram vs cypress vs newman vs supertest vs nightwatch vs selenium-webdriver](##参考)。
个人拙见：selenium是面向UI的自动化测试，newman是面向API接口的自动化测试，这决定了他们有所不同，newman更侧重于测试接口边界，接口功能完备性，selenium更侧重于接口与UI集成后整体功能的完备性。两者并不冲突，先进行API接口自动化测试，后进行UI自动化测试。

## 参考

1. [github newman](https://github.com/postmanlabs/newman)
2. [Installing Node.js via package manager](https://nodejs.org/en/download/package-manager/)
3. [Working with data files](https://learning.getpostman.com/docs/postman/collection-runs/working-with-data-files/)
4. [Integration with Jenkins](https://learning.getpostman.com/docs/postman/collection-runs/integration-with-jenkins/)
5. [chakram vs cypress vs newman vs supertest vs nightwatch vs selenium-webdriver](https://www.npmtrends.com/chakram-vs-cypress-vs-newman-vs-supertest-vs-nightwatch-vs-selenium-webdriver)