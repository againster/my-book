#!/bin/bash
#servers=(192.168.0.1 192.168.0.2)
servers=`cat /etc/hosts | grep "^10.*" | awk '{print $2}' | xargs`

function install_zabbix_agent {
	ssh -p 22 root@$1 << ENDSSH
rpm -ivh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-1.el7.noarch.rpm
yum install -y zabbix-agent
setenforce 0
rm -rf /var/log/zabbix
mkdir -p /var/log/zabbix
chown zabbix:zabbix /var/run/zabbix
chmod 0775 /var/run/zabbix
iptables -I INPUT -p tcp --dport 10050 -j ACCEPT
sed -i "s/^Server=127.0.0.1/Server=0.0.0.0\/0/g" /etc/zabbix/zabbix_agentd.conf
sed -i "s/^ServerActive=127.0.0.1/#ServerActive=127.0.0.1/" /etc/zabbix/zabbix_agentd.conf
service zabbix-agent start
ps -ef |grep zabbix
ENDSSH
}

for x in ${servers[*]}
do
    install_zabbix_agent $x
done