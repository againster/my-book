# zabbix

***Author: ArrowLee***

***Date: 2019/2/15***

---

## zabbix基础安装

1. 安装Zabbix镜像源
    ```bash
    # RHEL 7
    rpm -ivh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-1.el7.noarch.rpm
    ```

## zabbix server安装

参考官方网站

- [install_from_packages](https://www.zabbix.com/documentation/4.0/manual/installation/install_from_packages/rhel_centos)
- [db_scripts](https://www.zabbix.com/documentation/4.0/manual/appendix/install/db_scripts#mysql)

1. Zabbix server安装
    ```bash
    yum install -y zabbix-server-mysql
    yum install -y zabbix-web-mysql
    ```
2. 配置MySQL
    ```bash
    # 创建database
    MYSQL_ROOT_PASSWORD=123456
    mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE zabbix CHARACTER SET utf8 COLLATE utf8_bin; GRANT ALL PRIVILEGES ON zabbix.* TO zabbix@'%' IDENTIFIED BY '$MYSQL_ROOT_PASSWORD'; FLUSH PRIVILEGES;"

    # 初始化zabbix数据库
    zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -p$MYSQL_ROOT_PASSWORD zabbix
    ```
3. 配置server
    修改如下的配置项:
    ```ini
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=123456
    ```
4. 启动服务
    ```bash
    # 启动server
    setenforce 0
    service zabbix-server start
    ```

## zabbix agent安装

    ```bash
    #安装zabbix agent
    yum install -y zabbix-agent

    #创建zabbix agent的log目录
    setenforce 0
    mkdir -p /var/log/zabbix
    chown zabbix:zabbix /var/log/zabbix
    chmod 0775 /var/log/zabbix

    #更改配置项
    sed -i "s/^Server=127.0.0.1/Server=0.0.0.0\/0/g" /etc/zabbix/zabbix_agentd.conf
    sed -i "s/^ServerActive=127.0.0.1/#ServerActive=127.0.0.1/" /etc/zabbix/zabbix_agentd.conf

    #开放防火墙
    iptables -I INPUT -p tcp --dport 10050 -j ACCEPT

    #启动及检测
    service zabbix-agent start
    ps -ef | grep zabbix
    ```
