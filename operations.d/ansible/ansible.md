# ansible

## 安装ansible控制机

参考[intro_installation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

控制机要求：
> Currently Ansible can be run from any machine with Python 2 (version 2.7) or Python 3 (versions 3.5 and higher) installed. Windows isn’t supported for the control machine.

被管理节点要求：
> On the managed nodes, you need a way to communicate, which is normally ssh. By default this uses sftp. If that’s not available, you can switch to scp in ansible.cfg. You also need Python 2 (version 2.6 or later) or Python 3 (version 3.5 or later).

- On Fedora:

    ```bash
    sudo dnf install ansible
    ```
- On RHEL and CentOS:
    ```bash
    sudo yum install ansible
    ```

## 配置ansible

参考[intro_configuration](https://docs.ansible.com/ansible/latest/installation_guide/intro_configuration.html)

```ini
[defaults]
host_key_checking = False
```

1. host_key_checking的解释

    参考[Host Key Checking](https://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html#host-key-checking)

    当初次ssh到其他主机时会询问需要添加到已知主机列表‘known_hosts’，配置为False避免询问

    Ansible has host key checking enabled by default.

    If a host is reinstalled and has a different key in ‘known_hosts’, this will result in an error message until corrected. If a host is not initially in ‘known_hosts’ this will result in prompting for confirmation of the key, which results in an interactive experience if using Ansible, from say, cron. You might not want this.

    If you understand the implications and wish to disable this behavior, you can do so by editing /etc/ansible/ansible.cfg or ~/.ansible.cfg:

    ```ini
    [defaults]
    host_key_checking = False
    ```

    Alternatively this can be set by the ANSIBLE_HOST_KEY_CHECKING environment variable:

    ```bash
    export ANSIBLE_HOST_KEY_CHECKING=False
    ```

    Also note that host key checking in paramiko mode is reasonably slow, therefore switching to ‘ssh’ is also recommended when using this feature.

## 命令行概览

参考[command_line_tools](https://docs.ansible.com/ansible/latest/user_guide/command_line_tools.html)

Below is a complete list of Ansible utilities

- ansible: Define and run a single task ‘playbook’ against a set of hosts
- ansible-config: View, edit, and manage ansible configuration.
- ansible-console: REPL console for executing Ansible tasks.
- ansible-doc: plugin documentation tool
- ansible-galaxy: Perform various Role related operations.
- ansible-inventory: None
- ansible-playbook: Runs Ansible playbooks, executing the defined tasks on the targeted hosts.
- ansible-pull: pulls playbooks from a VCS repo and executes them for the local host
- ansible-vault: encryption/decryption utility for Ansible data files

## inventory

参考[intro_inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

通过inventory列表ansible可以工作在多个系统之间，通过```/etc/ansible/hosts```和```-i <path>```可以指定。
Ansible works against multiple systems in your infrastructure at the same time. It does this by selecting portions of systems listed in Ansible’s inventory, which defaults to being saved in the location ```/etc/ansible/hosts```. You can specify a different inventory file using the ```-i <path>``` option on the command line.

inventory可以有YAML, ini等配置格式。Not only is this inventory configurable, but you can also use multiple inventory files at the same time and pull inventory from dynamic or cloud sources or different formats (YAML, ini, etc)

1. 主机和组(Hosts and Groups)

2. 主机变量(Host Variables)

3. 组变量(Group Variables)

4. 组的组，和组变量(Groups of Groups, and Group Variables)

5. 默认组(Default groups)

    all和ungrouped是两个默认组，all包含所有主机，ungrouped包含除了没有被组包含的主机。这两个组是隐藏的没有出现在组列表中。
    There are two default groups: ```all``` and ```ungrouped```. ```all``` contains every host. ```ungrouped``` contains all hosts that don’t have another group aside from all. Every host will always belong to at least 2 groups. Though ```all``` and ```ungrouped``` are always present, they can be implicit and not appear in group listings like ```group_names```.

6. 分隔组合主机定义的数据(Splitting Out Host and Group Specific Data)

    参考[splitting-out-host-and-group-specific-data](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#splitting-out-host-and-group-specific-data)

    在ansible中最好的实践是不要存储变量和inventory文件。
    The preferred practice in Ansible is to not store variables in the main inventory file.

    主机和组变量可以存储在独立的文件中，通过相对于inventory文件。
    In addition to storing variables directly in the inventory file, host and group variables can be stored in individual files relative to the inventory file (not directory, it is always the file).

7. 变量如何被合并(How Variables Are Merged)

    By default variables are merged/flattened to the specific host before a play is run. This keeps Ansible focused on the Host and Task, so groups don’t really survive outside of inventory and host matching.The order/precedence is (from lowest to highest):

    - all group (because it is the ‘parent’ of all other groups)
    - parent group
    - child group
    - host

8. inventory参数(List of Behavioral Inventory Parameters)

    参考[list-of-behavioral-inventory-parameters](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#list-of-behavioral-inventory-parameters)


## playbooks