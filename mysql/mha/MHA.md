
# MHA

- [MySQL安装](##MySQL安装)
- [MySQL基础配置](##MySQL基础配置)
- [Replication 配置](##Replication-配置)
- [Replication 半同步配置](##Replication-半同步配置)
  - [以运行时方式配置](###以运行时方式配置)
  - [以配置文件方式配置](###以配置文件方式配置)
- [安装MHA](##安装MHA)
  - [MHA Node, MHA Manager前置配置](###MHA-Node,-MHA-Manager前置配置)
  - [安装MHA Node](###安装MHA-Node)
  - [安装配置MHA Manager](###安装配置MHA-Manager)
  - [配置Slave自动清除relay log](###配置Slave自动清除relay-log)
  - [配置Master虚拟ip](###配置Master虚拟ip)
- [Wiki](##Wiki)
- [Refer](##Refer)
- [FAQ](##FAQ)

***Author: ArrowLee***

***Date: 2019/1/4***

---

## MySQL安装

1. 解压tar.gz包到/opt目录
    ```bash
    tar zxvf mysql-5.7.24-linux-glibc2.12-x86_64.tar.gz -C /opt
    mv /opt/mysql-5.7.24-linux-glibc2.12-x86_64 /opt/mysql
    ```
2. 配置环境变量

    ```bash
    #创建/etc/profile.d/mysql.sh
    touch /etc/profile.d/mysql.sh

    #向文件中写入如下内容
    cat << -DONE- > /etc/profile.d/mysql.sh
    #!/bin/bash
    MYSQL_HOME=/opt/mysql
    PATH=\$MYSQL_HOME/bin:\$MYSQL_HOME/support-files:\$PATH
    export PATH
    -DONE-

    #使配置的环境变量生效
    source /etc/profile.d/mysql.sh
    ```

## MySQL基础配置

1. 创建MySQL用户和组

    ```bash
    groupadd -f mysql
    useradd -r -g mysql mysql
    ```

2. MySQL基础配置

    向/etc/my.cnf文件添加配置
    ```ini
    [mysqld]
    port=3306
    user=mysql
    basedir=/opt/mysql
    datadir=/opt/data/mysql/db
    tmpdir=/opt/data/mysql/tmp
    plugin-dir=/opt/mysql/lib/plugin
    character-sets-dir=/opt/mysql/share/charsets
    general-log-file=/opt/data/mysql/log/general.log
    log_error=/opt/data/mysql/log/error.log
    slow-query-log-file=/opt/data/mysql/log/slow.log
    pid-file=/opt/data/mysql/run/mysql.pid
    socket=/opt/data/mysql/run/mysql.sock
    explicit_defaults_for_timestamp=TRUE
    sql_mode="STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"
    lower_case_table_names=1
    character-set-server=utf8

    [mysql]
    default-character-set=utf8

    [client]
    socket=/opt/data/mysql/run/mysql.sock
    ```

3. 初始化数据目录
    ```bash
    #初始化数据目录/opt/data/mysql
    mkdir -p /opt/data/mysql/db /opt/data/mysql/tmp /opt/data/mysql/log /opt/data/mysql/run
    chown --recursive mysql:mysql /opt/data/mysql/*
    mysqld --defaults-file=/etc/my.cnf --initialize --user=mysql
    ```

4. 启动，修改密码，修改远程连接权限
    ```bash
    #启动MySQL
    mysql.server start

    #系统分配的初始密码
    MYSQL_SYS_PASSWORD=`cat /opt/data/mysql/log/error.log | grep -o "root@localhost: .*" | sed "s/root@localhost: //"`
    #请更改初始密码MYSQL_INIT_PASSWORD
    MYSQL_INIT_PASSWORD="123456"

    #更改初始密码
    mysql --connect-expired-password -h localhost -uroot -p$MYSQL_SYS_PASSWORD -e "ALTER USER root@localhost IDENTIFIED BY '$MYSQL_INIT_PASSWORD';"

    #修改远程连接权限
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD mysql -e "UPDATE user SET host = '%' WHERE user = 'root'; FLUSH PRIVILEGES;"
    ```

## Replication 配置

参考官方网站：[replication-howto](https://dev.mysql.com/doc/refman/5.5/en/replication-howto.html)，以下两种基于 Binary Log File Position 和 GTIDs 的方式任选其一配置即可。

### 创建 Replication 账号

参考官方网站：[replication-howto-repuser](https://dev.mysql.com/doc/refman/5.7/en/replication-howto-repuser.html)

每个连接到 Master 的 Slave 都使用了 MySQL 用户名和密码，因此必须有一个用户帐户供 Slave 连接。在设置主从复制时，由 ```CHANGE MASTER TO``` 命令上的 ```MASTER_USER``` 选项指定用户名。只要账号被授予了 [`REPLICATION SLAVE`](https://dev.mysql.com/doc/refman/5.7/en/privileges-provided.html#priv_replication-slave) 权限，任何帐户都可以用于此操作。你可以选择为每个 Slave 服务器创建不同的帐户，或者为每个 Slave 服务器使用相同的帐户连接到 Master 服务器。

虽然不必专门创建用于主从复制的帐户，但你应该注意主从复制的用户名和密码以纯文本的形式存储在 Master 的信息存储库文件 (the master info repository file) 或表中 (详见 [Slave Status Logs](https://dev.mysql.com/doc/refman/5.7/en/slave-logs-status.html))。因此，你可能希望创建一个仅对主从复制过程具有特权的独立帐户，以最小化对其他帐户的损害。

要创建新帐户，请使用 ```CREATE USER```。若要授予此帐户主从复制所需的特权，请使用 ```GRANT``` 语句。如果您仅为主从复制而创建帐户，则该帐户只需要  [`REPLICATION SLAVE`](https://dev.mysql.com/doc/refman/5.7/en/privileges-provided.html#priv_replication-slave) 特权。例如，要设置一个新用户 repl (它可以连接 example.com 域名中的任何主机)，请在 Master 上执行以下语句：

```sql
CREATE USER 'repl'@'%.example.com' IDENTIFIED BY 'password';
GRANT REPLICATION SLAVE ON *.* TO 'repl'@'%.example.com';
```

用户账号操作更详细的操作，详见 [Account Management Statements](https://dev.mysql.com/doc/refman/5.7/en/account-management-statements.html)。

### 选择一个 Data Snapshots 方式

参考官方网站：[replication-snapshot-method](https://dev.mysql.com/doc/refman/5.7/en/replication-snapshot-method.html)

如果 Master 数据库包含存在的数据，则必须将这些数据复制到每个 Slave 数据库。从主数据库转储数据有不同的方法。以下部分描述了这些方法。要选择转储数据库的适当方法，请在这些选项之间进行选择：

- 使用 mysqldump 工具创建你要复制的所有数据库的转储文件 (dump) 。这是推荐的方法，尤其是在使用 [InnoDB](https://dev.mysql.com/doc/refman/5.7/en/innodb-storage-engine.html) 时。
- 如果你的数据库存储在二进制可移植 (portable) 文件中，则可以将原始数据文件复制到 Slave 中。这可能比使用 mysqldump 在每一台 Slave 上导入文件更高效，因为在 INSERT 语句重放(replayed)时它跳过了更新索引的开销。对于 [InnoDB](https://dev.mysql.com/doc/refman/5.7/en/innodb-storage-engine.html) 这样的存储引擎，不推荐这样做。

#### 使用 mysqldump 创建 snapchot

在现有 Master 数据库中创建数据的快照。数据转储完成后，在主从复制过程开始之前，需要将这些数据导入 Slave 服务器。

下面的示例将所有数据库转储到名为 dbdump.db 的文件中，[--master-data](https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html#option_mysqldump_master-data) 选项自动追加了 ```CHANGE MASTER TO``` 语句，这个语句是 Slave 需要的去开启主从进程的语句：

```sql
mysqldump --all-databases --master-data > dbdump.db
```

使用 mysqldump 工具时也可以从转储文件中排除某些数据库。如果要选择要在转储文件中包含哪些数据库，不要使用 [--all-databases](https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html#option_mysqldump_all-databases) 。使用如下的选项：

- 使用 [--ignore-table==*db_name.tbl_name*](https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html#option_mysqldump_ignore-table) 选项排除数据库中的部分表。有多个表时多次使用这个选项。
- 使用 [--databases](https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html#option_mysqldump_databases) 选项仅指定要转储的数据库的名称。

更多信息请参考 [mysqldump](https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html)。要导入转储的数据，要么将转储文件复制到 Slave 服务器，要么在远程连接到从属服务器时从主服务器访问该文件。

#### 使用 Raw Data Files 创建 snapshot

本节描述如何使用组成数据库的原始文件 (Raw Data Files) 创建数据快照。当数据库表使用具有复杂缓存 (caching) 或日志算法 (logging algorithms) 的存储引擎时，使用这种方法需要额外的步骤来生成完美的快照：即使你获得了全局读锁，初始复制命令也可能会遗漏缓存信息和日志更新。存储引擎如何应对这种情况取决于它的崩溃恢复能力。

如果你使用了 InnoDB 表，可以使用 MySQL Enterprise Backup 组件中的 **mysqlbackup** 命令生成一致的快照。此命令记录了要在 Slave 上使用的对应快照的日志名称和偏移量。MySQL Enterprise Backup 是一个商业产品，它是MySQL企业订阅的一部分，详见 [MySQL Enterprise Backup Overview](https://dev.mysql.com/doc/refman/5.7/en/mysql-enterprise-backup.html)。

此方法也不能可靠地工作，当 Master 和 Slave 的[`ft_stopword_file`](https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_ft_stopword_file), [`ft_min_word_len`](https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_ft_min_word_len), 或者 [`ft_max_word_len`](https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_ft_max_word_len) 值不同而且表使用了全文索引。

假设上述异常不适用于你的数据库，请使用冷备份技术获得 InnoDB 表的可靠二进制快照：缓慢关闭 MySQL 服务器，然后手动复制数据文件。

为了创建一个 MyISAM 表的原始数据的快照。当你的 MySQL 数据文件存在于一个文件系统，您可以使用标准的文件复制工具，如 **cp** 或 **copy** ，远程拷贝工具 **scp** 或 **rsync** ，归档工具如 **zip** 或 **tar**，或一个文件系统快照工具 **dump**。如果只复制某些数据库，则只复制与那些表相关的文件。对于 InnoDB 而言，所有数据库中的所有表都存储在表空间 (tablespace) 文件中，除非启用了每个表都有一个InnoDB文件的选项。

如下文件是不被主从复制要求的：

- 与```mysql``` 数据库相关的文件
- Master 信息存储库文件，如果使用的话。(详见 [Replication Relay and Status Logs](https://dev.mysql.com/doc/refman/5.7/en/slave-logs.html))
- Master 的二进制日志文件
- 任何 relay log 文件

根据你是否使用 InnoDB 类型的表，选择下面的一个：

**如果你正在使用 InnoDB 表**

1. 获得一个读锁并获得 Master 的状态，详见 [Obtaining the Replication Master Binary Log Coordinates](https://dev.mysql.com/doc/refman/5.7/en/replication-howto-masterstatus.html)。

2. 关闭 Master 服务器。

   ``````bash
   mysqladmin shutdown
   ``````

3. 复制 MySQL 数据文件。下面的示例展示了实现此目的的常见方法。你只需要选择其中之一：

   ```bash
   # 选择其中之一来拷贝
   tar cf /tmp/db.tar ./data
   zip -r /tmp/db.zip ./data
   rsync --recursive ./data /tmp/dbdata
   ```

4. 从新启动 Master 服务器。

**如果你没有在使用 InnoDB 表**

1. 获得一个读锁并获得 Master 的状态，详见 [Obtaining the Replication Master Binary Log Coordinates](https://dev.mysql.com/doc/refman/5.7/en/replication-howto-masterstatus.html)。

2. 复制 MySQL 数据文件。下面的示例展示了实现此目的的常见方法。你只需要选择其中之一：

   ```bash
   # 选择其中之一来拷贝
   tar cf /tmp/db.tar ./data
   zip -r /tmp/db.zip ./data
   rsync --recursive ./data /tmp/dbdata
   ```

3. 在你请求读锁的客户端释放这个锁。

   ```sql
   UNLOCK TABLES;
   ```

创建了归档文件或数据库副本之后，请在启动 Slave 的主从复制过程之前，将这些文件复制到每个 Slave 服务器。

### 基于 Binary Log File Position 的 Replication 配置

#### Replication Master 配置

参考官方网站 [replication-howto-masterbaseconfig](https://dev.mysql.com/doc/refman/5.7/en/replication-howto-masterbaseconfig.html)

为了使 Master 服务器配置为使用基于二进制日志文件位置的主从复制，你必须启用二进制日志并建立唯一的服务器 ID。如果还没有这样做，则需要重新启动服务器。

二进制日志必须在 Master 服务器上被启用，因为二进制日志是 Master 服务器主从复制到 Slave 服务器的基础。如果在 Master 上使用 ```log-bin``` 选项未启用二进制日志记录，则不可能进行主从复制。

主从复制组中的每个服务器都必须配置一个惟一的服务器 ID。这个 ID 用于标识组中的各个服务器，并且必须是 1 到 $2^{32}-1$ 之间的正整数。如何组织和选择这些数字是你自己的选择。

为了配置二进制日志和服务器 ID 选项，关闭 MySQL 服务器并编辑 ```my.cnf``` 或 ```my.ini``` 文件。在配置文件的 ```[mysqld]``` 部分中，添加 ```log-bin``` 和 ```server-id``` 选项。如果这些选项已经存在，但是被注释掉了，那么取消注释这些选项，并根据需要修改它们。例如，为了启用二进制日志，使用 ```mysql-bin``` 的日志文件名前缀，并配置服务器 ID 为1，请使用以下配置行：

```ini
[mysqld]
log-bin=mysql-bin
server-id=1
```

更改之后，重新启动服务器。

> **提示**
>
> 以下选项对这个过程有影响：
>
> - 如果没有设置服务器 ID (或将其显式设置为默认值0)，则 Master 服务器拒绝来自 Slave 服务器的任何连接。
> - 为了在主从复制中获得尽可能好的持久性和一致性，在使用 InnoDB 进行事务处理，应该在主 ```my.cnf``` 文件中使用 ```innodb_flush_log_at_trx_commit=1``` 和 ```sync_binlog=1```。
> - 确保在主从复制的 Master 服务器上未启用 [`skip_networking`](https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_skip_networking) 系统变量。如果网络已禁用，则从服务器无法与主服务器通信，主从复制失败。

#### Replication Slave 配置

参考官方网站 [replication-setup-slaves](https://dev.mysql.com/doc/refman/5.7/en/replication-setup-slaves.html)

本节将描述如何设置 Slave 服务器。在你继续之前，确保如下条件：

- 配置 MySQL Master 服务器必要的一些属性。参考前一节 [Replication Master 配置](####Replication-Master-配置)

- 获取 Master 的状态信息，或在因制作数据快照而关闭 Master 期间，生成的 Master 的二进制日志索引文件的副本。详见 [Obtaining the Replication Master Binary Log Coordinates](https://dev.mysql.com/doc/refman/5.7/en/replication-howto-masterstatus.html)

- 在 Master 上释放了读锁。

  ```sql
  UNLOCK TABLES;
  ```

##### Setting the Replication Slave Configuration

每个主从复制的 Slave 必须有一个唯一的服务器 ID。如果还没有这样做，这部分 Slave 需要重新启动服务器。如果还没有设置 Slave 服务器 ID，或者当前值与你为主服务器设置的值冲突，则关闭 Slave 服务器并编辑配置文件的 [mysqld] 部分，以指定唯一的服务器 ID。例如：

```ini
[mysqld]
server-id=2
```

更改之后，重新启动服务器。

如果要设置多个 Slave 服务器，则每个 Slave 服务器都必须具有与主服务器 ID 值和其他 Slave 服务器 ID 值不同的唯一服务器[`server_id`](https://dev.mysql.com/doc/refman/5.7/en/replication-options.html#sysvar_server_id)  值。

> **注意**
>
> 如果你没有设置服务器 ID (或显式地将其设置为默认值0)，则 Slave 服务器拒绝连接到 Master服务器。

在设置主从复制时，不必在 Slave 服务器上启用二进制日志。但是，如果在 Slave 服务器上启用了二进制日志，则可以使用 Slave 服务器的二进制日志进行数据备份和崩溃恢复，还可以将 Slave 服务器用作更复杂的复制拓扑的一部分。例如，这个 Slave 充当其他 Slave 的 Master。

##### Setting the Master Configuration on the Slave

为了主从复制，需要设置 Slave 服务器与 Master 服务器的通信，使用必要的连接信息配置 Slave 服务器。为此，在 Slave 服务器上执行以下语句，将选项值替换为与你的系统相关的实际值。

```sql
CHANGE MASTER TO MASTER_HOST='master_host_name', MASTER_USER='replication_user_name', MASTER_PASSWORD='replication_password', MASTER_LOG_FILE='recorded_log_file_name', MASTER_LOG_POS=recorded_log_position;
```

> **注意**
>
> 主从复制不能使用 Unix 套接字文件。你必须使用 TCP/IP 连接到 Master 服务器。

[`CHANGE MASTER TO`](https://dev.mysql.com/doc/refman/5.7/en/change-master-to.html) 语句还有其他选项。例如，可以使用 SSL 设置安全的主从复制。有关选项的完整列表，详见 [CHANGE MASTER TO Statemen](https://dev.mysql.com/doc/refman/5.7/en/change-master-to.html)。

下一步设置取决于是否要将现有数据导入 Slave 服务器。详见 [选择一个 Data Snapshots 方式](###选择一个-Data-Snapshots-方式)。选择如下之一的作为下一个步骤：

- 如果没有要导入的数据库快照。
- 如果要导入数据库的快照。

##### Setting Up Replication between a New Master and Slaves

当没有要导入的数据库的快照时，将 Slave 服务器配置为从新的 Master 主机开始复制。

在 Master, Slave 之间建立复制：

- 启动 MySQL Slave 并连接到它。
- 执行 ```CHANGE MASTER TO``` 语句来设置主从复制服务器配置。详见 [Setting the Master Configuration on the Slave](#####Setting-the-Master-Configuration-on-the-Slave)

在每个 Slave 服务器上执行这些从服务器设置步骤。

如果你正在设置新服务器，但是希望将来自不同服务器的现有的数据库的转储 (dump) 加载到主从复制配置中，也可以使用此方法。通过将数据加载到新的 Master 服务器，数据会被自动复制到 Slave 服务器。

如果你正在设置新的主从复制环境，而且使用了来自不同的现有数据库服务器的数据来创建新的 Master  服务器，请在新的 Master 服务器上运行生成的转储文件。Master 数据库的更新将自动传播到 Slave 服务器。

```bash
mysql -h master < fulldb.dump
```

##### Setting Up Replication with Existing Data

在使用现有数据设置主从复制时，在开始主从复制之前需要将快照从 Master 服务器传输到 Slave 服务器。将数据导入 Slave 服务器的过程取决于如何在 Master 服务器上创建的数据快照。

从下列选项中选择一个：

**如果你使用 [mysqldump](https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html):**

1. 启动 Slave 服务器，使用  [`--skip-slave-start`](https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html#option_mysqld_skip-slave-start) 选项，这样主从复制就不会启动。

2. 导入转储文件。

   ```bash
   mysql < fulldb.dump
   ```

**如果你使用 raw data files:**

1. 将数据文件解压缩到 Slave 数据目录中。例如：

   ```bash
   tar xvf dbdump.tar
   ```

   你可能需要设置文件的权限和所有权，以便从服务器能够访问和修改它们。

2. 启动 Slave 服务器，使用  [`--skip-slave-start`](https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html#option_mysqld_skip-slave-start) 选项，这样主从复制就不会启动。

3. 使用来自 Master 服务器的主从复制坐标来配置 Slave 服务器。这将告诉 Slave 服务器二进制日志文件和文件中需要开始复制的位置。另外还要 在Slave 服务器配置 Master 服务器的登录凭证和主机名。详见 [Setting the Master Configuration on the Slave](#####Setting-the-Master-Configuration-on-the-Slave)

4. 启动从服务器的主从同步线程：

   ```sql
   START SLAVE;
   ```

执行此过程后，Slave 服务器连接到 Master 服务器，并复制自捕获快照以来主服务器上发生的任何更新。

如果未正确设置 Master 服务器的服务器 [`server_id`](https://dev.mysql.com/doc/refman/5.7/en/replication-options.html#sysvar_server_id) 系统变量，则 Slave 服务器无法连接到它。类似地，如果没有为 Slave 服务器正确设置 [`server_id`](https://dev.mysql.com/doc/refman/5.7/en/replication-options.html#sysvar_server_id) ，将在 Slave 属服务器的错误日志中得到以下错误：

```txt
Warning: You should set server-id to a non-0 value if master_host
is set; we will force server id to 2, but this MySQL server will
not act as a slave.
```

如果由于其他原因无法进行主从复制，你还可以在 Slave 服务器的错误日志中找到错误消息。

Slave 服务器在其主信息存储库中存储有关已配置的 Master 服务器的信息。主信息存储库可以是文件或表的形式，由 [`master_info_repository`](https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html#sysvar_master_info_repository) 系统变量的值集决定。当一个 Slave 运行 [`master_info_repository=FILE`](https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html#sysvar_master_info_repository)，将会有两个文件存储在数据目录，名为 ```master.info``` 和 ```relry -log.info```。如果 [`master_info_repository=TABLE`](https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html#sysvar_master_info_repository) ，则此信息将保存在 ```mysql``` 数据库的 ```master_slave_info``` 表中。在这两种情况下，都不要删除或编辑这些文件或表。始终使用 ```CHANGE MASTER TO``` 语句改变主从复制的参数。Slave 服务器可以使用语句中指定的值来自动更新这些状态文件。

> **注意**
>
> 主信息存储库的内容覆盖了一些命令行或 my.cnf 文件中指定的一些服务器选项。详见 [Replication and Binary Logging Options and Variables](https://dev.mysql.com/doc/refman/5.7/en/replication-options.html)

Master 服务器的单个快照就足以支持多个 Slave 服务器。要设置更多的 Slave 服务器，请使用相同的主快照，并遵循刚才描述的设置 Slave 部分。

#### 配置实践

1. 向 Master 主机 my.cnf 添加配置

   ```ini
   [mysqld]
   server-id=1
   log-bin=mysql-bin
   sync_binlog=1
   innodb_flush_log_at_trx_commit=1
   ```

2. 在 Master 授权复制账号

    ```bash
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "GRANT REPLICATION SLAVE on *.* TO 'root'@'%'; FLUSH PRIVILEGES;"
    ```

3. 向 Slave 主机 my.cnf 添加配置

    ```ini
    [mysqld]
    server-id=2
    log-bin=mysql-bin
    ```

4. 在 Slave 上设置 Master 配置

    ```bash
    #请根据Master主机ip，log文件名更改下列变量，RECORDED_LOG_FILE_NAME，RECORDED_LOG_POSITION变量可根据Master主机执行的SQL"SHOW MASTER STATUS"结果对应填写。
    MASTER_HOST_NAME=192.168.0.7
    REPLICATION_USER_NAME=root
    REPLICATION_PASSWORD=123456
    RECORDED_LOG_FILE_NAME=mysql-bin.000017
    RECORDED_LOG_POSITION=154
    
    #在Slave上执行配置
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "CHANGE MASTER TO MASTER_HOST='$MASTER_HOST_NAME', MASTER_USER='$REPLICATION_USER_NAME', MASTER_PASSWORD='$REPLICATION_PASSWORD', MASTER_LOG_FILE='$RECORDED_LOG_FILE_NAME', MASTER_LOG_POS=$RECORDED_LOG_POSITION;"
    
    #在Slave上启动Slave IO线程
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "START SLAVE;"
    ```

    查看 Master 主机的状态
    ```bash
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "SHOW MASTER STATUS \G;"
    ```

    查看 Slave 主机的状态
    ```bash
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "SHOW SLAVE STATUS \G;"
    ```

    检查 Master, Slave 的状态是否正常。
    ```bash
    #在Slave主机，重点检查如下几项
    Slave_IO_State: Waiting for master to send event
    Slave_IO_Running: Yes
    Slave_SQL_Running: Yes
    Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
    #Master
    ```

###  基于 GTIDs (Global Transaction Identifiers) 的 Replication 配置

Replication GTID 配置参考官方网站：[replication-gtids-howto]( https://dev.mysql.com/doc/refman/5.7/en/replication-gtids-howto.html)

#### 以 MySQL 离线方式配置

由一主一从构成的复制拓扑（replication topology），最简单的 GTID 启动过程的关键步骤如下：

1. 如果 replication 已经在运行，通过设置为只读（read-only）来同步两个服务器。
2. 停止运行两个服务器。
3. 使能 GTIDs 选项，配置正确的选项，然后重启两个服务器。
4. 让从服务器使用主服务器作为复制数据源，并使用自动定位（auto-positioning）。
5. 重新备份。Binary logs 包含的是没有 GTIDs 的事务，因此在启用 GTIDs 的服务器上不能使用 Binary logs，所以在此之前所做的备份不能与现在的新配置一起使用。
6.  启动从服务器，然后在两个服务器上禁用只读模式，以便它们可以接受更新。

**Step 1: Synchronize the servers.**

当已经在运行的 MySQL 服务器，如果它正在进行不是以 GTIDs 方式的 Replication 时，这一步骤才是必须的。 对于新的 MySQL 服务器，跳转到步骤3，继续执行。

执行以下 SQL 命令，通过在每台主服务器上设置 [```read-only```]( https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_read_only ) 系统变量为 ON，从而使主服务器成为只读。

```sql
SET @@GLOBAL.read_only = ON;
```

等待所有正在进行的事务提交或回滚。然后，让从服务器追赶主服务器。**在继续之前，确保从服务器已经处理了所有更新是非常重要的。**可以通过执行语句 ```SHOW MASTER STATUS\G;```，```SHOW SLAVE STATUS\G;```，主服务器的 ```Position``` 和 从服务器的 ```Read_Master_Log_Pos``` 相同则完全同步了。如果 ```read_only``` 无作用，可以替换为[```super_read_only```](https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_super_read_only) 变量，即执行如下语句。

```sql
-- 当 read_only 变量不起作用时，可以执行此语句代替。
SET @@GLOBAL.super_read_only = ON;
```

如果将 Binary logs 用于除复制以外的其他目的， 例如在特定时间执行备份和恢复，请等待直到你不再需要老的 Binary logs，因为它没有包含 GTIDs 的事务。 理想情况下，等待服务器清除所有的二进制日志，并等待任何现有的备份过期。 

>**重要提示：**
>
>理解这一点比较重要，没有包含 GTIDs 的 logs 不能被用在 GTIDs 已经使能的服务器上。在继续之前，你必须确保没有 GTIDs 的事务在拓扑中的任何地方都不存在。 

**Step 2: Stop both servers. **

使用如下所示的 ```mysqladmin``` 停止每个服务器，其中 username 是拥有足够权限来关闭服务器的 MySQL 用户的用户名：

```bash
mysqladmin -uusername -p shutdown
```

然后在提示时提供该用户的密码。 

**Step 3: Start both servers with GTIDs enabled.**

要启用基于 GTIDs 的 Replication，必须在启动每个服务器时已经启用 GTIDs 模式。方法是将 GTIDs 模式变量 [gtid_mode](https://dev.mysql.com/doc/refman/5.7/en/replication-options-gtids.html#sysvar_gtid_mode) 设置为 ```gtid_mode=ON``` ， 并且 [```enforce_gtid_consistency```](https://dev.mysql.com/doc/refman/5.7/en/replication-options-gtids.html#sysvar_enforce_gtid_consistency) 变量已经开启，以确保基于 GTIDs 的 Replication 的语句被记录到log。配置配置文件 my.cnf：

```ini
gtid_mode=ON
enforce_gtid_consistency=ON
```

除此之外，在配置从服务器之前，你应该以 [```--skip-slave-start```](https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html#option_mysqld_skip-slave-start) 选项启动从服务器。

> [```--skip-slave-start```](https://dev.mysql.com/doc/refman/5.7/en/replication-options-slave.html#option_mysqld_skip-slave-start)
>
> | Property                | Value                           |
> | ----------------------- | ------------------------------- |
> | **Command-Line Format** | `--skip-slave-start[={OFF|ON}]` |
> | **Type**                | Boolean                         |
> | **Default Value**       | `OFF`                           |
>
> Tells the slave server not to start the slave threads when the server starts. To start the threads later, use a [`START SLAVE`](https://dev.mysql.com/doc/refman/5.7/en/start-slave.html) statement.

当使用了 [mysql.gtid_executed Table](https://dev.mysql.com/doc/refman/5.7/en/replication-gtids-concepts.html#replication-gtids-gtid-executed-table)，并不一定要启用 binary logging 才能使用 GTIDs。为了能够进行 Replication，主服务器必须始终启用 binary logging 。从服务器可以使用 GTIDs，而不需要 binary logging 。 如果需要禁用从服务器上的 binary logging，你可以通过在从服务器上设置  [```--skip-log-bin```](https://dev.mysql.com/doc/refman/5.7/en/replication-options-binary-log.html#option_mysqld_log-bin) 和 [```--log-slave-updates=OFF```](https://dev.mysql.com/doc/refman/5.7/en/replication-options-binary-log.html#sysvar_log_slave_updates) 命令行选项。

**Step 4: Configure the slave to use GTID-based auto-positioning.**

告诉从服务器使用基于 GTIDs 的事务作为 Replication 的数据源，而且使用基于 GTIDs 的自动定位，而不是基于文件的定位。在从服务器上执行一个 ```CHANGE MASTER TO``` 语句，语句中的 ```MASTER AUTO POSITION``` 选项，告诉从服务器主服务器的事务是由 GTIDs 标识的。 

你可能还需要为主服务器的主机名和端口号提供适当的值， 以及一个用于 Replication 的用户名和密码， 从服务器可以使用该用户帐户连接到主机。如果在步骤 1 之前已经设置了这些选项，并且不需要进行进一步的更改，也可以从如下语句中安全地忽略相应的选项。 

```sql
-- 修改如下 SQL 语句的变量值，并在从服务器上执行如下语句：
CHANGE MASTER TO MASTER_HOST = host, MASTER_PORT = port, MASTER_USER = user, MASTER_PASSWORD = password, MASTER_AUTO_POSITION = 1;
```

 ```MASTER_LOG_FILE``` 选项和  ```MASTER_LOG_POS``` 选项都不能与设置为 1 的 ```MASTER_AUTO_POSITION``` 一起使用 。 这样做会导致 ```CHANGE MASTER TO``` 语句失败并出现错误。 

**Step 5: Take a new backup.**

在使能 GTIDs 之前存在的备份，再也不能用在这些服务器上了，因为已经启用了 GTIDs。此时请进行新的备份， 这样就不会缺少可用的备份。

例如，可以在进行备份的服务器上执行 ```FLUSH LOGS``` 。 然后显式地进行备份，或等待已设置的任何定期备份例程的下一次。 

**Step 6: Start the slave and disable read-only mode.**

启动从服务器，像这样：

```sql
START SLAVE;
```

只有在第 1 步中将服务器配置为只读时，才需要执行以下步骤 。要允许服务器再次开始接受更新，请执行以下语句 ：

```sql
SET @@GLOBAL.read_only = OFF;
```

基于 GTIDs 的复制现在应该正在运行了，你可以像以前一样在主 MySQL 服务器上开始（或恢复）活动。 

#### 以 MySQL 在线方式配置

参考官方文档： [replication-mode-change-online-enable-gtids](https://dev.mysql.com/doc/refman/5.7/en/replication-mode-change-online-enable-gtids.html)

本节描述如何在运行时并使用匿名事务的服务器上启用 GTIDs 事务，以及自动定位（可选）。此过程不需要使MySQL服务器离线，比较适合在生产环境中使用。但是，如果您能够在启用 GTIDs 事务时使服务器离线，则操作起来会更容易。 

前置条件：

> - All servers in your topology must use MySQL 5.7.6 or later. You cannot enable GTID transactions online on any single server unless all servers which are in the topology are using this version.
> - All servers have [```gtid_mode```](https://dev.mysql.com/doc/refman/5.7/en/replication-options-gtids.html#sysvar_gtid_mode) set to the default value ```OFF```.

> **Note**
>
> It is crucial that you complete every step before continuing to the next step.

1. 在每一个服务器上执行：

   ```sql
   SET @@GLOBAL.ENFORCE_GTID_CONSISTENCY = WARN;
   ```

   让服务器在正常工作负载下运行一段时间并监视日志。如果此步骤导致日志中出现任何警告，请调整应用程序，使其只使用与 GTIDs 兼容的特性，而不生成任何警告。

   > **Important**
   >
   >  这是重要的第一步。在进入下一步之前，必须确保错误日志中没有生成任何警告。

2. 在每一个服务器上执行：

   ```sql
   SET @@GLOBAL.ENFORCE_GTID_CONSISTENCY = ON;
   ```

3. 在每一个服务器上执行：

   ```sql
   SET @@GLOBAL.GTID_MODE = OFF_PERMISSIVE;
   ```

   哪个服务器先执行这个语句并不重要，重要的是所有服务器在都需要在下一个步骤之前完成这个步骤。 

4. 在每一个服务器上执行：

   ```sql
   SET @@GLOBAL.GTID_MODE = ON_PERMISSIVE;
   ```

   哪个服务器先执行这个语句并不重要。

5.  在每个服务器上，等待状态变量 ```ONGOING_ANONYMOUS_TRANSACTION_COUNT``` 是 0 。使用如下指令检查：

   ```sql
   SHOW STATUS LIKE 'ONGOING_ANONYMOUS_TRANSACTION_COUNT';
   ```

   在从服务器上，从理论上说，它有可能先显示 0，然后又显示非 0。这不是问题，只要显示一次为 0 就足够了。

6. 等待在步骤 5 之前生成的所有事务复制到所有服务器。你可以在不停止执行更新 SQL 的情况下执行第 6 步的操作，惟一重要的是复制完所有匿名事务。 

7. 如果将 Binary logs 用于除复制以外的其他目的， 例如在特定时间执行备份和恢复，请等待直到你不再需要老的 Binary logs，因为它没有包含 GTIDs 的事务。 

   例如，可以在进行备份的服务器上执行 ```FLUSH LOGS``` 。 然后显式地进行备份，或等待已设置的任何定期备份例程的下一次。 

   理想情况下，不仅要等待服务器清除第 6 步完成时存在的所有二进制日志。还要等待在第 6 步之前执行的备份过程已过期。 

   **验证匿名事务的 Replication**

   参考官方文档：[Verifying Replication of Anonymous Transactions]( https://dev.mysql.com/doc/refman/5.7/en/replication-mode-change-online-verify-transactions.html )

   方法：

   1. 最简单的方法：

      它与你的拓扑无关，但是依赖于时间：如果你确定从服务器的滞后时间不会超过 N 秒，那么只需等待稍多于 N 秒。或者等待一天，或者等待你认为安全的部署时间。

   2. 更安全的方法：

      此方法不依赖于时间：如果你只有一个主服务器，它拥有一个或多个从服务器，请执行以下操作 ：

      1. 在主服务器，执行：

         ```sql
         SHOW MASTER STATUS;
         ```

         请记住此条 SQL 语句执行的 ```File``` 和 ```Position``` 的值。

      2.  在每个从服务器上，使用来自主服务器的 ```File``` 和 ```Posision``` 的值来执行 ：

         ```sql
         --- 请以第 1 步执行的 File 和 Position 的值替换如下的函数参数。
         SELECT MASTER_POS_WAIT(file, position);
         ```

         这个语句输出了结果则已完成同步。

         > [function_master-pos-wait]( https://dev.mysql.com/doc/refman/5.7/en/miscellaneous-functions.html#function_master-pos-wait )
         >
         > This function is useful for control of master/slave synchronization. It blocks until the slave has read and applied all updates up to the specified position in the master log. The return value is the number of log events the slave had to wait for to advance to the specified position. The function returns `NULL` if the slave SQL thread is not started, the slave's master information is not initialized, the arguments are incorrect, or an error occurs. It returns `-1` if the timeout has been exceeded. If the slave SQL thread stops while [`MASTER_POS_WAIT()`](https://dev.mysql.com/doc/refman/5.7/en/miscellaneous-functions.html#function_master-pos-wait) is waiting, the function returns `NULL`. If the slave is past the specified position, the function returns immediately.

8. 在每一个服务器上执行：

   ```sql
   SET @@GLOBAL.GTID_MODE = ON;
   ```

9. 在每一个服务器上，向 my.cnf 添加：

   ```ini
   gtid_mode=ON
   enforce_gtid_consistency=ON
   ```

## Replication 半同步配置

半同步复制安装配置参考官网：[Semisynchronous Replication Installation and Configuration](https://dev.mysql.com/doc/refman/5.5/en/replication-semisync-installation.html)

为使用半同步复制，如下条件必须满足：

> - MySQL 5.5及以上版本
> - MySQL server必须支持动态库加载
> - MySQL Replication必须已在正常工作

以下两种配置方式可以同时配置，也可只配置其一。插件加载配置参考官方网站：[plugin-loading](https://dev.mysql.com/doc/refman/8.0/en/plugin-loading.html)

### 以运行时方式配置

1. 安装插件
    ```bash
    #在master和备master上安装semisync_master.so插件，在slave上安装semisync_slave.so，当然所有主机都安装这两个插件也是可以的。
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "INSTALL PLUGIN rpl_semi_sync_master SONAME 'semisync_master.so';"
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "INSTALL PLUGIN rpl_semi_sync_slave SONAME 'semisync_slave.so';"

    #请确认已经安装rpl_semi_sync_master, rpl_semi_sync_slave插件
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "SELECT PLUGIN_NAME, PLUGIN_STATUS FROM INFORMATION_SCHEMA.PLUGINS WHERE PLUGIN_NAME LIKE '%semi%';"
    ```

2. 使能插件，在Master端使能rpl_semi_sync_master插件
    ```bash
    #在Master端使能rpl_semi_sync_master插件
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "SET GLOBAL rpl_semi_sync_master_enabled = 1;
    SET GLOBAL rpl_semi_sync_master_timeout = 10000;"
    ```

3. 使能插件，在Slave端使能rpl_semi_sync_slave插件
    ```bash
    #在Slave端使能rpl_semi_sync_slave插件
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "SET GLOBAL rpl_semi_sync_slave_enabled = 1;"
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "STOP SLAVE IO_THREAD;
    START SLAVE IO_THREAD;"
    ```

4. 检查Master端，Slave端的插件启用状态
    ```bash
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "SHOW VARIABLES LIKE 'rpl_semi_sync%'; SHOW STATUS LIKE 'Rpl_semi_sync%';"
    ```

    Master端的主要状态如下：
    > - Rpl_semi_sync_master_status – Whether semisynchronous replication currently is operational on the master.
    > - Rpl_semi_sync_master_yes_tx – The number of commits that were acknowledged successfully by a slave.
    > - Rpl_semi_sync_master_no_tx – The number of commits that were not acknowledged successfully by a slave.

    结果显示的各个状态的详细意义参考官网[Server Status Variables](https://dev.mysql.com/doc/refman/5.5/en/server-status-variables.html)

5. 使Slave只读
    ```bash
    mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "SET GLOBAL read_only=1"
    ```

### 以配置文件方式配置

1. 向Master主机my.cnf添加配置
    ```ini
    [mysqld]
    plugin-load="rpl_semi_sync_master=semisync_master.so;rpl_semi_sync_slave=semisync_slave.so"
    rpl_semi_sync_master_enabled=1
    rpl_semi_sync_master_timeout=10000 # 10 second
    ```
2. 向Slave主机my.cnf添加配置
    ```ini
    [mysqld]
    plugin-load="rpl_semi_sync_slave=semisync_slave.so;"
    rpl_semi_sync_slave_enabled=1
    read_only=1
    ```
3. 重新启动MySQL
    ```bash
    mysql.server restart
    ```

## 安装MHA

前置要求:

> - Replication must already be running. MHA manages replication and monitors it, but it is not a tool to deploy it. So MySQL and replication need to be running already.
> - All hosts should be able to connect to each other using public SSH keys.
> - All nodes need to be able to connect to each other’s MySQL servers.
> - All nodes should have the same replication user and password.
> - In the case of multi-master setups, only one writable node is allowed. All others need to be configured with read_only.
> - MySQL version has to be 5.0 or later.
> - Candidates for master failover should have binary log enabled. The replication user must exist there too.
> - Binary log filtering variables should be the same on all servers (replicate-wild%, binlog-do-db…).
> - Disable automatic relay-log purge and do it regularly from a cron task. You can use an MHA-included script called “purge_relay_logs”.

### MHA Node, MHA Manager前置配置

1. 安装依赖的perl模块

    也可以通过源码方式(```perl -MCPAN -e "install Log::Dispatch"```)安装依赖，参考官方网站[installing-mha-manager](https://github.com/yoshinorim/mha4mysql-manager/wiki/Installation#installing-mha-manager)

    MHA Node, MHA Manager都依赖如下的perl模块，CentOS-Base中可能没有perl-Log-Dispatch，perl-Parallel-ForkManager安装包，需要安装epel镜像源，参考[FAQ](##FAQ)第10项。
    ```bash
    #安装依赖的perl模块
    yum install -y perl
    yum install -y perl-Module-Install
    yum install -y perl-DBD-MySQL
    yum install -y perl-Config-Tiny
    yum install -y perl-Log-Dispatch
    yum install -y perl-Parallel-ForkManager
    ```

2. 相互配置SSH keys

    ```bash
    #MHA Manager, Master, Slave都需要生成公共SSH keys，而且ssh-keygen, ssh-copy-id产生的key需要在免密登陆的用户home目录下。
    ssh-keygen -t rsa -f ~/.ssh/id_rsa -P ""
    #所有主机都要拷贝公钥到每台其他的MHA Manager, Master, Slave主机
    ssh-copy-id -i ~/.ssh/id_rsa.pub 192.168.0.5
    ssh-copy-id -i ~/.ssh/id_rsa.pub 192.168.0.7
    ```

更多前置配置项，参考官方网站[Requirements](https://github.com/yoshinorim/mha4mysql-manager/wiki/Requirements)

### 安装MHA Node

请参考官方网站[installing-mha-node](https://github.com/yoshinorim/mha4mysql-manager/wiki/Installation#installing-mha-node)

每台MHA Manager, MySQL Server主机都需要安装MHA Node。

1. 解压zip包到/opt目录
    ```bash
    unzip mha4mysql-node-0.58.zip
    mv mha4mysql-node-0.58 mha4mysql-node
    cd mha4mysql-node
    perl Makefile.PL
    make && make install
    ```

### 安装配置MHA Manager

请参考官方网站[installing-mha-manager](https://github.com/yoshinorim/mha4mysql-manager/wiki/Installation#installing-mha-manager)

1. 解压zip包，编译安装
    ```bash
    # MHA Manager主机需要安装MHA Node，MHA Manager
    unzip mha4mysql-manager-0.58.zip
    mv mha4mysql-manager-0.58 mha4mysql-manager
    cd mha4mysql-manager
    perl Makefile.PL
    make && make install
    ```

2. 配置MHA Manager

    参考官方网站[Parameters](https://github.com/yoshinorim/mha4mysql-manager/wiki/Parameters)

    初始化数据和配置目录
    ```bash
    #初始化目录
    mkdir -p /opt/data/mha_manager/app1
    ```

    向MHA Manager主机的/opt/data/mha_manager/app1/mha_manager.cnf添加配置，请更改主机ip地址，MySQL用户名和密码
    ```ini
    [server default]
    # mysql user and password
    user=root
    password=123456
    ssh_user=root
    repl_user=root
    repl_password=123456
    # working directory on the manager
    manager_workdir=/opt/data/mha_manager/app1
    manager_log=/opt/data/mha_manager/app1/app1.log
    # binlog dir
    master_binlog_dir=/opt/data/mysql/db
    [server1]
    hostname=192.168.0.7
    candidate_master=1
    [server2]
    hostname=192.168.0.6
    candidate_master=1
    #[server3]
    #hostname=192.168.0.9
    #no_master=1
    ```

    ```bash
    #初始化目录
    mkdir -p /opt/data/mha_manager/app1

    #检查SSH配置, 出现"All SSH connection tests passed successfully"
    masterha_check_ssh --conf=/opt/data/mha_manager/app1/mha_manager.cnf

    #检查主从复制，检查"MySQL Replication Health is OK."
    masterha_check_repl --conf=/opt/data/mha_manager/app1/mha_manager.cnf
    ```

3. 有计划的Master切换

    参考官方网站[masterha_master_switch](https://github.com/yoshinorim/mha4mysql-manager/wiki/masterha_master_switch)

    前置条件：
    > - IO threads on all slaves are running
    > - SQL threads on all slaves are running
    > - Seconds_Behind_Master on all slaves are less or equal than --running_updates_limit seconds
    > - On master, none of update queries take more than --running_updates_limit seconds in the show processlist output

    请根据候选Master的ip更改new_master_host的值。你可以执行两次下列脚本，它将会根据你定义的new_master_host切换Master。
    ```bash
    masterha_master_switch --master_state=alive  --conf=/opt/data/mha_manager/app1/mha_manager.cnf --orig_master_is_new_slave --interactive=0 --new_master_host=192.168.0.6
    ```
4. 启动MHA Manager

    ```bash
    masterha_manager --remove_dead_master_conf --conf=/opt/data/mha_manager/app1/mha_manager.cnf
    ```

    > - remove_dead_master_conf tells the manager that if the master goes down, it must edit the config file and remove the master’s configuration from it after a successful failover. This avoids the “there is a dead slave” error when you restart the manager. All servers listed in the conf should be part of the replication and in good health, or the manager will refuse to work.

### 配置Slave自动清除relay log

- 关闭MySQL自动清除relay logs选项，参考官方网站[option_mysqld_relay-log-purge](https://dev.mysql.com/doc/refman/8.0/en/replication-options-slave.html#option_mysqld_relay-log-purge)
- 关闭MySQL自动清除relay logs选项的原因，参考官方网站[preserve-relay-logs-and-purge-regularly](https://github.com/yoshinorim/mha4mysql-manager/wiki/Requirements#preserve-relay-logs-and-purge-regularly)

1. 关闭MySQL自动清除relay logs

    以下两种配置方式可以同时配置，也可只配置其一。

    - 以运行时方式配置。在Slave，候选Master主机执行
        ```bash
        #关闭MySQL自动清除relay logs
        mysql -h localhost -uroot -p$MYSQL_INIT_PASSWORD -e "FLUSH LOGS; SET GLOBAL relay_log_purge=0;"
        ```

    - 以配置文件方式配置。向Slave，候选Master主机的my.cnf添加配置
        ```ini
        [mysqld]
        relay_log_purge=0
        ```
2. 启用MHA定时清除relay logs脚本

    向Slave，候选Master主机的/etc/cron.d/purge_relay_logs添加配置
    ```bash
    #创建清理日志目录
    mkdir -p /opt/data/mha_node/app1

    #请修改定时任务user, password选项
    cat << -END- > /etc/cron.d/purge_relay_logs
    # purge relay logs at 5am
    0 5 * * * mysql /usr/local/bin/purge_relay_logs --user=root --password=123456 --disable_relay_log_purge >> /opt/data/mha_node/app1/purge_relay_logs.log 2>&1
    -END-

    #加载crontab配置
    service crond reload
    ```

### 配置Master虚拟ip

1. 添加MHA Manager的master_ip_failover.pl脚本

    脚本的作用参考官方网站[master_ip_failover_script](https://github.com/yoshinorim/mha4mysql-manager/wiki/Parameters#master_ip_failover_script)

    > - MHA Manager calls master_ip_failover_script three times. First time is before entering master monitor (for script validity checking), second time is just before calling shutdown_script, and third time is after applying all relay logs to the new master.

    脚本内容参考官方网站[master_ip_failover](https://github.com/yoshinorim/mha4mysql-manager/blob/master/samples/scripts/master_ip_failover)

    向MHA Manager主机的/opt/data/mha_manager/app1/master_ip_failover.pl添加配置。请根据你的虚拟ip地址，更改脚本中的vip, dev变量的值。
    ```perl
    #!/usr/bin/env perl

    #  Copyright (C) 2011 DeNA Co.,Ltd.
    #
    #  This program is free software; you can redistribute it and/or modify
    #  it under the terms of the GNU General Public License as published by
    #  the Free Software Foundation; either version 2 of the License, or
    #  (at your option) any later version.
    #
    #  This program is distributed in the hope that it will be useful,
    #  but WITHOUT ANY WARRANTY; without even the implied warranty of
    #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #  GNU General Public License for more details.
    #
    #  You should have received a copy of the GNU General Public License
    #   along with this program; if not, write to the Free Software
    #  Foundation, Inc.,
    #  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    ## Note: This is a sample script and is not complete. Modify the script based on your environment.

    use strict;
    use warnings FATAL => 'all';

    use Getopt::Long;
    use MHA::DBHelper;
    use Config::IniFiles;

    my (
        $command,        $ssh_user,         $orig_master_host,
        $orig_master_ip, $orig_master_port, $new_master_host,
        $new_master_ip,  $new_master_port,  $new_master_user,
        $new_master_password
    );

    my $vip = ''; #虚拟ip和子网掩码, 192.168.1.8/22
    my $dev = ''; #网络接口设备名, eth0
    my $vip_conf = "/opt/data/mha_manager/app1/vip.cnf"; #vip, dev配置文件

    GetOptions(
        'command=s'             => \$command,
        'ssh_user=s'            => \$ssh_user,
        'orig_master_host=s'    => \$orig_master_host,
        'orig_master_ip=s'      => \$orig_master_ip,
        'orig_master_port=i'    => \$orig_master_port,
        'new_master_host=s'     => \$new_master_host,
        'new_master_ip=s'       => \$new_master_ip,
        'new_master_port=i'     => \$new_master_port,
        'new_master_user=s'     => \$new_master_user,
        'new_master_password=s' => \$new_master_password,
    );

    exit &main();

    sub main {
        if ( $command eq "stop" || $command eq "stopssh" ) {

            # $orig_master_host, $orig_master_ip, $orig_master_port are passed.
            # If you manage master ip address at global catalog database,
            # invalidate orig_master_ip here.
            my $exit_code = 1;
            eval {
                print " Disabling the VIP $vip on old master $orig_master_host \n";
                &stop_vip();
                # updating global catalog, etc
                $exit_code = 0;
            };
            if ($@) {
                warn "Got Error: $@\n";
                exit $exit_code;
            }
            exit $exit_code;
        }
        elsif ( $command eq "start" ) {

            # all arguments are passed.
            # If you manage master ip address at global catalog database,
            # activate new_master_ip here.
            # You can also grant write access (create user, set read_only=0, etc) here.
            my $exit_code = 10;
            eval {
                my $new_master_handler = new MHA::DBHelper();

                # args: hostname, port, user, password, raise_error_or_not
                $new_master_handler->connect( $new_master_ip, $new_master_port,
                    $new_master_user, $new_master_password, 1 );

                ## Set read_only=0 on the new master
                $new_master_handler->disable_log_bin_local();
                print "Set read_only=0 on the new master.\n";
                $new_master_handler->disable_read_only();

                ## Creating an app user on the new master
                print "Creating app user on the new master..\n";
                $new_master_handler->enable_log_bin_local();
                $new_master_handler->disconnect();

                ## Update master ip on the catalog database, etc
                print "Enabling the VIP $vip on the new master...\n";
                &start_vip();
                print "Arping the VIP $vip on the new master....\n";
                &notify_vip();
                $exit_code = 0;
            };
            if ($@) {
                warn $@;

                # If you want to continue failover, exit 10.
                exit $exit_code;
            }
            exit $exit_code;
        }
        elsif ( $command eq "status" ) {

            # do nothing
            exit 0;
        }
        else {
            &usage();
            exit 1;
        }
    }

    sub get_conf() {
        my $cfg = Config::IniFiles->new( -file => $vip_conf );
        $vip = $cfg->val('server default', 'vip');
        $dev = $cfg->val('server default', 'dev');
    }

    sub start_vip() {
        get_conf();
        my $ssh_start_vip = "ip addr add $vip dev $dev"; #启用虚拟ip
        `ssh $ssh_user\@$new_master_host \" $ssh_start_vip \"`;
    }

    sub stop_vip() {
        get_conf();
        my $ssh_stop_vip = "ip addr del $vip dev $dev"; #关闭虚拟ip
        return 0  unless  ($ssh_user);
        `ssh $ssh_user\@$orig_master_host \" $ssh_stop_vip \"`;
    }

    sub notify_vip() {
        get_conf();
        my $gateway = `ip route list | grep "default via" | awk '{print \$3}'`; #网关
        my $vipp = `echo -n "$vip" | sed "s/\\/[[:digit:]]*//"`; #虚拟ip
        my $ssh_notify_vip = "arping -U -I $dev -c 2 -w 5 -s $vipp $gateway"; #通知网关ip改变
        `ssh $ssh_user\@$new_master_host \" $ssh_notify_vip \"`;
    }

    sub usage {
        print
        "Usage: master_ip_failover --command=start|stop|stopssh|status --orig_master_host=host --orig_master_ip=ip --orig_master_port=port --new_master_host=host --new_master_ip=ip --new_master_port=port\n";
    }
    ```

2. 添加MHA Manager的master_ip_online_change.pl脚本

    脚本的作用参考官方网站[master_ip_online_change_script](https://github.com/yoshinorim/mha4mysql-manager/wiki/Parameters#master_ip_online_change_script)

    > - This is similar to master_ip_failover_script parameter, but this is not used by master failover command, but by master online change command (masterha_master_switch --master_state=alive)

    脚本内容参考官方网站[master_ip_online_change](https://github.com/yoshinorim/mha4mysql-manager/edit/master/samples/scripts/master_ip_online_change)

    向MHA Manager主机的/opt/data/mha_manager/app1/master_ip_online_change.pl添加配置。请根据你的虚拟ip地址，更改脚本中的vip, dev变量的值。

    ```perl
    #!/usr/bin/env perl

    #  Copyright (C) 2011 DeNA Co.,Ltd.
    #
    #  This program is free software; you can redistribute it and/or modify
    #  it under the terms of the GNU General Public License as published by
    #  the Free Software Foundation; either version 2 of the License, or
    #  (at your option) any later version.
    #
    #  This program is distributed in the hope that it will be useful,
    #  but WITHOUT ANY WARRANTY; without even the implied warranty of
    #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #  GNU General Public License for more details.
    #
    #  You should have received a copy of the GNU General Public License
    #   along with this program; if not, write to the Free Software
    #  Foundation, Inc.,
    #  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    ## Note: This is a sample script and is not complete. Modify the script based on your environment.

    use strict;
    use warnings FATAL => 'all';

    use Getopt::Long;
    use MHA::DBHelper;
    use MHA::NodeUtil;
    use Time::HiRes qw( sleep gettimeofday tv_interval );
    use Data::Dumper;
    use Config::IniFiles;

    my $_tstart;
    my $_running_interval = 0.1;
    my (
        $command,              $orig_master_is_new_slave, $orig_master_host,
        $orig_master_ip,       $orig_master_port,         $orig_master_user,
        $orig_master_password, $orig_master_ssh_user,     $new_master_host,
        $new_master_ip,        $new_master_port,          $new_master_user,
        $new_master_password,  $new_master_ssh_user,
    );

    my $vip = ''; #虚拟ip和子网掩码
    my $dev = ''; #网络接口设备名
    my $vip_conf = "/opt/data/mha_manager/app1/vip.cnf"; #vip, dev配置文件

    GetOptions(
        'command=s'                => \$command,
        'orig_master_is_new_slave' => \$orig_master_is_new_slave,
        'orig_master_host=s'       => \$orig_master_host,
        'orig_master_ip=s'         => \$orig_master_ip,
        'orig_master_port=i'       => \$orig_master_port,
        'orig_master_user=s'       => \$orig_master_user,
        'orig_master_password=s'   => \$orig_master_password,
        'orig_master_ssh_user=s'   => \$orig_master_ssh_user,
        'new_master_host=s'        => \$new_master_host,
        'new_master_ip=s'          => \$new_master_ip,
        'new_master_port=i'        => \$new_master_port,
        'new_master_user=s'        => \$new_master_user,
        'new_master_password=s'    => \$new_master_password,
        'new_master_ssh_user=s'    => \$new_master_ssh_user,
    );

    exit &main();

    sub current_time_us {
        my ( $sec, $microsec ) = gettimeofday();
        my $curdate = localtime($sec);
        return $curdate . " " . sprintf( "%06d", $microsec );
    }

    sub sleep_until {
        my $elapsed = tv_interval($_tstart);
        if ( $_running_interval > $elapsed ) {
            sleep( $_running_interval - $elapsed );
        }
    }

    sub get_threads_util {
        my $dbh                    = shift;
        my $my_connection_id       = shift;
        my $running_time_threshold = shift;
        my $type                   = shift;
        $running_time_threshold = 0 unless ($running_time_threshold);
        $type                   = 0 unless ($type);
        my @threads;

        my $sth = $dbh->prepare("SHOW PROCESSLIST");
        $sth->execute();

        while ( my $ref = $sth->fetchrow_hashref() ) {
            my $id         = $ref->{Id};
            my $user       = $ref->{User};
            my $host       = $ref->{Host};
            my $command    = $ref->{Command};
            my $state      = $ref->{State};
            my $query_time = $ref->{Time};
            my $info       = $ref->{Info};
            $info =~ s/^\s*(.*?)\s*$/$1/ if defined($info);
            next if ( $my_connection_id == $id );
            next if ( defined($query_time) && $query_time < $running_time_threshold );
            next if ( defined($command)    && $command eq "Binlog Dump" );
            next if ( defined($user)       && $user eq "system user" );
            next
            if ( defined($command)
            && $command eq "Sleep"
            && defined($query_time)
            && $query_time >= 1 );

            if ( $type >= 1 ) {
                next if ( defined($command) && $command eq "Sleep" );
                next if ( defined($command) && $command eq "Connect" );
            }

            if ( $type >= 2 ) {
                next if ( defined($info) && $info =~ m/^select/i );
                next if ( defined($info) && $info =~ m/^show/i );
            }

            push @threads, $ref;
        }
        return @threads;
    }

    sub main {
        if ( $command eq "stop" ) {
            ## Gracefully killing connections on the current master
            # 1. Set read_only= 1 on the new master
            # 2. DROP USER so that no app user can establish new connections
            # 3. Set read_only= 1 on the current master
            # 4. Kill current queries
            # * Any database access failure will result in script die.
            my $exit_code = 1;
            eval {
                ## Setting read_only=1 on the new master (to avoid accident)
                my $new_master_handler = new MHA::DBHelper();

                # args: hostname, port, user, password, raise_error(die_on_error)_or_not
                $new_master_handler->connect( $new_master_ip, $new_master_port,
                    $new_master_user, $new_master_password, 1 );
                print current_time_us() . " Set read_only on the new master.. ";
                $new_master_handler->enable_read_only();
                if ( $new_master_handler->is_read_only() ) {
                    print "ok.\n";
                }
                else {
                    die "Failed!\n";
                }
                $new_master_handler->disconnect();

                # Connecting to the orig master, die if any database error happens
                my $orig_master_handler = new MHA::DBHelper();
                $orig_master_handler->connect( $orig_master_ip, $orig_master_port,
                    $orig_master_user, $orig_master_password, 1 );

                ## Drop application user so that nobody can connect. Disabling per-session binlog beforehand
                $orig_master_handler->disable_log_bin_local();

                ## Waiting for N * 100 milliseconds so that current connections can exit
                my $time_until_read_only = 15;
                $_tstart = [gettimeofday];
                my @threads = get_threads_util( $orig_master_handler->{dbh},
                    $orig_master_handler->{connection_id} );
                while ( $time_until_read_only > 0 && $#threads >= 0 ) {
                    if ( $time_until_read_only % 5 == 0 ) {
                    printf
            "%s Waiting all running %d threads are disconnected.. (max %d milliseconds)\n",
                        current_time_us(), $#threads + 1, $time_until_read_only * 100;
                    if ( $#threads < 5 ) {
                        print Data::Dumper->new( [$_] )->Indent(0)->Terse(1)->Dump . "\n"
                        foreach (@threads);
                    }
                    }
                    sleep_until();
                    $_tstart = [gettimeofday];
                    $time_until_read_only--;
                    @threads = get_threads_util( $orig_master_handler->{dbh},
                    $orig_master_handler->{connection_id} );
                }

                ## Setting read_only=1 on the current master so that nobody(except SUPER) can write
                print current_time_us() . " Set read_only=1 on the orig master.. ";
                $orig_master_handler->enable_read_only();
                if ( $orig_master_handler->is_read_only() ) {
                    print "ok.\n";
                }
                else {
                    die "Failed!\n";
                }

                ## Waiting for M * 100 milliseconds so that current update queries can complete
                my $time_until_kill_threads = 5;
                @threads = get_threads_util( $orig_master_handler->{dbh},
                    $orig_master_handler->{connection_id} );
                while ( $time_until_kill_threads > 0 && $#threads >= 0 ) {
                    if ( $time_until_kill_threads % 5 == 0 ) {
                        printf
                "%s Waiting all running %d queries are disconnected.. (max %d milliseconds)\n",
                            current_time_us(), $#threads + 1, $time_until_kill_threads * 100;
                        if ( $#threads < 5 ) {
                            print Data::Dumper->new( [$_] )->Indent(0)->Terse(1)->Dump . "\n"
                            foreach (@threads);
                        }
                    }
                    sleep_until();
                    $_tstart = [gettimeofday];
                    $time_until_kill_threads--;
                    @threads = get_threads_util( $orig_master_handler->{dbh},
                    $orig_master_handler->{connection_id} );
                }

                ## Terminating all threads
                print current_time_us() . " Killing all application threads..\n";
                $orig_master_handler->kill_threads(@threads) if ( $#threads >= 0 );
                print current_time_us() . " done.\n";
                $orig_master_handler->enable_log_bin_local();
                $orig_master_handler->disconnect();

                ## After finishing the script, MHA executes FLUSH TABLES WITH READ LOCK
                ## Stop master ip
                print current_time_us() . " Disabling the VIP $vip on old master $orig_master_host...\n";
                &stop_vip();
                $exit_code = 0;
            };
            if ($@) {
                warn "Got Error: $@\n";
                exit $exit_code;
            }
            exit $exit_code;
        }
        elsif ( $command eq "start" ) {
            ## Activating master ip on the new master
            # 1. Create app user with write privileges
            # 2. Moving backup script if needed
            # 3. Register new master's ip to the catalog database

            # We don't return error even though activating updatable accounts/ip failed so that we don't interrupt slaves' recovery.
            # If exit code is 0 or 10, MHA does not abort
            my $exit_code = 10;
            eval {
                my $new_master_handler = new MHA::DBHelper();

                # args: hostname, port, user, password, raise_error_or_not
                $new_master_handler->connect( $new_master_ip, $new_master_port,
                    $new_master_user, $new_master_password, 1 );

                ## Set read_only=0 on the new master
                $new_master_handler->disable_log_bin_local();
                print current_time_us() . " Set read_only=0 on the new master.\n";
                $new_master_handler->disable_read_only();

                ## Creating an app user on the new master
                print current_time_us() . " Creating app user on the new master..\n";
                $new_master_handler->enable_log_bin_local();
                $new_master_handler->disconnect();

                ## Update master ip on the catalog database, etc
                print current_time_us() . " Enabling the VIP $vip on the new master...\n";
                &start_vip();
                print current_time_us() . " Arping the VIP $vip on the new master....\n";
                &notify_vip();
                $exit_code = 0;
            };
            if ($@) {
                warn "Got Error: $@\n";
                exit $exit_code;
            }
            exit $exit_code;
        }
        elsif ( $command eq "status" ) {

            # do nothing
            exit 0;
        }
        else {
            &usage();
            exit 1;
        }
    }

    sub get_conf() {
        my $cfg = Config::IniFiles->new( -file => $vip_conf );
        $vip = $cfg->val('server default', 'vip');
        $dev = $cfg->val('server default', 'dev');
    }

    sub start_vip() {
        get_conf();
        my $ssh_start_vip = "ip addr add $vip dev $dev"; #启用虚拟ip
        `ssh $new_master_ssh_user\@$new_master_host \" $ssh_start_vip \"`;
    }

    sub stop_vip() {
        get_conf();
        my $ssh_stop_vip = "ip addr del $vip dev $dev"; #关闭虚拟ip
        return 0  unless  ($orig_master_ssh_user);
        `ssh $orig_master_ssh_user\@$orig_master_host \" $ssh_stop_vip \"`;
    }

    sub notify_vip() {
        get_conf();
        my $gateway = `ip route list | grep "default via" | awk '{print \$3}'`; #网关
        my $vipp = `echo -n "$vip" | sed "s/\\/[[:digit:]]*//"`; #虚拟ip
        my $ssh_notify_vip = "arping -U -I $dev -c 2 -w 5 -s $vipp $gateway"; #通知网关ip改变
        `ssh $new_master_ssh_user\@$new_master_host \" $ssh_notify_vip \"`;
    }

    sub usage {
    print
    "Usage: master_ip_online_change --command=start|stop|status --orig_master_host=host --orig_master_ip=ip --orig_master_port=port --new_master_host=host --new_master_ip=ip --new_master_port=port\n";
    die;
    }
    ```

3. 配置MHA Manager的master_ip_failover.pl, master_ip_online_change.pl脚本

    添加可执行权限
    ```bash
    chmod +x /opt/data/mha_manager/app1/master_ip_failover.pl
    chmod +x /opt/data/mha_manager/app1/master_ip_online_change.pl
    ```

    向MHA Manager主机的/opt/data/mha_manager/app1/mha_manager.cnf的server default的section添加配置
    ```ini
    master_ip_failover_script=/opt/data/mha_manager/app1/master_ip_failover.pl
    master_ip_online_change_script=/opt/data/mha_manager/app1/master_ip_online_change.pl
    ```

    向MHA Manager主机的/opt/data/mha_manager/app1/vip.cnf的server default的section添加配置，请修改下列配置的vip，dev值，vip是带有子网掩码表示形式的ip，dev是网络接口设备名称。
    ```ini
    [server default]
    vip=10.118.196.109/22
    dev=ens18
    ```

4. 添加Master虚拟ip

    向Master主机添加虚拟ip。请更改虚拟ip地址，网络设备名称。
    ```bash
    #向Master主机添加虚拟ip
    vip=192.168.0.8/22
    dev=ens18
    ip addr add $vip dev $dev
    ```

## Wiki

## Refer

- 其他HA解决方案的优缺点对比，参考官方网站[Other_HA_Solutions](https://raw.githubusercontent.com/wiki/yoshinorim/mha4mysql-manager/Other_HA_Solutions.md)
- HA架构，参考官方网站[Architecture](https://raw.githubusercontent.com/wiki/yoshinorim/mha4mysql-manager/Architecture.md)
- MHA优点，参考官方网站[Advantages](https://github.com/yoshinorim/mha4mysql-manager/wiki/Advantages)
- GTID Replication方式带来的新问题[GTIDs in MySQL 5.6: New replication protocol; new ways to break replication](https://www.percona.com/blog/2014/05/09/gtids-in-mysql-5-6-new-replication-protocol-new-ways-to-break-replication/)

## FAQ

1. 出现问题```Can't locate inc/Module/Install.pm```
    原因是没有安装perl的模块，需要安装：
    ```bash
    yum install -y perl-Module-Install
    ```

2. 出现问题```mysqld: error while loading shared libraries: libaio.so.1: cannot open shared object file: No such file or directory```

    参考[libaio-so-1-cannot-open-shared-object-file](https://stackoverflow.com/questions/10619298/libaio-so-1-cannot-open-shared-object-file)

    原因是没有安装libaio库，需要安装：
    ```bash
    yum install -y libaio
    ```

3. 出现问题```There is no alive slave. We can't do failover```

    可能的原因：

    - 没有在Slave中设置复制Master配置。参考[MySQL-Replication配置](##MySQL-Replication配置)的第3节

4. 出现问题```There is no alive server. We can't do failover```

    可能的原因：
    - iptables防火墙没有开放MySQL Server的服务端口3306。需要开放该端口。
        ```bash
        iptables -I INPUT -p tcp --dport 3306 -j ACCEPT
        ```
    - Slave的MySQL重启后没有及时响应，请等待几秒钟。

5. 出现问题```Failed to save binary log: Binlog not found from mysql-bin```

    需要在MHA Manager的配置文件添加配置：
    ```ini
    #请更改master_binlog_dir的值
    [server default]
    master_binlog_dir=/path/to/master/binlog/dir
    ```

6. 出现问题```Slave IO thread is not running```

    可能的原因：
    - Slave的IO Thread没有开启，需要手动开启。参考[MySQL-Replication配置](##MySQL-Replication配置)的第3节

    - 没有授权复制账号。参考[MySQL-Replication配置](##MySQL-Replication配置)的第4节

7. 出现问题```Slave SQL thread is not running```

    可能的原因：
    - 没有正确配置主从复制。参考[MySQL-Replication配置](##MySQL-Replication配置)的第3节

    - Slave配置更改后需要重新启动MySQL服务。

8. 出现问题```Server 192.168.0.6(192.168.0.6:3306) is dead, but must be alive! Check server settings.```

    可能的原因：

    - 检查MySQL服务已经在运行。需要稍微等一会。

9. 出现问题```Master 192.168.0.7:3306 from which slave 192.168.0.6(192.168.0.6:3306) replicates is not defined in the configuration file!```

    可能的原因：

    - 请检查MHA Manager的配置文件的MySQL服务器ip配置的正确性。

10. 用yum安装MHA依赖的perl模块，出现问题```No package perl-Parallel-ForkManager available.```, 或```No package perl-Log-Dispatch available.```

    可能的原因：
    - 镜像源中没有对应的安装包。需要添加epel镜像源。
    ```bash
    curl -L -C - -O https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    yum localinstall -y epel-release-latest-7.noarch.rpm
    ```

11. 启动MySQL时，出现问题"[ERROR] unknown variable 'rpl_semi_sync_master_enabled=1'"

    可能的原因：
    - my.cnf的配置项出错，不应该如下配置：
        ```ini
        plugin-load=rpl_semi_sync_master=semisync_master.so
        plugin-load=rpl_semi_sync_slave=semisync_slave.so
        ```
        应该这样配置：
        ```ini
        plugin-load=rpl_semi_sync_master=semisync_master.so;rpl_semi_sync_slave=semisync_slave.so
        ```