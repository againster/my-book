
# xtrabackup

- [安装xtrabackup](##安装xtrabackup)
- [备份](##备份)
  - [前置要求](###前置要求)
  - [全量备份](###全量备份)
  - [增量备份](###增量备份)
- [Restoring备份](##Restoring备份)
- [定时任务](##定时任务)

***Author: ArrowLee***

***Date: 2018/1/9***

## 安装xtrabackup

参考官方网站[installation](https://www.percona.com/doc/percona-xtrabackup/LATEST/installation.html)

1. 下载，解压tar.gz包到/opt目录

    ```bash
    curl -L -C - -O https://www.percona.com/downloads/XtraBackup/Percona-XtraBackup-2.4.9/binary/tarball/percona-xtrabackup-2.4.9-Linux-x86_64.tar.gz
    tar zxvf percona-xtrabackup-2.4.9-Linux-x86_64.tar.gz -C /opt
    mv /opt/percona-xtrabackup-2.4.9-Linux-x86_64 /opt/xtrabackup
    ```

2. 配置环境变量

    ```bash
    #创建/etc/profile.d/xtrabackup.sh
    touch /etc/profile.d/xtrabackup.sh

    #向文件中写入如下内容
    cat << -DONE- > /etc/profile.d/xtrabackup.sh
    #!/bin/bash
    XTRABACKUP_HOME=/opt/xtrabackup
    PATH=\$XTRABACKUP_HOME/bin:\$PATH
    export PATH
    -DONE-

    #使配置的环境变量生效
    source /etc/profile.d/xtrabackup.sh
    ```

## 备份

### 前置要求

- 参考官方网站[privileges](https://www.percona.com/doc/percona-xtrabackup/LATEST/using_xtrabackup/privileges.html)
- 参考官方网站[configuring](https://www.percona.com/doc/percona-xtrabackup/LATEST/using_xtrabackup/configuring.html)

### 全量备份

参考官方网站[full_backup](https://www.percona.com/doc/percona-xtrabackup/LATEST/backup_scenarios/full_backup.html)

1. Creating备份

    **前置要求：**
    > - If the target directory does not exist, xtrabackup creates it. If the directory does exist and is empty, xtrabackup will succeed. xtrabackup will not overwrite existing files, it will fail with operating system error 17, file exists.

    创建全量备份，请更改命令的参数。
    ```bash
    #在被备份的主机，创建全量备份，请更改参数MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, BACKUP_FULL_DIR的值。
    MYSQL_HOST=localhost
    MYSQL_PORT=3307
    MYSQL_USER=root
    MYSQL_PASSWORD=bbc@sfdc
    BACKUP_FULL_DIR=/opt/data/backup_full
    xtrabackup --backup --host=$MYSQL_HOST --port=$MYSQL_PORT --user=$MYSQL_USER --password=$MYSQL_PASSWORD --target-dir=$BACKUP_FULL_DIR
    ```

    > The backup can take a long time, depending on how large the database is. It is safe to cancel at any time, because xtrabackup does not modify the database.

    > - ```--host=HOST``` This option accepts a string argument that specifies the host to use when connecting to the database server with TCP/IP. It is passed to the mysql child process without alteration. See mysql --help for details.
    > - ```--user=USERNAME``` This option specifies the MySQL username used when connecting to the server, if that’s not the current user. The option accepts a string argument. See mysql –help for details.
    > - ```--port=PORT``` This option accepts a string argument that specifies the port to use when connecting to the database server with TCP/IP. It is passed to the mysql child process without alteration. See mysql --help for details.
    > - ```--password=PASSWORD``` This option specifies the password to use when connecting to the database. It accepts a string argument. See mysql --help for details.
    > - ```--target-dir=DIRECTORY``` This option specifies the destination directory for the backup. If the directory does not exist, xtrabackup creates it. If the directory does exist and is empty, xtrabackup will succeed. xtrabackup will not overwrite existing files, however; it will fail with operating system error 17, file exists.

2. Preparing备份

    ```bash
    #在被备份的主机，prepare备份
    xtrabackup --prepare --target-dir=/opt/data/backup_full
    ```
    > - ```--prepare``` Makes xtrabackup perform a recovery on a backup created with --backup, so that it is ready to use. See [preparing a backup](https://www.percona.com/doc/percona-xtrabackup/LATEST/backup_scenarios/full_backup.html#preparing-a-backup).

### 增量备份

参考官方网站[incremental_backup](https://www.percona.com/doc/percona-xtrabackup/LATEST/backup_scenarios/incremental_backup.html)

1. Creating增量备份

    ```bash
    #在被备份的主机，创建增量备份，请更改参数MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, BACKUP_INC_DIR, BACKUP_FULL_DIR的值。
    MYSQL_HOST=localhost
    MYSQL_PORT=3307
    MYSQL_USER=root
    MYSQL_PASSWORD=bbc@sfdc
    BACKUP_INC_DIR=/opt/data/backup_inc
    BACKUP_FULL_DIR=/opt/data/backup_full

    xtrabackup --backup --host=$MYSQL_HOST --port=$MYSQL_PORT --user=$MYSQL_USER --password=$MYSQL_PASSWORD --target-dir=$BACKUP_INC_DIR --incremental-basedir=$BACKUP_FULL_DIR
    ```

    > - ```--incremental-basedir=DIRECTORY``` When creating an incremental backup, this is the directory containing the full backup that is the base dataset for the incremental backups.

2. Preparing增量备份

    > - The ```--prepare``` step for incremental backups is not the same as for full backups. In full backups, two types of operations are performed to make the database consistent: committed transactions are replayed from the log file against the data files, and uncommitted transactions are rolled back. You must skip the rollback of uncommitted transactions when preparing an incremental backup, because transactions that were uncommitted at the time of your backup may be in progress, and it’s likely that they will be committed in the next incremental backup. You should use the ```--apply-log-only``` option to prevent the rollback phase.

    **注意：**
    > - If you do not use the --apply-log-only option to prevent the rollback phase, then your incremental backups will be useless. After transactions have been rolled back, further incremental backups cannot be applied.

    在备份的主机，preparing增量备份
    ```bash
    #拷贝增量备份的文件到备份主机，preparing增量备份。请更改参数REMOTE_HOST, REMOTE_BUCKUP_INC_DIR, BACKUP_INC_DIR, RESTORE_DATADIR的值。
    REMOTE_HOST=10.118.195.6
    REMOTE_BUCKUP_INC_DIR=/opt/data/backup_inc
    BACKUP_INC_DIR=/opt/data/mysql/backup_inc
    RESTORE_DATADIR=/opt/data/mysql/db

    scp -r root@$REMOTE_HOST:$REMOTE_BUCKUP_INC_DIR/* $BACKUP_INC_DIR

    #在备份的主机，preparing。
    xtrabackup --prepare --apply-log-only --target-dir=$RESTORE_DATADIR
    xtrabackup --prepare --target-dir=$RESTORE_DATADIR --incremental-dir=$BACKUP_INC_DIR
    ```
    > - ```--apply-log-only``` This option causes only the redo stage to be performed when preparing a backup. It is very important for incremental backups.
    > - ```--incremental-dir=DIRECTORY``` When preparing an incremental backup, this is the directory where the incremental backup is combined with the full backup to make a new full backup.

## Restoring备份

**前置要求：**

> - The datadir must be empty before restoring the backup. Also it’s important to note that MySQL server needs to be shut down before restore is performed. You cannot restore to a datadir of a running mysqld instance (except when importing a partial backup).
> - Backup needs to be prepared before it can be restored.

主要是停止MySQL服务，拷贝全量备份的文件到备份主机，然后修改备份文件权限为mysql，启动MySQL服务。以下命令仅作为Restoring备份的参考。

```bash
#停止MySQL服务
mysql.server stop

#拷贝全量备份的文件到备份主机的MySQL的datadir目录。请更改参数REMOTE_HOST, REMOTE_BACKUP_FULL_DIR, RESTORE_DATADIR的值。
REMOTE_HOST=10.118.195.6
REMOTE_BACKUP_FULL_DIR=/opt/data/backup_full
RESTORE_DATADIR=/opt/data/mysql/db
scp -r root@$REMOTE_HOST:$REMOTE_BACKUP_FULL_DIR/* $RESTORE_DATADIR

#更改文件ownership
chown -R mysql:mysql /opt/data/mysql/db

#开启MySQL服务，可以无错误启动
mysql.server start
```

## 定时任务

以下两个脚本需要依赖远程rsync服务器搭建完成，以及关联的rsync备份主机上的模块、用户、secrets file配置，还有ssh key的交换。如果用其他方式同步，请更改脚本末尾同步命令。

1. 全量备份定时任务

    向/
    ```bash
    #!/bin/bash
    #在被备份的主机，创建全量备份，请更改参数MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, BACKUP_FULL_DIR的值。
    MYSQL_HOST=localhost
    MYSQL_PORT=3307
    MYSQL_USER=root
    MYSQL_PASSWORD=bbc@sfdc
    BACKUP_DIR=/opt/data/xtrabackup/backup
    TMP_DIR=/tmp
    #同步
    RSYNC_BACKUP_HOST=121.46.26.149
    RSYNC_BACKUP_SECRETS_FILE=/opt/data/xtrabackup/secrets.passwd
    RSYNC_BACKUP_DIR=MySQLbackup
    RSYNC_BACKUP_USER=dc
    SSH_USER=root

    set -ex

    #命令检测
    COMMAND_CHECK=("mysql" "xtrabackup")
    for x in ${COMMAND_CHECK[*]};
    do
        command -v $x > /dev/null || (echo "$x has not been installed" && exit 1);
    done

    #备份目录构造
    RAND=`od -An -N4 -tx4 /dev/urandom | sed 's/ //'`
    DATE_NOW=`date "+%Y%m%d%H%M%S"`
    BACKUP_FULL_DIR=$BACKUP_DIR/$DATE_NOW/full
    TMP_FULL_DIR=$TMP_DIR/$DATE_NOW$RAND

    #目录检测
    test -d $BACKUP_FULL_DIR && (echo "$BACKUP_FULL_DIR has exist" && exit 1);

    DIR_CHECK=("$BACKUP_DIR" "$BACKUP_FULL_DIR" "$TMP_FULL_DIR")
    for x in ${DIR_CHECK[*]};
    do
        test -d $x || mkdir -p $x
    done

    #备份，preparing
    xtrabackup --backup --host=$MYSQL_HOST --port=$MYSQL_PORT --user=$MYSQL_USER --password=$MYSQL_PASSWORD --target-dir=$TMP_FULL_DIR && xtrabackup --prepare --target-dir=$TMP_FULL_DIR
    mv $TMP_FULL_DIR/* $BACKUP_FULL_DIR || echo "the same name $BACKUP_FULL_DIR exists"
    rm -rf $TMP_FULL_DIR

    #通过rsync同步
    rsync -rlzv -e "ssh -l $SSH_USER" --password-file $RSYNC_BACKUP_SECRETS_FILE $BACKUP_DIR $RSYNC_BACKUP_USER@$RSYNC_BACKUP_HOST::$RSYNC_BACKUP_DIR
    ```

2. 增量备份定时任务

    ```bash
    #!/bin/bash
    #在被备份的主机，创建增量备份，请更改参数MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, BACKUP_INC_DIR, BACKUP_FULL_DIR的值。
    MYSQL_HOST=localhost
    MYSQL_PORT=3307
    MYSQL_USER=root
    MYSQL_PASSWORD=bbc@sfdc
    BACKUP_DIR=/opt/data/xtrabackup/backup
    TMP_DIR=/tmp
    #同步
    RSYNC_BACKUP_HOST=121.46.26.149
    RSYNC_BACKUP_SECRETS_FILE=/usr/local/data/xtrabackup/secrets.passwd
    RSYNC_BACKUP_DIR=MySQLbackup
    RSYNC_BACKUP_USER=dc
    SSH_USER=root

    set -ex

    #命令检测
    COMMAND_CHECK=("mysql" "xtrabackup")
    for x in ${COMMAND_CHECK[*]};
    do
        command -v $x > /dev/null || (echo "$x has not been installed" && exit 1);
    done

    #备份目录构造
    RAND=`od -An -N4 -tx4 /dev/urandom | sed 's/ //'`
    DATE_NOW=`date "+%Y%m%d%H%M%S"`
    LATEST_BACKUP_FULL_DATE=`ls $BACKUP_DIR | grep -P "\d+" | sort | tail -n 1`
    BACKUP_INC_DIR=$BACKUP_DIR/$LATEST_BACKUP_FULL_DATE/inc_$DATE_NOW
    BACKUP_FULL_DIR=$BACKUP_DIR/$LATEST_BACKUP_FULL_DATE/full
    TMP_INC_DIR=$TMP_DIR/$DATE_NOW$RAND
    if [ x"$LATEST_BACKUP_FULL_DATE" == "x" ];then
        echo "full backup has not been executed, wait for it." && exit 1
    fi

    #目录检测
    test -d $BACKUP_INC_DIR && (echo "$BACKUP_INC_DIR has exist" && exit 1);

    DIR_CHECK=("$BACKUP_DIR" "$BACKUP_INC_DIR" "$TMP_INC_DIR")
    for x in ${DIR_CHECK[*]};
    do
        test -d $x || mkdir -p $x
    done

    #备份
    xtrabackup --backup --host=$MYSQL_HOST --port=$MYSQL_PORT --user=$MYSQL_USER --password=$MYSQL_PASSWORD --target-dir=$TMP_INC_DIR --incremental-basedir=$BACKUP_FULL_DIR
    mv $TMP_INC_DIR/* $BACKUP_INC_DIR || echo "the same name $BACKUP_INC_DIR exists"
    rm -rf $TMP_INC_DIR

    ##preparing
    xtrabackup --prepare --apply-log-only --target-dir=$BACKUP_FULL_DIR && xtrabackup --prepare --target-dir=$BACKUP_FULL_DIR --incremental-dir=$BACKUP_INC_DIR

    #同步
    rsync -rlzv -e "ssh -l $SSH_USER" --password-file $RSYNC_BACKUP_SECRETS_FILE $BACKUP_DIR $RSYNC_BACKUP_USER@$RSYNC_BACKUP_HOST::$RSYNC_BACKUP_DIR
    ```

3. 添加到crontab

    1. 添加配置到/etc/cron.d/mysql_backup文件
        ```bash
        # Run the hourly jobs
        SHELL=/bin/bash
        PATH=/sbin:/bin:/usr/sbin:/usr/bin:/opt/xtrabackup/bin:/opt/mysql/bin
        MAILTO=root
        BACKUP_DIR=/opt/data/xtrabackup
        */30 * * * * root $BACKUP_DIR/xtrabackup_full.sh >> $BACKUP_DIR/xtrabackup_full.log 2>&1
        */10 * * * * root $BACKUP_DIR/xtrabackup_inc.sh >> $BACKUP_DIR/xtrabackup_inc.log 2>&1
        ```

    2. 加载配置

        ```bash
        service crond reload
        ```
