#!/bin/bash
#在被备份的主机，创建全量备份，请更改参数MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, BACKUP_FULL_DIR的值。
MYSQL_HOST=localhost
MYSQL_PORT=3306
MYSQL_USER=root
MYSQL_PASSWORD=123456
BACKUP_DIR=/sf/scloud/data/xtrabackup/backup
TMP_DIR=/tmp
#同步
RSYNC_BACKUP_HOST=121.46.26.149
RSYNC_BACKUP_PORT="3500"
RSYNC_BACKUP_SECRETS_FILE=/usr/local/data/xtrabackup/secrets.passwd
RSYNC_BACKUP_DIR=MySQLbackup
RSYNC_BACKUP_USER=dc
SSH_USER=root
SSH_PORT=33345

set -e

#命令检测
COMMAND_CHECK=("mysql" "xtrabackup")
for x in ${COMMAND_CHECK[*]};
do
    command -v $x > /dev/null || (echo "$x has not been installed" && exit 1);
done

#备份目录构造
RAND=`od -An -N4 -tx4 /dev/urandom | sed 's/ //'`
DATE_NOW=`date "+%Y%m%d%H%M%S"`
BACKUP_DATE_DIR=$BACKUP_DIR/$DATE_NOW
BACKUP_FULL_DIR=$BACKUP_DIR/$DATE_NOW/full
TMP_FULL_DIR=$TMP_DIR/$DATE_NOW$RAND

#目录检测
test -d $BACKUP_FULL_DIR && (echo "$BACKUP_FULL_DIR has exist" && exit 1);

DIR_CHECK=("$BACKUP_DIR" "$BACKUP_FULL_DIR" "$TMP_FULL_DIR")
for x in ${DIR_CHECK[*]};
do
    test -d $x || mkdir -p $x
done

#备份，preparing
xtrabackup --backup --host=$MYSQL_HOST --port=$MYSQL_PORT --user=$MYSQL_USER --password=$MYSQL_PASSWORD --target-dir=$TMP_FULL_DIR && xtrabackup --prepare --target-dir=$TMP_FULL_DIR
mv $TMP_FULL_DIR/* $BACKUP_FULL_DIR || echo "the same name $BACKUP_FULL_DIR exists"
rm -rf $TMP_FULL_DIR

#同步
rsync -rlzv -e "ssh -l $SSH_USER -p $SSH_PORT" --port=$RSYNC_BACKUP_PORT --password-file $RSYNC_BACKUP_SECRETS_FILE $BACKUP_DATE_DIR $RSYNC_BACKUP_USER@$RSYNC_BACKUP_HOST::$RSYNC_BACKUP_DIR
