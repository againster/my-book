# mysql-password validation plugin

## 安装
本节描述如何安装验证密码密码验证插件。有关安装插件的信息，请参见5.5.1节，“Installing and Uninstalling Plugins”。This section describes how to install the validate_password password-validation plugin. For general information about installing plugins, see Section 5.5.1, “Installing and Uninstalling Plugins”.

要使服务器可以使用这个插件，插件库文件必须位于MySQL插件目录中(由系统变量[plugin_dir](https://dev.mysql.com/doc/refman/5.6/en/server-system-variables.html#sysvar_plugin_dir)定义的目录)。如果有需要，通过在服务器启动时设置[plugin_dir](https://dev.mysql.com/doc/refman/5.6/en/server-system-variables.html#sysvar_plugin_dir)的值来配置插件目录位置。To be usable by the server, the plugin library file must be located in the MySQL plugin directory (the directory named by the [plugin_dir](https://dev.mysql.com/doc/refman/5.6/en/server-system-variables.html#sysvar_plugin_dir) system variable). If necessary, configure the plugin directory location by setting the value of [plugin_dir](https://dev.mysql.com/doc/refman/5.6/en/server-system-variables.html#sysvar_plugin_dir) at server startup.

插件库的文件名是validate password。文件名后缀根据平台的不同而不同(例如，```.so```对于Unix和类Unix系统，```.dll```对于Windows)。The plugin library file base name is validate_password. The file name suffix differs per platform (for example, ```.so``` for Unix and Unix-like systems, ```.dll``` for Windows).

要在服务器启动时加载插件，可以使用```--plugin-load-add```选项来声明包含它的库文件。使用这种插件加载方法，必须在每次服务器启动时提供该选项。例如，将这些行放到服务器my.cnf文件中(根据需要调整平台的```.so```后缀)。To load the plugin at server startup, use the ```--plugin-load-add``` option to name the library file that contains it. With this plugin-loading method, the option must be given each time the server starts. For example, put these lines in the server my.cnf file (adjust the ```.so``` suffix for your platform as necessary):

```ini
[mysqld]
plugin-load-add=validate_password.so
```

修改```my.cnf```之后，重启服务器以使新设置生效。After modifying my.cnf, restart the server to cause the new settings to take effect.

或者，要在运行时加载插件，可以使用以下语句(根据需要调整平台的```.so```后缀)。Alternatively, to load the plugin at runtime, use this statement (adjust the ```.so``` suffix for your platform as necessary):

```sql
INSTALL PLUGIN validate_password SONAME 'validate_password.so';
```

[INSTALL PLUGIN](https://dev.mysql.com/doc/refman/5.6/en/install-plugin.html)加载插件，并将其注册到```mysql.plugins```表中，使插件在以后的每次正常服务器启动时加载，而不需要[--plugin-load-add](https://dev.mysql.com/doc/refman/5.6/en/server-options.html#option_mysqld_plugin-load-add)。[INSTALL PLUGIN](https://dev.mysql.com/doc/refman/5.6/en/install-plugin.html) loads the plugin, and also registers it in the ```mysql.plugins``` system table to cause the plugin to be loaded for each subsequent normal server startup without the need for [--plugin-load-add](https://dev.mysql.com/doc/refman/5.6/en/server-options.html#option_mysqld_plugin-load-add).

要验证插件是否安装，请检查[INFORMATION_SCHEMA.PLUGINS](https://dev.mysql.com/doc/refman/5.6/en/plugins-table.html)表或使用[SHOW PLUGINS](https://dev.mysql.com/doc/refman/5.6/en/show-plugins.html)语句(参见第5.5.2节， [“Obtaining Server Plugin Information”](https://dev.mysql.com/doc/refman/5.6/en/obtaining-plugin-information.html))。例如：To verify plugin installation, examine the [INFORMATION_SCHEMA.PLUGINS](https://dev.mysql.com/doc/refman/5.6/en/plugins-table.html) table or use the [SHOW PLUGINS](https://dev.mysql.com/doc/refman/5.6/en/show-plugins.html)statement (see Section 5.5.2, [INFORMATION_SCHEMA.PLUGINS](https://dev.mysql.com/doc/refman/5.6/en/plugins-table.html)). For example:

```sql
mysql> SELECT PLUGIN_NAME, PLUGIN_STATUS
       FROM INFORMATION_SCHEMA.PLUGINS
       WHERE PLUGIN_NAME LIKE 'validate%';
+-------------------+---------------+
| PLUGIN_NAME       | PLUGIN_STATUS |
+-------------------+---------------+
| validate_password | ACTIVE        |
+-------------------+---------------+
```

如果插件初始化失败，请检查服务器错误日志以获取诊断消息。If the plugin failed to initialize, check the server error log for diagnostic messages.

如果这个插件之前已经使用[INSTALL PLUGIN](https://dev.mysql.com/doc/refman/5.6/en/install-plugin.html)注册，或者加载了[--plugin-load-add](https://dev.mysql.com/doc/refman/5.6/en/server-options.html#option_mysqld_plugin-load-add)，那么你可以在服务器启动时使用```--validate-password```选项来控制插件的激活。例如，要在启动时加载插件并防止在运行时删除它，可以使用以下选项。If the plugin has been previously registered with INSTALL PLUGIN or is loaded with --plugin-load-add, you can use the --validate-password option at server startup to control plugin activation. For example, to load the plugin at startup and prevent it from being removed at runtime, use these options:

```ini
[mysqld]
plugin-load-add=validate_password.so
validate-password=FORCE_PLUS_PERMANENT
```

如果想要避免服务器在没有password-validation插件的情况下运行，可以使用带有```FORCE```或```FORCE_PLUS_PERMANENT```值的```--validate-password```强制服务器在插件初始化不成功时启动失败。If it is desired to prevent the server from running without the password-validation plugin, use ```--validate-password``` with a value of ```FORCE``` or ```FORCE_PLUS_PERMANENT``` to force server startup to fail if the plugin does not initialize successfully.

## 参考

- [官网validate-password-installation](https://dev.mysql.com/doc/refman/5.6/en/validate-password-installation.html)
- [官网validate-password-options-variables](https://dev.mysql.com/doc/refman/5.6/en/validate-password-options-variables.html)