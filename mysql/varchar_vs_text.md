
### Some Differences Between VARCHAR and TEXT

https://navicat.com/en/company/aboutus/blog/1308-choosing-between-varchar-and-text-in-mysql

While both data types share a maximum length of 65,535 characters, there are still a few differences:

-   The VAR in VARCHAR means that you can set the max size to anything between 1 and 65,535. TEXT fields have a fixed max size of 65,535 characters.
-   A VARCHAR can be part of an index whereas a TEXT field requires you to specify a prefix length, which can be part of an index.
-   VARCHAR is stored inline with the table (at least for the MyISAM storage engine), making it potentially faster when the size is reasonable. Of course, how much faster depends on both your data and your hardware. Meanwhile, TEXT is stored off table with the table having a pointer to the location of the actual storage.
-   Using a TEXT column in a sort will require the use of a disk-based temporary table, as the MEMORY (HEAP) storage engine.