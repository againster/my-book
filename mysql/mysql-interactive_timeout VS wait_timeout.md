# mysql-interactive_timeout VS wait_timeout

## interactive_timeout简介

服务器在关闭交互式连接之前等待活动的秒数。交互式客户端被定义为使用```mysql_real_connect()```的```CLIENT_INTERACTIVE```选项的客户端。见```wait_timeout```。The number of seconds the server waits for activity on an interactive connection before closing it. An interactive client is defined as a client that uses the CLIENT_INTERACTIVE option to mysql_real_connect(). See also wait_timeout.

## interactive_timeout属性

```txt
Property                Value
Command-Line Format     --interactive-timeout=#
System Variable         interactive_timeout
Scope                   Global, Session
Dynamic                 Yes
SET_VAR Hint Applies    No
Type                    Integer
Default Value           28800
Minimum Value           1
```

## wait_timeout简介

服务器在关闭非交互式连接之前等待活动的秒数。The number of seconds the server waits for activity on a noninteractive connection before closing it.

在线程启动时，会话```wait_timeout```值由全局```wait_timeout```值或全局```interactive_timeout```值初始化，具体取决于客户端的类型(由```mysql_real_connect()```的```CLIENT_INTERACTIVE```连接选项定义)，见```interactive_timeout```。On thread startup, the session wait_timeout value is initialized from the global wait_timeout value or from the global interactive_timeout value, depending on the type of client (as defined by the CLIENT_INTERACTIVE connect option to mysql_real_connect()). See also interactive_timeout.

## wait_timeout属性
```txt
Property                Value
Command-Line Format     --wait-timeout=#
System Variable         wait_timeout
Scope                   Global, Session
Dynamic	                Yes
SET_VAR Hint Applies    No
Type                    Integer
Default Value           28800
Minimum Value           1
Maximum Value (Other)   31536000
Maximum Value (Windows) 2147483
```

## interactive_timeout VS wait_timeout

mysqldump, mysql命令行工具都是以交互式类型登录的。

根据简介描述，显而易见，```interactive_timeout```是交互的连接超时，```wait_timeout```是非交互连接超时。但是```interactive_timeout```并不是想象中的那样。

```sql
show global variables like '%timeout%';
+-----------------------------+----------+
| Variable_name               | Value    |
+-----------------------------+----------+
| connect_timeout             | 10       |
| delayed_insert_timeout      | 300      |
| have_statement_timeout      | YES      |
| innodb_flush_log_at_timeout | 1        |
| innodb_lock_wait_timeout    | 50       |
| innodb_rollback_on_timeout  | OFF      |
| interactive_timeout         | 60       |
| lock_wait_timeout           | 31536000 |
| net_read_timeout            | 30       |
| net_write_timeout           | 60       |
| rpl_stop_slave_timeout      | 31536000 |
| slave_net_timeout           | 60       |
| wait_timeout                | 40       |
+-----------------------------+----------+
```
全局变量```wait_timeout```，```interactive_timeout```各为40，60。再看一下会话变量
```sql
show session variables like '%timeout%';
+-----------------------------+----------+
| Variable_name               | Value    |
+-----------------------------+----------+
| connect_timeout             | 10       |
| delayed_insert_timeout      | 300      |
| have_statement_timeout      | YES      |
| innodb_flush_log_at_timeout | 1        |
| innodb_lock_wait_timeout    | 50       |
| innodb_rollback_on_timeout  | OFF      |
| interactive_timeout         | 60       |
| lock_wait_timeout           | 31536000 |
| net_read_timeout            | 30       |
| net_write_timeout           | 60       |
| rpl_stop_slave_timeout      | 31536000 |
| slave_net_timeout           | 60       |
| wait_timeout                | 60       |
+-----------------------------+----------+
```
可以看到会话变量```wait_timeout```与同名的全局变量值不同，但是与全局```interactive_timeout```相同，其值与官方解释的相同，根据客户端端连接类型而选择```wait_timeout```或者```interactive_timeout```其一，而现在是使用命令行mysql登录的，所以为交互式连接，所以会话变量```wait_timeout```其值为全局```interactive_timeout```的值。当断开连接，重新连接后，仅设置会话变量```interactive_timeout```时
```sql
set session interactive_timeout=120;
show session variables like '%timeout%';
+-----------------------------+----------+
| Variable_name               | Value    |
+-----------------------------+----------+
| connect_timeout             | 10       |
| delayed_insert_timeout      | 300      |
| have_statement_timeout      | YES      |
| innodb_flush_log_at_timeout | 1        |
| innodb_lock_wait_timeout    | 50       |
| innodb_rollback_on_timeout  | OFF      |
| interactive_timeout         | 120      |
| lock_wait_timeout           | 31536000 |
| net_read_timeout            | 30       |
| net_write_timeout           | 60       |
| rpl_stop_slave_timeout      | 31536000 |
| slave_net_timeout           | 60       |
| wait_timeout                | 60       |
+-----------------------------+----------+
```
当不做任何操作，当前连接不会在```interactive_timeout```的120秒后自动断开连接，而是在等待```wait_timeout```60秒后，就已经断开连接。当断开连接，重新连接后，仅设置会话变量```wait_timeout```时
```sql
set session wait_timeout=120;
show session variables like '%timeout%';
+-----------------------------+----------+
| Variable_name               | Value    |
+-----------------------------+----------+
| connect_timeout             | 10       |
| delayed_insert_timeout      | 300      |
| have_statement_timeout      | YES      |
| innodb_flush_log_at_timeout | 1        |
| innodb_lock_wait_timeout    | 50       |
| innodb_rollback_on_timeout  | OFF      |
| interactive_timeout         | 60       |
| lock_wait_timeout           | 31536000 |
| net_read_timeout            | 30       |
| net_write_timeout           | 60       |
| rpl_stop_slave_timeout      | 31536000 |
| slave_net_timeout           | 60       |
| wait_timeout                | 120      |
+-----------------------------+----------+
```
当不做任何操作，当前连接不会在```interactive_timeout```的60秒后自动断开连接，而是在等待```wait_timeout```120秒后，才断开连接。

结论：会话变量```interactive_timeout```是没有任何作用的；不管连接的会话是交互式的还是非交互式的，会话变量```wait_timeout```才起到会话超时后断开连接的作用。只不过，会话变量```wait_timeout```根据会话是交互式的还是非交互式的而初始化为全局变量```interactive_timeout```或全局变量```wait_timeout```，就像简介中描述的那样。

## 参考
[bugs.mysql](https://bugs.mysql.com/bug.php?id=45689),[serveridol](http://www.serveridol.com/2012/04/13/mysql-interactive_timeout-vs-wait_timeout/),[sysvar_interactive_timeout](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_interactive_timeout),[sysvar_wait_timeout](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_wait_timeout)