# mysql

## 简单介绍

## 应用示例

- 设置远程连接

    ```sql
    use mysql;
    update user set host = '%' where user = 'root';
    FLUSH PRIVILEGES;
    ```
- 导入导出

    ```sql
    -- 导出所有表的表结构
    mysqldump -h localhost -P 3307 -u root -p --no-data some_db --result-file=create_schema.sql

    -- 导出某张表的表结构
    mysqldump -h localhost -P 3307 -u root -p --no-data some_db some_table --result-file=create_some_table.sql

    -- 导出某张表数据
    mysqldump -h localhost -P 3307 -u root -p --no-create-info some_db some_table --result-file=data_some_table.sql

    -- 导出某张表数据为CSV
    mysql -h localhost -P 3307 -uroot -p some_db -e 'SELECT * FROM some_table INTO OUTFILE "data_some_table.csv" FIELDS TERMINATED BY "," ENCLOSED BY "\"" LINES TERMINATED BY "\n";'

    -- 导入某张表数据
    mysql -h localhost -P 3307 -u root -p some_db < data_some_table.sql
    -- 或者
    cat create_some_table.sql | mysql -h localhost -P 3306 -u root -p123456 some_db;

    -- 导入某张表数据
    mysql -h localhost -P 3307 -uroot -p some_db -e 'LOAD DATA INFILE "data_some_table.csv" INTO TABLE some_table FIELDS TERMINATED BY "," ENCLOSED BY "\"" LINES TERMINATED BY "\n";'
    ```

## SQL语句陷阱

- INSERT ... ON DUPLICATE KEY UPDATE Syntax

  参考官方文档[insert-on-duplicate](https://dev.mysql.com/doc/refman/5.7/en/insert-on-duplicate.html)

  > In general, you should try to avoid using an ON DUPLICATE KEY UPDATE clause on tables with multiple unique indexes.

  陷阱：
  - 当存在两个唯一键时，其中一个相等，另一个会被更新。

```sql
+---------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Table   | Create Table                                                                                                                                                                                                                                                                            |
+---------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| account | CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` int(11) DEFAULT '0',
  `age` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  UNIQUE KEY `uk_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 |
+---------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.01 sec)

mysql> INSERT INTO account(name, code, age) VALUES('lizhi', 76302, 28) ON DUPLICATE KEY UPDATE name='lizhi', code=76302, age=28;
Query OK, 1 row affected (0.00 sec)

mysql> select * from account;
+----+-------+-------+------+
| id | name  | code  | age  |
+----+-------+-------+------+
|  1 | lizhi | 76302 |   28 |
+----+-------+-------+------+
1 row in set (0.00 sec)

mysql> INSERT INTO account(name, code, age) VALUES('lizhi', 76303, 29) ON DUPLICATE KEY UPDATE name='lizhi', code=76303, age=29;
Query OK, 2 rows affected (0.04 sec)

mysql> select * from account;
+----+-------+-------+------+
| id | name  | code  | age  |
+----+-------+-------+------+
|  1 | lizhi | 76303 |   29 |
+----+-------+-------+------+
1 row in set (0.00 sec)

mysql> INSERT INTO account(code, name, age) VALUES(76303, 'lizhi2', 30) ON DUPLICATE KEY UPDATE code=76303, name='lizhi2', age=30;
Query OK, 2 rows affected (0.01 sec)

mysql> select * from account;
+----+--------+-------+------+
| id | name   | code  | age  |
+----+--------+-------+------+
|  1 | lizhi2 | 76303 |   30 |
+----+--------+-------+------+
1 row in set (0.00 sec)

mysql> INSERT INTO account(code, name, age) VALUES(76303, 'lizhi2', 31) ON DUPLICATE KEY UPDATE code=76303, name='lizhi2', age=31;
Query OK, 2 rows affected (0.00 sec)

mysql> select * from account;
+----+--------+-------+------+
| id | name   | code  | age  |
+----+--------+-------+------+
|  1 | lizhi2 | 76303 |   31 |
+----+--------+-------+------+
1 row in set (0.00 sec)

```

## FAQ

- ERROR 1290 (HY000) at line 1: The MySQL server is running with the --secure-file-priv option so it cannot execute this statement

    1. 设置my.cnf如下：
        ```ini
        [mysqld]
        secure-file-priv=""
        ```
    2. 重启mysql