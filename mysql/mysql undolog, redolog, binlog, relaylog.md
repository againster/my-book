[TOC]

# mysql redo log, undo log, relay log, bin log

*2020-11-12 21:05*

参考：

- https://www.programmersought.com/article/66172773749/
- https://www.programmersought.com/article/23524959929/
- http://simyy.cn/2020/06/23/mysql-mvcc/
- http://www.forcoding.club/2019/04/28/elementor-62/
- https://www.jianshu.com/p/f692d4f8a53e

## redo log

When the database data when making changes, you need to read data pages from disk buffer pool, and then make changes in the buffer pool, then this time the data page buffer pool is inconsistent with the data on the page content on the disk, said buffer pool data page is dirty page dirty, if abnormal DB service restart happened this time, these data have not in memory, and not synchronized to a disk file (note, synchronized to the disk file is random IO), that is, data loss can occur, if this time, it is possible to have a document, when the end of the data page is changed buffer pool is, the corresponding edit recording to the file (note log is order IO), then when the service DB occurrence crash restore DB, but also can record the contents of this file, reapply the file to disk, the data is consistent. This file is the redo log, for recording the recording data changes, the order of recording.

It can bring these benefits:

When the buffer pool of dirty page has not been flushed to disk when the occurrence crash, after starting the service can be found by redo log records need to be flushed to disk file;

Data buffer pool directly flush to the disk file, the IO is a random, poor efficiency, and recording data in the buffer pool to redo log, a sequence is the IO, can improve the speed of the transaction commits; tba assumptions modify the table id = 2 line data, the Name = 'B' modify Name = 'B2', it will redo log record used to store Name = 'B2', and if this modification to the flush exception occurs when a disk file, can be used redo log redo operations to achieve and ensure durability of transactions

redo log is used to ensure crash-safe capability. `innodb_flush_log_at_trx_commit` this parameter is set to 1, each transaction represents the redo log directly persisted to disk. I suggest you set this parameter to 1. This ensures that data is not lost after the abnormal restart MySQL. `sync_binlog` When this parameter is set to 1, each transaction represents binlog are persisted to disk. I also suggest you this parameter is set to 1, so you can ensure MySQL binlog is not lost after the abnormal restart.

In order to ensure Redo Log can have a better IO performance, Redo Log InnoDB design has the following characteristics:

1. keep on a contiguous storage space in the Redo Log. So it will be fully allocated space in the log file when the system first starts. Additionally recording a sequential manner Redo Log, to improve performance by sequentially IO.

2. Batch written to the log. Log not directly write to a file, but the first written redo log buffer. When you need to refresh the log to disk (such as transaction commits), is written to disk along with many of the log.

3. concurrent transactions Redo Log shared storage space thereof Redo Log executed in the order of the statements, are alternately recorded together, to reduce the space occupied by the log. e.g., Redo Log recorded content might look like this:

   ```txt
        Record 1: <trx1, insert ...>
              Record 2: <trx2, update ...>
              Record 3: <trx1, delete ...>
              Record 4: <trx3, update ...>
              Record 5: <trx2, insert ...>
   ```

4. C of reasons, when a transaction is written to disk Redo Log will log the other uncommitted transactions are written to disk.

5. Redo Log carried out only on the order of an additional operation, when a transaction needs to roll back, it's Redo Log records are not deleted from Redo Log in.

redo log 属于物理日志，被称为重做日志，记录数据的修改，防止数据丢失，主要用于数据库重启恢复的时候被使用。

为了提高持久化效率，可用`Redo Log Buffer（重做日志缓冲）`来缓存变更，达到阈值大小或定期flush到`Redo Log File（重做日志文件）`。

为确保每次日志都能写入到事务日志文件，每次日志缓存的写入都会调用操作系统`fsync操作`（将OS Buffer中的日志刷到磁盘上的Log File中）。

## undo log

undo log for storing modified data values before being modified, modifying assumed tba line data table id = 2, the Name = 'B' modify Name = 'B2', it will be used to store undo log Name = ' record B ', and if this modification is abnormal, can be used to achieve rollback operation undo logs, to ensure transactional consistency. change operation on the data, from the main `INSERT UPDATE DELETE`, UNDO LOG and is divided into two types, one is INSERT_UNDO (INSERT operation), the recording of the unique key inserted; one kind is UPDATE_UNDO (UPDATE, and DELETE operations comprising a), and recording the modified unique key recorded old column.

Simplified transaction process: 

```
Quoted from: https://blog.csdn.net/mydriverc2/article/details/50629599

 Suppose there are two data of A and B, the values ​​are 1,2 respectively, now modify to 3, 4
1. The transaction begins.
2. Record A=1 to undolog.
3. Modify A=3.
4. Record A=3 to redolog.
5. Record B=2 to undolog.
6. Modify B=4.
7. Record B=4 to redolog, and write redolog to disk.
8. The transaction is committed.
```

undo log是事务回滚与可重复读的关键。undo log属于逻辑日志，包含每行数据的所有版本的数据，用于支持事务与MVCC特性。

![img](http://simyy.cn/images/mysql-mvcc-4.png)

```c++
/* undo结构 */
struct trx_undo_t {
    ...
    /* undo ID */
    ulint id;
    /* 类型：insert or update */
    ulint type;  
    /* 事务ID */
    trx_id_t trx_id;
    /* undo对应回滚段 */
    trx_rseg_t *rseg;
    /* undo链表 */
    UT_LIST_NODE_T(trx_undo_t) undo_list;
    ...
}
```

`undo_list`是一个链表结构，链表节点正是`trx_undo_t`，也就是说Undo Log在内存里是按照链表存储的。此外，每一个`trx_undo_t`都绑定了对应的`事务ID`、`操作类型`以及对应的`回滚段`。

## bin log

The bin log records all data changes, which can be used for local data recovery and master-slave synchronization.

How to flush to disk: flush to disk every 1s | flush to disk every transaction commit | flush to disk every 1s + flush to disk every transaction commit

## relay log

The Mysql master node writes the bin log to the local, and the slave node periodically requests the incremental bin log, and the master node synchronizes the bin log to the slave node.

A separate process from the node will copy the bin log to the local relay log. Replay the relay log regularly from the node.

## mvcc

InnoDB 的每一行数据都隐藏了四个字段，具体如下：

- `DB_TRX_ID`：记录更新该行最后一次的`事务ID`。

  > 事务ID是不断递增的，最新的事务ID永远大于原有事务ID，便于校验事务是否已经过期。

- `DB_ROLL_PTR`：记录该行`回滚段（Undo Log）的指针`，用于查找该行的历史数据。

- `DB_ROW_ID`：在没有主键的情况下生成的`隐藏单调自增ID`。

  > 被物理删除的DB_ROW_ID会被重用。

- `DELELE_BIT`：用于标识该记录`是否被删除`。

  > DELELE_BIT状态位属于逻辑删除，MySQL利用后台运行`purge`线程来实现异步的物理删除。

![img](http://simyy.cn/images/mysql-mvcc-1.png)

其中，`DB_TRX_ID`与`DB_ROLL_PTR`是实现MVCC的关键。

 Innodb通过Undolog（Undolog会记录我们每次使用Update和Delete修改记录时的历史数据版本）实现了历史事务的视图。

在我们的数据表中，除了我们所定义的列外，还有一个DATA_TRX_ID列，该列记录了最新修改该行数据的事务ID。当某个事务想要读取一行数据时，则会将自己的事务ID与DATA_TRX_ID进行对比，如果小于DATA_TRX_ID，则会通过Undo log链进行查询，然后取出数据相应的历史版本。

![undo log链](https://spike.dev/wp-content/uploads/2019/04/%E6%9C%AA%E5%91%BD%E5%90%8D%E6%96%87%E4%BB%B6-45.png)

## ReadView

`ReadView`是用来判断undo log中多版本数据的可见性。

> 每一个事务都包含一个`ReadView`。

`trx_t`是事务的结构体，每一个事务都包含一个`read_view`字段。

```
/* 事务 */
struct trx_t {
  ...
  /* 事务ID */
  trx_id_t id;
  /* 解决可见性 */
  ReadView *read_view;
  ...
};
/* ReadView事务可见范围 */
class ReadView {
    /* 大于该ID的事务不可见（high water mark） */
    trx_id_t m_low_limit_id;
    /* 小于该ID的事务不可见（low water mark） */
    trx_id_t m_up_limit_id;
    /* 创建ReadView的事务ID */
    trx_id_t m_creator_trx_id;
    /* 创建ReadView时活跃事务 */
    ids_t m_ids;
    /* 用于记录可被purge清理的事务 */
    trx_id_t m_low_limit_no;
    /* 状态 */
    bool m_closed;
    
    typedef UT_LIST_NODE_T(ReadView) node_t;
    
    /* 具体view链表 */
    byte pad1[64 - sizeof(node_t)];
    node_t m_view_list;
}

#define UT_LIST_NODE_T(TYPE)/
struct {/
       TYPE *   prev;       /* pointer to the previous node, NULL if start of list *//
       TYPE *   next;       /* pointer to next node, NULL if end of list *//
}/
```

InnoDB每次创建一个事务，都会创建一个包含当前活跃事务列表以及保证事务可见性的ReadView。

> 事务的可见性的判断规则如下：
>
> ① `m_ids`中大于`m_low_limit_id`的事务不可见；
> ② `m_ids`中小于`m_up_limit_id`的事务可见。

[![img](http://simyy.cn/images/mysql-mvcc-3.png)](http://simyy.cn/images/mysql-mvcc-3.png)

------

针对相同事务的不同隔离级别，`m_low_limit_id`与`m_up_limit_id`是不同的，

- 对于RC来说，已提交事务的数据对当前事务来说是可见的；每个select语句都会开启一个新的视图，数据库也就会返回当前记录的最新值，这样就可以解释为什么在读已提交的状态下为什么事务不可重复读。
- 对于RR来说，事务启动前所有未提交事务的数据对当前事务来说都是不可见的。视图由当前事务的第一个select语句创建，之后都用相同的视图，这也是隔离级别在可重复读的状态下事务内部可重复读的原因。

