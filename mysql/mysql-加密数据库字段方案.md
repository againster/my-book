
# mysql - 加密数据库敏感字段方案

***Author: lizhi***

***Date: 2019/10/28***

## 数据库层加密

mysql内部提供的有一些内置的加密及散列函数，详细请参考，https://dev.mysql.com/doc/refman/5.5/en/encryption-functions.html#function_aes-encrypt

假如存在如下的数据库表：

```txt
+-----------------+---------------+------+-----+-------------------+----------------+
| Field           | Type          | Null | Key | Default           | Extra          |
+-----------------+---------------+------+-----+-------------------+----------------+
| id              | int(11)       | NO   | PRI | NULL              | auto_increment |
| username        | varchar(100)  | NO   | MUL | NULL              |                |
| password        | varchar(1024) | NO   |     | NULL              |                |
| salt            | varchar(1024) | NO   |     | NULL              |                |
| email           | varchar(256)  | YES  |     | NULL              |                |
| phone           | varchar(96)   | YES  |     | NULL              |                |
```

我们可以采用，aes_encrypt函数对敏感的数据加密，插入和查询的SQL如下：

```sql
-- 插入
INSERT INTO account (`username`, `password`, `phone`, `email`) VALUES ("ldizfhi", "tdest", to_base64(aes_encrypt("18888872155", "123456")), to_base64(aes_encrypt("lizhi@163.com","123456")));

-- 查询phone
select convert(aes_decrypt(from_base64(phone), "123456") using utf8) from account where phone="wO15Y5WlrybjsUVASnIZWQ==";
```

数据查询结果如下：

```txt
+---------------------------------------------------------------+
| convert(aes_decrypt(from_base64(phone), "123456") using utf8) |
+---------------------------------------------------------------+
|18888872155                                                    |
+---------------------------------------------------------------+
```

## ORM模型层加密

在ORM层加密，就需要程序在orm的model层中调用系统的aes加密方法加密。然后再把加密结果写到数据库中。读取时同理，把controller（MVC中的C层）传递过来的明文，加密后再从数据库读取对应的行。如下：

```go
// 字符串加密，输出加密后的hex字符串
func Encrypt(text string) (string, error) {
    cipher, err := aes128.AES128ECBEncrypt([]byte(text), sensitive.EncKeyUserInfo)
    if err != nil {
        return "", err
    }
    return hex.EncodeToString(cipher), nil
}
```

## 方案缺点对比

方案1：

- SQL语句比较长，且稍微复杂。
- 无法直接在ORM中查询使用加密的字段。只能通过原生SQL查询。
- 加密过程比较消耗CPU，而传统的关系型数据库的横向扩展能力较弱，不利于扩展。
- 定制化程度低。更换另一种数据库时，aes_encrypt函数可能并不被支持。

方案2：

- 在model层做了加解密处理，使用N个计算机语言操作数据库时，需要实现N遍加解密方法，除非把model层作为一个服务，只需要实现一遍即可。
- 增加了model层的实现复杂度。不过这一点可以容忍。
- 更换数据库时，只需要把model层的关联数据库的部分地方修改即可，也是可以容忍的。

综上所述，方案2在扩展性上比较好，在缺陷方面并没有突出明显的地方，多个微服务同时开启，也避免给mysql主机在CPU计算方面造成压力。方案1强依赖于数据库实现，当然也减少了model层的复杂性。当然没有哪一种方案是万能的，只有适合自己的业务的方案才是最好的。

## 参考

- [dev.mysql.com](https://dev.mysql.com/doc/refman/5.5/en/encryption-functions.html#function_password)
- [stackoverflow](https://stackoverflow.com/questions/4840725/how-to-encrypt-emails-in-mysql-database-but-still-be-able-to-query-them)