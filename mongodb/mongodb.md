# mongodb安装

## tarball安装

```bash
## 下载tarball
ARCH=x86_64
OS=rhel70
VER=4.0.0

TARBALLNAME=mongodb-linux-${ARCH}-${OS}-${VER}
TARBALL=${TARBALLNAME}.tgz
curl -L -C - -O http://downloads.mongodb.org/linux/${TARBALL}

## 安装
ROOTFS=/usr/local/mongodb
tar zxvf ${TARBALL}
mv -f ${TARBALLNAME}/* ${ROOTFS}

## 引入PATH
touch /etc/profile.d/mongodb.sh
cat << __END__ > /etc/profile.d/mongodb.sh
export PATH=/usr/local/mongodb/bin:$PATH
__END__

## 添加用户和组
USER=mongodb
GROUP=mongodb
groupadd -r -f ${GROUP}
id -u ${USER} > /dev/null 2>&1 || useradd -M -r -s /sbin/nologin ${USER} -g ${GROUP}

## 安装system servcie
cat << __END__ > /usr/lib/systemd/system/mongodb.service
[Unit]
Description=MongoDB Database Server
Documentation=https://docs.mongodb.org/manual
After=network.target

[Service]
Type=forking
User=mongodb
Group=mongodb
Environment="OPTIONS=--config /etc/mongod.conf"
EnvironmentFile=-/etc/sysconfig/mongod
ExecStart=/usr/local/mongodb/bin/mongod \$OPTIONS
ExecStartPre=/usr/bin/mkdir -p /usr/local/data/mongodb
ExecStartPre=/usr/bin/chown mongodb:mongodb /usr/local/data/mongodb
ExecStartPre=/usr/bin/chmod 0755 /usr/local/data/mongodb
PermissionsStartOnly=true
PIDFile=/usr/local/data/mongodb/mongod.pid

[Install]
WantedBy=multi-user.target
__END__
systemctl daemon-reload

## 安装配置文件mongodb.conf
cat << __END__ > /etc/mongod.conf
storage:
  dbPath: /usr/local/data/mongodb
  wiredTiger:
    engineConfig:
      cacheSizeGB: 1.5
systemLog:
  destination: file
  path: /usr/local/data/mongodb/mongod.log
  logAppend: true
net:
  ssl:
    mode: disabled
  bindIp: 0.0.0.0
  port: 27017
processManagement:
  fork: true  # fork and run in background
  pidFilePath: /usr/local/data/mongodb/mongod.pid  # location of pidfile
security:
  authorization: enabled
__END__

## 启动服务
systemctl start mongodb

## 开放防火墙
firewall-cmd --permanent --zone=public --add-port=27017/tcp
firewall-cmd --reload
```

## 参考
[install-mongodb-on-red-hat](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/)