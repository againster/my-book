
# HTML - CSRF

CSRF (Cross Site Request Forgery), 跨站点请求伪造

## 简介

Cross-site request forgery, also known as one-click attack or session riding and abbreviated as CSRF (sometimes pronounced sea-surf[1]) or XSRF, is a type of malicious exploit of a website where unauthorized commands are transmitted from a user that the web application trusts.[2] There are many ways in which a malicious website can transmit such commands; specially-crafted image tags, hidden forms, and JavaScript XMLHttpRequests, for example, can all work without the user's interaction or even knowledge. Unlike cross-site scripting (XSS), which exploits the trust a user has for a particular site, CSRF exploits the trust that a site has in a user's browser.

## 产生过程

Welcome to another edition of Security Corner. This month's topic is cross-site request forgeries, an attack vector that enables an attacker to send arbitrary HTTP requests from a victim user. That's worth reading a couple of times, and it will likely not be until you've seen your first example attack that you can fully understand or appreciate the danger.

The typical scenario involves a victim that has an established level of privilege with the target site, and this allows an attacker to initiate unauthorized actions.

This article introduces cross-site request forgeries (CSRF, pronounced "sea surf") and provides a few simple steps to help prevent these types of attacks in your own applications.

### WHERE IS THE TRUST?

CSRF attacks exploit the trust that a site has for a particular user. The site is the target of the attack, and the user is both the victim and an unknowing accomplice.

Because the victim sends the request (not the attacker), it can be very difficult to determine that the request represents a CSRF attack. In fact, if you have not taken specific steps to mitigate the risk of CSRF attacks, your applications are most likely vulnerable.

When developing an application, challenging tasks include authentication, identification, and authorization. Assuming that you have hypothetically achieved maximum security regarding these tasks, a CSRF attack can still be successful, because it allows an attacker to bypass traditional safeguards.

### SAMPLE APPLICATION

Every good example needs a sample application, and this article uses one that allows users to buy stocks. So that the actual buying process doesn't complicate the various examples, I use a hypothetical function called buy_stocks(). For my security conscious readers, you can assume that this function contains sufficient input filtering. The form handling is the important part for this discussion.

The interface that allows users to buy stocks includes an HTML form:

```php
<form action="buy.php" method="POST">
<p>Symbol: <input type="text" name="symbol" /></p>
<p>Shares: <input type="text" name="shares" /></p>
<p><input type="submit" value="Buy" /></p>
</form>
```

This form allows the user to specify the stock symbol and the number of shares. It sends this information to buy.php:

```php
<?php
session_start();
if (isset($_REQUEST['symbol'] &&
    isset($_REQUEST['shares']))
{
    buy_stocks($_REQUEST['symbol'],
               $_REQUEST['shares']);
}
?>
```

Keep this application in mind as you continue reading.

### EXAMPLE EXPLOIT

The simplest example exploit uses an image. To understand the exploit, it is first necessary to understand how a browser requests images. Consider a very simple page:

```php
<html>
<p>Here is my sample image:
<img src="http://example.org/example.png" /></p>
</html>
```

When a browser requests this page, it cannot know that the page has an image. The browser only realizes this once it parses the HTML within the response. It is at this point that the browser requests the image, and it uses a standard GET request to do so. This is the important characteristic. It is impossible for the target site to distinguish between a request for an image and a request for any other resource.

> When requesting an image, some browsers alter the value of the Accept header to give a higher priority to image types. Resist the urge to rely upon this behavior for protection - I show you a more reliable approach at the end of this article.

Now, imagine that the image in a page is the following:

```html
<img src="http://example.org/buy.php?symbol=SCOX&shares=1000" />
```

Every user that visits this page sends a request to example.org just as if the user clicked a link to the same URL. Because the sample application uses $_REQUEST instead of the more specific $_POST, it cannot distinguish between data sent in the URL from data provided in the proper HTML form.

This is an intentional mistake that I want to highlight. Using $_REQUEST unnecessarily increases your risk. In addition, if you perform an action (such as buying stocks) as a result of a GET request, you are violating the HTTP specification. Section 9.1.1 of RFC 2616 states the following:

> In particular, the convention has been established that the GET and HEAD methods SHOULD NOT have the significance of taking an action other than retrieval. These methods ought to be considered "safe". This allows user agents to represent other methods, such as POST, PUT and DELETE, in a special way, so that the user is made aware of the fact that a possibly unsafe action is being requested.

POST requests can also be forged, so do not consider a strict use of $_POST to be sufficient protection.

### SAFEGUARDING AGAINST CSRF

There are a few steps you can take to mitigate the risk of CSRF attacks. Minor steps include using POST rather than GET in HTML forms that perform actions, using $_POST instead of $_REQUEST, and requiring verification for critical actions (convenience typically increases risk, and it's up to you to decide the appropriate balance).

The most important thing you can do is to try to force the use of your own forms. If a user sends a request that looks like it is the result of a form submission, doesn't it make sense to be a little suspicious if the user has not recently requested the form?

Consider the following replacement for the HTML form in the sample application:

```php
<?php
$token = md5(uniqid(rand(), TRUE));
$_SESSION['token'] = $token;
$_SESSION['token_time'] = time();
?>
<form action="buy.php" method="post">
<input type="hidden" name="token" value="<?php echo $token; ?>" />
<p>
Symbol: <input type="text" name="symbol" /><br />
Shares: <input type="text" name="shares" /><br />
<input type="submit" value="Buy" />
</p>
</form>
```

> Because this form does not represent an entire script, I do not include the call to session_start(). You can safely assume that this required step takes place prior to the form.

With this simple modification, a CSRF attack must include a valid token (anti-CSRF token) in order to perfectly mimic the form submission. Because you store the user's token in the session, it is also necessary that the attacker uses the token unique to the victim. This effectively limits any attack to a single user, and it requires the attacker to obtain a valid token for another user (obtaining your own token is useless when it comes to forging requests from someone else).

This token should be initialized just like any other session variable:

```php
<?php
if (!isset($_SESSION['token']))
{
    $_SESSION['token'] = md5(uniqid(rand(), TRUE));
}
?>
```

The token can be checked with a simple conditional statement:

```php
<?php
if ($_POST['token'] == $_SESSION['token'])
{
    /* Valid Token */
}
?>
```

The validity of the token can also be limited to a small window of time, such as five minutes:

```php
<?php
$token_age = time() - $_SESSION['token_time'];
if ($token_age <= 300)
{
    /* Less than five minutes has passed. */
}
?>
```

### UNTIL NEXT TIME...

CSRF attacks are very dangerous, and most applications that do not take specific steps to prevent CSRF attacks are vulnerable. Because the requests originate from the victim, it is possible for an attacker to target sites that only the victim can access, such as ones on a local network.

If you use a token in all of your forms as I have suggested, you can eliminate CSRF from your list of concerns. While no safeguard can be considered absolute (an attacker can theoretically guess a valid token), this approach mitigates the majority of the risk. Until next month, be safe.

## 解决方法

Most CSRF prevention techniques work by embedding additional authentication data into requests that allows the web application to detect requests from unauthorized locations.

### Synchronizer token pattern

Synchronizer token pattern (STP) is a technique where a token, secret and unique value for each request, is embedded by the web application in all HTML forms and verified on the server side. The token may be generated by any method that ensures unpredictability and uniqueness (e.g. using a hash chain of random seed). The attacker is thus unable to place a correct token in their requests to authenticate them.[[1]](http://shiflett.org/articles/cross-site-request-forgeries)[[20]](https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html)[[21]](http://halls-of-valhalla.org/beta/articles/cross-site-request-forgery-demystified,47/)

Example of STP set by Django in a HTML form:

```html
<input type="hidden" name="csrfmiddlewaretoken" value="KbyUmhTLMpYj7CD2di7JKP1P3qmLlkPt" />
```

STP is the most compatible as it only relies on HTML, but introduces some complexity on the server side, due to the burden associated with checking validity of the token on each request. As the token is unique and unpredictable, it also enforces proper sequence of events (e.g. screen 1, then 2, then 3) which raises usability problem (e.g. user opens multiple tabs). It can be relaxed by using per session CSRF token instead of per request CSRF token.

### Cookie-to-header token

Web applications that use JavaScript for the majority of their operations may use an anti-CSRF technique that relies on [same-origin policy](https://en.wikipedia.org/wiki/Same-origin_policy):

- On an initial visit without an associated server session, the web application sets a cookie containing a random token that remains the same for the whole web session

    ```http
    Set-Cookie: Csrf-token=i8XNjC4b8KVok4uw5RftR38Wgp2BFwql; expires=Thu, 23-Jul-2015 10:25:33 GMT; Max-Age=31449600; Path=/
    ```

- JavaScript operating on the client side reads its value and copies it into a custom HTTP header sent with each transactional request

    ```http
    X-Csrf-Token: i8XNjC4b8KVok4uw5RftR38Wgp2BFwql
    ```

- The server validates presence and integrity of the token

Security of this technique is based on the assumption that only JavaScript running within the same origin will be able to read the cookie's value. JavaScript running from a rogue file or email will not be able to read it and copy into the custom header. Even though the ```csrf-token``` **cookie** will be automatically sent with the rogue request, the server will be still expecting a valid ```X-Csrf-Token``` **header**.

The CSRF token itself should be unique and unpredictable. It may be generated randomly, or it may be derived from the session token using [HMAC](https://en.wikipedia.org/wiki/HMAC):

```c
csrf_token = HMAC(session_token, application_secret)
```

The CSRF token cookie must not have ```httpOnly``` flag, as it is intended to be read by the JavaScript by design.

This technique is implemented by many modern frameworks, such as Django[[22]](https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/) and AngularJS.[[23]](https://docs.angularjs.org/api/ng/service/$http#cross-site-request-forgery-xsrf-protection) Because the token remains constant over the whole user session, it works well with AJAX applications, but does not enforce sequence of events in the web application.

The protection provided by this technique can be thwarted if the target website disables its same-origin policy using one of the following techniques:

- Permissive ```Access-Control-Allow-Origin``` Cross-origin resource sharing header (with asterisk argument)
- clientaccesspolicy.xml file granting unintended access to Silverlight controls[[24]](http://msdn.microsoft.com/en-us/library/cc197955.aspx)
- crossdomain.xml file granting unintended access to Flash movies[[25]](https://www.adobe.com/devnet/flashplayer/articles/cross_domain_policy.html)

### Double Submit Cookie

Similarly to the cookie-to-header approach, but without involving JavaScript, a site can set a CSRF token as a cookie, and also insert it as a hidden field in each HTML form sent to the client. When the form is submitted, the site can check that the cookie token matches the form token. The same-origin policy prevents an attacker from reading or setting cookies on the target domain, so they cannot put a valid token in their crafted form.[[26]](https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html#double-submit-cookie)

The advantage of this technique over the Synchronizer pattern is that the token does not need to be stored on the server.

### Client-side safeguards

Browser extensions such as RequestPolicy (for Mozilla Firefox) or uMatrix (for both Firefox and Google Chrome/Chromium) can prevent CSRF by providing a default-deny policy for cross-site requests. However, this can significantly interfere with the normal operation of many websites. The CsFire extension (also for Firefox) can mitigate the impact of CSRF with less impact on normal browsing, by removing authentication information from cross-site requests.

The NoScript extension for Firefox mitigates CSRF threats by distinguishing trusted from untrusted sites, and removing authentication & payloads from POST requests sent by untrusted sites to trusted ones. The Application Boundary Enforcer module in NoScript also blocks requests sent from internet pages to local sites (e.g. localhost), preventing CSRF attacks on local services (such as uTorrent) or routers.

The Self Destructing Cookies extension for Firefox does not directly protect from CSRF, but can reduce the attack window, by deleting cookies as soon as they are no longer associated with an open tab.

### Other techniques

Various other techniques have been used or proposed for CSRF prevention historically:

- Verifying that the request's headers contain ```X-Requested-With``` (used by Ruby on Rails before v2.0 and Django before v1.2.5), or checking the HTTP ```Referer``` header and/or HTTP ```Origin``` header. However, this is insecure – a combination of browser plugins and redirects can allow an attacker to provide custom HTTP headers on a request to any website, hence allowing a forged request.

- Checking the [HTTP ```Referer``` header](https://en.wikipedia.org/wiki/HTTP_referer) to see if the request is coming from an authorized page is commonly used for embedded network devices because it does not increase memory requirements. However, a request that omits the   ```Referer``` header must be treated as unauthorized because an attacker can suppress the Referer header by issuing requests from FTP or HTTPS URLs. This strict ```Referer``` validation may cause issues with browsers or proxies that omit the Referer header for privacy reasons. Also, old versions of Flash (before 9.0.18) allow malicious Flash to generate GET or POST requests with arbitrary HTTP request headers using CRLF Injection. Similar CRLF injection vulnerabilities in a client can be used to spoof the referrer of an HTTP request.

- POST request method was for a while perceived as immune to trivial CSRF attacks using parameters in URL (using GET method). However, both POST and any other HTTP method can be now easily executed using XMLHttpRequest. Filtering out unexpected GET requests still prevents some particular attacks, such as cross-site attacks using malicious image URLs or link addresses and cross-site information leakage through ```<script>``` elements (JavaScript hijacking); it also prevents (non-security-related) problems with aggressive web crawlers and link prefetching.[1]

Cross-site scripting (XSS) vulnerabilities (even in other applications running on the same domain) allow attackers to bypass essentially all CSRF preventions.[[31]](http://www.webappsecblog.com/CsrfAndSameOriginXss.html)

## 参考

- [wikipedia](https://en.wikipedia.org/wiki/Cross-site_request_forgery)
- [cross-site-request-forgeries](http://shiflett.org/articles/cross-site-request-forgeries)
- 