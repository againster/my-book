# SMTP - The AUTH Command

## 简介

The ```AUTH``` command is an ESMTP command (SMTP service extension) that is used to authenticate the client to the server. The```AUTH```command sends the clients username and password to the e-mail server.```AUTH```can be combined with some other keywords as PLAIN, LOGIN, CRAM-MD5 and DIGEST-MD5 (e.g. ```AUTH LOGIN```) to choose an authentication mechanism. The authentication mechanism chooses how to login and which level of security that should be used.
 
Below are the ```AUTH PLAIN```, ```AUTH LOGIN``` and ```AUTH CRAM-MD5```commands/mechanisms described.

> In the SMTP communication examples listed below the letters C and S are used to refer to the client and the server (C = Client, S = Server).

## AUTH PLAIN

One common method to login to an SMTP server is to use the **PLAIN** mechanism. The example below shows how ```AUTH PLAIN``` can be used to login:

```yml
S: 220 smtp.server.com Simple Mail Transfer Service Ready
C: EHLO client.example.com
S: 250-smtp.server.com Hello client.example.com
S: 250-SIZE 1000000
S: 250 AUTH LOGIN PLAIN CRAM-MD5
C: AUTH PLAIN
S: 334
C: dGVzdAB0ZXN0ADEyMzQ=
S: 235 2.7.0 Authentication successful
```

After the client has sent the ```AUTH PLAIN``` command to the server, the server responds with a 334 reply code. Then the username and password are sent from the client to the server. The username and password are combined to one string and BASE64 encoded ("dGVzdAB0ZXN0ADEyMzQ=" in the example above). Although the keyword PLAIN is used, the username and password are not sent as plain text over the Internet - they are always BASE64 encoded. If you want to read more about BASE64 encoding, you can open this Internet page.

It is also possible to send the username and password, together with the ```AUTH PLAIN``` command, as a single line. Then the whole login process can be handled this easy:

```yml
S: 220 smtp.server.com Simple Mail Transfer Service Ready
C: EHLO client.example.com
S: 250-smtp.server.com Hello client.example.com
S: 250-SIZE 1000000
S: 250 AUTH LOGIN PLAIN CRAM-MD5
C: AUTH PLAIN dGVzdAB0ZXN0ADEyMzQ= *
S: 235 2.7.0 Authentication successful
```

*) The ```AUTH PLAIN``` command and the username and the password are sent to the server in one single line.

## AUTH LOGIN

The **LOGIN** mechanism is another common method to login to an SMTP server. The SMTP communication example below shows how ```AUTH LOGIN``` can be used to make an authenticated login to an server:

```yml
S: 220 smtp.server.com Simple Mail Transfer Service Ready
C: EHLO client.example.com
S: 250-smtp.server.com Hello client.example.com
S: 250-SIZE 1000000
S: 250 AUTH LOGIN PLAIN CRAM-MD5
C: AUTH LOGIN
S: 334 VXNlcm5hbWU6
C: adlxdkej
S: 334 UGFzc3dvcmQ6
C: lkujsefxlj
S: 235 2.7.0 Authentication successful
```

After that the ```AUTH LOGIN``` command has been sent to the server, the server asks for username and password by sending BASE64 encoded text (questions) to the client. “VXNlcm5hbWU6” is the BASE64 encoded text for the word "Username" and “UGFzc3dvcmQ6” is the BASE64 encoded text for the word "Password" in the example above. The client sends username and password also using BASE64 encoding. "adlxdkej", in the example above, is a BASE64 encoded username and "lkujsefxlj" is a BASE64 encoded password.

## AUTH CRAM-MD5

One drawback using the PLAIN and LOGIN authentication mechanisms is that the username and password can be decoded quite easy if somebody monitor the SMTP communication. To obtain higher security an authentication mechanism with the name **CRAM-MD5** can be used instead. CRAM-MD5 combines a challenge-response authentication mechanism to exchange information and a cryptographic Message Digest 5 algorithm to encrypt important information.

The example below shows how ```AUTH CRAM-MD5``` can be used to login to an SMTP server:

```yml
S: 220 smtp.server.com Simple Mail Transfer Service Ready
C: EHLO client.example.com
S: 250-smtp.server.com Hello client.example.com
S: 250-SIZE 1000000
S: 250 AUTH LOGIN PLAIN CRAM-MD5
C: AUTH CRAM-MD5
S: 334 PDQxOTI5NDIzNDEuMTI4Mjg0NzJAc291cmNlZm91ci5hbmRyZXcuY211LmVkdT4=  [1]
C: cmpzMyBlYzNhNTlmZWQzOTVhYmExZWM2MzY3YzRmNGI0MWFjMA==  [2]
S: 235 2.7.0 Authentication successful
```

After that the ```AUTH CRAM-MD5``` command has been sent to the server, the servers sends back an one-time BASE64 encoded "challenge" to the client (see [1] above). The client responds by sending a BASE64 encoded string to the server that contains a username and a 16-byte digest in hexadecimal notation (see [2] above).

The digest in the reply string is the output of an [HMAC](https://en.wikipedia.org/wiki/HMAC) (Hash-based Message Authentication Code) calculation with the password as the secret key and the SMTP server's original challenge as the message. The SMTP server also calculates its own digest with its notion of the user's password, and if the client's digest and the server's digest match then authentication was successful and a 235 reply code is sent to the client.

The CRAM-MD5 authentication mechanism is more secure than the other two mechanisms described earlier because the password can not be retrieved by decoding the BASE64 encoded client response. The password is used as the key to calculate the HMAC but the password is not stored anywhere in the response. The client response is also invalid for further authentications because of the "challenge" sent from the server was an one-time "challenge" (often with a current time stamp included) and can not be re-used by somebody who monitors the SMTP communication.

## 参考

[smtp-commands-reference-auth](https://www.samlogic.net/articles/smtp-commands-reference-auth.htm)