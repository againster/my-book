# DNS缓存投毒攻击与防御原理

此攻击主要针对DNS缓存服务器。

## 攻击原理

DNS缓存投毒是攻击者先向DNS缓存服务器发送一个不存在域名的请求报文，触发缓存服务器向DNS授权服务器发出查询请求，同时攻击者向缓存服务器发送大量的伪造回应报文，以期在DNS授权服务器回应到达之前命中缓存服务器的请求信息，将恶意授权服务器地址置入DNS缓存服务器缓存项中，篡改某些网站的域名和IP地址对应关系。后续针对此二级域名的子域名解析请求转到恶意DNS授权服务器，导致用户请求被发送到钓鱼网站、反政府网站等。如[图1](https://forum.huawei.com/enterprise/zh/#vsm_attk_dfc_0178__fig01)所示。

**图1 DNS缓存投毒攻击原理**

![dns_poison](.img/dns_poison.png)

## 防御原理

通过会话检查防御DNS缓存投毒是最有效的防御方式。通过检查DNS Reply报文中的Query ID和域名与DNS Request报文中是否匹配来决定是否允许报文通过。

具体处理过程如[图2](https://forum.huawei.com/enterprise/zh/#vsm_attk_dfc_0178__fig02)所示。

**图2 DNS缓存投毒防御**

![dns_anti_poison](.img/dns_anti_poison.png)

1. 当DNS缓存服务器向授权服务器发出域名查询请求时，Anti-DDoS设备记录会话信息及请求报文中的Query ID和域名。
2. 当Anti-DDoS设备收到回应报文时，检查会话是否存在，回应报文中的Query ID和域名与请求报文中的Query ID和域名是否匹配。
   - 如果命中会话，并且Query ID和域名与请求报文中的Query ID和域名匹配，则放行该报文。
   - 如果没有命中会话，则丢弃该报文。
   - 如果命中会话，但是域名或Query ID与请求报文不匹配，则丢弃该报文，同时要删除该会话，以免后续投毒报文完成投毒。

## 参考

- https://forum.huawei.com/enterprise/zh/thread-293895-1-1.html

