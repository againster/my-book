# OSI七层网络模型

参考：[wiki](https://en.wikipedia.org/wiki/Transport_layer)

7. 应用层 http/ntp/dns/telnet/pop3/smtp/dhcp/ldap
6. 表示层 mime/xdr/asn.1
5. 会话层 socks/spdy/pptp/rtp/netbios/named pipe
4. 传输层 tcp/udp/sctp/dccp/spx
3. 网络层 ipv4/ipv6/icmp/icmpv6/igmp/ipsec/ipx 路由器
2. 链路层 mac/arp/atm/is-is/sdlc 交换机
1. 物理层 802.11/802.3/bluetooth

