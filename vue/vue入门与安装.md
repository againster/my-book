# vue入门与安装

## 安装

### 安装js包管理工具 - yarn

当然，也可以使用npm的js包管理工具，npm的安装请参考官网[Installing Node.js via package manager](https://nodejs.org/en/download/package-manager/)。由于yarn比npm有明显的优势，参考[WHY AND HOW TO MIGRATE FROM NPM TO YARN]( https://waverleysoftware.com/blog/yarn-vs-npm/ )，所以我们选用yarn。以下是yarn的CentOS版本的安装方法，其他OS请参考官网[Installation]( https://yarn.bootcss.com/docs/install/ )

1. 配置yarn的repo

   ```
   curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
   ```

   或者直接新建```/etc/yum.repos.d/yarn.repo```文件，如有需要请替换最快的镜像源，内容如下：

   ```
   [yarn]
   name=Yarn Repository
   baseurl=https://dl.yarnpkg.com/rpm/
   enabled=1
   gpgcheck=1
   gpgkey=https://dl.yarnpkg.com/rpm/pubkey.gpg
   ```

2. 安装yarn

   ```bash
   yum install yarn
   yarn --version
   ```

### 安装vue, vue-cli, vue-router

```bash
yarn global add vue
yarn global add vue-cli
```

## 入门

### vue, vue-cli, vue-router的区别

## QA

1. 执行命令```yarn global add vue```时出现```info There appears to be trouble with your network connection. Retrying..```

   可以通过```yarn global add vue --verbose```查看是哪里出现了问题。

2. 执行命令``` vue-init webpack project```时出现错误``` vue-cli · Failed to download repo vuejs-templates/webpack: connect ECONNREFUSED 13.229.188.59:443```

   网络无法连接，当在内网隔离外网的环境时，或者主机离线时，vue-cli无法下载webpack的模板。解决方法参考[Failed to download repo vuejs-templates/simple]( https://stackoverflow.com/questions/49021402/failed-to-download-repo-vuejs-templates-simple )。即：

   You can also try to clone the git repo and then initiate project offline

   ```bash
   git clone https://github.com/vuejs-templates/webpack ~/.vue-templates/webpack
   vue init webpack my-project --offline
   cd my-project
   npm install
   npm run dev
   ```

   

